(self["webpackChunkvue_nomina_project"]=self["webpackChunkvue_nomina_project"]||[]).push([[998],{9662:function(e,t,n){var r=n(7854),i=n(614),o=n(6330),s=r.TypeError;e.exports=function(e){if(i(e))return e;throw s(o(e)+" is not a function")}},6077:function(e,t,n){var r=n(7854),i=n(614),o=r.String,s=r.TypeError;e.exports=function(e){if("object"==typeof e||i(e))return e;throw s("Can't set "+o(e)+" as a prototype")}},5787:function(e,t,n){var r=n(7854),i=n(7976),o=r.TypeError;e.exports=function(e,t){if(i(t,e))return e;throw o("Incorrect invocation")}},9670:function(e,t,n){var r=n(7854),i=n(111),o=r.String,s=r.TypeError;e.exports=function(e){if(i(e))return e;throw s(o(e)+" is not an object")}},4019:function(e){e.exports="undefined"!=typeof ArrayBuffer&&"undefined"!=typeof DataView},260:function(e,t,n){"use strict";var r,i,o,s=n(4019),a=n(9781),u=n(7854),c=n(614),l=n(111),h=n(2597),d=n(648),f=n(6330),p=n(8880),m=n(1320),v=n(3070).f,g=n(7976),_=n(9518),y=n(7674),b=n(5112),w=n(9711),I=u.Int8Array,E=I&&I.prototype,k=u.Uint8ClampedArray,T=k&&k.prototype,O=I&&_(I),S=E&&_(E),R=Object.prototype,A=u.TypeError,C=b("toStringTag"),x=w("TYPED_ARRAY_TAG"),P=w("TYPED_ARRAY_CONSTRUCTOR"),N=s&&!!y&&"Opera"!==d(u.opera),F=!1,D={Int8Array:1,Uint8Array:1,Uint8ClampedArray:1,Int16Array:2,Uint16Array:2,Int32Array:4,Uint32Array:4,Float32Array:4,Float64Array:8},j={BigInt64Array:8,BigUint64Array:8},L=function(e){if(!l(e))return!1;var t=d(e);return"DataView"===t||h(D,t)||h(j,t)},U=function(e){if(!l(e))return!1;var t=d(e);return h(D,t)||h(j,t)},M=function(e){if(U(e))return e;throw A("Target is not a typed array")},$=function(e){if(c(e)&&(!y||g(O,e)))return e;throw A(f(e)+" is not a typed array constructor")},V=function(e,t,n,r){if(a){if(n)for(var i in D){var o=u[i];if(o&&h(o.prototype,e))try{delete o.prototype[e]}catch(s){try{o.prototype[e]=t}catch(c){}}}S[e]&&!n||m(S,e,n?t:N&&E[e]||t,r)}},z=function(e,t,n){var r,i;if(a){if(y){if(n)for(r in D)if(i=u[r],i&&h(i,e))try{delete i[e]}catch(o){}if(O[e]&&!n)return;try{return m(O,e,n?t:N&&O[e]||t)}catch(o){}}for(r in D)i=u[r],!i||i[e]&&!n||m(i,e,t)}};for(r in D)i=u[r],o=i&&i.prototype,o?p(o,P,i):N=!1;for(r in j)i=u[r],o=i&&i.prototype,o&&p(o,P,i);if((!N||!c(O)||O===Function.prototype)&&(O=function(){throw A("Incorrect invocation")},N))for(r in D)u[r]&&y(u[r],O);if((!N||!S||S===R)&&(S=O.prototype,N))for(r in D)u[r]&&y(u[r].prototype,S);if(N&&_(T)!==S&&y(T,S),a&&!h(S,C))for(r in F=!0,v(S,C,{get:function(){return l(this)?this[x]:void 0}}),D)u[r]&&p(u[r],x,r);e.exports={NATIVE_ARRAY_BUFFER_VIEWS:N,TYPED_ARRAY_CONSTRUCTOR:P,TYPED_ARRAY_TAG:F&&x,aTypedArray:M,aTypedArrayConstructor:$,exportTypedArrayMethod:V,exportTypedArrayStaticMethod:z,isView:L,isTypedArray:U,TypedArray:O,TypedArrayPrototype:S}},1318:function(e,t,n){var r=n(5656),i=n(1400),o=n(6244),s=function(e){return function(t,n,s){var a,u=r(t),c=o(u),l=i(s,c);if(e&&n!=n){while(c>l)if(a=u[l++],a!=a)return!0}else for(;c>l;l++)if((e||l in u)&&u[l]===n)return e||l||0;return!e&&-1}};e.exports={includes:s(!0),indexOf:s(!1)}},4326:function(e,t,n){var r=n(1702),i=r({}.toString),o=r("".slice);e.exports=function(e){return o(i(e),8,-1)}},648:function(e,t,n){var r=n(7854),i=n(1694),o=n(614),s=n(4326),a=n(5112),u=a("toStringTag"),c=r.Object,l="Arguments"==s(function(){return arguments}()),h=function(e,t){try{return e[t]}catch(n){}};e.exports=i?s:function(e){var t,n,r;return void 0===e?"Undefined":null===e?"Null":"string"==typeof(n=h(t=c(e),u))?n:l?s(t):"Object"==(r=s(t))&&o(t.callee)?"Arguments":r}},7741:function(e,t,n){var r=n(1702),i=r("".replace),o=function(e){return String(Error(e).stack)}("zxcasd"),s=/\n\s*at [^:]*:[^\n]*/,a=s.test(o);e.exports=function(e,t){if(a&&"string"==typeof e)while(t--)e=i(e,s,"");return e}},9920:function(e,t,n){var r=n(2597),i=n(3887),o=n(1236),s=n(3070);e.exports=function(e,t,n){for(var a=i(t),u=s.f,c=o.f,l=0;l<a.length;l++){var h=a[l];r(e,h)||n&&r(n,h)||u(e,h,c(t,h))}}},8544:function(e,t,n){var r=n(7293);e.exports=!r((function(){function e(){}return e.prototype.constructor=null,Object.getPrototypeOf(new e)!==e.prototype}))},8880:function(e,t,n){var r=n(9781),i=n(3070),o=n(9114);e.exports=r?function(e,t,n){return i.f(e,t,o(1,n))}:function(e,t,n){return e[t]=n,e}},9114:function(e){e.exports=function(e,t){return{enumerable:!(1&e),configurable:!(2&e),writable:!(4&e),value:t}}},9781:function(e,t,n){var r=n(7293);e.exports=!r((function(){return 7!=Object.defineProperty({},1,{get:function(){return 7}})[1]}))},317:function(e,t,n){var r=n(7854),i=n(111),o=r.document,s=i(o)&&i(o.createElement);e.exports=function(e){return s?o.createElement(e):{}}},3678:function(e){e.exports={IndexSizeError:{s:"INDEX_SIZE_ERR",c:1,m:1},DOMStringSizeError:{s:"DOMSTRING_SIZE_ERR",c:2,m:0},HierarchyRequestError:{s:"HIERARCHY_REQUEST_ERR",c:3,m:1},WrongDocumentError:{s:"WRONG_DOCUMENT_ERR",c:4,m:1},InvalidCharacterError:{s:"INVALID_CHARACTER_ERR",c:5,m:1},NoDataAllowedError:{s:"NO_DATA_ALLOWED_ERR",c:6,m:0},NoModificationAllowedError:{s:"NO_MODIFICATION_ALLOWED_ERR",c:7,m:1},NotFoundError:{s:"NOT_FOUND_ERR",c:8,m:1},NotSupportedError:{s:"NOT_SUPPORTED_ERR",c:9,m:1},InUseAttributeError:{s:"INUSE_ATTRIBUTE_ERR",c:10,m:1},InvalidStateError:{s:"INVALID_STATE_ERR",c:11,m:1},SyntaxError:{s:"SYNTAX_ERR",c:12,m:1},InvalidModificationError:{s:"INVALID_MODIFICATION_ERR",c:13,m:1},NamespaceError:{s:"NAMESPACE_ERR",c:14,m:1},InvalidAccessError:{s:"INVALID_ACCESS_ERR",c:15,m:1},ValidationError:{s:"VALIDATION_ERR",c:16,m:0},TypeMismatchError:{s:"TYPE_MISMATCH_ERR",c:17,m:1},SecurityError:{s:"SECURITY_ERR",c:18,m:1},NetworkError:{s:"NETWORK_ERR",c:19,m:1},AbortError:{s:"ABORT_ERR",c:20,m:1},URLMismatchError:{s:"URL_MISMATCH_ERR",c:21,m:1},QuotaExceededError:{s:"QUOTA_EXCEEDED_ERR",c:22,m:1},TimeoutError:{s:"TIMEOUT_ERR",c:23,m:1},InvalidNodeTypeError:{s:"INVALID_NODE_TYPE_ERR",c:24,m:1},DataCloneError:{s:"DATA_CLONE_ERR",c:25,m:1}}},8113:function(e,t,n){var r=n(5005);e.exports=r("navigator","userAgent")||""},7392:function(e,t,n){var r,i,o=n(7854),s=n(8113),a=o.process,u=o.Deno,c=a&&a.versions||u&&u.version,l=c&&c.v8;l&&(r=l.split("."),i=r[0]>0&&r[0]<4?1:+(r[0]+r[1])),!i&&s&&(r=s.match(/Edge\/(\d+)/),(!r||r[1]>=74)&&(r=s.match(/Chrome\/(\d+)/),r&&(i=+r[1]))),e.exports=i},748:function(e){e.exports=["constructor","hasOwnProperty","isPrototypeOf","propertyIsEnumerable","toLocaleString","toString","valueOf"]},2914:function(e,t,n){var r=n(7293),i=n(9114);e.exports=!r((function(){var e=Error("a");return!("stack"in e)||(Object.defineProperty(e,"stack",i(1,7)),7!==e.stack)}))},2109:function(e,t,n){var r=n(7854),i=n(1236).f,o=n(8880),s=n(1320),a=n(3505),u=n(9920),c=n(4705);e.exports=function(e,t){var n,l,h,d,f,p,m=e.target,v=e.global,g=e.stat;if(l=v?r:g?r[m]||a(m,{}):(r[m]||{}).prototype,l)for(h in t){if(f=t[h],e.noTargetGet?(p=i(l,h),d=p&&p.value):d=l[h],n=c(v?h:m+(g?".":"#")+h,e.forced),!n&&void 0!==d){if(typeof f==typeof d)continue;u(f,d)}(e.sham||d&&d.sham)&&o(f,"sham",!0),s(l,h,f,e)}}},7293:function(e){e.exports=function(e){try{return!!e()}catch(t){return!0}}},2104:function(e,t,n){var r=n(4374),i=Function.prototype,o=i.apply,s=i.call;e.exports="object"==typeof Reflect&&Reflect.apply||(r?s.bind(o):function(){return s.apply(o,arguments)})},4374:function(e,t,n){var r=n(7293);e.exports=!r((function(){var e=function(){}.bind();return"function"!=typeof e||e.hasOwnProperty("prototype")}))},6916:function(e,t,n){var r=n(4374),i=Function.prototype.call;e.exports=r?i.bind(i):function(){return i.apply(i,arguments)}},6530:function(e,t,n){var r=n(9781),i=n(2597),o=Function.prototype,s=r&&Object.getOwnPropertyDescriptor,a=i(o,"name"),u=a&&"something"===function(){}.name,c=a&&(!r||r&&s(o,"name").configurable);e.exports={EXISTS:a,PROPER:u,CONFIGURABLE:c}},1702:function(e,t,n){var r=n(4374),i=Function.prototype,o=i.bind,s=i.call,a=r&&o.bind(s,s);e.exports=r?function(e){return e&&a(e)}:function(e){return e&&function(){return s.apply(e,arguments)}}},5005:function(e,t,n){var r=n(7854),i=n(614),o=function(e){return i(e)?e:void 0};e.exports=function(e,t){return arguments.length<2?o(r[e]):r[e]&&r[e][t]}},8173:function(e,t,n){var r=n(9662);e.exports=function(e,t){var n=e[t];return null==n?void 0:r(n)}},7854:function(e,t,n){var r=function(e){return e&&e.Math==Math&&e};e.exports=r("object"==typeof globalThis&&globalThis)||r("object"==typeof window&&window)||r("object"==typeof self&&self)||r("object"==typeof n.g&&n.g)||function(){return this}()||Function("return this")()},2597:function(e,t,n){var r=n(1702),i=n(7908),o=r({}.hasOwnProperty);e.exports=Object.hasOwn||function(e,t){return o(i(e),t)}},3501:function(e){e.exports={}},4664:function(e,t,n){var r=n(9781),i=n(7293),o=n(317);e.exports=!r&&!i((function(){return 7!=Object.defineProperty(o("div"),"a",{get:function(){return 7}}).a}))},8361:function(e,t,n){var r=n(7854),i=n(1702),o=n(7293),s=n(4326),a=r.Object,u=i("".split);e.exports=o((function(){return!a("z").propertyIsEnumerable(0)}))?function(e){return"String"==s(e)?u(e,""):a(e)}:a},9587:function(e,t,n){var r=n(614),i=n(111),o=n(7674);e.exports=function(e,t,n){var s,a;return o&&r(s=t.constructor)&&s!==n&&i(a=s.prototype)&&a!==n.prototype&&o(e,a),e}},2788:function(e,t,n){var r=n(1702),i=n(614),o=n(5465),s=r(Function.toString);i(o.inspectSource)||(o.inspectSource=function(e){return s(e)}),e.exports=o.inspectSource},8340:function(e,t,n){var r=n(111),i=n(8880);e.exports=function(e,t){r(t)&&"cause"in t&&i(e,"cause",t.cause)}},9909:function(e,t,n){var r,i,o,s=n(8536),a=n(7854),u=n(1702),c=n(111),l=n(8880),h=n(2597),d=n(5465),f=n(6200),p=n(3501),m="Object already initialized",v=a.TypeError,g=a.WeakMap,_=function(e){return o(e)?i(e):r(e,{})},y=function(e){return function(t){var n;if(!c(t)||(n=i(t)).type!==e)throw v("Incompatible receiver, "+e+" required");return n}};if(s||d.state){var b=d.state||(d.state=new g),w=u(b.get),I=u(b.has),E=u(b.set);r=function(e,t){if(I(b,e))throw new v(m);return t.facade=e,E(b,e,t),t},i=function(e){return w(b,e)||{}},o=function(e){return I(b,e)}}else{var k=f("state");p[k]=!0,r=function(e,t){if(h(e,k))throw new v(m);return t.facade=e,l(e,k,t),t},i=function(e){return h(e,k)?e[k]:{}},o=function(e){return h(e,k)}}e.exports={set:r,get:i,has:o,enforce:_,getterFor:y}},614:function(e){e.exports=function(e){return"function"==typeof e}},4705:function(e,t,n){var r=n(7293),i=n(614),o=/#|\.prototype\./,s=function(e,t){var n=u[a(e)];return n==l||n!=c&&(i(t)?r(t):!!t)},a=s.normalize=function(e){return String(e).replace(o,".").toLowerCase()},u=s.data={},c=s.NATIVE="N",l=s.POLYFILL="P";e.exports=s},111:function(e,t,n){var r=n(614);e.exports=function(e){return"object"==typeof e?null!==e:r(e)}},1913:function(e){e.exports=!1},2190:function(e,t,n){var r=n(7854),i=n(5005),o=n(614),s=n(7976),a=n(3307),u=r.Object;e.exports=a?function(e){return"symbol"==typeof e}:function(e){var t=i("Symbol");return o(t)&&s(t.prototype,u(e))}},6244:function(e,t,n){var r=n(7466);e.exports=function(e){return r(e.length)}},133:function(e,t,n){var r=n(7392),i=n(7293);e.exports=!!Object.getOwnPropertySymbols&&!i((function(){var e=Symbol();return!String(e)||!(Object(e)instanceof Symbol)||!Symbol.sham&&r&&r<41}))},8536:function(e,t,n){var r=n(7854),i=n(614),o=n(2788),s=r.WeakMap;e.exports=i(s)&&/native code/.test(o(s))},6277:function(e,t,n){var r=n(1340);e.exports=function(e,t){return void 0===e?arguments.length<2?"":t:r(e)}},3070:function(e,t,n){var r=n(7854),i=n(9781),o=n(4664),s=n(3353),a=n(9670),u=n(4948),c=r.TypeError,l=Object.defineProperty,h=Object.getOwnPropertyDescriptor,d="enumerable",f="configurable",p="writable";t.f=i?s?function(e,t,n){if(a(e),t=u(t),a(n),"function"===typeof e&&"prototype"===t&&"value"in n&&p in n&&!n[p]){var r=h(e,t);r&&r[p]&&(e[t]=n.value,n={configurable:f in n?n[f]:r[f],enumerable:d in n?n[d]:r[d],writable:!1})}return l(e,t,n)}:l:function(e,t,n){if(a(e),t=u(t),a(n),o)try{return l(e,t,n)}catch(r){}if("get"in n||"set"in n)throw c("Accessors not supported");return"value"in n&&(e[t]=n.value),e}},1236:function(e,t,n){var r=n(9781),i=n(6916),o=n(5296),s=n(9114),a=n(5656),u=n(4948),c=n(2597),l=n(4664),h=Object.getOwnPropertyDescriptor;t.f=r?h:function(e,t){if(e=a(e),t=u(t),l)try{return h(e,t)}catch(n){}if(c(e,t))return s(!i(o.f,e,t),e[t])}},8006:function(e,t,n){var r=n(6324),i=n(748),o=i.concat("length","prototype");t.f=Object.getOwnPropertyNames||function(e){return r(e,o)}},5181:function(e,t){t.f=Object.getOwnPropertySymbols},9518:function(e,t,n){var r=n(7854),i=n(2597),o=n(614),s=n(7908),a=n(6200),u=n(8544),c=a("IE_PROTO"),l=r.Object,h=l.prototype;e.exports=u?l.getPrototypeOf:function(e){var t=s(e);if(i(t,c))return t[c];var n=t.constructor;return o(n)&&t instanceof n?n.prototype:t instanceof l?h:null}},7976:function(e,t,n){var r=n(1702);e.exports=r({}.isPrototypeOf)},6324:function(e,t,n){var r=n(1702),i=n(2597),o=n(5656),s=n(1318).indexOf,a=n(3501),u=r([].push);e.exports=function(e,t){var n,r=o(e),c=0,l=[];for(n in r)!i(a,n)&&i(r,n)&&u(l,n);while(t.length>c)i(r,n=t[c++])&&(~s(l,n)||u(l,n));return l}},5296:function(e,t){"use strict";var n={}.propertyIsEnumerable,r=Object.getOwnPropertyDescriptor,i=r&&!n.call({1:2},1);t.f=i?function(e){var t=r(this,e);return!!t&&t.enumerable}:n},7674:function(e,t,n){var r=n(1702),i=n(9670),o=n(6077);e.exports=Object.setPrototypeOf||("__proto__"in{}?function(){var e,t=!1,n={};try{e=r(Object.getOwnPropertyDescriptor(Object.prototype,"__proto__").set),e(n,[]),t=n instanceof Array}catch(s){}return function(n,r){return i(n),o(r),t?e(n,r):n.__proto__=r,n}}():void 0)},2140:function(e,t,n){var r=n(7854),i=n(6916),o=n(614),s=n(111),a=r.TypeError;e.exports=function(e,t){var n,r;if("string"===t&&o(n=e.toString)&&!s(r=i(n,e)))return r;if(o(n=e.valueOf)&&!s(r=i(n,e)))return r;if("string"!==t&&o(n=e.toString)&&!s(r=i(n,e)))return r;throw a("Can't convert object to primitive value")}},3887:function(e,t,n){var r=n(5005),i=n(1702),o=n(8006),s=n(5181),a=n(9670),u=i([].concat);e.exports=r("Reflect","ownKeys")||function(e){var t=o.f(a(e)),n=s.f;return n?u(t,n(e)):t}},1320:function(e,t,n){var r=n(7854),i=n(614),o=n(2597),s=n(8880),a=n(3505),u=n(2788),c=n(9909),l=n(6530).CONFIGURABLE,h=c.get,d=c.enforce,f=String(String).split("String");(e.exports=function(e,t,n,u){var c,h=!!u&&!!u.unsafe,p=!!u&&!!u.enumerable,m=!!u&&!!u.noTargetGet,v=u&&void 0!==u.name?u.name:t;i(n)&&("Symbol("===String(v).slice(0,7)&&(v="["+String(v).replace(/^Symbol\(([^)]*)\)/,"$1")+"]"),(!o(n,"name")||l&&n.name!==v)&&s(n,"name",v),c=d(n),c.source||(c.source=f.join("string"==typeof v?v:""))),e!==r?(h?!m&&e[t]&&(p=!0):delete e[t],p?e[t]=n:s(e,t,n)):p?e[t]=n:a(t,n)})(Function.prototype,"toString",(function(){return i(this)&&h(this).source||u(this)}))},4488:function(e,t,n){var r=n(7854),i=r.TypeError;e.exports=function(e){if(void 0==e)throw i("Can't call method on "+e);return e}},3505:function(e,t,n){var r=n(7854),i=Object.defineProperty;e.exports=function(e,t){try{i(r,e,{value:t,configurable:!0,writable:!0})}catch(n){r[e]=t}return t}},6200:function(e,t,n){var r=n(2309),i=n(9711),o=r("keys");e.exports=function(e){return o[e]||(o[e]=i(e))}},5465:function(e,t,n){var r=n(7854),i=n(3505),o="__core-js_shared__",s=r[o]||i(o,{});e.exports=s},2309:function(e,t,n){var r=n(1913),i=n(5465);(e.exports=function(e,t){return i[e]||(i[e]=void 0!==t?t:{})})("versions",[]).push({version:"3.21.1",mode:r?"pure":"global",copyright:"© 2014-2022 Denis Pushkarev (zloirock.ru)",license:"https://github.com/zloirock/core-js/blob/v3.21.1/LICENSE",source:"https://github.com/zloirock/core-js"})},1400:function(e,t,n){var r=n(9303),i=Math.max,o=Math.min;e.exports=function(e,t){var n=r(e);return n<0?i(n+t,0):o(n,t)}},5656:function(e,t,n){var r=n(8361),i=n(4488);e.exports=function(e){return r(i(e))}},9303:function(e){var t=Math.ceil,n=Math.floor;e.exports=function(e){var r=+e;return r!==r||0===r?0:(r>0?n:t)(r)}},7466:function(e,t,n){var r=n(9303),i=Math.min;e.exports=function(e){return e>0?i(r(e),9007199254740991):0}},7908:function(e,t,n){var r=n(7854),i=n(4488),o=r.Object;e.exports=function(e){return o(i(e))}},4590:function(e,t,n){var r=n(7854),i=n(3002),o=r.RangeError;e.exports=function(e,t){var n=i(e);if(n%t)throw o("Wrong offset");return n}},3002:function(e,t,n){var r=n(7854),i=n(9303),o=r.RangeError;e.exports=function(e){var t=i(e);if(t<0)throw o("The argument can't be less than 0");return t}},7593:function(e,t,n){var r=n(7854),i=n(6916),o=n(111),s=n(2190),a=n(8173),u=n(2140),c=n(5112),l=r.TypeError,h=c("toPrimitive");e.exports=function(e,t){if(!o(e)||s(e))return e;var n,r=a(e,h);if(r){if(void 0===t&&(t="default"),n=i(r,e,t),!o(n)||s(n))return n;throw l("Can't convert object to primitive value")}return void 0===t&&(t="number"),u(e,t)}},4948:function(e,t,n){var r=n(7593),i=n(2190);e.exports=function(e){var t=r(e,"string");return i(t)?t:t+""}},1694:function(e,t,n){var r=n(5112),i=r("toStringTag"),o={};o[i]="z",e.exports="[object z]"===String(o)},1340:function(e,t,n){var r=n(7854),i=n(648),o=r.String;e.exports=function(e){if("Symbol"===i(e))throw TypeError("Cannot convert a Symbol value to a string");return o(e)}},6330:function(e,t,n){var r=n(7854),i=r.String;e.exports=function(e){try{return i(e)}catch(t){return"Object"}}},9711:function(e,t,n){var r=n(1702),i=0,o=Math.random(),s=r(1..toString);e.exports=function(e){return"Symbol("+(void 0===e?"":e)+")_"+s(++i+o,36)}},3307:function(e,t,n){var r=n(133);e.exports=r&&!Symbol.sham&&"symbol"==typeof Symbol.iterator},3353:function(e,t,n){var r=n(9781),i=n(7293);e.exports=r&&i((function(){return 42!=Object.defineProperty((function(){}),"prototype",{value:42,writable:!1}).prototype}))},5112:function(e,t,n){var r=n(7854),i=n(2309),o=n(2597),s=n(9711),a=n(133),u=n(3307),c=i("wks"),l=r.Symbol,h=l&&l["for"],d=u?l:l&&l.withoutSetter||s;e.exports=function(e){if(!o(c,e)||!a&&"string"!=typeof c[e]){var t="Symbol."+e;a&&o(l,e)?c[e]=l[e]:c[e]=u&&h?h(t):d(t)}return c[e]}},9191:function(e,t,n){"use strict";var r=n(5005),i=n(2597),o=n(8880),s=n(7976),a=n(7674),u=n(9920),c=n(9587),l=n(6277),h=n(8340),d=n(7741),f=n(2914),p=n(1913);e.exports=function(e,t,n,m){var v=m?2:1,g=e.split("."),_=g[g.length-1],y=r.apply(null,g);if(y){var b=y.prototype;if(!p&&i(b,"cause")&&delete b.cause,!n)return y;var w=r("Error"),I=t((function(e,t){var n=l(m?t:e,void 0),r=m?new y(e):new y;return void 0!==n&&o(r,"message",n),f&&o(r,"stack",d(r.stack,2)),this&&s(b,this)&&c(r,this,I),arguments.length>v&&h(r,arguments[v]),r}));if(I.prototype=b,"Error"!==_&&(a?a(I,w):u(I,w,{name:!0})),u(I,y),!p)try{b.name!==_&&o(b,"name",_),b.constructor=I}catch(E){}return I}}},1703:function(e,t,n){var r=n(2109),i=n(7854),o=n(2104),s=n(9191),a="WebAssembly",u=i[a],c=7!==Error("e",{cause:7}).cause,l=function(e,t){var n={};n[e]=s(e,t,c),r({global:!0,forced:c},n)},h=function(e,t){if(u&&u[e]){var n={};n[e]=s(a+"."+e,t,c),r({target:a,stat:!0,forced:c},n)}};l("Error",(function(e){return function(t){return o(e,this,arguments)}})),l("EvalError",(function(e){return function(t){return o(e,this,arguments)}})),l("RangeError",(function(e){return function(t){return o(e,this,arguments)}})),l("ReferenceError",(function(e){return function(t){return o(e,this,arguments)}})),l("SyntaxError",(function(e){return function(t){return o(e,this,arguments)}})),l("TypeError",(function(e){return function(t){return o(e,this,arguments)}})),l("URIError",(function(e){return function(t){return o(e,this,arguments)}})),h("CompileError",(function(e){return function(t){return o(e,this,arguments)}})),h("LinkError",(function(e){return function(t){return o(e,this,arguments)}})),h("RuntimeError",(function(e){return function(t){return o(e,this,arguments)}}))},8675:function(e,t,n){"use strict";var r=n(260),i=n(6244),o=n(9303),s=r.aTypedArray,a=r.exportTypedArrayMethod;a("at",(function(e){var t=s(this),n=i(t),r=o(e),a=r>=0?r:n+r;return a<0||a>=n?void 0:t[a]}))},3462:function(e,t,n){"use strict";var r=n(7854),i=n(6916),o=n(260),s=n(6244),a=n(4590),u=n(7908),c=n(7293),l=r.RangeError,h=r.Int8Array,d=h&&h.prototype,f=d&&d.set,p=o.aTypedArray,m=o.exportTypedArrayMethod,v=!c((function(){var e=new Uint8ClampedArray(2);return i(f,e,{length:1,0:3},1),3!==e[1]})),g=v&&o.NATIVE_ARRAY_BUFFER_VIEWS&&c((function(){var e=new h(2);return e.set(1),e.set("2",1),0!==e[0]||2!==e[1]}));m("set",(function(e){p(this);var t=a(arguments.length>1?arguments[1]:void 0,1),n=u(e);if(v)return i(f,this,n,t);var r=this.length,o=s(n),c=0;if(o+t>r)throw l("Wrong length");while(c<o)this[t+c]=n[c++]}),!v||g)},2801:function(e,t,n){"use strict";var r=n(2109),i=n(5005),o=n(9114),s=n(3070).f,a=n(2597),u=n(5787),c=n(9587),l=n(6277),h=n(3678),d=n(7741),f=n(1913),p="DOMException",m=i("Error"),v=i(p),g=function(){u(this,_);var e=arguments.length,t=l(e<1?void 0:arguments[0]),n=l(e<2?void 0:arguments[1],"Error"),r=new v(t,n),i=m(t);return i.name=p,s(r,"stack",o(1,d(i.stack,1))),c(r,this,g),r},_=g.prototype=v.prototype,y="stack"in m(p),b="stack"in new v(1,2),w=y&&!b;r({global:!0,forced:f||w},{DOMException:w?g:v});var I=i(p),E=I.prototype;if(E.constructor!==I)for(var k in f||s(E,"constructor",o(1,I)),h)if(a(h,k)){var T=h[k],O=T.s;a(I,O)||s(I,O,o(6,T.c))}},8895:function(e,t,n){"use strict";n.d(t,{$:function(){return zt},A:function(){return c},E:function(){return Fe},G:function(){return Me},I:function(){return et},K:function(){return fe},L:function(){return lt},M:function(){return pt},N:function(){return mt},P:function(){return Nr},Q:function(){return vt},R:function(){return Tr},T:function(){return ft},U:function(){return bt},V:function(){return wt},W:function(){return Tt},X:function(){return Ft},Y:function(){return Dt},Z:function(){return jt},_:function(){return Ut},a:function(){return Nn},a0:function(){return Bt},a1:function(){return Ht},a2:function(){return qt},a3:function(){return Wt},a4:function(){return Gt},a5:function(){return Jt},a6:function(){return Kt},a7:function(){return Yt},a8:function(){return Zt},a9:function(){return Qt},aA:function(){return si},aB:function(){return Yr},aC:function(){return Pe},aG:function(){return jn},aJ:function(){return _t},aa:function(){return en},ab:function(){return tn},ae:function(){return rn},af:function(){return on},ag:function(){return sn},aj:function(){return At},ak:function(){return mn},am:function(){return _n},an:function(){return kn},ao:function(){return Te},ap:function(){return k},aq:function(){return ke},ar:function(){return we},as:function(){return g},at:function(){return $i},au:function(){return fi},av:function(){return _},aw:function(){return I},ax:function(){return O},ay:function(){return pe},az:function(){return ci},b:function(){return xn},c:function(){return Vr},d:function(){return Br},e:function(){return zr},f:function(){return Qr},g:function(){return ri},h:function(){return ti},i:function(){return ir},j:function(){return oi},k:function(){return Hi},l:function(){return Ar},m:function(){return Gi},n:function(){return Qi},o:function(){return u},r:function(){return Cr},s:function(){return Rr},u:function(){return Pr},z:function(){return d}});n(1703);var r=n(223),i=n(9684);function o(e,t){var n={};for(var r in e)Object.prototype.hasOwnProperty.call(e,r)&&t.indexOf(r)<0&&(n[r]=e[r]);if(null!=e&&"function"===typeof Object.getOwnPropertySymbols){var i=0;for(r=Object.getOwnPropertySymbols(e);i<r.length;i++)t.indexOf(r[i])<0&&Object.prototype.propertyIsEnumerable.call(e,r[i])&&(n[r[i]]=e[r[i]])}return n}Object.create;Object.create;var s=n(5168),a=n(7142);
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const u={FACEBOOK:"facebook.com",GITHUB:"github.com",GOOGLE:"google.com",PASSWORD:"password",PHONE:"phone",TWITTER:"twitter.com"},c={EMAIL_SIGNIN:"EMAIL_SIGNIN",PASSWORD_RESET:"PASSWORD_RESET",RECOVER_EMAIL:"RECOVER_EMAIL",REVERT_SECOND_FACTOR_ADDITION:"REVERT_SECOND_FACTOR_ADDITION",VERIFY_AND_CHANGE_EMAIL:"VERIFY_AND_CHANGE_EMAIL",VERIFY_EMAIL:"VERIFY_EMAIL"};
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function l(){return{["admin-restricted-operation"]:"This operation is restricted to administrators only.",["argument-error"]:"",["app-not-authorized"]:"This app, identified by the domain where it's hosted, is not authorized to use Firebase Authentication with the provided API key. Review your key configuration in the Google API console.",["app-not-installed"]:"The requested mobile application corresponding to the identifier (Android package name or iOS bundle ID) provided is not installed on this device.",["captcha-check-failed"]:"The reCAPTCHA response token provided is either invalid, expired, already used or the domain associated with it does not match the list of whitelisted domains.",["code-expired"]:"The SMS code has expired. Please re-send the verification code to try again.",["cordova-not-ready"]:"Cordova framework is not ready.",["cors-unsupported"]:"This browser is not supported.",["credential-already-in-use"]:"This credential is already associated with a different user account.",["custom-token-mismatch"]:"The custom token corresponds to a different audience.",["requires-recent-login"]:"This operation is sensitive and requires recent authentication. Log in again before retrying this request.",["dependent-sdk-initialized-before-auth"]:"Another Firebase SDK was initialized and is trying to use Auth before Auth is initialized. Please be sure to call `initializeAuth` or `getAuth` before starting any other Firebase SDK.",["dynamic-link-not-activated"]:"Please activate Dynamic Links in the Firebase Console and agree to the terms and conditions.",["email-change-needs-verification"]:"Multi-factor users must always have a verified email.",["email-already-in-use"]:"The email address is already in use by another account.",["emulator-config-failed"]:'Auth instance has already been used to make a network call. Auth can no longer be configured to use the emulator. Try calling "connectAuthEmulator()" sooner.',["expired-action-code"]:"The action code has expired.",["cancelled-popup-request"]:"This operation has been cancelled due to another conflicting popup being opened.",["internal-error"]:"An internal AuthError has occurred.",["invalid-app-credential"]:"The phone verification request contains an invalid application verifier. The reCAPTCHA token response is either invalid or expired.",["invalid-app-id"]:"The mobile app identifier is not registed for the current project.",["invalid-user-token"]:"This user's credential isn't valid for this project. This can happen if the user's token has been tampered with, or if the user isn't for the project associated with this API key.",["invalid-auth-event"]:"An internal AuthError has occurred.",["invalid-verification-code"]:"The SMS verification code used to create the phone auth credential is invalid. Please resend the verification code sms and be sure to use the verification code provided by the user.",["invalid-continue-uri"]:"The continue URL provided in the request is invalid.",["invalid-cordova-configuration"]:"The following Cordova plugins must be installed to enable OAuth sign-in: cordova-plugin-buildinfo, cordova-universal-links-plugin, cordova-plugin-browsertab, cordova-plugin-inappbrowser and cordova-plugin-customurlscheme.",["invalid-custom-token"]:"The custom token format is incorrect. Please check the documentation.",["invalid-dynamic-link-domain"]:"The provided dynamic link domain is not configured or authorized for the current project.",["invalid-email"]:"The email address is badly formatted.",["invalid-emulator-scheme"]:"Emulator URL must start with a valid scheme (http:// or https://).",["invalid-api-key"]:"Your API key is invalid, please check you have copied it correctly.",["invalid-cert-hash"]:"The SHA-1 certificate hash provided is invalid.",["invalid-credential"]:"The supplied auth credential is malformed or has expired.",["invalid-message-payload"]:"The email template corresponding to this action contains invalid characters in its message. Please fix by going to the Auth email templates section in the Firebase Console.",["invalid-multi-factor-session"]:"The request does not contain a valid proof of first factor successful sign-in.",["invalid-oauth-provider"]:"EmailAuthProvider is not supported for this operation. This operation only supports OAuth providers.",["invalid-oauth-client-id"]:"The OAuth client ID provided is either invalid or does not match the specified API key.",["unauthorized-domain"]:"This domain is not authorized for OAuth operations for your Firebase project. Edit the list of authorized domains from the Firebase console.",["invalid-action-code"]:"The action code is invalid. This can happen if the code is malformed, expired, or has already been used.",["wrong-password"]:"The password is invalid or the user does not have a password.",["invalid-persistence-type"]:"The specified persistence type is invalid. It can only be local, session or none.",["invalid-phone-number"]:"The format of the phone number provided is incorrect. Please enter the phone number in a format that can be parsed into E.164 format. E.164 phone numbers are written in the format [+][country code][subscriber number including area code].",["invalid-provider-id"]:"The specified provider ID is invalid.",["invalid-recipient-email"]:"The email corresponding to this action failed to send as the provided recipient email address is invalid.",["invalid-sender"]:"The email template corresponding to this action contains an invalid sender email or name. Please fix by going to the Auth email templates section in the Firebase Console.",["invalid-verification-id"]:"The verification ID used to create the phone auth credential is invalid.",["invalid-tenant-id"]:"The Auth instance's tenant ID is invalid.",["missing-android-pkg-name"]:"An Android Package Name must be provided if the Android App is required to be installed.",["auth-domain-config-required"]:"Be sure to include authDomain when calling firebase.initializeApp(), by following the instructions in the Firebase console.",["missing-app-credential"]:"The phone verification request is missing an application verifier assertion. A reCAPTCHA response token needs to be provided.",["missing-verification-code"]:"The phone auth credential was created with an empty SMS verification code.",["missing-continue-uri"]:"A continue URL must be provided in the request.",["missing-iframe-start"]:"An internal AuthError has occurred.",["missing-ios-bundle-id"]:"An iOS Bundle ID must be provided if an App Store ID is provided.",["missing-or-invalid-nonce"]:"The request does not contain a valid nonce. This can occur if the SHA-256 hash of the provided raw nonce does not match the hashed nonce in the ID token payload.",["missing-multi-factor-info"]:"No second factor identifier is provided.",["missing-multi-factor-session"]:"The request is missing proof of first factor successful sign-in.",["missing-phone-number"]:"To send verification codes, provide a phone number for the recipient.",["missing-verification-id"]:"The phone auth credential was created with an empty verification ID.",["app-deleted"]:"This instance of FirebaseApp has been deleted.",["multi-factor-info-not-found"]:"The user does not have a second factor matching the identifier provided.",["multi-factor-auth-required"]:"Proof of ownership of a second factor is required to complete sign-in.",["account-exists-with-different-credential"]:"An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.",["network-request-failed"]:"A network AuthError (such as timeout, interrupted connection or unreachable host) has occurred.",["no-auth-event"]:"An internal AuthError has occurred.",["no-such-provider"]:"User was not linked to an account with the given provider.",["null-user"]:"A null user object was provided as the argument for an operation which requires a non-null user object.",["operation-not-allowed"]:"The given sign-in provider is disabled for this Firebase project. Enable it in the Firebase console, under the sign-in method tab of the Auth section.",["operation-not-supported-in-this-environment"]:'This operation is not supported in the environment this application is running on. "location.protocol" must be http, https or chrome-extension and web storage must be enabled.',["popup-blocked"]:"Unable to establish a connection with the popup. It may have been blocked by the browser.",["popup-closed-by-user"]:"The popup has been closed by the user before finalizing the operation.",["provider-already-linked"]:"User can only be linked to one identity for the given provider.",["quota-exceeded"]:"The project's quota for this operation has been exceeded.",["redirect-cancelled-by-user"]:"The redirect operation has been cancelled by the user before finalizing.",["redirect-operation-pending"]:"A redirect sign-in operation is already pending.",["rejected-credential"]:"The request contains malformed or mismatching credentials.",["second-factor-already-in-use"]:"The second factor is already enrolled on this account.",["maximum-second-factor-count-exceeded"]:"The maximum allowed number of second factors on a user has been exceeded.",["tenant-id-mismatch"]:"The provided tenant ID does not match the Auth instance's tenant ID",["timeout"]:"The operation has timed out.",["user-token-expired"]:"The user's credential is no longer valid. The user must sign in again.",["too-many-requests"]:"We have blocked all requests from this device due to unusual activity. Try again later.",["unauthorized-continue-uri"]:"The domain of the continue URL is not whitelisted.  Please whitelist the domain in the Firebase console.",["unsupported-first-factor"]:"Enrolling a second factor or signing in with a multi-factor account requires sign-in with a supported first factor.",["unsupported-persistence-type"]:"The current environment does not support the specified persistence type.",["unsupported-tenant-operation"]:"This operation is not supported in a multi-tenant context.",["unverified-email"]:"The operation requires a verified email.",["user-cancelled"]:"The user did not grant your application the permissions it requested.",["user-not-found"]:"There is no user record corresponding to this identifier. The user may have been deleted.",["user-disabled"]:"The user account has been disabled by an administrator.",["user-mismatch"]:"The supplied credentials do not correspond to the previously signed in user.",["user-signed-out"]:"",["weak-password"]:"The password must be 6 characters long or more.",["web-storage-unsupported"]:"This browser is not supported or 3rd party cookies and data may be disabled.",["already-initialized"]:"initializeAuth() has already been called with different options. To avoid this error, call initializeAuth() with the same options as when it was originally called, or call getAuth() to return the already initialized instance."}}function h(){return{["dependent-sdk-initialized-before-auth"]:"Another Firebase SDK was initialized and is trying to use Auth before Auth is initialized. Please be sure to call `initializeAuth` or `getAuth` before starting any other Firebase SDK."}}const d=l,f=h,p=new r.LL("auth","Firebase",h()),m=new s.Yd("@firebase/auth");function v(e,...t){m.logLevel<=s["in"].ERROR&&m.error(`Auth (${i.SDK_VERSION}): ${e}`,...t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function g(e,...t){throw w(e,...t)}function _(e,...t){return w(e,...t)}function y(e,t,n){const i=Object.assign(Object.assign({},f()),{[t]:n}),o=new r.LL("auth","Firebase",i);return o.create(t,{appName:e.name})}function b(e,t,n){const r=n;if(!(t instanceof r))throw r.name!==t.constructor.name&&g(e,"argument-error"),y(e,"argument-error",`Type of ${t.constructor.name} does not match expected instance.Did you pass a reference from a different Auth SDK?`)}function w(e,...t){if("string"!==typeof e){const n=t[0],r=[...t.slice(1)];return r[0]&&(r[0].appName=e.name),e._errorFactory.create(n,...r)}return p.create(e,...t)}function I(e,t,...n){if(!e)throw w(t,...n)}function E(e){const t="INTERNAL ASSERTION FAILED: "+e;throw v(t),new Error(t)}function k(e,t){e||E(t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const T=new Map;function O(e){k(e instanceof Function,"Expected a class definition");let t=T.get(e);return t?(k(t instanceof e,"Instance stored in cache mismatched with class"),t):(t=new e,T.set(e,t),t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function S(e,t){const n=(0,i._getProvider)(e,"auth");if(n.isInitialized()){const e=n.getImmediate(),i=n.getOptions();if((0,r.vZ)(i,null!==t&&void 0!==t?t:{}))return e;g(e,"already-initialized")}const o=n.initialize({options:t});return o}function R(e,t){const n=(null===t||void 0===t?void 0:t.persistence)||[],r=(Array.isArray(n)?n:[n]).map(O);(null===t||void 0===t?void 0:t.errorMap)&&e._updateErrorMap(t.errorMap),e._initializeWithPersistence(r,null===t||void 0===t?void 0:t.popupRedirectResolver)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function A(){var e;return"undefined"!==typeof self&&(null===(e=self.location)||void 0===e?void 0:e.href)||""}function C(){return"http:"===x()||"https:"===x()}function x(){var e;return"undefined"!==typeof self&&(null===(e=self.location)||void 0===e?void 0:e.protocol)||null}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function P(){return!("undefined"!==typeof navigator&&navigator&&"onLine"in navigator&&"boolean"===typeof navigator.onLine&&(C()||(0,r.ru)()||"connection"in navigator))||navigator.onLine}function N(){if("undefined"===typeof navigator)return null;const e=navigator;return e.languages&&e.languages[0]||e.language||null}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class F{constructor(e,t){this.shortDelay=e,this.longDelay=t,k(t>e,"Short delay should be less than long delay!"),this.isMobile=(0,r.uI)()||(0,r.b$)()}get(){return P()?this.isMobile?this.longDelay:this.shortDelay:Math.min(5e3,this.shortDelay)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function D(e,t){k(e.emulator,"Emulator should always be set here");const{url:n}=e.emulator;return t?`${n}${t.startsWith("/")?t.slice(1):t}`:n}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class j{static initialize(e,t,n){this.fetchImpl=e,t&&(this.headersImpl=t),n&&(this.responseImpl=n)}static fetch(){return this.fetchImpl?this.fetchImpl:"undefined"!==typeof self&&"fetch"in self?self.fetch:void E("Could not find fetch implementation, make sure you call FetchProvider.initialize() with an appropriate polyfill")}static headers(){return this.headersImpl?this.headersImpl:"undefined"!==typeof self&&"Headers"in self?self.Headers:void E("Could not find Headers implementation, make sure you call FetchProvider.initialize() with an appropriate polyfill")}static response(){return this.responseImpl?this.responseImpl:"undefined"!==typeof self&&"Response"in self?self.Response:void E("Could not find Response implementation, make sure you call FetchProvider.initialize() with an appropriate polyfill")}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const L={["CREDENTIAL_MISMATCH"]:"custom-token-mismatch",["MISSING_CUSTOM_TOKEN"]:"internal-error",["INVALID_IDENTIFIER"]:"invalid-email",["MISSING_CONTINUE_URI"]:"internal-error",["INVALID_PASSWORD"]:"wrong-password",["MISSING_PASSWORD"]:"internal-error",["EMAIL_EXISTS"]:"email-already-in-use",["PASSWORD_LOGIN_DISABLED"]:"operation-not-allowed",["INVALID_IDP_RESPONSE"]:"invalid-credential",["INVALID_PENDING_TOKEN"]:"invalid-credential",["FEDERATED_USER_ID_ALREADY_LINKED"]:"credential-already-in-use",["MISSING_REQ_TYPE"]:"internal-error",["EMAIL_NOT_FOUND"]:"user-not-found",["RESET_PASSWORD_EXCEED_LIMIT"]:"too-many-requests",["EXPIRED_OOB_CODE"]:"expired-action-code",["INVALID_OOB_CODE"]:"invalid-action-code",["MISSING_OOB_CODE"]:"internal-error",["CREDENTIAL_TOO_OLD_LOGIN_AGAIN"]:"requires-recent-login",["INVALID_ID_TOKEN"]:"invalid-user-token",["TOKEN_EXPIRED"]:"user-token-expired",["USER_NOT_FOUND"]:"user-token-expired",["TOO_MANY_ATTEMPTS_TRY_LATER"]:"too-many-requests",["INVALID_CODE"]:"invalid-verification-code",["INVALID_SESSION_INFO"]:"invalid-verification-id",["INVALID_TEMPORARY_PROOF"]:"invalid-credential",["MISSING_SESSION_INFO"]:"missing-verification-id",["SESSION_EXPIRED"]:"code-expired",["MISSING_ANDROID_PACKAGE_NAME"]:"missing-android-pkg-name",["UNAUTHORIZED_DOMAIN"]:"unauthorized-continue-uri",["INVALID_OAUTH_CLIENT_ID"]:"invalid-oauth-client-id",["ADMIN_ONLY_OPERATION"]:"admin-restricted-operation",["INVALID_MFA_PENDING_CREDENTIAL"]:"invalid-multi-factor-session",["MFA_ENROLLMENT_NOT_FOUND"]:"multi-factor-info-not-found",["MISSING_MFA_ENROLLMENT_ID"]:"missing-multi-factor-info",["MISSING_MFA_PENDING_CREDENTIAL"]:"missing-multi-factor-session",["SECOND_FACTOR_EXISTS"]:"second-factor-already-in-use",["SECOND_FACTOR_LIMIT_EXCEEDED"]:"maximum-second-factor-count-exceeded",["BLOCKING_FUNCTION_ERROR_RESPONSE"]:"internal-error"},U=new F(3e4,6e4);
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function M(e,t){return e.tenantId&&!t.tenantId?Object.assign(Object.assign({},t),{tenantId:e.tenantId}):t}async function $(e,t,n,i,o={}){return V(e,o,(async()=>{let o={},s={};i&&("GET"===t?s=i:o={body:JSON.stringify(i)});const a=(0,r.xO)(Object.assign({key:e.config.apiKey},s)).slice(1),u=await e._getAdditionalHeaders();return u["Content-Type"]="application/json",e.languageCode&&(u["X-Firebase-Locale"]=e.languageCode),j.fetch()(B(e,e.config.apiHost,n,a),Object.assign({method:t,headers:u,referrerPolicy:"no-referrer"},o))}))}async function V(e,t,n){e._canInitEmulator=!1;const i=Object.assign(Object.assign({},L),t);try{const t=new H(e),r=await Promise.race([n(),t.promise]);t.clearNetworkTimeout();const o=await r.json();if("needConfirmation"in o)throw q(e,"account-exists-with-different-credential",o);if(r.ok&&!("errorMessage"in o))return o;{const t=r.ok?o.errorMessage:o.error.message,[n,s]=t.split(" : ");if("FEDERATED_USER_ID_ALREADY_LINKED"===n)throw q(e,"credential-already-in-use",o);if("EMAIL_EXISTS"===n)throw q(e,"email-already-in-use",o);const a=i[n]||n.toLowerCase().replace(/[_\s]+/g,"-");if(s)throw y(e,a,s);g(e,a)}}catch(o){if(o instanceof r.ZR)throw o;g(e,"network-request-failed")}}async function z(e,t,n,r,i={}){const o=await $(e,t,n,r,i);return"mfaPendingCredential"in o&&g(e,"multi-factor-auth-required",{_serverResponse:o}),o}function B(e,t,n,r){const i=`${t}${n}?${r}`;return e.config.emulator?D(e.config,i):`${e.config.apiScheme}://${i}`}class H{constructor(e){this.auth=e,this.timer=null,this.promise=new Promise(((e,t)=>{this.timer=setTimeout((()=>t(_(this.auth,"network-request-failed"))),U.get())}))}clearNetworkTimeout(){clearTimeout(this.timer)}}function q(e,t,n){const r={appName:e.name};n.email&&(r.email=n.email),n.phoneNumber&&(r.phoneNumber=n.phoneNumber);const i=_(e,t,r);return i.customData._tokenResponse=n,i}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function W(e,t){return $(e,"POST","/v1/accounts:delete",t)}async function G(e,t){return $(e,"POST","/v1/accounts:update",t)}async function J(e,t){return $(e,"POST","/v1/accounts:lookup",t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function K(e){if(e)try{const t=new Date(Number(e));if(!isNaN(t.getTime()))return t.toUTCString()}catch(t){}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Y(e,t=!1){const n=(0,r.m9)(e),i=await n.getIdToken(t),o=X(i);I(o&&o.exp&&o.auth_time&&o.iat,n.auth,"internal-error");const s="object"===typeof o.firebase?o.firebase:void 0,a=null===s||void 0===s?void 0:s["sign_in_provider"];return{claims:o,token:i,authTime:K(Z(o.auth_time)),issuedAtTime:K(Z(o.iat)),expirationTime:K(Z(o.exp)),signInProvider:a||null,signInSecondFactor:(null===s||void 0===s?void 0:s["sign_in_second_factor"])||null}}function Z(e){return 1e3*Number(e)}function X(e){const[t,n,i]=e.split(".");if(void 0===t||void 0===n||void 0===i)return v("JWT malformed, contained fewer than 3 sections"),null;try{const e=(0,r.tV)(n);return e?JSON.parse(e):(v("Failed to decode base64 JWT payload"),null)}catch(o){return v("Caught error parsing JWT payload as JSON",o),null}}function Q(e){const t=X(e);return I(t,"internal-error"),I("undefined"!==typeof t.exp,"internal-error"),I("undefined"!==typeof t.iat,"internal-error"),Number(t.exp)-Number(t.iat)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function ee(e,t,n=!1){if(n)return t;try{return await t}catch(i){throw i instanceof r.ZR&&te(i)&&e.auth.currentUser===e&&await e.auth.signOut(),i}}function te({code:e}){return"auth/user-disabled"===e||"auth/user-token-expired"===e}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class ne{constructor(e){this.user=e,this.isRunning=!1,this.timerId=null,this.errorBackoff=3e4}_start(){this.isRunning||(this.isRunning=!0,this.schedule())}_stop(){this.isRunning&&(this.isRunning=!1,null!==this.timerId&&clearTimeout(this.timerId))}getInterval(e){var t;if(e){const e=this.errorBackoff;return this.errorBackoff=Math.min(2*this.errorBackoff,96e4),e}{this.errorBackoff=3e4;const e=null!==(t=this.user.stsTokenManager.expirationTime)&&void 0!==t?t:0,n=e-Date.now()-3e5;return Math.max(0,n)}}schedule(e=!1){if(!this.isRunning)return;const t=this.getInterval(e);this.timerId=setTimeout((async()=>{await this.iteration()}),t)}async iteration(){try{await this.user.getIdToken(!0)}catch(e){return void("auth/network-request-failed"===e.code&&this.schedule(!0))}this.schedule()}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class re{constructor(e,t){this.createdAt=e,this.lastLoginAt=t,this._initializeTime()}_initializeTime(){this.lastSignInTime=K(this.lastLoginAt),this.creationTime=K(this.createdAt)}_copy(e){this.createdAt=e.createdAt,this.lastLoginAt=e.lastLoginAt,this._initializeTime()}toJSON(){return{createdAt:this.createdAt,lastLoginAt:this.lastLoginAt}}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function ie(e){var t;const n=e.auth,r=await e.getIdToken(),i=await ee(e,J(n,{idToken:r}));I(null===i||void 0===i?void 0:i.users.length,n,"internal-error");const o=i.users[0];e._notifyReloadListener(o);const s=(null===(t=o.providerUserInfo)||void 0===t?void 0:t.length)?ae(o.providerUserInfo):[],a=se(e.providerData,s),u=e.isAnonymous,c=!(e.email&&o.passwordHash)&&!(null===a||void 0===a?void 0:a.length),l=!!u&&c,h={uid:o.localId,displayName:o.displayName||null,photoURL:o.photoUrl||null,email:o.email||null,emailVerified:o.emailVerified||!1,phoneNumber:o.phoneNumber||null,tenantId:o.tenantId||null,providerData:a,metadata:new re(o.createdAt,o.lastLoginAt),isAnonymous:l};Object.assign(e,h)}async function oe(e){const t=(0,r.m9)(e);await ie(t),await t.auth._persistUserIfCurrent(t),t.auth._notifyListenersIfCurrent(t)}function se(e,t){const n=e.filter((e=>!t.some((t=>t.providerId===e.providerId))));return[...n,...t]}function ae(e){return e.map((e=>{var{providerId:t}=e,n=o(e,["providerId"]);return{providerId:t,uid:n.rawId||"",displayName:n.displayName||null,email:n.email||null,phoneNumber:n.phoneNumber||null,photoURL:n.photoUrl||null}}))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function ue(e,t){const n=await V(e,{},(async()=>{const n=(0,r.xO)({grant_type:"refresh_token",refresh_token:t}).slice(1),{tokenApiHost:i,apiKey:o}=e.config,s=B(e,i,"/v1/token",`key=${o}`),a=await e._getAdditionalHeaders();return a["Content-Type"]="application/x-www-form-urlencoded",j.fetch()(s,{method:"POST",headers:a,body:n})}));return{accessToken:n.access_token,expiresIn:n.expires_in,refreshToken:n.refresh_token}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class ce{constructor(){this.refreshToken=null,this.accessToken=null,this.expirationTime=null}get isExpired(){return!this.expirationTime||Date.now()>this.expirationTime-3e4}updateFromServerResponse(e){I(e.idToken,"internal-error"),I("undefined"!==typeof e.idToken,"internal-error"),I("undefined"!==typeof e.refreshToken,"internal-error");const t="expiresIn"in e&&"undefined"!==typeof e.expiresIn?Number(e.expiresIn):Q(e.idToken);this.updateTokensAndExpiration(e.idToken,e.refreshToken,t)}async getToken(e,t=!1){return I(!this.accessToken||this.refreshToken,e,"user-token-expired"),t||!this.accessToken||this.isExpired?this.refreshToken?(await this.refresh(e,this.refreshToken),this.accessToken):null:this.accessToken}clearRefreshToken(){this.refreshToken=null}async refresh(e,t){const{accessToken:n,refreshToken:r,expiresIn:i}=await ue(e,t);this.updateTokensAndExpiration(n,r,Number(i))}updateTokensAndExpiration(e,t,n){this.refreshToken=t||null,this.accessToken=e||null,this.expirationTime=Date.now()+1e3*n}static fromJSON(e,t){const{refreshToken:n,accessToken:r,expirationTime:i}=t,o=new ce;return n&&(I("string"===typeof n,"internal-error",{appName:e}),o.refreshToken=n),r&&(I("string"===typeof r,"internal-error",{appName:e}),o.accessToken=r),i&&(I("number"===typeof i,"internal-error",{appName:e}),o.expirationTime=i),o}toJSON(){return{refreshToken:this.refreshToken,accessToken:this.accessToken,expirationTime:this.expirationTime}}_assign(e){this.accessToken=e.accessToken,this.refreshToken=e.refreshToken,this.expirationTime=e.expirationTime}_clone(){return Object.assign(new ce,this.toJSON())}_performRefresh(){return E("not implemented")}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function le(e,t){I("string"===typeof e||"undefined"===typeof e,"internal-error",{appName:t})}class he{constructor(e){var{uid:t,auth:n,stsTokenManager:r}=e,i=o(e,["uid","auth","stsTokenManager"]);this.providerId="firebase",this.proactiveRefresh=new ne(this),this.reloadUserInfo=null,this.reloadListener=null,this.uid=t,this.auth=n,this.stsTokenManager=r,this.accessToken=r.accessToken,this.displayName=i.displayName||null,this.email=i.email||null,this.emailVerified=i.emailVerified||!1,this.phoneNumber=i.phoneNumber||null,this.photoURL=i.photoURL||null,this.isAnonymous=i.isAnonymous||!1,this.tenantId=i.tenantId||null,this.providerData=i.providerData?[...i.providerData]:[],this.metadata=new re(i.createdAt||void 0,i.lastLoginAt||void 0)}async getIdToken(e){const t=await ee(this,this.stsTokenManager.getToken(this.auth,e));return I(t,this.auth,"internal-error"),this.accessToken!==t&&(this.accessToken=t,await this.auth._persistUserIfCurrent(this),this.auth._notifyListenersIfCurrent(this)),t}getIdTokenResult(e){return Y(this,e)}reload(){return oe(this)}_assign(e){this!==e&&(I(this.uid===e.uid,this.auth,"internal-error"),this.displayName=e.displayName,this.photoURL=e.photoURL,this.email=e.email,this.emailVerified=e.emailVerified,this.phoneNumber=e.phoneNumber,this.isAnonymous=e.isAnonymous,this.tenantId=e.tenantId,this.providerData=e.providerData.map((e=>Object.assign({},e))),this.metadata._copy(e.metadata),this.stsTokenManager._assign(e.stsTokenManager))}_clone(e){return new he(Object.assign(Object.assign({},this),{auth:e,stsTokenManager:this.stsTokenManager._clone()}))}_onReload(e){I(!this.reloadListener,this.auth,"internal-error"),this.reloadListener=e,this.reloadUserInfo&&(this._notifyReloadListener(this.reloadUserInfo),this.reloadUserInfo=null)}_notifyReloadListener(e){this.reloadListener?this.reloadListener(e):this.reloadUserInfo=e}_startProactiveRefresh(){this.proactiveRefresh._start()}_stopProactiveRefresh(){this.proactiveRefresh._stop()}async _updateTokensIfNecessary(e,t=!1){let n=!1;e.idToken&&e.idToken!==this.stsTokenManager.accessToken&&(this.stsTokenManager.updateFromServerResponse(e),n=!0),t&&await ie(this),await this.auth._persistUserIfCurrent(this),n&&this.auth._notifyListenersIfCurrent(this)}async delete(){const e=await this.getIdToken();return await ee(this,W(this.auth,{idToken:e})),this.stsTokenManager.clearRefreshToken(),this.auth.signOut()}toJSON(){return Object.assign(Object.assign({uid:this.uid,email:this.email||void 0,emailVerified:this.emailVerified,displayName:this.displayName||void 0,isAnonymous:this.isAnonymous,photoURL:this.photoURL||void 0,phoneNumber:this.phoneNumber||void 0,tenantId:this.tenantId||void 0,providerData:this.providerData.map((e=>Object.assign({},e))),stsTokenManager:this.stsTokenManager.toJSON(),_redirectEventId:this._redirectEventId},this.metadata.toJSON()),{apiKey:this.auth.config.apiKey,appName:this.auth.name})}get refreshToken(){return this.stsTokenManager.refreshToken||""}static _fromJSON(e,t){var n,r,i,o,s,a,u,c;const l=null!==(n=t.displayName)&&void 0!==n?n:void 0,h=null!==(r=t.email)&&void 0!==r?r:void 0,d=null!==(i=t.phoneNumber)&&void 0!==i?i:void 0,f=null!==(o=t.photoURL)&&void 0!==o?o:void 0,p=null!==(s=t.tenantId)&&void 0!==s?s:void 0,m=null!==(a=t._redirectEventId)&&void 0!==a?a:void 0,v=null!==(u=t.createdAt)&&void 0!==u?u:void 0,g=null!==(c=t.lastLoginAt)&&void 0!==c?c:void 0,{uid:_,emailVerified:y,isAnonymous:b,providerData:w,stsTokenManager:E}=t;I(_&&E,e,"internal-error");const k=ce.fromJSON(this.name,E);I("string"===typeof _,e,"internal-error"),le(l,e.name),le(h,e.name),I("boolean"===typeof y,e,"internal-error"),I("boolean"===typeof b,e,"internal-error"),le(d,e.name),le(f,e.name),le(p,e.name),le(m,e.name),le(v,e.name),le(g,e.name);const T=new he({uid:_,auth:e,email:h,emailVerified:y,displayName:l,isAnonymous:b,photoURL:f,phoneNumber:d,tenantId:p,stsTokenManager:k,createdAt:v,lastLoginAt:g});return w&&Array.isArray(w)&&(T.providerData=w.map((e=>Object.assign({},e)))),m&&(T._redirectEventId=m),T}static async _fromIdTokenResponse(e,t,n=!1){const r=new ce;r.updateFromServerResponse(t);const i=new he({uid:t.localId,auth:e,stsTokenManager:r,isAnonymous:n});return await ie(i),i}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class de{constructor(){this.type="NONE",this.storage={}}async _isAvailable(){return!0}async _set(e,t){this.storage[e]=t}async _get(e){const t=this.storage[e];return void 0===t?null:t}async _remove(e){delete this.storage[e]}_addListener(e,t){}_removeListener(e,t){}}de.type="NONE";const fe=de;
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function pe(e,t,n){return`firebase:${e}:${t}:${n}`}class me{constructor(e,t,n){this.persistence=e,this.auth=t,this.userKey=n;const{config:r,name:i}=this.auth;this.fullUserKey=pe(this.userKey,r.apiKey,i),this.fullPersistenceKey=pe("persistence",r.apiKey,i),this.boundEventHandler=t._onStorageEvent.bind(t),this.persistence._addListener(this.fullUserKey,this.boundEventHandler)}setCurrentUser(e){return this.persistence._set(this.fullUserKey,e.toJSON())}async getCurrentUser(){const e=await this.persistence._get(this.fullUserKey);return e?he._fromJSON(this.auth,e):null}removeCurrentUser(){return this.persistence._remove(this.fullUserKey)}savePersistenceForRedirect(){return this.persistence._set(this.fullPersistenceKey,this.persistence.type)}async setPersistence(e){if(this.persistence===e)return;const t=await this.getCurrentUser();return await this.removeCurrentUser(),this.persistence=e,t?this.setCurrentUser(t):void 0}delete(){this.persistence._removeListener(this.fullUserKey,this.boundEventHandler)}static async create(e,t,n="authUser"){if(!t.length)return new me(O(fe),e,n);const r=(await Promise.all(t.map((async e=>{if(await e._isAvailable())return e})))).filter((e=>e));let i=r[0]||O(fe);const o=pe(n,e.config.apiKey,e.name);let s=null;for(const c of t)try{const t=await c._get(o);if(t){const n=he._fromJSON(e,t);c!==i&&(s=n),i=c;break}}catch(u){}const a=r.filter((e=>e._shouldAllowMigration));return i._shouldAllowMigration&&a.length?(i=a[0],s&&await i._set(o,s.toJSON()),await Promise.all(t.map((async e=>{if(e!==i)try{await e._remove(o)}catch(u){}}))),new me(i,e,n)):new me(i,e,n)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function ve(e){const t=e.toLowerCase();if(t.includes("opera/")||t.includes("opr/")||t.includes("opios/"))return"Opera";if(be(t))return"IEMobile";if(t.includes("msie")||t.includes("trident/"))return"IE";if(t.includes("edge/"))return"Edge";if(ge(t))return"Firefox";if(t.includes("silk/"))return"Silk";if(Ie(t))return"Blackberry";if(Ee(t))return"Webos";if(_e(t))return"Safari";if((t.includes("chrome/")||ye(t))&&!t.includes("edge/"))return"Chrome";if(we(t))return"Android";{const t=/([a-zA-Z\d\.]+)\/[a-zA-Z\d\.]*$/,n=e.match(t);if(2===(null===n||void 0===n?void 0:n.length))return n[1]}return"Other"}function ge(e=(0,r.z$)()){return/firefox\//i.test(e)}function _e(e=(0,r.z$)()){const t=e.toLowerCase();return t.includes("safari/")&&!t.includes("chrome/")&&!t.includes("crios/")&&!t.includes("android")}function ye(e=(0,r.z$)()){return/crios\//i.test(e)}function be(e=(0,r.z$)()){return/iemobile/i.test(e)}function we(e=(0,r.z$)()){return/android/i.test(e)}function Ie(e=(0,r.z$)()){return/blackberry/i.test(e)}function Ee(e=(0,r.z$)()){return/webos/i.test(e)}function ke(e=(0,r.z$)()){return/iphone|ipad|ipod/i.test(e)}function Te(e=(0,r.z$)()){return/(iPad|iPhone|iPod).*OS 7_\d/i.test(e)||/(iPad|iPhone|iPod).*OS 8_\d/i.test(e)}function Oe(e=(0,r.z$)()){var t;return ke(e)&&!!(null===(t=window.navigator)||void 0===t?void 0:t.standalone)}function Se(){return(0,r.w1)()&&10===document.documentMode}function Re(e=(0,r.z$)()){return ke(e)||we(e)||Ee(e)||Ie(e)||/windows phone/i.test(e)||be(e)}function Ae(){try{return!(!window||window===window.top)}catch(e){return!1}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Ce(e,t=[]){let n;switch(e){case"Browser":n=ve((0,r.z$)());break;case"Worker":n=`${ve((0,r.z$)())}-${e}`;break;default:n=e}const o=t.length?t.join(","):"FirebaseCore-web";return`${n}/JsCore/${i.SDK_VERSION}/${o}`}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class xe{constructor(e,t,n){this.app=e,this.heartbeatServiceProvider=t,this.config=n,this.currentUser=null,this.emulatorConfig=null,this.operations=Promise.resolve(),this.authStateSubscription=new Ne(this),this.idTokenSubscription=new Ne(this),this.redirectUser=null,this.isProactiveRefreshEnabled=!1,this._canInitEmulator=!0,this._isInitialized=!1,this._deleted=!1,this._initializationPromise=null,this._popupRedirectResolver=null,this._errorFactory=p,this.lastNotifiedUid=void 0,this.languageCode=null,this.tenantId=null,this.settings={appVerificationDisabledForTesting:!1},this.frameworks=[],this.name=e.name,this.clientVersion=n.sdkClientVersion}_initializeWithPersistence(e,t){return t&&(this._popupRedirectResolver=O(t)),this._initializationPromise=this.queue((async()=>{var n,r;if(!this._deleted&&(this.persistenceManager=await me.create(this,e),!this._deleted)){if(null===(n=this._popupRedirectResolver)||void 0===n?void 0:n._shouldInitProactively)try{await this._popupRedirectResolver._initialize(this)}catch(i){}await this.initializeCurrentUser(t),this.lastNotifiedUid=(null===(r=this.currentUser)||void 0===r?void 0:r.uid)||null,this._deleted||(this._isInitialized=!0)}})),this._initializationPromise}async _onStorageEvent(){if(this._deleted)return;const e=await this.assertedPersistence.getCurrentUser();return this.currentUser||e?this.currentUser&&e&&this.currentUser.uid===e.uid?(this._currentUser._assign(e),void await this.currentUser.getIdToken()):void await this._updateCurrentUser(e):void 0}async initializeCurrentUser(e){var t;let n=await this.assertedPersistence.getCurrentUser();if(e&&this.config.authDomain){await this.getOrInitRedirectPersistenceManager();const r=null===(t=this.redirectUser)||void 0===t?void 0:t._redirectEventId,i=null===n||void 0===n?void 0:n._redirectEventId,o=await this.tryRedirectSignIn(e);r&&r!==i||!(null===o||void 0===o?void 0:o.user)||(n=o.user)}return n?n._redirectEventId?(I(this._popupRedirectResolver,this,"argument-error"),await this.getOrInitRedirectPersistenceManager(),this.redirectUser&&this.redirectUser._redirectEventId===n._redirectEventId?this.directlySetCurrentUser(n):this.reloadAndSetCurrentUserOrClear(n)):this.reloadAndSetCurrentUserOrClear(n):this.directlySetCurrentUser(null)}async tryRedirectSignIn(e){let t=null;try{t=await this._popupRedirectResolver._completeRedirectFn(this,e,!0)}catch(n){await this._setRedirectUser(null)}return t}async reloadAndSetCurrentUserOrClear(e){try{await ie(e)}catch(t){if("auth/network-request-failed"!==t.code)return this.directlySetCurrentUser(null)}return this.directlySetCurrentUser(e)}useDeviceLanguage(){this.languageCode=N()}async _delete(){this._deleted=!0}async updateCurrentUser(e){const t=e?(0,r.m9)(e):null;return t&&I(t.auth.config.apiKey===this.config.apiKey,this,"invalid-user-token"),this._updateCurrentUser(t&&t._clone(this))}async _updateCurrentUser(e){if(!this._deleted)return e&&I(this.tenantId===e.tenantId,this,"tenant-id-mismatch"),this.queue((async()=>{await this.directlySetCurrentUser(e),this.notifyAuthListeners()}))}async signOut(){return(this.redirectPersistenceManager||this._popupRedirectResolver)&&await this._setRedirectUser(null),this._updateCurrentUser(null)}setPersistence(e){return this.queue((async()=>{await this.assertedPersistence.setPersistence(O(e))}))}_getPersistence(){return this.assertedPersistence.persistence.type}_updateErrorMap(e){this._errorFactory=new r.LL("auth","Firebase",e())}onAuthStateChanged(e,t,n){return this.registerStateListener(this.authStateSubscription,e,t,n)}onIdTokenChanged(e,t,n){return this.registerStateListener(this.idTokenSubscription,e,t,n)}toJSON(){var e;return{apiKey:this.config.apiKey,authDomain:this.config.authDomain,appName:this.name,currentUser:null===(e=this._currentUser)||void 0===e?void 0:e.toJSON()}}async _setRedirectUser(e,t){const n=await this.getOrInitRedirectPersistenceManager(t);return null===e?n.removeCurrentUser():n.setCurrentUser(e)}async getOrInitRedirectPersistenceManager(e){if(!this.redirectPersistenceManager){const t=e&&O(e)||this._popupRedirectResolver;I(t,this,"argument-error"),this.redirectPersistenceManager=await me.create(this,[O(t._redirectPersistence)],"redirectUser"),this.redirectUser=await this.redirectPersistenceManager.getCurrentUser()}return this.redirectPersistenceManager}async _redirectUserForId(e){var t,n;return this._isInitialized&&await this.queue((async()=>{})),(null===(t=this._currentUser)||void 0===t?void 0:t._redirectEventId)===e?this._currentUser:(null===(n=this.redirectUser)||void 0===n?void 0:n._redirectEventId)===e?this.redirectUser:null}async _persistUserIfCurrent(e){if(e===this.currentUser)return this.queue((async()=>this.directlySetCurrentUser(e)))}_notifyListenersIfCurrent(e){e===this.currentUser&&this.notifyAuthListeners()}_key(){return`${this.config.authDomain}:${this.config.apiKey}:${this.name}`}_startProactiveRefresh(){this.isProactiveRefreshEnabled=!0,this.currentUser&&this._currentUser._startProactiveRefresh()}_stopProactiveRefresh(){this.isProactiveRefreshEnabled=!1,this.currentUser&&this._currentUser._stopProactiveRefresh()}get _currentUser(){return this.currentUser}notifyAuthListeners(){var e,t;if(!this._isInitialized)return;this.idTokenSubscription.next(this.currentUser);const n=null!==(t=null===(e=this.currentUser)||void 0===e?void 0:e.uid)&&void 0!==t?t:null;this.lastNotifiedUid!==n&&(this.lastNotifiedUid=n,this.authStateSubscription.next(this.currentUser))}registerStateListener(e,t,n,r){if(this._deleted)return()=>{};const i="function"===typeof t?t:t.next.bind(t),o=this._isInitialized?Promise.resolve():this._initializationPromise;return I(o,this,"internal-error"),o.then((()=>i(this.currentUser))),"function"===typeof t?e.addObserver(t,n,r):e.addObserver(t)}async directlySetCurrentUser(e){this.currentUser&&this.currentUser!==e&&(this._currentUser._stopProactiveRefresh(),e&&this.isProactiveRefreshEnabled&&e._startProactiveRefresh()),this.currentUser=e,e?await this.assertedPersistence.setCurrentUser(e):await this.assertedPersistence.removeCurrentUser()}queue(e){return this.operations=this.operations.then(e,e),this.operations}get assertedPersistence(){return I(this.persistenceManager,this,"internal-error"),this.persistenceManager}_logFramework(e){e&&!this.frameworks.includes(e)&&(this.frameworks.push(e),this.frameworks.sort(),this.clientVersion=Ce(this.config.clientPlatform,this._getFrameworks()))}_getFrameworks(){return this.frameworks}async _getAdditionalHeaders(){var e;const t={["X-Client-Version"]:this.clientVersion};this.app.options.appId&&(t["X-Firebase-gmpid"]=this.app.options.appId);const n=await(null===(e=this.heartbeatServiceProvider.getImmediate({optional:!0}))||void 0===e?void 0:e.getHeartbeatsHeader());return n&&(t["X-Firebase-Client"]=n),t}}function Pe(e){return(0,r.m9)(e)}class Ne{constructor(e){this.auth=e,this.observer=null,this.addObserver=(0,r.ne)((e=>this.observer=e))}get next(){return I(this.observer,this.auth,"internal-error"),this.observer.next.bind(this.observer)}}function Fe(e,t,n){const r=Pe(e);I(r._canInitEmulator,r,"emulator-config-failed"),I(/^https?:\/\//.test(t),r,"invalid-emulator-scheme");const i=!!(null===n||void 0===n?void 0:n.disableWarnings),o=De(t),{host:s,port:a}=je(t),u=null===a?"":`:${a}`;r.config.emulator={url:`${o}//${s}${u}/`},r.settings.appVerificationDisabledForTesting=!0,r.emulatorConfig=Object.freeze({host:s,port:a,protocol:o.replace(":",""),options:Object.freeze({disableWarnings:i})}),i||Ue()}function De(e){const t=e.indexOf(":");return t<0?"":e.substr(0,t+1)}function je(e){const t=De(e),n=/(\/\/)?([^?#/]+)/.exec(e.substr(t.length));if(!n)return{host:"",port:null};const r=n[2].split("@").pop()||"",i=/^(\[[^\]]+\])(:|$)/.exec(r);if(i){const e=i[1];return{host:e,port:Le(r.substr(e.length+1))}}{const[e,t]=r.split(":");return{host:e,port:Le(t)}}}function Le(e){if(!e)return null;const t=Number(e);return isNaN(t)?null:t}function Ue(){function e(){const e=document.createElement("p"),t=e.style;e.innerText="Running in emulator mode. Do not use with production credentials.",t.position="fixed",t.width="100%",t.backgroundColor="#ffffff",t.border=".1em solid #000000",t.color="#b50000",t.bottom="0px",t.left="0px",t.margin="0px",t.zIndex="10000",t.textAlign="center",e.classList.add("firebase-emulator-warning"),document.body.appendChild(e)}"undefined"!==typeof console&&"function"===typeof console.info&&console.info("WARNING: You are using the Auth Emulator, which is intended for local testing only.  Do not use with production credentials."),"undefined"!==typeof window&&"undefined"!==typeof document&&("loading"===document.readyState?window.addEventListener("DOMContentLoaded",e):e())}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Me{constructor(e,t){this.providerId=e,this.signInMethod=t}toJSON(){return E("not implemented")}_getIdTokenResponse(e){return E("not implemented")}_linkToIdToken(e,t){return E("not implemented")}_getReauthenticationResolver(e){return E("not implemented")}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function $e(e,t){return $(e,"POST","/v1/accounts:resetPassword",M(e,t))}async function Ve(e,t){return $(e,"POST","/v1/accounts:update",t)}async function ze(e,t){return $(e,"POST","/v1/accounts:update",M(e,t))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Be(e,t){return z(e,"POST","/v1/accounts:signInWithPassword",M(e,t))}async function He(e,t){return $(e,"POST","/v1/accounts:sendOobCode",M(e,t))}async function qe(e,t){return He(e,t)}async function We(e,t){return He(e,t)}async function Ge(e,t){return He(e,t)}async function Je(e,t){return He(e,t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Ke(e,t){return z(e,"POST","/v1/accounts:signInWithEmailLink",M(e,t))}async function Ye(e,t){return z(e,"POST","/v1/accounts:signInWithEmailLink",M(e,t))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ze extends Me{constructor(e,t,n,r=null){super("password",n),this._email=e,this._password=t,this._tenantId=r}static _fromEmailAndPassword(e,t){return new Ze(e,t,"password")}static _fromEmailAndCode(e,t,n=null){return new Ze(e,t,"emailLink",n)}toJSON(){return{email:this._email,password:this._password,signInMethod:this.signInMethod,tenantId:this._tenantId}}static fromJSON(e){const t="string"===typeof e?JSON.parse(e):e;if((null===t||void 0===t?void 0:t.email)&&(null===t||void 0===t?void 0:t.password)){if("password"===t.signInMethod)return this._fromEmailAndPassword(t.email,t.password);if("emailLink"===t.signInMethod)return this._fromEmailAndCode(t.email,t.password,t.tenantId)}return null}async _getIdTokenResponse(e){switch(this.signInMethod){case"password":return Be(e,{returnSecureToken:!0,email:this._email,password:this._password});case"emailLink":return Ke(e,{email:this._email,oobCode:this._password});default:g(e,"internal-error")}}async _linkToIdToken(e,t){switch(this.signInMethod){case"password":return Ve(e,{idToken:t,returnSecureToken:!0,email:this._email,password:this._password});case"emailLink":return Ye(e,{idToken:t,email:this._email,oobCode:this._password});default:g(e,"internal-error")}}_getReauthenticationResolver(e){return this._getIdTokenResponse(e)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Xe(e,t){return z(e,"POST","/v1/accounts:signInWithIdp",M(e,t))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Qe="http://localhost";class et extends Me{constructor(){super(...arguments),this.pendingToken=null}static _fromParams(e){const t=new et(e.providerId,e.signInMethod);return e.idToken||e.accessToken?(e.idToken&&(t.idToken=e.idToken),e.accessToken&&(t.accessToken=e.accessToken),e.nonce&&!e.pendingToken&&(t.nonce=e.nonce),e.pendingToken&&(t.pendingToken=e.pendingToken)):e.oauthToken&&e.oauthTokenSecret?(t.accessToken=e.oauthToken,t.secret=e.oauthTokenSecret):g("argument-error"),t}toJSON(){return{idToken:this.idToken,accessToken:this.accessToken,secret:this.secret,nonce:this.nonce,pendingToken:this.pendingToken,providerId:this.providerId,signInMethod:this.signInMethod}}static fromJSON(e){const t="string"===typeof e?JSON.parse(e):e,{providerId:n,signInMethod:r}=t,i=o(t,["providerId","signInMethod"]);if(!n||!r)return null;const s=new et(n,r);return s.idToken=i.idToken||void 0,s.accessToken=i.accessToken||void 0,s.secret=i.secret,s.nonce=i.nonce,s.pendingToken=i.pendingToken||null,s}_getIdTokenResponse(e){const t=this.buildRequest();return Xe(e,t)}_linkToIdToken(e,t){const n=this.buildRequest();return n.idToken=t,Xe(e,n)}_getReauthenticationResolver(e){const t=this.buildRequest();return t.autoCreate=!1,Xe(e,t)}buildRequest(){const e={requestUri:Qe,returnSecureToken:!0};if(this.pendingToken)e.pendingToken=this.pendingToken;else{const t={};this.idToken&&(t["id_token"]=this.idToken),this.accessToken&&(t["access_token"]=this.accessToken),this.secret&&(t["oauth_token_secret"]=this.secret),t["providerId"]=this.providerId,this.nonce&&!this.pendingToken&&(t["nonce"]=this.nonce),e.postBody=(0,r.xO)(t)}return e}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function tt(e,t){return $(e,"POST","/v1/accounts:sendVerificationCode",M(e,t))}async function nt(e,t){return z(e,"POST","/v1/accounts:signInWithPhoneNumber",M(e,t))}async function rt(e,t){const n=await z(e,"POST","/v1/accounts:signInWithPhoneNumber",M(e,t));if(n.temporaryProof)throw q(e,"account-exists-with-different-credential",n);return n}const it={["USER_NOT_FOUND"]:"user-not-found"};async function ot(e,t){const n=Object.assign(Object.assign({},t),{operation:"REAUTH"});return z(e,"POST","/v1/accounts:signInWithPhoneNumber",M(e,n),it)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class st extends Me{constructor(e){super("phone","phone"),this.params=e}static _fromVerification(e,t){return new st({verificationId:e,verificationCode:t})}static _fromTokenResponse(e,t){return new st({phoneNumber:e,temporaryProof:t})}_getIdTokenResponse(e){return nt(e,this._makeVerificationRequest())}_linkToIdToken(e,t){return rt(e,Object.assign({idToken:t},this._makeVerificationRequest()))}_getReauthenticationResolver(e){return ot(e,this._makeVerificationRequest())}_makeVerificationRequest(){const{temporaryProof:e,phoneNumber:t,verificationId:n,verificationCode:r}=this.params;return e&&t?{temporaryProof:e,phoneNumber:t}:{sessionInfo:n,code:r}}toJSON(){const e={providerId:this.providerId};return this.params.phoneNumber&&(e.phoneNumber=this.params.phoneNumber),this.params.temporaryProof&&(e.temporaryProof=this.params.temporaryProof),this.params.verificationCode&&(e.verificationCode=this.params.verificationCode),this.params.verificationId&&(e.verificationId=this.params.verificationId),e}static fromJSON(e){"string"===typeof e&&(e=JSON.parse(e));const{verificationId:t,verificationCode:n,phoneNumber:r,temporaryProof:i}=e;return n||t||r||i?new st({verificationId:t,verificationCode:n,phoneNumber:r,temporaryProof:i}):null}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function at(e){switch(e){case"recoverEmail":return"RECOVER_EMAIL";case"resetPassword":return"PASSWORD_RESET";case"signIn":return"EMAIL_SIGNIN";case"verifyEmail":return"VERIFY_EMAIL";case"verifyAndChangeEmail":return"VERIFY_AND_CHANGE_EMAIL";case"revertSecondFactorAddition":return"REVERT_SECOND_FACTOR_ADDITION";default:return null}}function ut(e){const t=(0,r.zd)((0,r.pd)(e))["link"],n=t?(0,r.zd)((0,r.pd)(t))["deep_link_id"]:null,i=(0,r.zd)((0,r.pd)(e))["deep_link_id"],o=i?(0,r.zd)((0,r.pd)(i))["link"]:null;return o||i||n||t||e}class ct{constructor(e){var t,n,i,o,s,a;const u=(0,r.zd)((0,r.pd)(e)),c=null!==(t=u["apiKey"])&&void 0!==t?t:null,l=null!==(n=u["oobCode"])&&void 0!==n?n:null,h=at(null!==(i=u["mode"])&&void 0!==i?i:null);I(c&&l&&h,"argument-error"),this.apiKey=c,this.operation=h,this.code=l,this.continueUrl=null!==(o=u["continueUrl"])&&void 0!==o?o:null,this.languageCode=null!==(s=u["languageCode"])&&void 0!==s?s:null,this.tenantId=null!==(a=u["tenantId"])&&void 0!==a?a:null}static parseLink(e){const t=ut(e);try{return new ct(t)}catch(n){return null}}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class lt{constructor(){this.providerId=lt.PROVIDER_ID}static credential(e,t){return Ze._fromEmailAndPassword(e,t)}static credentialWithLink(e,t){const n=ct.parseLink(t);return I(n,"argument-error"),Ze._fromEmailAndCode(e,n.code,n.tenantId)}}lt.PROVIDER_ID="password",lt.EMAIL_PASSWORD_SIGN_IN_METHOD="password",lt.EMAIL_LINK_SIGN_IN_METHOD="emailLink";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class ht{constructor(e){this.providerId=e,this.defaultLanguageCode=null,this.customParameters={}}setDefaultLanguage(e){this.defaultLanguageCode=e}setCustomParameters(e){return this.customParameters=e,this}getCustomParameters(){return this.customParameters}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class dt extends ht{constructor(){super(...arguments),this.scopes=[]}addScope(e){return this.scopes.includes(e)||this.scopes.push(e),this}getScopes(){return[...this.scopes]}}class ft extends dt{static credentialFromJSON(e){const t="string"===typeof e?JSON.parse(e):e;return I("providerId"in t&&"signInMethod"in t,"argument-error"),et._fromParams(t)}credential(e){return this._credential(Object.assign(Object.assign({},e),{nonce:e.rawNonce}))}_credential(e){return I(e.idToken||e.accessToken,"argument-error"),et._fromParams(Object.assign(Object.assign({},e),{providerId:this.providerId,signInMethod:this.providerId}))}static credentialFromResult(e){return ft.oauthCredentialFromTaggedObject(e)}static credentialFromError(e){return ft.oauthCredentialFromTaggedObject(e.customData||{})}static oauthCredentialFromTaggedObject({_tokenResponse:e}){if(!e)return null;const{oauthIdToken:t,oauthAccessToken:n,oauthTokenSecret:r,pendingToken:i,nonce:o,providerId:s}=e;if(!n&&!r&&!t&&!i)return null;if(!s)return null;try{return new ft(s)._credential({idToken:t,accessToken:n,nonce:o,pendingToken:i})}catch(a){return null}}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class pt extends dt{constructor(){super("facebook.com")}static credential(e){return et._fromParams({providerId:pt.PROVIDER_ID,signInMethod:pt.FACEBOOK_SIGN_IN_METHOD,accessToken:e})}static credentialFromResult(e){return pt.credentialFromTaggedObject(e)}static credentialFromError(e){return pt.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e||!("oauthAccessToken"in e))return null;if(!e.oauthAccessToken)return null;try{return pt.credential(e.oauthAccessToken)}catch(t){return null}}}pt.FACEBOOK_SIGN_IN_METHOD="facebook.com",pt.PROVIDER_ID="facebook.com";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class mt extends dt{constructor(){super("google.com"),this.addScope("profile")}static credential(e,t){return et._fromParams({providerId:mt.PROVIDER_ID,signInMethod:mt.GOOGLE_SIGN_IN_METHOD,idToken:e,accessToken:t})}static credentialFromResult(e){return mt.credentialFromTaggedObject(e)}static credentialFromError(e){return mt.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e)return null;const{oauthIdToken:t,oauthAccessToken:n}=e;if(!t&&!n)return null;try{return mt.credential(t,n)}catch(r){return null}}}mt.GOOGLE_SIGN_IN_METHOD="google.com",mt.PROVIDER_ID="google.com";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class vt extends dt{constructor(){super("github.com")}static credential(e){return et._fromParams({providerId:vt.PROVIDER_ID,signInMethod:vt.GITHUB_SIGN_IN_METHOD,accessToken:e})}static credentialFromResult(e){return vt.credentialFromTaggedObject(e)}static credentialFromError(e){return vt.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e||!("oauthAccessToken"in e))return null;if(!e.oauthAccessToken)return null;try{return vt.credential(e.oauthAccessToken)}catch(t){return null}}}vt.GITHUB_SIGN_IN_METHOD="github.com",vt.PROVIDER_ID="github.com";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const gt="http://localhost";class _t extends Me{constructor(e,t){super(e,e),this.pendingToken=t}_getIdTokenResponse(e){const t=this.buildRequest();return Xe(e,t)}_linkToIdToken(e,t){const n=this.buildRequest();return n.idToken=t,Xe(e,n)}_getReauthenticationResolver(e){const t=this.buildRequest();return t.autoCreate=!1,Xe(e,t)}toJSON(){return{signInMethod:this.signInMethod,providerId:this.providerId,pendingToken:this.pendingToken}}static fromJSON(e){const t="string"===typeof e?JSON.parse(e):e,{providerId:n,signInMethod:r,pendingToken:i}=t;return n&&r&&i&&n===r?new _t(n,i):null}static _create(e,t){return new _t(e,t)}buildRequest(){return{requestUri:gt,returnSecureToken:!0,pendingToken:this.pendingToken}}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const yt="saml.";class bt extends ht{constructor(e){I(e.startsWith(yt),"argument-error"),super(e)}static credentialFromResult(e){return bt.samlCredentialFromTaggedObject(e)}static credentialFromError(e){return bt.samlCredentialFromTaggedObject(e.customData||{})}static credentialFromJSON(e){const t=_t.fromJSON(e);return I(t,"argument-error"),t}static samlCredentialFromTaggedObject({_tokenResponse:e}){if(!e)return null;const{pendingToken:t,providerId:n}=e;if(!t||!n)return null;try{return _t._create(n,t)}catch(r){return null}}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class wt extends dt{constructor(){super("twitter.com")}static credential(e,t){return et._fromParams({providerId:wt.PROVIDER_ID,signInMethod:wt.TWITTER_SIGN_IN_METHOD,oauthToken:e,oauthTokenSecret:t})}static credentialFromResult(e){return wt.credentialFromTaggedObject(e)}static credentialFromError(e){return wt.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e)return null;const{oauthAccessToken:t,oauthTokenSecret:n}=e;if(!t||!n)return null;try{return wt.credential(t,n)}catch(r){return null}}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
async function It(e,t){return z(e,"POST","/v1/accounts:signUp",M(e,t))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */wt.TWITTER_SIGN_IN_METHOD="twitter.com",wt.PROVIDER_ID="twitter.com";class Et{constructor(e){this.user=e.user,this.providerId=e.providerId,this._tokenResponse=e._tokenResponse,this.operationType=e.operationType}static async _fromIdTokenResponse(e,t,n,r=!1){const i=await he._fromIdTokenResponse(e,n,r),o=kt(n),s=new Et({user:i,providerId:o,_tokenResponse:n,operationType:t});return s}static async _forOperation(e,t,n){await e._updateTokensIfNecessary(n,!0);const r=kt(n);return new Et({user:e,providerId:r,_tokenResponse:n,operationType:t})}}function kt(e){return e.providerId?e.providerId:"phoneNumber"in e?"phone":null}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Tt(e){var t;const n=Pe(e);if(await n._initializationPromise,null===(t=n.currentUser)||void 0===t?void 0:t.isAnonymous)return new Et({user:n.currentUser,providerId:null,operationType:"signIn"});const r=await It(n,{returnSecureToken:!0}),i=await Et._fromIdTokenResponse(n,"signIn",r,!0);return await n._updateCurrentUser(i.user),i}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ot extends r.ZR{constructor(e,t,n,r){var i;super(t.code,t.message),this.operationType=n,this.user=r,Object.setPrototypeOf(this,Ot.prototype),this.customData={appName:e.name,tenantId:null!==(i=e.tenantId)&&void 0!==i?i:void 0,_serverResponse:t.customData._serverResponse,operationType:n}}static _fromErrorAndOperation(e,t,n,r){return new Ot(e,t,n,r)}}function St(e,t,n,r){const i="reauthenticate"===t?n._getReauthenticationResolver(e):n._getIdTokenResponse(e);return i.catch((n=>{if("auth/multi-factor-auth-required"===n.code)throw Ot._fromErrorAndOperation(e,n,t,r);throw n}))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Rt(e){return new Set(e.map((({providerId:e})=>e)).filter((e=>!!e)))}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function At(e,t){const n=(0,r.m9)(e);await xt(!0,n,t);const{providerUserInfo:i}=await G(n.auth,{idToken:await n.getIdToken(),deleteProvider:[t]}),o=Rt(i||[]);return n.providerData=n.providerData.filter((e=>o.has(e.providerId))),o.has("phone")||(n.phoneNumber=null),await n.auth._persistUserIfCurrent(n),n}async function Ct(e,t,n=!1){const r=await ee(e,t._linkToIdToken(e.auth,await e.getIdToken()),n);return Et._forOperation(e,"link",r)}async function xt(e,t,n){await ie(t);const r=Rt(t.providerData),i=!1===e?"provider-already-linked":"no-such-provider";I(r.has(n)===e,t.auth,i)}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Pt(e,t,n=!1){const{auth:r}=e,i="reauthenticate";try{const o=await ee(e,St(r,i,t,e),n);I(o.idToken,r,"internal-error");const s=X(o.idToken);I(s,r,"internal-error");const{sub:a}=s;return I(e.uid===a,r,"user-mismatch"),Et._forOperation(e,i,o)}catch(o){throw"auth/user-not-found"===(null===o||void 0===o?void 0:o.code)&&g(r,"user-mismatch"),o}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Nt(e,t,n=!1){const r="signIn",i=await St(e,r,t),o=await Et._fromIdTokenResponse(e,r,i);return n||await e._updateCurrentUser(o.user),o}async function Ft(e,t){return Nt(Pe(e),t)}async function Dt(e,t){const n=(0,r.m9)(e);return await xt(!1,n,t.providerId),Ct(n,t)}async function jt(e,t){return Pt((0,r.m9)(e),t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Lt(e,t){return z(e,"POST","/v1/accounts:signInWithCustomToken",M(e,t))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Ut(e,t){const n=Pe(e),r=await Lt(n,{token:t,returnSecureToken:!0}),i=await Et._fromIdTokenResponse(n,"signIn",r);return await n._updateCurrentUser(i.user),i}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Mt{constructor(e,t){this.factorId=e,this.uid=t.mfaEnrollmentId,this.enrollmentTime=new Date(t.enrolledAt).toUTCString(),this.displayName=t.displayName}static _fromServerResponse(e,t){return"phoneInfo"in t?$t._fromServerResponse(e,t):g(e,"internal-error")}}class $t extends Mt{constructor(e){super("phone",e),this.phoneNumber=e.phoneInfo}static _fromServerResponse(e,t){return new $t(t)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Vt(e,t,n){var r;I((null===(r=n.url)||void 0===r?void 0:r.length)>0,e,"invalid-continue-uri"),I("undefined"===typeof n.dynamicLinkDomain||n.dynamicLinkDomain.length>0,e,"invalid-dynamic-link-domain"),t.continueUrl=n.url,t.dynamicLinkDomain=n.dynamicLinkDomain,t.canHandleCodeInApp=n.handleCodeInApp,n.iOS&&(I(n.iOS.bundleId.length>0,e,"missing-ios-bundle-id"),t.iOSBundleId=n.iOS.bundleId),n.android&&(I(n.android.packageName.length>0,e,"missing-android-pkg-name"),t.androidInstallApp=n.android.installApp,t.androidMinimumVersionCode=n.android.minimumVersion,t.androidPackageName=n.android.packageName)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function zt(e,t,n){const i=(0,r.m9)(e),o={requestType:"PASSWORD_RESET",email:t};n&&Vt(i,o,n),await We(i,o)}async function Bt(e,t,n){await $e((0,r.m9)(e),{oobCode:t,newPassword:n})}async function Ht(e,t){await ze((0,r.m9)(e),{oobCode:t})}async function qt(e,t){const n=(0,r.m9)(e),i=await $e(n,{oobCode:t}),o=i.requestType;switch(I(o,n,"internal-error"),o){case"EMAIL_SIGNIN":break;case"VERIFY_AND_CHANGE_EMAIL":I(i.newEmail,n,"internal-error");break;case"REVERT_SECOND_FACTOR_ADDITION":I(i.mfaInfo,n,"internal-error");default:I(i.email,n,"internal-error")}let s=null;return i.mfaInfo&&(s=Mt._fromServerResponse(Pe(n),i.mfaInfo)),{data:{email:("VERIFY_AND_CHANGE_EMAIL"===i.requestType?i.newEmail:i.email)||null,previousEmail:("VERIFY_AND_CHANGE_EMAIL"===i.requestType?i.email:i.newEmail)||null,multiFactorInfo:s},operation:o}}async function Wt(e,t){const{data:n}=await qt((0,r.m9)(e),t);return n.email}async function Gt(e,t,n){const r=Pe(e),i=await It(r,{returnSecureToken:!0,email:t,password:n}),o=await Et._fromIdTokenResponse(r,"signIn",i);return await r._updateCurrentUser(o.user),o}function Jt(e,t,n){return Ft((0,r.m9)(e),lt.credential(t,n))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Kt(e,t,n){const i=(0,r.m9)(e),o={requestType:"EMAIL_SIGNIN",email:t};I(n.handleCodeInApp,i,"argument-error"),n&&Vt(i,o,n),await Ge(i,o)}function Yt(e,t){const n=ct.parseLink(t);return"EMAIL_SIGNIN"===(null===n||void 0===n?void 0:n.operation)}async function Zt(e,t,n){const i=(0,r.m9)(e),o=lt.credentialWithLink(t,n||A());return I(o._tenantId===(i.tenantId||null),i,"tenant-id-mismatch"),Ft(i,o)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Xt(e,t){return $(e,"POST","/v1/accounts:createAuthUri",M(e,t))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function Qt(e,t){const n=C()?A():"http://localhost",i={identifier:t,continueUri:n},{signinMethods:o}=await Xt((0,r.m9)(e),i);return o||[]}async function en(e,t){const n=(0,r.m9)(e),i=await e.getIdToken(),o={requestType:"VERIFY_EMAIL",idToken:i};t&&Vt(n.auth,o,t);const{email:s}=await qe(n.auth,o);s!==e.email&&await e.reload()}async function tn(e,t,n){const i=(0,r.m9)(e),o=await e.getIdToken(),s={requestType:"VERIFY_AND_CHANGE_EMAIL",idToken:o,newEmail:t};n&&Vt(i.auth,s,n);const{email:a}=await Je(i.auth,s);a!==e.email&&await e.reload()}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function nn(e,t){return $(e,"POST","/v1/accounts:update",t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function rn(e,{displayName:t,photoURL:n}){if(void 0===t&&void 0===n)return;const i=(0,r.m9)(e),o=await i.getIdToken(),s={idToken:o,displayName:t,photoUrl:n,returnSecureToken:!0},a=await ee(i,nn(i.auth,s));i.displayName=a.displayName||null,i.photoURL=a.photoUrl||null;const u=i.providerData.find((({providerId:e})=>"password"===e));u&&(u.displayName=i.displayName,u.photoURL=i.photoURL),await i._updateTokensIfNecessary(a)}function on(e,t){return an((0,r.m9)(e),t,null)}function sn(e,t){return an((0,r.m9)(e),null,t)}async function an(e,t,n){const{auth:r}=e,i=await e.getIdToken(),o={idToken:i,returnSecureToken:!0};t&&(o.email=t),n&&(o.password=n);const s=await ee(e,Ve(r,o));await e._updateTokensIfNecessary(s,!0)}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function un(e){var t,n;if(!e)return null;const{providerId:r}=e,i=e.rawUserInfo?JSON.parse(e.rawUserInfo):{},o=e.isNewUser||"identitytoolkit#SignupNewUserResponse"===e.kind;if(!r&&(null===e||void 0===e?void 0:e.idToken)){const r=null===(n=null===(t=X(e.idToken))||void 0===t?void 0:t.firebase)||void 0===n?void 0:n["sign_in_provider"];if(r){const e="anonymous"!==r&&"custom"!==r?r:null;return new cn(o,e)}}if(!r)return null;switch(r){case"facebook.com":return new hn(o,i);case"github.com":return new dn(o,i);case"google.com":return new fn(o,i);case"twitter.com":return new pn(o,i,e.screenName||null);case"custom":case"anonymous":return new cn(o,null);default:return new cn(o,r,i)}}class cn{constructor(e,t,n={}){this.isNewUser=e,this.providerId=t,this.profile=n}}class ln extends cn{constructor(e,t,n,r){super(e,t,n),this.username=r}}class hn extends cn{constructor(e,t){super(e,"facebook.com",t)}}class dn extends ln{constructor(e,t){super(e,"github.com",t,"string"===typeof(null===t||void 0===t?void 0:t.login)?null===t||void 0===t?void 0:t.login:null)}}class fn extends cn{constructor(e,t){super(e,"google.com",t)}}class pn extends ln{constructor(e,t,n){super(e,"twitter.com",t,n)}}function mn(e){const{user:t,_tokenResponse:n}=e;return t.isAnonymous&&!n?{providerId:null,isNewUser:!1,profile:null}:un(n)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class vn{constructor(e,t){this.type=e,this.credential=t}static _fromIdtoken(e){return new vn("enroll",e)}static _fromMfaPendingCredential(e){return new vn("signin",e)}toJSON(){const e="enroll"===this.type?"idToken":"pendingCredential";return{multiFactorSession:{[e]:this.credential}}}static fromJSON(e){var t,n;if(null===e||void 0===e?void 0:e.multiFactorSession){if(null===(t=e.multiFactorSession)||void 0===t?void 0:t.pendingCredential)return vn._fromMfaPendingCredential(e.multiFactorSession.pendingCredential);if(null===(n=e.multiFactorSession)||void 0===n?void 0:n.idToken)return vn._fromIdtoken(e.multiFactorSession.idToken)}return null}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class gn{constructor(e,t,n){this.session=e,this.hints=t,this.signInResolver=n}static _fromError(e,t){const n=Pe(e),r=t.customData._serverResponse,i=(r.mfaInfo||[]).map((e=>Mt._fromServerResponse(n,e)));I(r.mfaPendingCredential,n,"internal-error");const o=vn._fromMfaPendingCredential(r.mfaPendingCredential);return new gn(o,i,(async e=>{const i=await e._process(n,o);delete r.mfaInfo,delete r.mfaPendingCredential;const s=Object.assign(Object.assign({},r),{idToken:i.idToken,refreshToken:i.refreshToken});switch(t.operationType){case"signIn":const e=await Et._fromIdTokenResponse(n,t.operationType,s);return await n._updateCurrentUser(e.user),e;case"reauthenticate":return I(t.user,n,"internal-error"),Et._forOperation(t.user,t.operationType,s);default:g(n,"internal-error")}}))}async resolveSignIn(e){const t=e;return this.signInResolver(t)}}function _n(e,t){var n;const i=(0,r.m9)(e),o=t;return I(t.customData.operationType,i,"argument-error"),I(null===(n=o.customData._serverResponse)||void 0===n?void 0:n.mfaPendingCredential,i,"argument-error"),gn._fromError(i,o)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function yn(e,t){return $(e,"POST","/v2/accounts/mfaEnrollment:start",M(e,t))}function bn(e,t){return $(e,"POST","/v2/accounts/mfaEnrollment:finalize",M(e,t))}function wn(e,t){return $(e,"POST","/v2/accounts/mfaEnrollment:withdraw",M(e,t))}class In{constructor(e){this.user=e,this.enrolledFactors=[],e._onReload((t=>{t.mfaInfo&&(this.enrolledFactors=t.mfaInfo.map((t=>Mt._fromServerResponse(e.auth,t))))}))}static _fromUser(e){return new In(e)}async getSession(){return vn._fromIdtoken(await this.user.getIdToken())}async enroll(e,t){const n=e,r=await this.getSession(),i=await ee(this.user,n._process(this.user.auth,r,t));return await this.user._updateTokensIfNecessary(i),this.user.reload()}async unenroll(e){const t="string"===typeof e?e:e.uid,n=await this.user.getIdToken(),r=await ee(this.user,wn(this.user.auth,{idToken:n,mfaEnrollmentId:t}));this.enrolledFactors=this.enrolledFactors.filter((({uid:e})=>e!==t)),await this.user._updateTokensIfNecessary(r);try{await this.user.reload()}catch(i){if("auth/user-token-expired"!==i.code)throw i}}}const En=new WeakMap;function kn(e){const t=(0,r.m9)(e);return En.has(t)||En.set(t,In._fromUser(t)),En.get(t)}const Tn="__sak";
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class On{constructor(e,t){this.storageRetriever=e,this.type=t}_isAvailable(){try{return this.storage?(this.storage.setItem(Tn,"1"),this.storage.removeItem(Tn),Promise.resolve(!0)):Promise.resolve(!1)}catch(e){return Promise.resolve(!1)}}_set(e,t){return this.storage.setItem(e,JSON.stringify(t)),Promise.resolve()}_get(e){const t=this.storage.getItem(e);return Promise.resolve(t?JSON.parse(t):null)}_remove(e){return this.storage.removeItem(e),Promise.resolve()}get storage(){return this.storageRetriever()}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Sn(){const e=(0,r.z$)();return _e(e)||ke(e)}const Rn=1e3,An=10;class Cn extends On{constructor(){super((()=>window.localStorage),"LOCAL"),this.boundEventHandler=(e,t)=>this.onStorageEvent(e,t),this.listeners={},this.localCache={},this.pollTimer=null,this.safariLocalStorageNotSynced=Sn()&&Ae(),this.fallbackToPolling=Re(),this._shouldAllowMigration=!0}forAllChangedKeys(e){for(const t of Object.keys(this.listeners)){const n=this.storage.getItem(t),r=this.localCache[t];n!==r&&e(t,r,n)}}onStorageEvent(e,t=!1){if(!e.key)return void this.forAllChangedKeys(((e,t,n)=>{this.notifyListeners(e,n)}));const n=e.key;if(t?this.detachListener():this.stopPolling(),this.safariLocalStorageNotSynced){const r=this.storage.getItem(n);if(e.newValue!==r)null!==e.newValue?this.storage.setItem(n,e.newValue):this.storage.removeItem(n);else if(this.localCache[n]===e.newValue&&!t)return}const r=()=>{const e=this.storage.getItem(n);(t||this.localCache[n]!==e)&&this.notifyListeners(n,e)},i=this.storage.getItem(n);Se()&&i!==e.newValue&&e.newValue!==e.oldValue?setTimeout(r,An):r()}notifyListeners(e,t){this.localCache[e]=t;const n=this.listeners[e];if(n)for(const r of Array.from(n))r(t?JSON.parse(t):t)}startPolling(){this.stopPolling(),this.pollTimer=setInterval((()=>{this.forAllChangedKeys(((e,t,n)=>{this.onStorageEvent(new StorageEvent("storage",{key:e,oldValue:t,newValue:n}),!0)}))}),Rn)}stopPolling(){this.pollTimer&&(clearInterval(this.pollTimer),this.pollTimer=null)}attachListener(){window.addEventListener("storage",this.boundEventHandler)}detachListener(){window.removeEventListener("storage",this.boundEventHandler)}_addListener(e,t){0===Object.keys(this.listeners).length&&(this.fallbackToPolling?this.startPolling():this.attachListener()),this.listeners[e]||(this.listeners[e]=new Set,this.localCache[e]=this.storage.getItem(e)),this.listeners[e].add(t)}_removeListener(e,t){this.listeners[e]&&(this.listeners[e].delete(t),0===this.listeners[e].size&&delete this.listeners[e]),0===Object.keys(this.listeners).length&&(this.detachListener(),this.stopPolling())}async _set(e,t){await super._set(e,t),this.localCache[e]=JSON.stringify(t)}async _get(e){const t=await super._get(e);return this.localCache[e]=JSON.stringify(t),t}async _remove(e){await super._remove(e),delete this.localCache[e]}}Cn.type="LOCAL";const xn=Cn;
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Pn extends On{constructor(){super((()=>window.sessionStorage),"SESSION")}_addListener(e,t){}_removeListener(e,t){}}Pn.type="SESSION";const Nn=Pn;
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Fn(e){return Promise.all(e.map((async e=>{try{const t=await e;return{fulfilled:!0,value:t}}catch(t){return{fulfilled:!1,reason:t}}})))}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Dn{constructor(e){this.eventTarget=e,this.handlersMap={},this.boundEventHandler=this.handleEvent.bind(this)}static _getInstance(e){const t=this.receivers.find((t=>t.isListeningto(e)));if(t)return t;const n=new Dn(e);return this.receivers.push(n),n}isListeningto(e){return this.eventTarget===e}async handleEvent(e){const t=e,{eventId:n,eventType:r,data:i}=t.data,o=this.handlersMap[r];if(!(null===o||void 0===o?void 0:o.size))return;t.ports[0].postMessage({status:"ack",eventId:n,eventType:r});const s=Array.from(o).map((async e=>e(t.origin,i))),a=await Fn(s);t.ports[0].postMessage({status:"done",eventId:n,eventType:r,response:a})}_subscribe(e,t){0===Object.keys(this.handlersMap).length&&this.eventTarget.addEventListener("message",this.boundEventHandler),this.handlersMap[e]||(this.handlersMap[e]=new Set),this.handlersMap[e].add(t)}_unsubscribe(e,t){this.handlersMap[e]&&t&&this.handlersMap[e].delete(t),t&&0!==this.handlersMap[e].size||delete this.handlersMap[e],0===Object.keys(this.handlersMap).length&&this.eventTarget.removeEventListener("message",this.boundEventHandler)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function jn(e="",t=10){let n="";for(let r=0;r<t;r++)n+=Math.floor(10*Math.random());return e+n}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */Dn.receivers=[];class Ln{constructor(e){this.target=e,this.handlers=new Set}removeMessageHandler(e){e.messageChannel&&(e.messageChannel.port1.removeEventListener("message",e.onMessage),e.messageChannel.port1.close()),this.handlers.delete(e)}async _send(e,t,n=50){const r="undefined"!==typeof MessageChannel?new MessageChannel:null;if(!r)throw new Error("connection_unavailable");let i,o;return new Promise(((s,a)=>{const u=jn("",20);r.port1.start();const c=setTimeout((()=>{a(new Error("unsupported_event"))}),n);o={messageChannel:r,onMessage(e){const t=e;if(t.data.eventId===u)switch(t.data.status){case"ack":clearTimeout(c),i=setTimeout((()=>{a(new Error("timeout"))}),3e3);break;case"done":clearTimeout(i),s(t.data.response);break;default:clearTimeout(c),clearTimeout(i),a(new Error("invalid_response"));break}}},this.handlers.add(o),r.port1.addEventListener("message",o.onMessage),this.target.postMessage({eventType:e,eventId:u,data:t},[r.port2])})).finally((()=>{o&&this.removeMessageHandler(o)}))}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Un(){return window}function Mn(e){Un().location.href=e}
/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function $n(){return"undefined"!==typeof Un()["WorkerGlobalScope"]&&"function"===typeof Un()["importScripts"]}async function Vn(){if(!(null===navigator||void 0===navigator?void 0:navigator.serviceWorker))return null;try{const e=await navigator.serviceWorker.ready;return e.active}catch(e){return null}}function zn(){var e;return(null===(e=null===navigator||void 0===navigator?void 0:navigator.serviceWorker)||void 0===e?void 0:e.controller)||null}function Bn(){return $n()?self:null}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Hn="firebaseLocalStorageDb",qn=1,Wn="firebaseLocalStorage",Gn="fbase_key";class Jn{constructor(e){this.request=e}toPromise(){return new Promise(((e,t)=>{this.request.addEventListener("success",(()=>{e(this.request.result)})),this.request.addEventListener("error",(()=>{t(this.request.error)}))}))}}function Kn(e,t){return e.transaction([Wn],t?"readwrite":"readonly").objectStore(Wn)}function Yn(){const e=indexedDB.deleteDatabase(Hn);return new Jn(e).toPromise()}function Zn(){const e=indexedDB.open(Hn,qn);return new Promise(((t,n)=>{e.addEventListener("error",(()=>{n(e.error)})),e.addEventListener("upgradeneeded",(()=>{const t=e.result;try{t.createObjectStore(Wn,{keyPath:Gn})}catch(r){n(r)}})),e.addEventListener("success",(async()=>{const n=e.result;n.objectStoreNames.contains(Wn)?t(n):(n.close(),await Yn(),t(await Zn()))}))}))}async function Xn(e,t,n){const r=Kn(e,!0).put({[Gn]:t,value:n});return new Jn(r).toPromise()}async function Qn(e,t){const n=Kn(e,!1).get(t),r=await new Jn(n).toPromise();return void 0===r?null:r.value}function er(e,t){const n=Kn(e,!0).delete(t);return new Jn(n).toPromise()}const tr=800,nr=3;class rr{constructor(){this.type="LOCAL",this._shouldAllowMigration=!0,this.listeners={},this.localCache={},this.pollTimer=null,this.pendingWrites=0,this.receiver=null,this.sender=null,this.serviceWorkerReceiverAvailable=!1,this.activeServiceWorker=null,this._workerInitializationPromise=this.initializeServiceWorkerMessaging().then((()=>{}),(()=>{}))}async _openDb(){return this.db||(this.db=await Zn()),this.db}async _withRetries(e){let t=0;while(1)try{const t=await this._openDb();return await e(t)}catch(n){if(t++>nr)throw n;this.db&&(this.db.close(),this.db=void 0)}}async initializeServiceWorkerMessaging(){return $n()?this.initializeReceiver():this.initializeSender()}async initializeReceiver(){this.receiver=Dn._getInstance(Bn()),this.receiver._subscribe("keyChanged",(async(e,t)=>{const n=await this._poll();return{keyProcessed:n.includes(t.key)}})),this.receiver._subscribe("ping",(async(e,t)=>["keyChanged"]))}async initializeSender(){var e,t;if(this.activeServiceWorker=await Vn(),!this.activeServiceWorker)return;this.sender=new Ln(this.activeServiceWorker);const n=await this.sender._send("ping",{},800);n&&(null===(e=n[0])||void 0===e?void 0:e.fulfilled)&&(null===(t=n[0])||void 0===t?void 0:t.value.includes("keyChanged"))&&(this.serviceWorkerReceiverAvailable=!0)}async notifyServiceWorker(e){if(this.sender&&this.activeServiceWorker&&zn()===this.activeServiceWorker)try{await this.sender._send("keyChanged",{key:e},this.serviceWorkerReceiverAvailable?800:50)}catch(t){}}async _isAvailable(){try{if(!indexedDB)return!1;const e=await Zn();return await Xn(e,Tn,"1"),await er(e,Tn),!0}catch(e){}return!1}async _withPendingWrite(e){this.pendingWrites++;try{await e()}finally{this.pendingWrites--}}async _set(e,t){return this._withPendingWrite((async()=>(await this._withRetries((n=>Xn(n,e,t))),this.localCache[e]=t,this.notifyServiceWorker(e))))}async _get(e){const t=await this._withRetries((t=>Qn(t,e)));return this.localCache[e]=t,t}async _remove(e){return this._withPendingWrite((async()=>(await this._withRetries((t=>er(t,e))),delete this.localCache[e],this.notifyServiceWorker(e))))}async _poll(){const e=await this._withRetries((e=>{const t=Kn(e,!1).getAll();return new Jn(t).toPromise()}));if(!e)return[];if(0!==this.pendingWrites)return[];const t=[],n=new Set;for(const{fbase_key:r,value:i}of e)n.add(r),JSON.stringify(this.localCache[r])!==JSON.stringify(i)&&(this.notifyListeners(r,i),t.push(r));for(const r of Object.keys(this.localCache))this.localCache[r]&&!n.has(r)&&(this.notifyListeners(r,null),t.push(r));return t}notifyListeners(e,t){this.localCache[e]=t;const n=this.listeners[e];if(n)for(const r of Array.from(n))r(t)}startPolling(){this.stopPolling(),this.pollTimer=setInterval((async()=>this._poll()),tr)}stopPolling(){this.pollTimer&&(clearInterval(this.pollTimer),this.pollTimer=null)}_addListener(e,t){0===Object.keys(this.listeners).length&&this.startPolling(),this.listeners[e]||(this.listeners[e]=new Set,this._get(e)),this.listeners[e].add(t)}_removeListener(e,t){this.listeners[e]&&(this.listeners[e].delete(t),0===this.listeners[e].size&&delete this.listeners[e]),0===Object.keys(this.listeners).length&&this.stopPolling()}}rr.type="LOCAL";const ir=rr;
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function or(e,t){return $(e,"POST","/v2/accounts/mfaSignIn:start",M(e,t))}function sr(e,t){return $(e,"POST","/v2/accounts/mfaSignIn:finalize",M(e,t))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function ar(e){return(await $(e,"GET","/v1/recaptchaParams")).recaptchaSiteKey||""}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function ur(){var e,t;return null!==(t=null===(e=document.getElementsByTagName("head"))||void 0===e?void 0:e[0])&&void 0!==t?t:document}function cr(e){return new Promise(((t,n)=>{const r=document.createElement("script");r.setAttribute("src",e),r.onload=t,r.onerror=e=>{const t=_("internal-error");t.customData=e,n(t)},r.type="text/javascript",r.charset="UTF-8",ur().appendChild(r)}))}function lr(e){return`__${e}${Math.floor(1e6*Math.random())}`}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const hr=500,dr=6e4,fr=1e12;class pr{constructor(e){this.auth=e,this.counter=fr,this._widgets=new Map}render(e,t){const n=this.counter;return this._widgets.set(n,new mr(e,this.auth.name,t||{})),this.counter++,n}reset(e){var t;const n=e||fr;null===(t=this._widgets.get(n))||void 0===t||t.delete(),this._widgets.delete(n)}getResponse(e){var t;const n=e||fr;return(null===(t=this._widgets.get(n))||void 0===t?void 0:t.getResponse())||""}async execute(e){var t;const n=e||fr;return null===(t=this._widgets.get(n))||void 0===t||t.execute(),""}}class mr{constructor(e,t,n){this.params=n,this.timerId=null,this.deleted=!1,this.responseToken=null,this.clickHandler=()=>{this.execute()};const r="string"===typeof e?document.getElementById(e):e;I(r,"argument-error",{appName:t}),this.container=r,this.isVisible="invisible"!==this.params.size,this.isVisible?this.execute():this.container.addEventListener("click",this.clickHandler)}getResponse(){return this.checkIfDeleted(),this.responseToken}delete(){this.checkIfDeleted(),this.deleted=!0,this.timerId&&(clearTimeout(this.timerId),this.timerId=null),this.container.removeEventListener("click",this.clickHandler)}execute(){this.checkIfDeleted(),this.timerId||(this.timerId=window.setTimeout((()=>{this.responseToken=vr(50);const{callback:e,"expired-callback":t}=this.params;if(e)try{e(this.responseToken)}catch(n){}this.timerId=window.setTimeout((()=>{if(this.timerId=null,this.responseToken=null,t)try{t()}catch(n){}this.isVisible&&this.execute()}),dr)}),hr))}checkIfDeleted(){if(this.deleted)throw new Error("reCAPTCHA mock was already deleted!")}}function vr(e){const t=[],n="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";for(let r=0;r<e;r++)t.push(n.charAt(Math.floor(Math.random()*n.length)));return t.join("")}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const gr=lr("rcb"),_r=new F(3e4,6e4),yr="https://www.google.com/recaptcha/api.js?";class br{constructor(){this.hostLanguage="",this.counter=0,this.librarySeparatelyLoaded=!!Un().grecaptcha}load(e,t=""){return I(wr(t),e,"argument-error"),this.shouldResolveImmediately(t)?Promise.resolve(Un().grecaptcha):new Promise(((n,i)=>{const o=Un().setTimeout((()=>{i(_(e,"network-request-failed"))}),_r.get());Un()[gr]=()=>{Un().clearTimeout(o),delete Un()[gr];const r=Un().grecaptcha;if(!r)return void i(_(e,"internal-error"));const s=r.render;r.render=(e,t)=>{const n=s(e,t);return this.counter++,n},this.hostLanguage=t,n(r)};const s=`${yr}?${(0,r.xO)({onload:gr,render:"explicit",hl:t})}`;cr(s).catch((()=>{clearTimeout(o),i(_(e,"internal-error"))}))}))}clearedOneInstance(){this.counter--}shouldResolveImmediately(e){return!!Un().grecaptcha&&(e===this.hostLanguage||this.counter>0||this.librarySeparatelyLoaded)}}function wr(e){return e.length<=6&&/^\s*[a-zA-Z0-9\-]*\s*$/.test(e)}class Ir{async load(e){return new pr(e)}clearedOneInstance(){}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Er="recaptcha",kr={theme:"light",type:"image"};class Tr{constructor(e,t=Object.assign({},kr),n){this.parameters=t,this.type=Er,this.destroyed=!1,this.widgetId=null,this.tokenChangeListeners=new Set,this.renderPromise=null,this.recaptcha=null,this.auth=Pe(n),this.isInvisible="invisible"===this.parameters.size,I("undefined"!==typeof document,this.auth,"operation-not-supported-in-this-environment");const r="string"===typeof e?document.getElementById(e):e;I(r,this.auth,"argument-error"),this.container=r,this.parameters.callback=this.makeTokenCallback(this.parameters.callback),this._recaptchaLoader=this.auth.settings.appVerificationDisabledForTesting?new Ir:new br,this.validateStartingState()}async verify(){this.assertNotDestroyed();const e=await this.render(),t=this.getAssertedRecaptcha(),n=t.getResponse(e);return n||new Promise((n=>{const r=e=>{e&&(this.tokenChangeListeners.delete(r),n(e))};this.tokenChangeListeners.add(r),this.isInvisible&&t.execute(e)}))}render(){try{this.assertNotDestroyed()}catch(e){return Promise.reject(e)}return this.renderPromise||(this.renderPromise=this.makeRenderPromise().catch((e=>{throw this.renderPromise=null,e}))),this.renderPromise}_reset(){this.assertNotDestroyed(),null!==this.widgetId&&this.getAssertedRecaptcha().reset(this.widgetId)}clear(){this.assertNotDestroyed(),this.destroyed=!0,this._recaptchaLoader.clearedOneInstance(),this.isInvisible||this.container.childNodes.forEach((e=>{this.container.removeChild(e)}))}validateStartingState(){I(!this.parameters.sitekey,this.auth,"argument-error"),I(this.isInvisible||!this.container.hasChildNodes(),this.auth,"argument-error"),I("undefined"!==typeof document,this.auth,"operation-not-supported-in-this-environment")}makeTokenCallback(e){return t=>{if(this.tokenChangeListeners.forEach((e=>e(t))),"function"===typeof e)e(t);else if("string"===typeof e){const n=Un()[e];"function"===typeof n&&n(t)}}}assertNotDestroyed(){I(!this.destroyed,this.auth,"internal-error")}async makeRenderPromise(){if(await this.init(),!this.widgetId){let e=this.container;if(!this.isInvisible){const t=document.createElement("div");e.appendChild(t),e=t}this.widgetId=this.getAssertedRecaptcha().render(e,this.parameters)}return this.widgetId}async init(){I(C()&&!$n(),this.auth,"internal-error"),await Or(),this.recaptcha=await this._recaptchaLoader.load(this.auth,this.auth.languageCode||void 0);const e=await ar(this.auth);I(e,this.auth,"internal-error"),this.parameters.sitekey=e}getAssertedRecaptcha(){return I(this.recaptcha,this.auth,"internal-error"),this.recaptcha}}function Or(){let e=null;return new Promise((t=>{"complete"!==document.readyState?(e=()=>t(),window.addEventListener("load",e)):t()})).catch((t=>{throw e&&window.removeEventListener("load",e),t}))}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Sr{constructor(e,t){this.verificationId=e,this.onConfirmation=t}confirm(e){const t=st._fromVerification(this.verificationId,e);return this.onConfirmation(t)}}async function Rr(e,t,n){const i=Pe(e),o=await xr(i,t,(0,r.m9)(n));return new Sr(o,(e=>Ft(i,e)))}async function Ar(e,t,n){const i=(0,r.m9)(e);await xt(!1,i,"phone");const o=await xr(i.auth,t,(0,r.m9)(n));return new Sr(o,(e=>Dt(i,e)))}async function Cr(e,t,n){const i=(0,r.m9)(e),o=await xr(i.auth,t,(0,r.m9)(n));return new Sr(o,(e=>jt(i,e)))}async function xr(e,t,n){var r;const i=await n.verify();try{let o;if(I("string"===typeof i,e,"argument-error"),I(n.type===Er,e,"argument-error"),o="string"===typeof t?{phoneNumber:t}:t,"session"in o){const t=o.session;if("phoneNumber"in o){I("enroll"===t.type,e,"internal-error");const n=await yn(e,{idToken:t.credential,phoneEnrollmentInfo:{phoneNumber:o.phoneNumber,recaptchaToken:i}});return n.phoneSessionInfo.sessionInfo}{I("signin"===t.type,e,"internal-error");const n=(null===(r=o.multiFactorHint)||void 0===r?void 0:r.uid)||o.multiFactorUid;I(n,e,"missing-multi-factor-info");const s=await or(e,{mfaPendingCredential:t.credential,mfaEnrollmentId:n,phoneSignInInfo:{recaptchaToken:i}});return s.phoneResponseInfo.sessionInfo}}{const{sessionInfo:t}=await tt(e,{phoneNumber:o.phoneNumber,recaptchaToken:i});return t}}finally{n._reset()}}async function Pr(e,t){await Ct((0,r.m9)(e),t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Nr{constructor(e){this.providerId=Nr.PROVIDER_ID,this.auth=Pe(e)}verifyPhoneNumber(e,t){return xr(this.auth,e,(0,r.m9)(t))}static credential(e,t){return st._fromVerification(e,t)}static credentialFromResult(e){const t=e;return Nr.credentialFromTaggedObject(t)}static credentialFromError(e){return Nr.credentialFromTaggedObject(e.customData||{})}static credentialFromTaggedObject({_tokenResponse:e}){if(!e)return null;const{phoneNumber:t,temporaryProof:n}=e;return t&&n?st._fromTokenResponse(t,n):null}}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function Fr(e,t){return t?O(t):(I(e._popupRedirectResolver,e,"argument-error"),e._popupRedirectResolver)}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */Nr.PROVIDER_ID="phone",Nr.PHONE_SIGN_IN_METHOD="phone";class Dr extends Me{constructor(e){super("custom","custom"),this.params=e}_getIdTokenResponse(e){return Xe(e,this._buildIdpRequest())}_linkToIdToken(e,t){return Xe(e,this._buildIdpRequest(t))}_getReauthenticationResolver(e){return Xe(e,this._buildIdpRequest())}_buildIdpRequest(e){const t={requestUri:this.params.requestUri,sessionId:this.params.sessionId,postBody:this.params.postBody,tenantId:this.params.tenantId,pendingToken:this.params.pendingToken,returnSecureToken:!0,returnIdpCredential:!0};return e&&(t.idToken=e),t}}function jr(e){return Nt(e.auth,new Dr(e),e.bypassAuthState)}function Lr(e){const{auth:t,user:n}=e;return I(n,t,"internal-error"),Pt(n,new Dr(e),e.bypassAuthState)}async function Ur(e){const{auth:t,user:n}=e;return I(n,t,"internal-error"),Ct(n,new Dr(e),e.bypassAuthState)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Mr{constructor(e,t,n,r,i=!1){this.auth=e,this.resolver=n,this.user=r,this.bypassAuthState=i,this.pendingPromise=null,this.eventManager=null,this.filter=Array.isArray(t)?t:[t]}execute(){return new Promise((async(e,t)=>{this.pendingPromise={resolve:e,reject:t};try{this.eventManager=await this.resolver._initialize(this.auth),await this.onExecution(),this.eventManager.registerConsumer(this)}catch(n){this.reject(n)}}))}async onAuthEvent(e){const{urlResponse:t,sessionId:n,postBody:r,tenantId:i,error:o,type:s}=e;if(o)return void this.reject(o);const a={auth:this.auth,requestUri:t,sessionId:n,tenantId:i||void 0,postBody:r||void 0,user:this.user,bypassAuthState:this.bypassAuthState};try{this.resolve(await this.getIdpTask(s)(a))}catch(u){this.reject(u)}}onError(e){this.reject(e)}getIdpTask(e){switch(e){case"signInViaPopup":case"signInViaRedirect":return jr;case"linkViaPopup":case"linkViaRedirect":return Ur;case"reauthViaPopup":case"reauthViaRedirect":return Lr;default:g(this.auth,"internal-error")}}resolve(e){k(this.pendingPromise,"Pending promise was never set"),this.pendingPromise.resolve(e),this.unregisterAndCleanUp()}reject(e){k(this.pendingPromise,"Pending promise was never set"),this.pendingPromise.reject(e),this.unregisterAndCleanUp()}unregisterAndCleanUp(){this.eventManager&&this.eventManager.unregisterConsumer(this),this.pendingPromise=null,this.cleanUp()}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const $r=new F(2e3,1e4);async function Vr(e,t,n){const r=Pe(e);b(e,t,ht);const i=Fr(r,n),o=new Hr(r,"signInViaPopup",t,i);return o.executeNotNull()}async function zr(e,t,n){const i=(0,r.m9)(e);b(i.auth,t,ht);const o=Fr(i.auth,n),s=new Hr(i.auth,"reauthViaPopup",t,o,i);return s.executeNotNull()}async function Br(e,t,n){const i=(0,r.m9)(e);b(i.auth,t,ht);const o=Fr(i.auth,n),s=new Hr(i.auth,"linkViaPopup",t,o,i);return s.executeNotNull()}class Hr extends Mr{constructor(e,t,n,r,i){super(e,t,r,i),this.provider=n,this.authWindow=null,this.pollId=null,Hr.currentPopupAction&&Hr.currentPopupAction.cancel(),Hr.currentPopupAction=this}async executeNotNull(){const e=await this.execute();return I(e,this.auth,"internal-error"),e}async onExecution(){k(1===this.filter.length,"Popup operations only handle one event");const e=jn();this.authWindow=await this.resolver._openPopup(this.auth,this.provider,this.filter[0],e),this.authWindow.associatedEvent=e,this.resolver._originValidation(this.auth).catch((e=>{this.reject(e)})),this.resolver._isIframeWebStorageSupported(this.auth,(e=>{e||this.reject(_(this.auth,"web-storage-unsupported"))})),this.pollUserCancellation()}get eventId(){var e;return(null===(e=this.authWindow)||void 0===e?void 0:e.associatedEvent)||null}cancel(){this.reject(_(this.auth,"cancelled-popup-request"))}cleanUp(){this.authWindow&&this.authWindow.close(),this.pollId&&window.clearTimeout(this.pollId),this.authWindow=null,this.pollId=null,Hr.currentPopupAction=null}pollUserCancellation(){const e=()=>{var t,n;(null===(n=null===(t=this.authWindow)||void 0===t?void 0:t.window)||void 0===n?void 0:n.closed)?this.pollId=window.setTimeout((()=>{this.pollId=null,this.reject(_(this.auth,"popup-closed-by-user"))}),2e3):this.pollId=window.setTimeout(e,$r.get())};e()}}Hr.currentPopupAction=null;
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const qr="pendingRedirect",Wr=new Map;class Gr extends Mr{constructor(e,t,n=!1){super(e,["signInViaRedirect","linkViaRedirect","reauthViaRedirect","unknown"],t,void 0,n),this.eventId=null}async execute(){let e=Wr.get(this.auth._key());if(!e){try{const t=await Jr(this.resolver,this.auth),n=t?await super.execute():null;e=()=>Promise.resolve(n)}catch(t){e=()=>Promise.reject(t)}Wr.set(this.auth._key(),e)}return this.bypassAuthState||Wr.set(this.auth._key(),(()=>Promise.resolve(null))),e()}async onAuthEvent(e){if("signInViaRedirect"===e.type)return super.onAuthEvent(e);if("unknown"!==e.type){if(e.eventId){const t=await this.auth._redirectUserForId(e.eventId);if(t)return this.user=t,super.onAuthEvent(e);this.resolve(null)}}else this.resolve(null)}async onExecution(){}cleanUp(){}}async function Jr(e,t){const n=Xr(t),r=Zr(e);if(!await r._isAvailable())return!1;const i="true"===await r._get(n);return await r._remove(n),i}async function Kr(e,t){return Zr(e)._set(Xr(t),"true")}function Yr(){Wr.clear()}function Zr(e){return O(e._redirectPersistence)}function Xr(e){return pe(qr,e.config.apiKey,e.name)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Qr(e,t,n){return ei(e,t,n)}async function ei(e,t,n){const r=Pe(e);b(e,t,ht);const i=Fr(r,n);return await Kr(i,r),i._openRedirect(r,t,"signInViaRedirect")}function ti(e,t,n){return ni(e,t,n)}async function ni(e,t,n){const i=(0,r.m9)(e);b(i.auth,t,ht);const o=Fr(i.auth,n);await Kr(o,i.auth);const s=await ai(i);return o._openRedirect(i.auth,t,"reauthViaRedirect",s)}function ri(e,t,n){return ii(e,t,n)}async function ii(e,t,n){const i=(0,r.m9)(e);b(i.auth,t,ht);const o=Fr(i.auth,n);await xt(!1,i,t.providerId),await Kr(o,i.auth);const s=await ai(i);return o._openRedirect(i.auth,t,"linkViaRedirect",s)}async function oi(e,t){return await Pe(e)._initializationPromise,si(e,t,!1)}async function si(e,t,n=!1){const r=Pe(e),i=Fr(r,t),o=new Gr(r,i,n),s=await o.execute();return s&&!n&&(delete s.user._redirectEventId,await r._persistUserIfCurrent(s.user),await r._setRedirectUser(null,t)),s}async function ai(e){const t=jn(`${e.uid}:::`);return e._redirectEventId=t,await e.auth._setRedirectUser(e),await e.auth._persistUserIfCurrent(e),t}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ui=6e5;class ci{constructor(e){this.auth=e,this.cachedEventUids=new Set,this.consumers=new Set,this.queuedRedirectEvent=null,this.hasHandledPotentialRedirect=!1,this.lastProcessedEventTime=Date.now()}registerConsumer(e){this.consumers.add(e),this.queuedRedirectEvent&&this.isEventForConsumer(this.queuedRedirectEvent,e)&&(this.sendToConsumer(this.queuedRedirectEvent,e),this.saveEventToCache(this.queuedRedirectEvent),this.queuedRedirectEvent=null)}unregisterConsumer(e){this.consumers.delete(e)}onEvent(e){if(this.hasEventBeenHandled(e))return!1;let t=!1;return this.consumers.forEach((n=>{this.isEventForConsumer(e,n)&&(t=!0,this.sendToConsumer(e,n),this.saveEventToCache(e))})),this.hasHandledPotentialRedirect||!di(e)||(this.hasHandledPotentialRedirect=!0,t||(this.queuedRedirectEvent=e,t=!0)),t}sendToConsumer(e,t){var n;if(e.error&&!hi(e)){const r=(null===(n=e.error.code)||void 0===n?void 0:n.split("auth/")[1])||"internal-error";t.onError(_(this.auth,r))}else t.onAuthEvent(e)}isEventForConsumer(e,t){const n=null===t.eventId||!!e.eventId&&e.eventId===t.eventId;return t.filter.includes(e.type)&&n}hasEventBeenHandled(e){return Date.now()-this.lastProcessedEventTime>=ui&&this.cachedEventUids.clear(),this.cachedEventUids.has(li(e))}saveEventToCache(e){this.cachedEventUids.add(li(e)),this.lastProcessedEventTime=Date.now()}}function li(e){return[e.type,e.eventId,e.sessionId,e.tenantId].filter((e=>e)).join("-")}function hi({type:e,error:t}){return"unknown"===e&&"auth/no-auth-event"===(null===t||void 0===t?void 0:t.code)}function di(e){switch(e.type){case"signInViaRedirect":case"linkViaRedirect":case"reauthViaRedirect":return!0;case"unknown":return hi(e);default:return!1}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */async function fi(e,t={}){return $(e,"GET","/v1/projects",t)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const pi=/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/,mi=/^https?/;async function vi(e){if(e.config.emulator)return;const{authorizedDomains:t}=await fi(e);for(const r of t)try{if(gi(r))return}catch(n){}g(e,"unauthorized-domain")}function gi(e){const t=A(),{protocol:n,hostname:r}=new URL(t);if(e.startsWith("chrome-extension://")){const i=new URL(e);return""===i.hostname&&""===r?"chrome-extension:"===n&&e.replace("chrome-extension://","")===t.replace("chrome-extension://",""):"chrome-extension:"===n&&i.hostname===r}if(!mi.test(n))return!1;if(pi.test(e))return r===e;const i=e.replace(/\./g,"\\."),o=new RegExp("^(.+\\."+i+"|"+i+")$","i");return o.test(r)}
/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const _i=new F(3e4,6e4);function yi(){const e=Un().___jsl;if(null===e||void 0===e?void 0:e.H)for(const t of Object.keys(e.H))if(e.H[t].r=e.H[t].r||[],e.H[t].L=e.H[t].L||[],e.H[t].r=[...e.H[t].L],e.CP)for(let n=0;n<e.CP.length;n++)e.CP[n]=null}function bi(e){return new Promise(((t,n)=>{var r,i,o;function s(){yi(),gapi.load("gapi.iframes",{callback:()=>{t(gapi.iframes.getContext())},ontimeout:()=>{yi(),n(_(e,"network-request-failed"))},timeout:_i.get()})}if(null===(i=null===(r=Un().gapi)||void 0===r?void 0:r.iframes)||void 0===i?void 0:i.Iframe)t(gapi.iframes.getContext());else{if(!(null===(o=Un().gapi)||void 0===o?void 0:o.load)){const t=lr("iframefcb");return Un()[t]=()=>{gapi.load?s():n(_(e,"network-request-failed"))},cr(`https://apis.google.com/js/api.js?onload=${t}`).catch((e=>n(e)))}s()}})).catch((e=>{throw wi=null,e}))}let wi=null;function Ii(e){return wi=wi||bi(e),wi}
/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Ei=new F(5e3,15e3),ki="__/auth/iframe",Ti="emulator/auth/iframe",Oi={style:{position:"absolute",top:"-100px",width:"1px",height:"1px"},"aria-hidden":"true",tabindex:"-1"},Si=new Map([["identitytoolkit.googleapis.com","p"],["staging-identitytoolkit.sandbox.googleapis.com","s"],["test-identitytoolkit.sandbox.googleapis.com","t"]]);function Ri(e){const t=e.config;I(t.authDomain,e,"auth-domain-config-required");const n=t.emulator?D(t,Ti):`https://${e.config.authDomain}/${ki}`,o={apiKey:t.apiKey,appName:e.name,v:i.SDK_VERSION},s=Si.get(e.config.apiHost);s&&(o.eid=s);const a=e._getFrameworks();return a.length&&(o.fw=a.join(",")),`${n}?${(0,r.xO)(o).slice(1)}`}async function Ai(e){const t=await Ii(e),n=Un().gapi;return I(n,e,"internal-error"),t.open({where:document.body,url:Ri(e),messageHandlersFilter:n.iframes.CROSS_ORIGIN_IFRAMES_FILTER,attributes:Oi,dontclear:!0},(t=>new Promise((async(n,r)=>{await t.restyle({setHideOnLeave:!1});const i=_(e,"network-request-failed"),o=Un().setTimeout((()=>{r(i)}),Ei.get());function s(){Un().clearTimeout(o),n(t)}t.ping(s).then(s,(()=>{r(i)}))}))))}
/**
 * @license
 * Copyright 2020 Google LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Ci={location:"yes",resizable:"yes",statusbar:"yes",toolbar:"no"},xi=500,Pi=600,Ni="_blank",Fi="http://localhost";class Di{constructor(e){this.window=e,this.associatedEvent=null}close(){if(this.window)try{this.window.close()}catch(e){}}}function ji(e,t,n,i=xi,o=Pi){const s=Math.max((window.screen.availHeight-o)/2,0).toString(),a=Math.max((window.screen.availWidth-i)/2,0).toString();let u="";const c=Object.assign(Object.assign({},Ci),{width:i.toString(),height:o.toString(),top:s,left:a}),l=(0,r.z$)().toLowerCase();n&&(u=ye(l)?Ni:n),ge(l)&&(t=t||Fi,c.scrollbars="yes");const h=Object.entries(c).reduce(((e,[t,n])=>`${e}${t}=${n},`),"");if(Oe(l)&&"_self"!==u)return Li(t||"",u),new Di(null);const d=window.open(t||"",u,h);I(d,e,"popup-blocked");try{d.focus()}catch(f){}return new Di(d)}function Li(e,t){const n=document.createElement("a");n.href=e,n.target=t;const r=document.createEvent("MouseEvent");r.initMouseEvent("click",!0,!0,window,1,0,0,0,0,!1,!1,!1,!1,1,null),n.dispatchEvent(r)}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Ui="__/auth/handler",Mi="emulator/auth/handler";function $i(e,t,n,o,s,a){I(e.config.authDomain,e,"auth-domain-config-required"),I(e.config.apiKey,e,"invalid-api-key");const u={apiKey:e.config.apiKey,appName:e.name,authType:n,redirectUrl:o,v:i.SDK_VERSION,eventId:s};if(t instanceof ht){t.setDefaultLanguage(e.languageCode),u.providerId=t.providerId||"",(0,r.xb)(t.getCustomParameters())||(u.customParameters=JSON.stringify(t.getCustomParameters()));for(const[e,t]of Object.entries(a||{}))u[e]=t}if(t instanceof dt){const e=t.getScopes().filter((e=>""!==e));e.length>0&&(u.scopes=e.join(","))}e.tenantId&&(u.tid=e.tenantId);const c=u;for(const r of Object.keys(c))void 0===c[r]&&delete c[r];return`${Vi(e)}?${(0,r.xO)(c).slice(1)}`}function Vi({config:e}){return e.emulator?D(e,Mi):`https://${e.authDomain}/${Ui}`}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const zi="webStorageSupport";class Bi{constructor(){this.eventManagers={},this.iframes={},this.originValidationPromises={},this._redirectPersistence=Nn,this._completeRedirectFn=si}async _openPopup(e,t,n,r){var i;k(null===(i=this.eventManagers[e._key()])||void 0===i?void 0:i.manager,"_initialize() not called before _openPopup()");const o=$i(e,t,n,A(),r);return ji(e,o,jn())}async _openRedirect(e,t,n,r){return await this._originValidation(e),Mn($i(e,t,n,A(),r)),new Promise((()=>{}))}_initialize(e){const t=e._key();if(this.eventManagers[t]){const{manager:e,promise:n}=this.eventManagers[t];return e?Promise.resolve(e):(k(n,"If manager is not set, promise should be"),n)}const n=this.initAndGetManager(e);return this.eventManagers[t]={promise:n},n.catch((()=>{delete this.eventManagers[t]})),n}async initAndGetManager(e){const t=await Ai(e),n=new ci(e);return t.register("authEvent",(t=>{I(null===t||void 0===t?void 0:t.authEvent,e,"invalid-auth-event");const r=n.onEvent(t.authEvent);return{status:r?"ACK":"ERROR"}}),gapi.iframes.CROSS_ORIGIN_IFRAMES_FILTER),this.eventManagers[e._key()]={manager:n},this.iframes[e._key()]=t,n}_isIframeWebStorageSupported(e,t){const n=this.iframes[e._key()];n.send(zi,{type:zi},(n=>{var r;const i=null===(r=null===n||void 0===n?void 0:n[0])||void 0===r?void 0:r[zi];void 0!==i&&t(!!i),g(e,"internal-error")}),gapi.iframes.CROSS_ORIGIN_IFRAMES_FILTER)}_originValidation(e){const t=e._key();return this.originValidationPromises[t]||(this.originValidationPromises[t]=vi(e)),this.originValidationPromises[t]}get _shouldInitProactively(){return Re()||_e()||ke()}}const Hi=Bi;class qi{constructor(e){this.factorId=e}_process(e,t,n){switch(t.type){case"enroll":return this._finalizeEnroll(e,t.credential,n);case"signin":return this._finalizeSignIn(e,t.credential);default:return E("unexpected MultiFactorSessionType")}}}class Wi extends qi{constructor(e){super("phone"),this.credential=e}static _fromCredential(e){return new Wi(e)}_finalizeEnroll(e,t,n){return bn(e,{idToken:t,displayName:n,phoneVerificationInfo:this.credential._makeVerificationRequest()})}_finalizeSignIn(e,t){return sr(e,{mfaPendingCredential:t,phoneVerificationInfo:this.credential._makeVerificationRequest()})}}class Gi{constructor(){}static assertion(e){return Wi._fromCredential(e)}}Gi.FACTOR_ID="phone";var Ji="@firebase/auth",Ki="0.19.11";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Yi{constructor(e){this.auth=e,this.internalListeners=new Map}getUid(){var e;return this.assertAuthConfigured(),(null===(e=this.auth.currentUser)||void 0===e?void 0:e.uid)||null}async getToken(e){if(this.assertAuthConfigured(),await this.auth._initializationPromise,!this.auth.currentUser)return null;const t=await this.auth.currentUser.getIdToken(e);return{accessToken:t}}addAuthTokenListener(e){if(this.assertAuthConfigured(),this.internalListeners.has(e))return;const t=this.auth.onIdTokenChanged((t=>{var n;e((null===(n=t)||void 0===n?void 0:n.stsTokenManager.accessToken)||null)}));this.internalListeners.set(e,t),this.updateProactiveRefresh()}removeAuthTokenListener(e){this.assertAuthConfigured();const t=this.internalListeners.get(e);t&&(this.internalListeners.delete(e),t(),this.updateProactiveRefresh())}assertAuthConfigured(){I(this.auth._initializationPromise,"dependent-sdk-initialized-before-auth")}updateProactiveRefresh(){this.internalListeners.size>0?this.auth._startProactiveRefresh():this.auth._stopProactiveRefresh()}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Zi(e){switch(e){case"Node":return"node";case"ReactNative":return"rn";case"Worker":return"webworker";case"Cordova":return"cordova";default:return}}function Xi(e){(0,i._registerComponent)(new a.wA("auth",((t,{options:n})=>{const r=t.getProvider("app").getImmediate(),i=t.getProvider("heartbeat"),{apiKey:o,authDomain:s}=r.options;return((t,r)=>{I(o&&!o.includes(":"),"invalid-api-key",{appName:t.name}),I(!(null===s||void 0===s?void 0:s.includes(":")),"argument-error",{appName:t.name});const i={apiKey:o,authDomain:s,clientPlatform:e,apiHost:"identitytoolkit.googleapis.com",tokenApiHost:"securetoken.googleapis.com",apiScheme:"https",sdkClientVersion:Ce(e)},a=new xe(t,r,i);return R(a,n),a})(r,i)}),"PUBLIC").setInstantiationMode("EXPLICIT").setInstanceCreatedCallback(((e,t,n)=>{const r=e.getProvider("auth-internal");r.initialize()}))),(0,i._registerComponent)(new a.wA("auth-internal",(e=>{const t=Pe(e.getProvider("auth").getImmediate());return(e=>new Yi(e))(t)}),"PRIVATE").setInstantiationMode("EXPLICIT")),(0,i.registerVersion)(Ji,Ki,Zi(e)),(0,i.registerVersion)(Ji,Ki,"esm2017")}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Qi(e=(0,i.getApp)()){const t=(0,i._getProvider)(e,"auth");return t.isInitialized()?t.getImmediate():S(e,{popupRedirectResolver:Hi,persistence:[ir,xn,Nn]})}Xi("Browser")},2575:function(e,t,n){"use strict";n.d(t,{v0:function(){return r.n}});var r=n(8895);n(223),n(9684),n(5168),n(7142)},2087:function(e,t,n){"use strict";n.d(t,{$Y:function(){return Qe},B0:function(){return Ct},FN:function(){return Xe},IX:function(){return at},Jt:function(){return Dt},KV:function(){return At},UJ:function(){return de},Ym:function(){return Pt},a1:function(){return Mt},aF:function(){return Ft},bm:function(){return ne},g6:function(){return Ut},gE:function(){return x},gH:function(){return O},iH:function(){return Lt},oq:function(){return jt},pb:function(){return Nt},qm:function(){return ie},sd:function(){return xt},y4:function(){return R}});n(2801),n(8675),n(3462),n(1703);var r=n(9684),i=n(223),o=n(7142);
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const s="firebasestorage.googleapis.com",a="storageBucket",u=12e4,c=6e5;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class l extends i.ZR{constructor(e,t){super(h(e),`Firebase Storage: ${t} (${h(e)})`),this.customData={serverResponse:null},this._baseMessage=this.message,Object.setPrototypeOf(this,l.prototype)}_codeEquals(e){return h(e)===this.code}get serverResponse(){return this.customData.serverResponse}set serverResponse(e){this.customData.serverResponse=e,this.customData.serverResponse?this.message=`${this._baseMessage}\n${this.customData.serverResponse}`:this.message=this._baseMessage}}function h(e){return"storage/"+e}function d(){const e="An unknown error occurred, please check the error payload for server response.";return new l("unknown",e)}function f(e){return new l("object-not-found","Object '"+e+"' does not exist.")}function p(e){return new l("quota-exceeded","Quota for bucket '"+e+"' exceeded, please view quota on https://firebase.google.com/pricing/.")}function m(){const e="User is not authenticated, please authenticate using Firebase Authentication and try again.";return new l("unauthenticated",e)}function v(){return new l("unauthorized-app","This app does not have permission to access Firebase Storage on this project.")}function g(e){return new l("unauthorized","User does not have permission to access '"+e+"'.")}function _(){return new l("retry-limit-exceeded","Max retry time for operation exceeded, please try again.")}function y(){return new l("canceled","User canceled the upload/download.")}function b(e){return new l("invalid-url","Invalid URL '"+e+"'.")}function w(e){return new l("invalid-default-bucket","Invalid default bucket '"+e+"'.")}function I(){return new l("no-default-bucket","No default bucket found. Did you set the '"+a+"' property when initializing the app?")}function E(){return new l("cannot-slice-blob","Cannot slice blob for upload. Please retry the upload.")}function k(){return new l("server-file-wrong-size","Server recorded incorrect upload file size, please retry the upload.")}function T(){return new l("no-download-url","The given file does not have any download URLs.")}function O(e){return new l("invalid-argument",e)}function S(){return new l("app-deleted","The Firebase app was deleted.")}function R(e){return new l("invalid-root-operation","The operation '"+e+"' cannot be performed on a root reference, create a non-root reference using child, such as .child('file.png').")}function A(e,t){return new l("invalid-format","String does not match format '"+e+"': "+t)}function C(e){throw new l("internal-error","Internal error: "+e)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class x{constructor(e,t){this.bucket=e,this.path_=t}get path(){return this.path_}get isRoot(){return 0===this.path.length}fullServerUrl(){const e=encodeURIComponent;return"/b/"+e(this.bucket)+"/o/"+e(this.path)}bucketOnlyServerUrl(){const e=encodeURIComponent;return"/b/"+e(this.bucket)+"/o"}static makeFromBucketSpec(e,t){let n;try{n=x.makeFromUrl(e,t)}catch(r){return new x(e,"")}if(""===n.path)return n;throw w(e)}static makeFromUrl(e,t){let n=null;const r="([A-Za-z0-9.\\-_]+)";function i(e){"/"===e.path.charAt(e.path.length-1)&&(e.path_=e.path_.slice(0,-1))}const o="(/(.*))?$",a=new RegExp("^gs://"+r+o,"i"),u={bucket:1,path:3};function c(e){e.path_=decodeURIComponent(e.path)}const l="v[A-Za-z0-9_]+",h=t.replace(/[.]/g,"\\."),d="(/([^?#]*).*)?$",f=new RegExp(`^https?://${h}/${l}/b/${r}/o${d}`,"i"),p={bucket:1,path:3},m=t===s?"(?:storage.googleapis.com|storage.cloud.google.com)":t,v="([^?#]*)",g=new RegExp(`^https?://${m}/${r}/${v}`,"i"),_={bucket:1,path:2},y=[{regex:a,indices:u,postModify:i},{regex:f,indices:p,postModify:c},{regex:g,indices:_,postModify:c}];for(let s=0;s<y.length;s++){const t=y[s],r=t.regex.exec(e);if(r){const e=r[t.indices.bucket];let i=r[t.indices.path];i||(i=""),n=new x(e,i),t.postModify(n);break}}if(null==n)throw b(e);return n}}class P{constructor(e){this.promise_=Promise.reject(e)}getPromise(){return this.promise_}cancel(e=!1){}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function N(e,t,n){let r=1,i=null,o=null,s=!1,a=0;function u(){return 2===a}let c=!1;function l(...e){c||(c=!0,t.apply(null,e))}function h(t){i=setTimeout((()=>{i=null,e(f,u())}),t)}function d(){o&&clearTimeout(o)}function f(e,...t){if(c)return void d();if(e)return d(),void l.call(null,e,...t);const n=u()||s;if(n)return d(),void l.call(null,e,...t);let i;r<64&&(r*=2),1===a?(a=2,i=0):i=1e3*(r+Math.random()),h(i)}let p=!1;function m(e){p||(p=!0,d(),c||(null!==i?(e||(a=2),clearTimeout(i),h(0)):e||(a=1)))}return h(0),o=setTimeout((()=>{s=!0,m(!0)}),n),m}function F(e){e(!1)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function D(e){return void 0!==e}function j(e){return"function"===typeof e}function L(e){return"object"===typeof e&&!Array.isArray(e)}function U(e){return"string"===typeof e||e instanceof String}function M(e){return $()&&e instanceof Blob}function $(){return"undefined"!==typeof Blob}function V(e,t,n,r){if(r<t)throw O(`Invalid value for '${e}'. Expected ${t} or greater.`);if(r>n)throw O(`Invalid value for '${e}'. Expected ${n} or less.`)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function z(e,t,n){let r=t;return null==n&&(r=`https://${t}`),`${n}://${r}/v0${e}`}function B(e){const t=encodeURIComponent;let n="?";for(const r in e)if(e.hasOwnProperty(r)){const i=t(r)+"="+t(e[r]);n=n+i+"&"}return n=n.slice(0,-1),n}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */var H;(function(e){e[e["NO_ERROR"]=0]="NO_ERROR",e[e["NETWORK_ERROR"]=1]="NETWORK_ERROR",e[e["ABORT"]=2]="ABORT"})(H||(H={}));
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class q{constructor(e,t,n,r,i,o,s,a,u,c,l){this.url_=e,this.method_=t,this.headers_=n,this.body_=r,this.successCodes_=i,this.additionalRetryCodes_=o,this.callback_=s,this.errorCallback_=a,this.timeout_=u,this.progressCallback_=c,this.connectionFactory_=l,this.pendingConnection_=null,this.backoffId_=null,this.canceled_=!1,this.appDelete_=!1,this.promise_=new Promise(((e,t)=>{this.resolve_=e,this.reject_=t,this.start_()}))}start_(){const e=(e,t)=>{if(t)return void e(!1,new W(!1,null,!0));const n=this.connectionFactory_();this.pendingConnection_=n;const r=e=>{const t=e.loaded,n=e.lengthComputable?e.total:-1;null!==this.progressCallback_&&this.progressCallback_(t,n)};null!==this.progressCallback_&&n.addUploadProgressListener(r),n.send(this.url_,this.method_,this.body_,this.headers_).then((()=>{null!==this.progressCallback_&&n.removeUploadProgressListener(r),this.pendingConnection_=null;const t=n.getErrorCode()===H.NO_ERROR,i=n.getStatus();if(!t||this.isRetryStatusCode_(i)){const t=n.getErrorCode()===H.ABORT;return void e(!1,new W(!1,null,t))}const o=-1!==this.successCodes_.indexOf(i);e(!0,new W(o,n))}))},t=(e,t)=>{const n=this.resolve_,r=this.reject_,i=t.connection;if(t.wasSuccessCode)try{const e=this.callback_(i,i.getResponse());D(e)?n(e):n()}catch(o){r(o)}else if(null!==i){const e=d();e.serverResponse=i.getErrorText(),this.errorCallback_?r(this.errorCallback_(i,e)):r(e)}else if(t.canceled){const e=this.appDelete_?S():y();r(e)}else{const e=_();r(e)}};this.canceled_?t(!1,new W(!1,null,!0)):this.backoffId_=N(e,t,this.timeout_)}getPromise(){return this.promise_}cancel(e){this.canceled_=!0,this.appDelete_=e||!1,null!==this.backoffId_&&F(this.backoffId_),null!==this.pendingConnection_&&this.pendingConnection_.abort()}isRetryStatusCode_(e){const t=e>=500&&e<600,n=[408,429],r=-1!==n.indexOf(e),i=-1!==this.additionalRetryCodes_.indexOf(e);return t||r||i}}class W{constructor(e,t,n){this.wasSuccessCode=e,this.connection=t,this.canceled=!!n}}function G(e,t){null!==t&&t.length>0&&(e["Authorization"]="Firebase "+t)}function J(e,t){e["X-Firebase-Storage-Version"]="webjs/"+(null!==t&&void 0!==t?t:"AppManager")}function K(e,t){t&&(e["X-Firebase-GMPID"]=t)}function Y(e,t){null!==t&&(e["X-Firebase-AppCheck"]=t)}function Z(e,t,n,r,i,o){const s=B(e.urlParams),a=e.url+s,u=Object.assign({},e.headers);return K(u,t),G(u,n),J(u,o),Y(u,r),new q(a,e.method,u,e.body,e.successCodes,e.additionalRetryCodes,e.handler,e.errorHandler,e.timeout,e.progressCallback,i)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function X(){return"undefined"!==typeof BlobBuilder?BlobBuilder:"undefined"!==typeof WebKitBlobBuilder?WebKitBlobBuilder:void 0}function Q(...e){const t=X();if(void 0!==t){const n=new t;for(let t=0;t<e.length;t++)n.append(e[t]);return n.getBlob()}if($())return new Blob(e);throw new l("unsupported-environment","This browser doesn't seem to support creating Blobs")}function ee(e,t,n){return e.webkitSlice?e.webkitSlice(t,n):e.mozSlice?e.mozSlice(t,n):e.slice?e.slice(t,n):null}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function te(e){return atob(e)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ne={RAW:"raw",BASE64:"base64",BASE64URL:"base64url",DATA_URL:"data_url"};class re{constructor(e,t){this.data=e,this.contentType=t||null}}function ie(e,t){switch(e){case ne.RAW:return new re(oe(t));case ne.BASE64:case ne.BASE64URL:return new re(ae(e,t));case ne.DATA_URL:return new re(ce(t),le(t))}throw d()}function oe(e){const t=[];for(let n=0;n<e.length;n++){let r=e.charCodeAt(n);if(r<=127)t.push(r);else if(r<=2047)t.push(192|r>>6,128|63&r);else if(55296===(64512&r)){const i=n<e.length-1&&56320===(64512&e.charCodeAt(n+1));if(i){const i=r,o=e.charCodeAt(++n);r=65536|(1023&i)<<10|1023&o,t.push(240|r>>18,128|r>>12&63,128|r>>6&63,128|63&r)}else t.push(239,191,189)}else 56320===(64512&r)?t.push(239,191,189):t.push(224|r>>12,128|r>>6&63,128|63&r)}return new Uint8Array(t)}function se(e){let t;try{t=decodeURIComponent(e)}catch(n){throw A(ne.DATA_URL,"Malformed data URL.")}return oe(t)}function ae(e,t){switch(e){case ne.BASE64:{const n=-1!==t.indexOf("-"),r=-1!==t.indexOf("_");if(n||r){const t=n?"-":"_";throw A(e,"Invalid character '"+t+"' found: is it base64url encoded?")}break}case ne.BASE64URL:{const n=-1!==t.indexOf("+"),r=-1!==t.indexOf("/");if(n||r){const t=n?"+":"/";throw A(e,"Invalid character '"+t+"' found: is it base64 encoded?")}t=t.replace(/-/g,"+").replace(/_/g,"/");break}}let n;try{n=te(t)}catch(i){throw A(e,"Invalid character found")}const r=new Uint8Array(n.length);for(let o=0;o<n.length;o++)r[o]=n.charCodeAt(o);return r}class ue{constructor(e){this.base64=!1,this.contentType=null;const t=e.match(/^data:([^,]+)?,/);if(null===t)throw A(ne.DATA_URL,"Must be formatted 'data:[<mediatype>][;base64],<data>");const n=t[1]||null;null!=n&&(this.base64=he(n,";base64"),this.contentType=this.base64?n.substring(0,n.length-";base64".length):n),this.rest=e.substring(e.indexOf(",")+1)}}function ce(e){const t=new ue(e);return t.base64?ae(ne.BASE64,t.rest):se(t.rest)}function le(e){const t=new ue(e);return t.contentType}function he(e,t){const n=e.length>=t.length;return!!n&&e.substring(e.length-t.length)===t}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class de{constructor(e,t){let n=0,r="";M(e)?(this.data_=e,n=e.size,r=e.type):e instanceof ArrayBuffer?(t?this.data_=new Uint8Array(e):(this.data_=new Uint8Array(e.byteLength),this.data_.set(new Uint8Array(e))),n=this.data_.length):e instanceof Uint8Array&&(t?this.data_=e:(this.data_=new Uint8Array(e.length),this.data_.set(e)),n=e.length),this.size_=n,this.type_=r}size(){return this.size_}type(){return this.type_}slice(e,t){if(M(this.data_)){const n=this.data_,r=ee(n,e,t);return null===r?null:new de(r)}{const n=new Uint8Array(this.data_.buffer,e,t-e);return new de(n,!0)}}static getBlob(...e){if($()){const t=e.map((e=>e instanceof de?e.data_:e));return new de(Q.apply(null,t))}{const t=e.map((e=>U(e)?ie(ne.RAW,e).data:e.data_));let n=0;t.forEach((e=>{n+=e.byteLength}));const r=new Uint8Array(n);let i=0;return t.forEach((e=>{for(let t=0;t<e.length;t++)r[i++]=e[t]})),new de(r,!0)}}uploadData(){return this.data_}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function fe(e){let t;try{t=JSON.parse(e)}catch(n){return null}return L(t)?t:null}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function pe(e){if(0===e.length)return null;const t=e.lastIndexOf("/");if(-1===t)return"";const n=e.slice(0,t);return n}function me(e,t){const n=t.split("/").filter((e=>e.length>0)).join("/");return 0===e.length?n:e+"/"+n}function ve(e){const t=e.lastIndexOf("/",e.length-2);return-1===t?e:e.slice(t+1)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function ge(e,t){return t}class _e{constructor(e,t,n,r){this.server=e,this.local=t||e,this.writable=!!n,this.xform=r||ge}}let ye=null;function be(e){return!U(e)||e.length<2?e:ve(e)}function we(){if(ye)return ye;const e=[];function t(e,t){return be(t)}e.push(new _e("bucket")),e.push(new _e("generation")),e.push(new _e("metageneration")),e.push(new _e("name","fullPath",!0));const n=new _e("name");function r(e,t){return void 0!==t?Number(t):t}n.xform=t,e.push(n);const i=new _e("size");return i.xform=r,e.push(i),e.push(new _e("timeCreated")),e.push(new _e("updated")),e.push(new _e("md5Hash",null,!0)),e.push(new _e("cacheControl",null,!0)),e.push(new _e("contentDisposition",null,!0)),e.push(new _e("contentEncoding",null,!0)),e.push(new _e("contentLanguage",null,!0)),e.push(new _e("contentType",null,!0)),e.push(new _e("metadata","customMetadata",!0)),ye=e,ye}function Ie(e,t){function n(){const n=e["bucket"],r=e["fullPath"],i=new x(n,r);return t._makeStorageReference(i)}Object.defineProperty(e,"ref",{get:n})}function Ee(e,t,n){const r={type:"file"},i=n.length;for(let o=0;o<i;o++){const e=n[o];r[e.local]=e.xform(r,t[e.server])}return Ie(r,e),r}function ke(e,t,n){const r=fe(t);if(null===r)return null;const i=r;return Ee(e,i,n)}function Te(e,t,n,r){const i=fe(t);if(null===i)return null;if(!U(i["downloadTokens"]))return null;const o=i["downloadTokens"];if(0===o.length)return null;const s=encodeURIComponent,a=o.split(","),u=a.map((t=>{const i=e["bucket"],o=e["fullPath"],a="/b/"+s(i)+"/o/"+s(o),u=z(a,n,r),c=B({alt:"media",token:t});return u+c}));return u[0]}function Oe(e,t){const n={},r=t.length;for(let i=0;i<r;i++){const r=t[i];r.writable&&(n[r.server]=e[r.local])}return JSON.stringify(n)}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Se="prefixes",Re="items";function Ae(e,t,n){const r={prefixes:[],items:[],nextPageToken:n["nextPageToken"]};if(n[Se])for(const i of n[Se]){const n=i.replace(/\/$/,""),o=e._makeStorageReference(new x(t,n));r.prefixes.push(o)}if(n[Re])for(const i of n[Re]){const n=e._makeStorageReference(new x(t,i["name"]));r.items.push(n)}return r}function Ce(e,t,n){const r=fe(n);if(null===r)return null;const i=r;return Ae(e,t,i)}class xe{constructor(e,t,n,r){this.url=e,this.method=t,this.handler=n,this.timeout=r,this.urlParams={},this.headers={},this.body=null,this.errorHandler=null,this.progressCallback=null,this.successCodes=[200],this.additionalRetryCodes=[]}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function Pe(e){if(!e)throw d()}function Ne(e,t){function n(n,r){const i=ke(e,r,t);return Pe(null!==i),i}return n}function Fe(e,t){function n(n,r){const i=Ce(e,t,r);return Pe(null!==i),i}return n}function De(e,t){function n(n,r){const i=ke(e,r,t);return Pe(null!==i),Te(i,r,e.host,e._protocol)}return n}function je(e){function t(t,n){let r;return r=401===t.getStatus()?t.getErrorText().includes("Firebase App Check token is invalid")?v():m():402===t.getStatus()?p(e.bucket):403===t.getStatus()?g(e.path):n,r.serverResponse=n.serverResponse,r}return t}function Le(e){const t=je(e);function n(n,r){let i=t(n,r);return 404===n.getStatus()&&(i=f(e.path)),i.serverResponse=r.serverResponse,i}return n}function Ue(e,t,n){const r=t.fullServerUrl(),i=z(r,e.host,e._protocol),o="GET",s=e.maxOperationRetryTime,a=new xe(i,o,Ne(e,n),s);return a.errorHandler=Le(t),a}function Me(e,t,n,r,i){const o={};t.isRoot?o["prefix"]="":o["prefix"]=t.path+"/",n&&n.length>0&&(o["delimiter"]=n),r&&(o["pageToken"]=r),i&&(o["maxResults"]=i);const s=t.bucketOnlyServerUrl(),a=z(s,e.host,e._protocol),u="GET",c=e.maxOperationRetryTime,l=new xe(a,u,Fe(e,t.bucket),c);return l.urlParams=o,l.errorHandler=je(t),l}function $e(e,t,n){const r=t.fullServerUrl(),i=z(r,e.host,e._protocol),o="GET",s=e.maxOperationRetryTime,a=new xe(i,o,De(e,n),s);return a.errorHandler=Le(t),a}function Ve(e,t,n,r){const i=t.fullServerUrl(),o=z(i,e.host,e._protocol),s="PATCH",a=Oe(n,r),u={"Content-Type":"application/json; charset=utf-8"},c=e.maxOperationRetryTime,l=new xe(o,s,Ne(e,r),c);return l.headers=u,l.body=a,l.errorHandler=Le(t),l}function ze(e,t){const n=t.fullServerUrl(),r=z(n,e.host,e._protocol),i="DELETE",o=e.maxOperationRetryTime;function s(e,t){}const a=new xe(r,i,s,o);return a.successCodes=[200,204],a.errorHandler=Le(t),a}function Be(e,t){return e&&e["contentType"]||t&&t.type()||"application/octet-stream"}function He(e,t,n){const r=Object.assign({},n);return r["fullPath"]=e.path,r["size"]=t.size(),r["contentType"]||(r["contentType"]=Be(null,t)),r}function qe(e,t,n,r,i){const o=t.bucketOnlyServerUrl(),s={"X-Goog-Upload-Protocol":"multipart"};function a(){let e="";for(let t=0;t<2;t++)e+=Math.random().toString().slice(2);return e}const u=a();s["Content-Type"]="multipart/related; boundary="+u;const c=He(t,r,i),l=Oe(c,n),h="--"+u+"\r\nContent-Type: application/json; charset=utf-8\r\n\r\n"+l+"\r\n--"+u+"\r\nContent-Type: "+c["contentType"]+"\r\n\r\n",d="\r\n--"+u+"--",f=de.getBlob(h,r,d);if(null===f)throw E();const p={name:c["fullPath"]},m=z(o,e.host,e._protocol),v="POST",g=e.maxUploadRetryTime,_=new xe(m,v,Ne(e,n),g);return _.urlParams=p,_.headers=s,_.body=f.uploadData(),_.errorHandler=je(t),_}class We{constructor(e,t,n,r){this.current=e,this.total=t,this.finalized=!!n,this.metadata=r||null}}function Ge(e,t){let n=null;try{n=e.getResponseHeader("X-Goog-Upload-Status")}catch(i){Pe(!1)}const r=t||["active"];return Pe(!!n&&-1!==r.indexOf(n)),n}function Je(e,t,n,r,i){const o=t.bucketOnlyServerUrl(),s=He(t,r,i),a={name:s["fullPath"]},u=z(o,e.host,e._protocol),c="POST",l={"X-Goog-Upload-Protocol":"resumable","X-Goog-Upload-Command":"start","X-Goog-Upload-Header-Content-Length":`${r.size()}`,"X-Goog-Upload-Header-Content-Type":s["contentType"],"Content-Type":"application/json; charset=utf-8"},h=Oe(s,n),d=e.maxUploadRetryTime;function f(e){let t;Ge(e);try{t=e.getResponseHeader("X-Goog-Upload-URL")}catch(n){Pe(!1)}return Pe(U(t)),t}const p=new xe(u,c,f,d);return p.urlParams=a,p.headers=l,p.body=h,p.errorHandler=je(t),p}function Ke(e,t,n,r){const i={"X-Goog-Upload-Command":"query"};function o(e){const t=Ge(e,["active","final"]);let n=null;try{n=e.getResponseHeader("X-Goog-Upload-Size-Received")}catch(o){Pe(!1)}n||Pe(!1);const i=Number(n);return Pe(!isNaN(i)),new We(i,r.size(),"final"===t)}const s="POST",a=e.maxUploadRetryTime,u=new xe(n,s,o,a);return u.headers=i,u.errorHandler=je(t),u}const Ye=262144;function Ze(e,t,n,r,i,o,s,a){const u=new We(0,0);if(s?(u.current=s.current,u.total=s.total):(u.current=0,u.total=r.size()),r.size()!==u.total)throw k();const c=u.total-u.current;let l=c;i>0&&(l=Math.min(l,i));const h=u.current,d=h+l,f=l===c?"upload, finalize":"upload",p={"X-Goog-Upload-Command":f,"X-Goog-Upload-Offset":`${u.current}`},m=r.slice(h,d);if(null===m)throw E();function v(e,n){const i=Ge(e,["active","final"]),s=u.current+l,a=r.size();let c;return c="final"===i?Ne(t,o)(e,n):null,new We(s,a,"final"===i,c)}const g="POST",_=t.maxUploadRetryTime,y=new xe(n,g,v,_);return y.headers=p,y.body=m.uploadData(),y.progressCallback=a||null,y.errorHandler=je(e),y}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Xe={STATE_CHANGED:"state_changed"},Qe={RUNNING:"running",PAUSED:"paused",SUCCESS:"success",CANCELED:"canceled",ERROR:"error"};function et(e){switch(e){case"running":case"pausing":case"canceling":return Qe.RUNNING;case"paused":return Qe.PAUSED;case"success":return Qe.SUCCESS;case"canceled":return Qe.CANCELED;case"error":return Qe.ERROR;default:return Qe.ERROR}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class tt{constructor(e,t,n){const r=j(e)||null!=t||null!=n;if(r)this.next=e,this.error=null!==t&&void 0!==t?t:void 0,this.complete=null!==n&&void 0!==n?n:void 0;else{const t=e;this.next=t.next,this.error=t.error,this.complete=t.complete}}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function nt(e){return(...t)=>{Promise.resolve().then((()=>e(...t)))}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */let rt=null;class it{constructor(){this.sent_=!1,this.xhr_=new XMLHttpRequest,this.initXhr(),this.errorCode_=H.NO_ERROR,this.sendPromise_=new Promise((e=>{this.xhr_.addEventListener("abort",(()=>{this.errorCode_=H.ABORT,e()})),this.xhr_.addEventListener("error",(()=>{this.errorCode_=H.NETWORK_ERROR,e()})),this.xhr_.addEventListener("load",(()=>{e()}))}))}send(e,t,n,r){if(this.sent_)throw C("cannot .send() more than once");if(this.sent_=!0,this.xhr_.open(t,e,!0),void 0!==r)for(const i in r)r.hasOwnProperty(i)&&this.xhr_.setRequestHeader(i,r[i].toString());return void 0!==n?this.xhr_.send(n):this.xhr_.send(),this.sendPromise_}getErrorCode(){if(!this.sent_)throw C("cannot .getErrorCode() before sending");return this.errorCode_}getStatus(){if(!this.sent_)throw C("cannot .getStatus() before sending");try{return this.xhr_.status}catch(e){return-1}}getResponse(){if(!this.sent_)throw C("cannot .getResponse() before sending");return this.xhr_.response}getErrorText(){if(!this.sent_)throw C("cannot .getErrorText() before sending");return this.xhr_.statusText}abort(){this.xhr_.abort()}getResponseHeader(e){return this.xhr_.getResponseHeader(e)}addUploadProgressListener(e){null!=this.xhr_.upload&&this.xhr_.upload.addEventListener("progress",e)}removeUploadProgressListener(e){null!=this.xhr_.upload&&this.xhr_.upload.removeEventListener("progress",e)}}class ot extends it{initXhr(){this.xhr_.responseType="text"}}function st(){return rt?rt():new ot}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class at{constructor(e,t,n=null){this._transferred=0,this._needToFetchStatus=!1,this._needToFetchMetadata=!1,this._observers=[],this._error=void 0,this._uploadUrl=void 0,this._request=void 0,this._chunkMultiplier=1,this._resolve=void 0,this._reject=void 0,this._ref=e,this._blob=t,this._metadata=n,this._mappings=we(),this._resumable=this._shouldDoResumable(this._blob),this._state="running",this._errorHandler=e=>{this._request=void 0,this._chunkMultiplier=1,e._codeEquals("canceled")?(this._needToFetchStatus=!0,this.completeTransitions_()):(this._error=e,this._transition("error"))},this._metadataErrorHandler=e=>{this._request=void 0,e._codeEquals("canceled")?this.completeTransitions_():(this._error=e,this._transition("error"))},this._promise=new Promise(((e,t)=>{this._resolve=e,this._reject=t,this._start()})),this._promise.then(null,(()=>{}))}_makeProgressCallback(){const e=this._transferred;return t=>this._updateProgress(e+t)}_shouldDoResumable(e){return e.size()>262144}_start(){"running"===this._state&&void 0===this._request&&(this._resumable?void 0===this._uploadUrl?this._createResumable():this._needToFetchStatus?this._fetchStatus():this._needToFetchMetadata?this._fetchMetadata():this._continueUpload():this._oneShotUpload())}_resolveToken(e){Promise.all([this._ref.storage._getAuthToken(),this._ref.storage._getAppCheckToken()]).then((([t,n])=>{switch(this._state){case"running":e(t,n);break;case"canceling":this._transition("canceled");break;case"pausing":this._transition("paused");break}}))}_createResumable(){this._resolveToken(((e,t)=>{const n=Je(this._ref.storage,this._ref._location,this._mappings,this._blob,this._metadata),r=this._ref.storage._makeRequest(n,st,e,t);this._request=r,r.getPromise().then((e=>{this._request=void 0,this._uploadUrl=e,this._needToFetchStatus=!1,this.completeTransitions_()}),this._errorHandler)}))}_fetchStatus(){const e=this._uploadUrl;this._resolveToken(((t,n)=>{const r=Ke(this._ref.storage,this._ref._location,e,this._blob),i=this._ref.storage._makeRequest(r,st,t,n);this._request=i,i.getPromise().then((e=>{e=e,this._request=void 0,this._updateProgress(e.current),this._needToFetchStatus=!1,e.finalized&&(this._needToFetchMetadata=!0),this.completeTransitions_()}),this._errorHandler)}))}_continueUpload(){const e=Ye*this._chunkMultiplier,t=new We(this._transferred,this._blob.size()),n=this._uploadUrl;this._resolveToken(((r,i)=>{let o;try{o=Ze(this._ref._location,this._ref.storage,n,this._blob,e,this._mappings,t,this._makeProgressCallback())}catch(a){return this._error=a,void this._transition("error")}const s=this._ref.storage._makeRequest(o,st,r,i);this._request=s,s.getPromise().then((e=>{this._increaseMultiplier(),this._request=void 0,this._updateProgress(e.current),e.finalized?(this._metadata=e.metadata,this._transition("success")):this.completeTransitions_()}),this._errorHandler)}))}_increaseMultiplier(){const e=Ye*this._chunkMultiplier;e<33554432&&(this._chunkMultiplier*=2)}_fetchMetadata(){this._resolveToken(((e,t)=>{const n=Ue(this._ref.storage,this._ref._location,this._mappings),r=this._ref.storage._makeRequest(n,st,e,t);this._request=r,r.getPromise().then((e=>{this._request=void 0,this._metadata=e,this._transition("success")}),this._metadataErrorHandler)}))}_oneShotUpload(){this._resolveToken(((e,t)=>{const n=qe(this._ref.storage,this._ref._location,this._mappings,this._blob,this._metadata),r=this._ref.storage._makeRequest(n,st,e,t);this._request=r,r.getPromise().then((e=>{this._request=void 0,this._metadata=e,this._updateProgress(this._blob.size()),this._transition("success")}),this._errorHandler)}))}_updateProgress(e){const t=this._transferred;this._transferred=e,this._transferred!==t&&this._notifyObservers()}_transition(e){if(this._state!==e)switch(e){case"canceling":this._state=e,void 0!==this._request&&this._request.cancel();break;case"pausing":this._state=e,void 0!==this._request&&this._request.cancel();break;case"running":const t="paused"===this._state;this._state=e,t&&(this._notifyObservers(),this._start());break;case"paused":this._state=e,this._notifyObservers();break;case"canceled":this._error=y(),this._state=e,this._notifyObservers();break;case"error":this._state=e,this._notifyObservers();break;case"success":this._state=e,this._notifyObservers();break}}completeTransitions_(){switch(this._state){case"pausing":this._transition("paused");break;case"canceling":this._transition("canceled");break;case"running":this._start();break}}get snapshot(){const e=et(this._state);return{bytesTransferred:this._transferred,totalBytes:this._blob.size(),state:e,metadata:this._metadata,task:this,ref:this._ref}}on(e,t,n,r){const i=new tt(t||void 0,n||void 0,r||void 0);return this._addObserver(i),()=>{this._removeObserver(i)}}then(e,t){return this._promise.then(e,t)}catch(e){return this.then(null,e)}_addObserver(e){this._observers.push(e),this._notifyObserver(e)}_removeObserver(e){const t=this._observers.indexOf(e);-1!==t&&this._observers.splice(t,1)}_notifyObservers(){this._finishPromise();const e=this._observers.slice();e.forEach((e=>{this._notifyObserver(e)}))}_finishPromise(){if(void 0!==this._resolve){let e=!0;switch(et(this._state)){case Qe.SUCCESS:nt(this._resolve.bind(null,this.snapshot))();break;case Qe.CANCELED:case Qe.ERROR:const t=this._reject;nt(t.bind(null,this._error))();break;default:e=!1;break}e&&(this._resolve=void 0,this._reject=void 0)}}_notifyObserver(e){const t=et(this._state);switch(t){case Qe.RUNNING:case Qe.PAUSED:e.next&&nt(e.next.bind(e,this.snapshot))();break;case Qe.SUCCESS:e.complete&&nt(e.complete.bind(e))();break;case Qe.CANCELED:case Qe.ERROR:e.error&&nt(e.error.bind(e,this._error))();break;default:e.error&&nt(e.error.bind(e,this._error))()}}resume(){const e="paused"===this._state||"pausing"===this._state;return e&&this._transition("running"),e}pause(){const e="running"===this._state;return e&&this._transition("pausing"),e}cancel(){const e="running"===this._state||"pausing"===this._state;return e&&this._transition("canceling"),e}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class ut{constructor(e,t){this._service=e,this._location=t instanceof x?t:x.makeFromUrl(t,e.host)}toString(){return"gs://"+this._location.bucket+"/"+this._location.path}_newRef(e,t){return new ut(e,t)}get root(){const e=new x(this._location.bucket,"");return this._newRef(this._service,e)}get bucket(){return this._location.bucket}get fullPath(){return this._location.path}get name(){return ve(this._location.path)}get storage(){return this._service}get parent(){const e=pe(this._location.path);if(null===e)return null;const t=new x(this._location.bucket,e);return new ut(this._service,t)}_throwIfRoot(e){if(""===this._location.path)throw R(e)}}function ct(e,t,n){e._throwIfRoot("uploadBytes");const r=qe(e.storage,e._location,we(),new de(t,!0),n);return e.storage.makeRequestWithTokens(r,st).then((t=>({metadata:t,ref:e})))}function lt(e,t,n){return e._throwIfRoot("uploadBytesResumable"),new at(e,new de(t),n)}function ht(e){const t={prefixes:[],items:[]};return dt(e,t).then((()=>t))}async function dt(e,t,n){const r={pageToken:n},i=await ft(e,r);t.prefixes.push(...i.prefixes),t.items.push(...i.items),null!=i.nextPageToken&&await dt(e,t,i.nextPageToken)}function ft(e,t){null!=t&&"number"===typeof t.maxResults&&V("options.maxResults",1,1e3,t.maxResults);const n=t||{},r=Me(e.storage,e._location,"/",n.pageToken,n.maxResults);return e.storage.makeRequestWithTokens(r,st)}function pt(e){e._throwIfRoot("getMetadata");const t=Ue(e.storage,e._location,we());return e.storage.makeRequestWithTokens(t,st)}function mt(e,t){e._throwIfRoot("updateMetadata");const n=Ve(e.storage,e._location,t,we());return e.storage.makeRequestWithTokens(n,st)}function vt(e){e._throwIfRoot("getDownloadURL");const t=$e(e.storage,e._location,we());return e.storage.makeRequestWithTokens(t,st).then((e=>{if(null===e)throw T();return e}))}function gt(e){e._throwIfRoot("deleteObject");const t=ze(e.storage,e._location);return e.storage.makeRequestWithTokens(t,st)}function _t(e,t){const n=me(e._location.path,t),r=new x(e._location.bucket,n);return new ut(e.storage,r)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function yt(e){return/^[A-Za-z]+:\/\//.test(e)}function bt(e,t){return new ut(e,t)}function wt(e,t){if(e instanceof Tt){const n=e;if(null==n._bucket)throw I();const r=new ut(n,n._bucket);return null!=t?wt(r,t):r}return void 0!==t?_t(e,t):e}function It(e,t){if(t&&yt(t)){if(e instanceof Tt)return bt(e,t);throw O("To use ref(service, url), the first argument must be a Storage instance.")}return wt(e,t)}function Et(e,t){const n=null===t||void 0===t?void 0:t[a];return null==n?null:x.makeFromBucketSpec(n,e)}function kt(e,t,n,r={}){e.host=`${t}:${n}`,e._protocol="http";const{mockUserToken:o}=r;o&&(e._overrideAuthToken="string"===typeof o?o:(0,i.Sg)(o,e.app.options.projectId))}class Tt{constructor(e,t,n,r,i){this.app=e,this._authProvider=t,this._appCheckProvider=n,this._url=r,this._firebaseVersion=i,this._bucket=null,this._host=s,this._protocol="https",this._appId=null,this._deleted=!1,this._maxOperationRetryTime=u,this._maxUploadRetryTime=c,this._requests=new Set,this._bucket=null!=r?x.makeFromBucketSpec(r,this._host):Et(this._host,this.app.options)}get host(){return this._host}set host(e){this._host=e,null!=this._url?this._bucket=x.makeFromBucketSpec(this._url,e):this._bucket=Et(e,this.app.options)}get maxUploadRetryTime(){return this._maxUploadRetryTime}set maxUploadRetryTime(e){V("time",0,Number.POSITIVE_INFINITY,e),this._maxUploadRetryTime=e}get maxOperationRetryTime(){return this._maxOperationRetryTime}set maxOperationRetryTime(e){V("time",0,Number.POSITIVE_INFINITY,e),this._maxOperationRetryTime=e}async _getAuthToken(){if(this._overrideAuthToken)return this._overrideAuthToken;const e=this._authProvider.getImmediate({optional:!0});if(e){const t=await e.getToken();if(null!==t)return t.accessToken}return null}async _getAppCheckToken(){const e=this._appCheckProvider.getImmediate({optional:!0});if(e){const t=await e.getToken();return t.token}return null}_delete(){return this._deleted||(this._deleted=!0,this._requests.forEach((e=>e.cancel())),this._requests.clear()),Promise.resolve()}_makeStorageReference(e){return new ut(this,e)}_makeRequest(e,t,n,r){if(this._deleted)return new P(S());{const i=Z(e,this._appId,n,r,t,this._firebaseVersion);return this._requests.add(i),i.getPromise().then((()=>this._requests.delete(i)),(()=>this._requests.delete(i))),i}}async makeRequestWithTokens(e,t){const[n,r]=await Promise.all([this._getAuthToken(),this._getAppCheckToken()]);return this._makeRequest(e,t,n,r).getPromise()}}const Ot="@firebase/storage",St="0.9.4",Rt="storage";function At(e,t,n){return e=(0,i.m9)(e),ct(e,t,n)}function Ct(e,t,n){return e=(0,i.m9)(e),lt(e,t,n)}function xt(e){return e=(0,i.m9)(e),pt(e)}function Pt(e,t){return e=(0,i.m9)(e),mt(e,t)}function Nt(e,t){return e=(0,i.m9)(e),ft(e,t)}function Ft(e){return e=(0,i.m9)(e),ht(e)}function Dt(e){return e=(0,i.m9)(e),vt(e)}function jt(e){return e=(0,i.m9)(e),gt(e)}function Lt(e,t){return e=(0,i.m9)(e),It(e,t)}function Ut(e,t){return _t(e,t)}function Mt(e,t,n,r={}){kt(e,t,n,r)}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function $t(e,{instanceIdentifier:t}){const n=e.getProvider("app").getImmediate(),i=e.getProvider("auth-internal"),o=e.getProvider("app-check-internal");return new Tt(n,i,o,t,r.SDK_VERSION)}function Vt(){(0,r._registerComponent)(new o.wA(Rt,$t,"PUBLIC").setMultipleInstances(!0)),(0,r.registerVersion)(Ot,St,""),(0,r.registerVersion)(Ot,St,"esm2017")}Vt()},223:function(e,t,n){"use strict";n.d(t,{BH:function(){return h},L:function(){return a},LL:function(){return k},Sg:function(){return d},UG:function(){return m},X3:function(){return H},ZB:function(){return c},ZR:function(){return E},b$:function(){return _},eu:function(){return w},hl:function(){return b},jU:function(){return v},m9:function(){return U},ne:function(){return F},pd:function(){return N},r3:function(){return S},ru:function(){return g},tV:function(){return u},uI:function(){return p},vZ:function(){return A},w1:function(){return y},xO:function(){return x},xb:function(){return R},z$:function(){return f},zd:function(){return P}});n(1703),n(2801);
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const r=function(e){const t=[];let n=0;for(let r=0;r<e.length;r++){let i=e.charCodeAt(r);i<128?t[n++]=i:i<2048?(t[n++]=i>>6|192,t[n++]=63&i|128):55296===(64512&i)&&r+1<e.length&&56320===(64512&e.charCodeAt(r+1))?(i=65536+((1023&i)<<10)+(1023&e.charCodeAt(++r)),t[n++]=i>>18|240,t[n++]=i>>12&63|128,t[n++]=i>>6&63|128,t[n++]=63&i|128):(t[n++]=i>>12|224,t[n++]=i>>6&63|128,t[n++]=63&i|128)}return t},i=function(e){const t=[];let n=0,r=0;while(n<e.length){const i=e[n++];if(i<128)t[r++]=String.fromCharCode(i);else if(i>191&&i<224){const o=e[n++];t[r++]=String.fromCharCode((31&i)<<6|63&o)}else if(i>239&&i<365){const o=e[n++],s=e[n++],a=e[n++],u=((7&i)<<18|(63&o)<<12|(63&s)<<6|63&a)-65536;t[r++]=String.fromCharCode(55296+(u>>10)),t[r++]=String.fromCharCode(56320+(1023&u))}else{const o=e[n++],s=e[n++];t[r++]=String.fromCharCode((15&i)<<12|(63&o)<<6|63&s)}}return t.join("")},o={byteToCharMap_:null,charToByteMap_:null,byteToCharMapWebSafe_:null,charToByteMapWebSafe_:null,ENCODED_VALS_BASE:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",get ENCODED_VALS(){return this.ENCODED_VALS_BASE+"+/="},get ENCODED_VALS_WEBSAFE(){return this.ENCODED_VALS_BASE+"-_."},HAS_NATIVE_SUPPORT:"function"===typeof atob,encodeByteArray(e,t){if(!Array.isArray(e))throw Error("encodeByteArray takes an array as a parameter");this.init_();const n=t?this.byteToCharMapWebSafe_:this.byteToCharMap_,r=[];for(let i=0;i<e.length;i+=3){const t=e[i],o=i+1<e.length,s=o?e[i+1]:0,a=i+2<e.length,u=a?e[i+2]:0,c=t>>2,l=(3&t)<<4|s>>4;let h=(15&s)<<2|u>>6,d=63&u;a||(d=64,o||(h=64)),r.push(n[c],n[l],n[h],n[d])}return r.join("")},encodeString(e,t){return this.HAS_NATIVE_SUPPORT&&!t?btoa(e):this.encodeByteArray(r(e),t)},decodeString(e,t){return this.HAS_NATIVE_SUPPORT&&!t?atob(e):i(this.decodeStringToByteArray(e,t))},decodeStringToByteArray(e,t){this.init_();const n=t?this.charToByteMapWebSafe_:this.charToByteMap_,r=[];for(let i=0;i<e.length;){const t=n[e.charAt(i++)],o=i<e.length,s=o?n[e.charAt(i)]:0;++i;const a=i<e.length,u=a?n[e.charAt(i)]:64;++i;const c=i<e.length,l=c?n[e.charAt(i)]:64;if(++i,null==t||null==s||null==u||null==l)throw Error();const h=t<<2|s>>4;if(r.push(h),64!==u){const e=s<<4&240|u>>2;if(r.push(e),64!==l){const e=u<<6&192|l;r.push(e)}}}return r},init_(){if(!this.byteToCharMap_){this.byteToCharMap_={},this.charToByteMap_={},this.byteToCharMapWebSafe_={},this.charToByteMapWebSafe_={};for(let e=0;e<this.ENCODED_VALS.length;e++)this.byteToCharMap_[e]=this.ENCODED_VALS.charAt(e),this.charToByteMap_[this.byteToCharMap_[e]]=e,this.byteToCharMapWebSafe_[e]=this.ENCODED_VALS_WEBSAFE.charAt(e),this.charToByteMapWebSafe_[this.byteToCharMapWebSafe_[e]]=e,e>=this.ENCODED_VALS_BASE.length&&(this.charToByteMap_[this.ENCODED_VALS_WEBSAFE.charAt(e)]=e,this.charToByteMapWebSafe_[this.ENCODED_VALS.charAt(e)]=e)}}},s=function(e){const t=r(e);return o.encodeByteArray(t,!0)},a=function(e){return s(e).replace(/\./g,"")},u=function(e){try{return o.decodeString(e,!0)}catch(t){console.error("base64Decode failed: ",t)}return null};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function c(e,t){if(!(t instanceof Object))return t;switch(t.constructor){case Date:const n=t;return new Date(n.getTime());case Object:void 0===e&&(e={});break;case Array:e=[];break;default:return t}for(const n in t)t.hasOwnProperty(n)&&l(n)&&(e[n]=c(e[n],t[n]));return e}function l(e){return"__proto__"!==e}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class h{constructor(){this.reject=()=>{},this.resolve=()=>{},this.promise=new Promise(((e,t)=>{this.resolve=e,this.reject=t}))}wrapCallback(e){return(t,n)=>{t?this.reject(t):this.resolve(n),"function"===typeof e&&(this.promise.catch((()=>{})),1===e.length?e(t):e(t,n))}}}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function d(e,t){if(e.uid)throw new Error('The "uid" field is no longer supported by mockUserToken. Please use "sub" instead for Firebase Auth User ID.');const n={alg:"none",type:"JWT"},r=t||"demo-project",i=e.iat||0,o=e.sub||e.user_id;if(!o)throw new Error("mockUserToken must contain 'sub' or 'user_id' field!");const s=Object.assign({iss:`https://securetoken.google.com/${r}`,aud:r,iat:i,exp:i+3600,auth_time:i,sub:o,user_id:o,firebase:{sign_in_provider:"custom",identities:{}}},e),u="";return[a(JSON.stringify(n)),a(JSON.stringify(s)),u].join(".")}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function f(){return"undefined"!==typeof navigator&&"string"===typeof navigator["userAgent"]?navigator["userAgent"]:""}function p(){return"undefined"!==typeof window&&!!(window["cordova"]||window["phonegap"]||window["PhoneGap"])&&/ios|iphone|ipod|ipad|android|blackberry|iemobile/i.test(f())}function m(){try{return"[object process]"===Object.prototype.toString.call(n.g.process)}catch(e){return!1}}function v(){return"object"===typeof self&&self.self===self}function g(){const e="object"===typeof chrome?chrome.runtime:"object"===typeof browser?browser.runtime:void 0;return"object"===typeof e&&void 0!==e.id}function _(){return"object"===typeof navigator&&"ReactNative"===navigator["product"]}function y(){const e=f();return e.indexOf("MSIE ")>=0||e.indexOf("Trident/")>=0}function b(){return"object"===typeof indexedDB}function w(){return new Promise(((e,t)=>{try{let n=!0;const r="validate-browser-context-for-indexeddb-analytics-module",i=self.indexedDB.open(r);i.onsuccess=()=>{i.result.close(),n||self.indexedDB.deleteDatabase(r),e(!0)},i.onupgradeneeded=()=>{n=!1},i.onerror=()=>{var e;t((null===(e=i.error)||void 0===e?void 0:e.message)||"")}}catch(n){t(n)}}))}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const I="FirebaseError";class E extends Error{constructor(e,t,n){super(t),this.code=e,this.customData=n,this.name=I,Object.setPrototypeOf(this,E.prototype),Error.captureStackTrace&&Error.captureStackTrace(this,k.prototype.create)}}class k{constructor(e,t,n){this.service=e,this.serviceName=t,this.errors=n}create(e,...t){const n=t[0]||{},r=`${this.service}/${e}`,i=this.errors[e],o=i?T(i,n):"Error",s=`${this.serviceName}: ${o} (${r}).`,a=new E(r,s,n);return a}}function T(e,t){return e.replace(O,((e,n)=>{const r=t[n];return null!=r?String(r):`<${n}?>`}))}const O=/\{\$([^}]+)}/g;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function S(e,t){return Object.prototype.hasOwnProperty.call(e,t)}function R(e){for(const t in e)if(Object.prototype.hasOwnProperty.call(e,t))return!1;return!0}function A(e,t){if(e===t)return!0;const n=Object.keys(e),r=Object.keys(t);for(const i of n){if(!r.includes(i))return!1;const n=e[i],o=t[i];if(C(n)&&C(o)){if(!A(n,o))return!1}else if(n!==o)return!1}for(const i of r)if(!n.includes(i))return!1;return!0}function C(e){return null!==e&&"object"===typeof e}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function x(e){const t=[];for(const[n,r]of Object.entries(e))Array.isArray(r)?r.forEach((e=>{t.push(encodeURIComponent(n)+"="+encodeURIComponent(e))})):t.push(encodeURIComponent(n)+"="+encodeURIComponent(r));return t.length?"&"+t.join("&"):""}function P(e){const t={},n=e.replace(/^\?/,"").split("&");return n.forEach((e=>{if(e){const[n,r]=e.split("=");t[decodeURIComponent(n)]=decodeURIComponent(r)}})),t}function N(e){const t=e.indexOf("?");if(!t)return"";const n=e.indexOf("#",t);return e.substring(t,n>0?n:void 0)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function F(e,t){const n=new D(e,t);return n.subscribe.bind(n)}class D{constructor(e,t){this.observers=[],this.unsubscribes=[],this.observerCount=0,this.task=Promise.resolve(),this.finalized=!1,this.onNoObservers=t,this.task.then((()=>{e(this)})).catch((e=>{this.error(e)}))}next(e){this.forEachObserver((t=>{t.next(e)}))}error(e){this.forEachObserver((t=>{t.error(e)})),this.close(e)}complete(){this.forEachObserver((e=>{e.complete()})),this.close()}subscribe(e,t,n){let r;if(void 0===e&&void 0===t&&void 0===n)throw new Error("Missing Observer.");r=j(e,["next","error","complete"])?e:{next:e,error:t,complete:n},void 0===r.next&&(r.next=L),void 0===r.error&&(r.error=L),void 0===r.complete&&(r.complete=L);const i=this.unsubscribeOne.bind(this,this.observers.length);return this.finalized&&this.task.then((()=>{try{this.finalError?r.error(this.finalError):r.complete()}catch(e){}})),this.observers.push(r),i}unsubscribeOne(e){void 0!==this.observers&&void 0!==this.observers[e]&&(delete this.observers[e],this.observerCount-=1,0===this.observerCount&&void 0!==this.onNoObservers&&this.onNoObservers(this))}forEachObserver(e){if(!this.finalized)for(let t=0;t<this.observers.length;t++)this.sendOne(t,e)}sendOne(e,t){this.task.then((()=>{if(void 0!==this.observers&&void 0!==this.observers[e])try{t(this.observers[e])}catch(n){"undefined"!==typeof console&&console.error&&console.error(n)}}))}close(e){this.finalized||(this.finalized=!0,void 0!==e&&(this.finalError=e),this.task.then((()=>{this.observers=void 0,this.onNoObservers=void 0})))}}function j(e,t){if("object"!==typeof e||null===e)return!1;for(const n of t)if(n in e&&"function"===typeof e[n])return!0;return!1}function L(){}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function U(e){return e&&e._delegate?e._delegate:e}
/**
 * @license
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function M(e,t){return new Promise(((n,r)=>{e.onsuccess=e=>{n(e.target.result)},e.onerror=e=>{var n;r(`${t}: ${null===(n=e.target.error)||void 0===n?void 0:n.message}`)}}))}class ${constructor(e){this._db=e,this.objectStoreNames=this._db.objectStoreNames}transaction(e,t){return new V(this._db.transaction.call(this._db,e,t))}createObjectStore(e,t){return new z(this._db.createObjectStore(e,t))}close(){this._db.close()}}class V{constructor(e){this._transaction=e,this.complete=new Promise(((e,t)=>{this._transaction.oncomplete=function(){e()},this._transaction.onerror=()=>{t(this._transaction.error)},this._transaction.onabort=()=>{t(this._transaction.error)}}))}objectStore(e){return new z(this._transaction.objectStore(e))}}class z{constructor(e){this._store=e}index(e){return new B(this._store.index(e))}createIndex(e,t,n){return new B(this._store.createIndex(e,t,n))}get(e){const t=this._store.get(e);return M(t,"Error reading from IndexedDB")}put(e,t){const n=this._store.put(e,t);return M(n,"Error writing to IndexedDB")}delete(e){const t=this._store.delete(e);return M(t,"Error deleting from IndexedDB")}clear(){const e=this._store.clear();return M(e,"Error clearing IndexedDB object store")}}class B{constructor(e){this._index=e}get(e){const t=this._index.get(e);return M(t,"Error reading from IndexedDB")}}function H(e,t,n){return new Promise(((r,i)=>{try{const o=indexedDB.open(e,t);o.onsuccess=e=>{r(new $(e.target.result))},o.onerror=e=>{var t;i(`Error opening indexedDB: ${null===(t=e.target.error)||void 0===t?void 0:t.message}`)},o.onupgradeneeded=e=>{n(new $(o.result),e.oldVersion,e.newVersion,new V(o.transaction))}}catch(o){i(`Error opening indexedDB: ${o.message}`)}}))}},4870:function(e,t,n){"use strict";n.d(t,{Bj:function(){return o},Fl:function(){return ze},IU:function(){return Se},Jd:function(){return E},PG:function(){return Ee},SU:function(){return Ue},Um:function(){return be},WL:function(){return $e},X$:function(){return S},X3:function(){return Oe},XI:function(){return De},Xl:function(){return Re},dq:function(){return Ne},iH:function(){return Fe},j:function(){return T},lk:function(){return k},qj:function(){return ye},qq:function(){return y},yT:function(){return Te}});var r=n(7139);let i;class o{constructor(e=!1){this.active=!0,this.effects=[],this.cleanups=[],!e&&i&&(this.parent=i,this.index=(i.scopes||(i.scopes=[])).push(this)-1)}run(e){if(this.active)try{return i=this,e()}finally{i=this.parent}else 0}on(){i=this}off(){i=this.parent}stop(e){if(this.active){let t,n;for(t=0,n=this.effects.length;t<n;t++)this.effects[t].stop();for(t=0,n=this.cleanups.length;t<n;t++)this.cleanups[t]();if(this.scopes)for(t=0,n=this.scopes.length;t<n;t++)this.scopes[t].stop(!0);if(this.parent&&!e){const e=this.parent.scopes.pop();e&&e!==this&&(this.parent.scopes[this.index]=e,e.index=this.index)}this.active=!1}}}function s(e,t=i){t&&t.active&&t.effects.push(e)}const a=e=>{const t=new Set(e);return t.w=0,t.n=0,t},u=e=>(e.w&p)>0,c=e=>(e.n&p)>0,l=({deps:e})=>{if(e.length)for(let t=0;t<e.length;t++)e[t].w|=p},h=e=>{const{deps:t}=e;if(t.length){let n=0;for(let r=0;r<t.length;r++){const i=t[r];u(i)&&!c(i)?i.delete(e):t[n++]=i,i.w&=~p,i.n&=~p}t.length=n}},d=new WeakMap;let f=0,p=1;const m=30;let v;const g=Symbol(""),_=Symbol("");class y{constructor(e,t=null,n){this.fn=e,this.scheduler=t,this.active=!0,this.deps=[],this.parent=void 0,s(this,n)}run(){if(!this.active)return this.fn();let e=v,t=w;while(e){if(e===this)return;e=e.parent}try{return this.parent=v,v=this,w=!0,p=1<<++f,f<=m?l(this):b(this),this.fn()}finally{f<=m&&h(this),p=1<<--f,v=this.parent,w=t,this.parent=void 0}}stop(){this.active&&(b(this),this.onStop&&this.onStop(),this.active=!1)}}function b(e){const{deps:t}=e;if(t.length){for(let n=0;n<t.length;n++)t[n].delete(e);t.length=0}}let w=!0;const I=[];function E(){I.push(w),w=!1}function k(){const e=I.pop();w=void 0===e||e}function T(e,t,n){if(w&&v){let t=d.get(e);t||d.set(e,t=new Map);let r=t.get(n);r||t.set(n,r=a());const i=void 0;O(r,i)}}function O(e,t){let n=!1;f<=m?c(e)||(e.n|=p,n=!u(e)):n=!e.has(v),n&&(e.add(v),v.deps.push(e))}function S(e,t,n,i,o,s){const u=d.get(e);if(!u)return;let c=[];if("clear"===t)c=[...u.values()];else if("length"===n&&(0,r.kJ)(e))u.forEach(((e,t)=>{("length"===t||t>=i)&&c.push(e)}));else switch(void 0!==n&&c.push(u.get(n)),t){case"add":(0,r.kJ)(e)?(0,r.S0)(n)&&c.push(u.get("length")):(c.push(u.get(g)),(0,r._N)(e)&&c.push(u.get(_)));break;case"delete":(0,r.kJ)(e)||(c.push(u.get(g)),(0,r._N)(e)&&c.push(u.get(_)));break;case"set":(0,r._N)(e)&&c.push(u.get(g));break}if(1===c.length)c[0]&&R(c[0]);else{const e=[];for(const t of c)t&&e.push(...t);R(a(e))}}function R(e,t){for(const n of(0,r.kJ)(e)?e:[...e])(n!==v||n.allowRecurse)&&(n.scheduler?n.scheduler():n.run())}const A=(0,r.fY)("__proto__,__v_isRef,__isVue"),C=new Set(Object.getOwnPropertyNames(Symbol).map((e=>Symbol[e])).filter(r.yk)),x=j(),P=j(!1,!0),N=j(!0),F=D();function D(){const e={};return["includes","indexOf","lastIndexOf"].forEach((t=>{e[t]=function(...e){const n=Se(this);for(let t=0,i=this.length;t<i;t++)T(n,"get",t+"");const r=n[t](...e);return-1===r||!1===r?n[t](...e.map(Se)):r}})),["push","pop","shift","unshift","splice"].forEach((t=>{e[t]=function(...e){E();const n=Se(this)[t].apply(this,e);return k(),n}})),e}function j(e=!1,t=!1){return function(n,i,o){if("__v_isReactive"===i)return!e;if("__v_isReadonly"===i)return e;if("__v_isShallow"===i)return t;if("__v_raw"===i&&o===(e?t?ve:me:t?pe:fe).get(n))return n;const s=(0,r.kJ)(n);if(!e&&s&&(0,r.RI)(F,i))return Reflect.get(F,i,o);const a=Reflect.get(n,i,o);if((0,r.yk)(i)?C.has(i):A(i))return a;if(e||T(n,"get",i),t)return a;if(Ne(a)){const e=!s||!(0,r.S0)(i);return e?a.value:a}return(0,r.Kn)(a)?e?we(a):ye(a):a}}const L=M(),U=M(!0);function M(e=!1){return function(t,n,i,o){let s=t[n];if(ke(s)&&Ne(s)&&!Ne(i))return!1;if(!e&&!ke(i)&&(Te(i)||(i=Se(i),s=Se(s)),!(0,r.kJ)(t)&&Ne(s)&&!Ne(i)))return s.value=i,!0;const a=(0,r.kJ)(t)&&(0,r.S0)(n)?Number(n)<t.length:(0,r.RI)(t,n),u=Reflect.set(t,n,i,o);return t===Se(o)&&(a?(0,r.aU)(i,s)&&S(t,"set",n,i,s):S(t,"add",n,i)),u}}function $(e,t){const n=(0,r.RI)(e,t),i=e[t],o=Reflect.deleteProperty(e,t);return o&&n&&S(e,"delete",t,void 0,i),o}function V(e,t){const n=Reflect.has(e,t);return(0,r.yk)(t)&&C.has(t)||T(e,"has",t),n}function z(e){return T(e,"iterate",(0,r.kJ)(e)?"length":g),Reflect.ownKeys(e)}const B={get:x,set:L,deleteProperty:$,has:V,ownKeys:z},H={get:N,set(e,t){return!0},deleteProperty(e,t){return!0}},q=(0,r.l7)({},B,{get:P,set:U}),W=e=>e,G=e=>Reflect.getPrototypeOf(e);function J(e,t,n=!1,r=!1){e=e["__v_raw"];const i=Se(e),o=Se(t);t!==o&&!n&&T(i,"get",t),!n&&T(i,"get",o);const{has:s}=G(i),a=r?W:n?Ce:Ae;return s.call(i,t)?a(e.get(t)):s.call(i,o)?a(e.get(o)):void(e!==i&&e.get(t))}function K(e,t=!1){const n=this["__v_raw"],r=Se(n),i=Se(e);return e!==i&&!t&&T(r,"has",e),!t&&T(r,"has",i),e===i?n.has(e):n.has(e)||n.has(i)}function Y(e,t=!1){return e=e["__v_raw"],!t&&T(Se(e),"iterate",g),Reflect.get(e,"size",e)}function Z(e){e=Se(e);const t=Se(this),n=G(t),r=n.has.call(t,e);return r||(t.add(e),S(t,"add",e,e)),this}function X(e,t){t=Se(t);const n=Se(this),{has:i,get:o}=G(n);let s=i.call(n,e);s||(e=Se(e),s=i.call(n,e));const a=o.call(n,e);return n.set(e,t),s?(0,r.aU)(t,a)&&S(n,"set",e,t,a):S(n,"add",e,t),this}function Q(e){const t=Se(this),{has:n,get:r}=G(t);let i=n.call(t,e);i||(e=Se(e),i=n.call(t,e));const o=r?r.call(t,e):void 0,s=t.delete(e);return i&&S(t,"delete",e,void 0,o),s}function ee(){const e=Se(this),t=0!==e.size,n=void 0,r=e.clear();return t&&S(e,"clear",void 0,void 0,n),r}function te(e,t){return function(n,r){const i=this,o=i["__v_raw"],s=Se(o),a=t?W:e?Ce:Ae;return!e&&T(s,"iterate",g),o.forEach(((e,t)=>n.call(r,a(e),a(t),i)))}}function ne(e,t,n){return function(...i){const o=this["__v_raw"],s=Se(o),a=(0,r._N)(s),u="entries"===e||e===Symbol.iterator&&a,c="keys"===e&&a,l=o[e](...i),h=n?W:t?Ce:Ae;return!t&&T(s,"iterate",c?_:g),{next(){const{value:e,done:t}=l.next();return t?{value:e,done:t}:{value:u?[h(e[0]),h(e[1])]:h(e),done:t}},[Symbol.iterator](){return this}}}}function re(e){return function(...t){return"delete"!==e&&this}}function ie(){const e={get(e){return J(this,e)},get size(){return Y(this)},has:K,add:Z,set:X,delete:Q,clear:ee,forEach:te(!1,!1)},t={get(e){return J(this,e,!1,!0)},get size(){return Y(this)},has:K,add:Z,set:X,delete:Q,clear:ee,forEach:te(!1,!0)},n={get(e){return J(this,e,!0)},get size(){return Y(this,!0)},has(e){return K.call(this,e,!0)},add:re("add"),set:re("set"),delete:re("delete"),clear:re("clear"),forEach:te(!0,!1)},r={get(e){return J(this,e,!0,!0)},get size(){return Y(this,!0)},has(e){return K.call(this,e,!0)},add:re("add"),set:re("set"),delete:re("delete"),clear:re("clear"),forEach:te(!0,!0)},i=["keys","values","entries",Symbol.iterator];return i.forEach((i=>{e[i]=ne(i,!1,!1),n[i]=ne(i,!0,!1),t[i]=ne(i,!1,!0),r[i]=ne(i,!0,!0)})),[e,n,t,r]}const[oe,se,ae,ue]=ie();function ce(e,t){const n=t?e?ue:ae:e?se:oe;return(t,i,o)=>"__v_isReactive"===i?!e:"__v_isReadonly"===i?e:"__v_raw"===i?t:Reflect.get((0,r.RI)(n,i)&&i in t?n:t,i,o)}const le={get:ce(!1,!1)},he={get:ce(!1,!0)},de={get:ce(!0,!1)};const fe=new WeakMap,pe=new WeakMap,me=new WeakMap,ve=new WeakMap;function ge(e){switch(e){case"Object":case"Array":return 1;case"Map":case"Set":case"WeakMap":case"WeakSet":return 2;default:return 0}}function _e(e){return e["__v_skip"]||!Object.isExtensible(e)?0:ge((0,r.W7)(e))}function ye(e){return ke(e)?e:Ie(e,!1,B,le,fe)}function be(e){return Ie(e,!1,q,he,pe)}function we(e){return Ie(e,!0,H,de,me)}function Ie(e,t,n,i,o){if(!(0,r.Kn)(e))return e;if(e["__v_raw"]&&(!t||!e["__v_isReactive"]))return e;const s=o.get(e);if(s)return s;const a=_e(e);if(0===a)return e;const u=new Proxy(e,2===a?i:n);return o.set(e,u),u}function Ee(e){return ke(e)?Ee(e["__v_raw"]):!(!e||!e["__v_isReactive"])}function ke(e){return!(!e||!e["__v_isReadonly"])}function Te(e){return!(!e||!e["__v_isShallow"])}function Oe(e){return Ee(e)||ke(e)}function Se(e){const t=e&&e["__v_raw"];return t?Se(t):e}function Re(e){return(0,r.Nj)(e,"__v_skip",!0),e}const Ae=e=>(0,r.Kn)(e)?ye(e):e,Ce=e=>(0,r.Kn)(e)?we(e):e;function xe(e){w&&v&&(e=Se(e),O(e.dep||(e.dep=a())))}function Pe(e,t){e=Se(e),e.dep&&R(e.dep)}function Ne(e){return!(!e||!0!==e.__v_isRef)}function Fe(e){return je(e,!1)}function De(e){return je(e,!0)}function je(e,t){return Ne(e)?e:new Le(e,t)}class Le{constructor(e,t){this.__v_isShallow=t,this.dep=void 0,this.__v_isRef=!0,this._rawValue=t?e:Se(e),this._value=t?e:Ae(e)}get value(){return xe(this),this._value}set value(e){e=this.__v_isShallow?e:Se(e),(0,r.aU)(e,this._rawValue)&&(this._rawValue=e,this._value=this.__v_isShallow?e:Ae(e),Pe(this,e))}}function Ue(e){return Ne(e)?e.value:e}const Me={get:(e,t,n)=>Ue(Reflect.get(e,t,n)),set:(e,t,n,r)=>{const i=e[t];return Ne(i)&&!Ne(n)?(i.value=n,!0):Reflect.set(e,t,n,r)}};function $e(e){return Ee(e)?e:new Proxy(e,Me)}class Ve{constructor(e,t,n,r){this._setter=t,this.dep=void 0,this.__v_isRef=!0,this._dirty=!0,this.effect=new y(e,(()=>{this._dirty||(this._dirty=!0,Pe(this))})),this.effect.computed=this,this.effect.active=this._cacheable=!r,this["__v_isReadonly"]=n}get value(){const e=Se(this);return xe(e),!e._dirty&&e._cacheable||(e._dirty=!1,e._value=e.effect.run()),e._value}set value(e){this._setter(e)}}function ze(e,t,n=!1){let i,o;const s=(0,r.mf)(e);s?(i=e,o=r.dG):(i=e.get,o=e.set);const a=new Ve(i,o,s||!o,n);return a}Promise.resolve()},3396:function(e,t,n){"use strict";n.d(t,{$d:function(){return s},Cn:function(){return V},FN:function(){return _n},Fl:function(){return Fn},HY:function(){return Pt},JJ:function(){return Z},P$:function(){return ue},Q6:function(){return pe},U2:function(){return le},Uk:function(){return tn},Us:function(){return wt},WI:function(){return cn},Wm:function(){return Zt},Y3:function(){return I},Y8:function(){return oe},YP:function(){return ee},_:function(){return Yt},aZ:function(){return me},bv:function(){return Re},dD:function(){return $},f3:function(){return X},h:function(){return Dn},iD:function(){return Bt},ic:function(){return Ce},j4:function(){return Ht},kq:function(){return nn},nK:function(){return fe},up:function(){return Rt},w5:function(){return z},wg:function(){return Ut},wy:function(){return ft}});n(1703);var r=n(4870),i=n(7139);function o(e,t,n,r){let i;try{i=r?e(...r):e()}catch(o){a(o,t,n)}return i}function s(e,t,n,r){if((0,i.mf)(e)){const s=o(e,t,n,r);return s&&(0,i.tI)(s)&&s.catch((e=>{a(e,t,n)})),s}const u=[];for(let i=0;i<e.length;i++)u.push(s(e[i],t,n,r));return u}function a(e,t,n,r=!0){const i=t?t.vnode:null;if(t){let r=t.parent;const i=t.proxy,s=n;while(r){const t=r.ec;if(t)for(let n=0;n<t.length;n++)if(!1===t[n](e,i,s))return;r=r.parent}const a=t.appContext.config.errorHandler;if(a)return void o(a,null,10,[e,i,s])}u(e,n,i,r)}function u(e,t,n,r=!0){console.error(e)}let c=!1,l=!1;const h=[];let d=0;const f=[];let p=null,m=0;const v=[];let g=null,_=0;const y=Promise.resolve();let b=null,w=null;function I(e){const t=b||y;return e?t.then(this?e.bind(this):e):t}function E(e){let t=d+1,n=h.length;while(t<n){const r=t+n>>>1,i=P(h[r]);i<e?t=r+1:n=r}return t}function k(e){h.length&&h.includes(e,c&&e.allowRecurse?d+1:d)||e===w||(null==e.id?h.push(e):h.splice(E(e.id),0,e),T())}function T(){c||l||(l=!0,b=y.then(N))}function O(e){const t=h.indexOf(e);t>d&&h.splice(t,1)}function S(e,t,n,r){(0,i.kJ)(e)?n.push(...e):t&&t.includes(e,e.allowRecurse?r+1:r)||n.push(e),T()}function R(e){S(e,p,f,m)}function A(e){S(e,g,v,_)}function C(e,t=null){if(f.length){for(w=t,p=[...new Set(f)],f.length=0,m=0;m<p.length;m++)p[m]();p=null,m=0,w=null,C(e,t)}}function x(e){if(v.length){const e=[...new Set(v)];if(v.length=0,g)return void g.push(...e);for(g=e,g.sort(((e,t)=>P(e)-P(t))),_=0;_<g.length;_++)g[_]();g=null,_=0}}const P=e=>null==e.id?1/0:e.id;function N(e){l=!1,c=!0,C(e),h.sort(((e,t)=>P(e)-P(t)));i.dG;try{for(d=0;d<h.length;d++){const e=h[d];e&&!1!==e.active&&o(e,null,14)}}finally{d=0,h.length=0,x(e),c=!1,b=null,(h.length||f.length||v.length)&&N(e)}}new Set;new Map;function F(e,t,...n){const r=e.vnode.props||i.kT;let o=n;const a=t.startsWith("update:"),u=a&&t.slice(7);if(u&&u in r){const e=`${"modelValue"===u?"model":u}Modifiers`,{number:t,trim:s}=r[e]||i.kT;s?o=n.map((e=>e.trim())):t&&(o=n.map(i.He))}let c;let l=r[c=(0,i.hR)(t)]||r[c=(0,i.hR)((0,i._A)(t))];!l&&a&&(l=r[c=(0,i.hR)((0,i.rs)(t))]),l&&s(l,e,6,o);const h=r[c+"Once"];if(h){if(e.emitted){if(e.emitted[c])return}else e.emitted={};e.emitted[c]=!0,s(h,e,6,o)}}function D(e,t,n=!1){const r=t.emitsCache,o=r.get(e);if(void 0!==o)return o;const s=e.emits;let a={},u=!1;if(!(0,i.mf)(e)){const r=e=>{const n=D(e,t,!0);n&&(u=!0,(0,i.l7)(a,n))};!n&&t.mixins.length&&t.mixins.forEach(r),e.extends&&r(e.extends),e.mixins&&e.mixins.forEach(r)}return s||u?((0,i.kJ)(s)?s.forEach((e=>a[e]=null)):(0,i.l7)(a,s),r.set(e,a),a):(r.set(e,null),null)}function j(e,t){return!(!e||!(0,i.F7)(t))&&(t=t.slice(2).replace(/Once$/,""),(0,i.RI)(e,t[0].toLowerCase()+t.slice(1))||(0,i.RI)(e,(0,i.rs)(t))||(0,i.RI)(e,t))}let L=null,U=null;function M(e){const t=L;return L=e,U=e&&e.type.__scopeId||null,t}function $(e){U=e}function V(){U=null}function z(e,t=L,n){if(!t)return e;if(e._n)return e;const r=(...n)=>{r._d&&Vt(-1);const i=M(t),o=e(...n);return M(i),r._d&&Vt(1),o};return r._n=!0,r._c=!0,r._d=!0,r}function B(e){const{type:t,vnode:n,proxy:r,withProxy:o,props:s,propsOptions:[u],slots:c,attrs:l,emit:h,render:d,renderCache:f,data:p,setupState:m,ctx:v,inheritAttrs:g}=e;let _,y;const b=M(e);try{if(4&n.shapeFlag){const e=o||r;_=rn(d.call(e,e,f,s,m,p,v)),y=l}else{const e=t;0,_=rn(e.length>1?e(s,{attrs:l,slots:c,emit:h}):e(s,null)),y=t.props?l:H(l)}}catch(I){jt.length=0,a(I,e,1),_=Zt(Ft)}let w=_;if(y&&!1!==g){const e=Object.keys(y),{shapeFlag:t}=w;e.length&&7&t&&(u&&e.some(i.tR)&&(y=q(y,u)),w=en(w,y))}return n.dirs&&(w.dirs=w.dirs?w.dirs.concat(n.dirs):n.dirs),n.transition&&(w.transition=n.transition),_=w,M(b),_}const H=e=>{let t;for(const n in e)("class"===n||"style"===n||(0,i.F7)(n))&&((t||(t={}))[n]=e[n]);return t},q=(e,t)=>{const n={};for(const r in e)(0,i.tR)(r)&&r.slice(9)in t||(n[r]=e[r]);return n};function W(e,t,n){const{props:r,children:i,component:o}=e,{props:s,children:a,patchFlag:u}=t,c=o.emitsOptions;if(t.dirs||t.transition)return!0;if(!(n&&u>=0))return!(!i&&!a||a&&a.$stable)||r!==s&&(r?!s||G(r,s,c):!!s);if(1024&u)return!0;if(16&u)return r?G(r,s,c):!!s;if(8&u){const e=t.dynamicProps;for(let t=0;t<e.length;t++){const n=e[t];if(s[n]!==r[n]&&!j(c,n))return!0}}return!1}function G(e,t,n){const r=Object.keys(t);if(r.length!==Object.keys(e).length)return!0;for(let i=0;i<r.length;i++){const o=r[i];if(t[o]!==e[o]&&!j(n,o))return!0}return!1}function J({vnode:e,parent:t},n){while(t&&t.subTree===e)(e=t.vnode).el=n,t=t.parent}const K=e=>e.__isSuspense;function Y(e,t){t&&t.pendingBranch?(0,i.kJ)(e)?t.effects.push(...e):t.effects.push(e):A(e)}function Z(e,t){if(gn){let n=gn.provides;const r=gn.parent&&gn.parent.provides;r===n&&(n=gn.provides=Object.create(r)),n[e]=t}else 0}function X(e,t,n=!1){const r=gn||L;if(r){const o=null==r.parent?r.vnode.appContext&&r.vnode.appContext.provides:r.parent.provides;if(o&&e in o)return o[e];if(arguments.length>1)return n&&(0,i.mf)(t)?t.call(r.proxy):t}else 0}const Q={};function ee(e,t,n){return te(e,t,n)}function te(e,t,{immediate:n,deep:a,flush:u,onTrack:c,onTrigger:l}=i.kT){const h=gn;let d,f,p=!1,m=!1;if((0,r.dq)(e)?(d=()=>e.value,p=(0,r.yT)(e)):(0,r.PG)(e)?(d=()=>e,a=!0):(0,i.kJ)(e)?(m=!0,p=e.some(r.PG),d=()=>e.map((e=>(0,r.dq)(e)?e.value:(0,r.PG)(e)?ie(e):(0,i.mf)(e)?o(e,h,2):void 0))):d=(0,i.mf)(e)?t?()=>o(e,h,2):()=>{if(!h||!h.isUnmounted)return f&&f(),s(e,h,3,[v])}:i.dG,t&&a){const e=d;d=()=>ie(e())}let v=e=>{f=b.onStop=()=>{o(e,h,4)}};if(kn)return v=i.dG,t?n&&s(t,h,3,[d(),m?[]:void 0,v]):d(),i.dG;let g=m?[]:Q;const _=()=>{if(b.active)if(t){const e=b.run();(a||p||(m?e.some(((e,t)=>(0,i.aU)(e,g[t]))):(0,i.aU)(e,g)))&&(f&&f(),s(t,h,3,[e,g===Q?void 0:g,v]),g=e)}else b.run()};let y;_.allowRecurse=!!t,y="sync"===u?_:"post"===u?()=>bt(_,h&&h.suspense):()=>{!h||h.isMounted?R(_):_()};const b=new r.qq(d,y);return t?n?_():g=b.run():"post"===u?bt(b.run.bind(b),h&&h.suspense):b.run(),()=>{b.stop(),h&&h.scope&&(0,i.Od)(h.scope.effects,b)}}function ne(e,t,n){const r=this.proxy,o=(0,i.HD)(e)?e.includes(".")?re(r,e):()=>r[e]:e.bind(r,r);let s;(0,i.mf)(t)?s=t:(s=t.handler,n=t);const a=gn;yn(this);const u=te(o,s.bind(r),n);return a?yn(a):bn(),u}function re(e,t){const n=t.split(".");return()=>{let t=e;for(let e=0;e<n.length&&t;e++)t=t[n[e]];return t}}function ie(e,t){if(!(0,i.Kn)(e)||e["__v_skip"])return e;if(t=t||new Set,t.has(e))return e;if(t.add(e),(0,r.dq)(e))ie(e.value,t);else if((0,i.kJ)(e))for(let n=0;n<e.length;n++)ie(e[n],t);else if((0,i.DM)(e)||(0,i._N)(e))e.forEach((e=>{ie(e,t)}));else if((0,i.PO)(e))for(const n in e)ie(e[n],t);return e}function oe(){const e={isMounted:!1,isLeaving:!1,isUnmounting:!1,leavingVNodes:new Map};return Re((()=>{e.isMounted=!0})),xe((()=>{e.isUnmounting=!0})),e}const se=[Function,Array],ae={name:"BaseTransition",props:{mode:String,appear:Boolean,persisted:Boolean,onBeforeEnter:se,onEnter:se,onAfterEnter:se,onEnterCancelled:se,onBeforeLeave:se,onLeave:se,onAfterLeave:se,onLeaveCancelled:se,onBeforeAppear:se,onAppear:se,onAfterAppear:se,onAppearCancelled:se},setup(e,{slots:t}){const n=_n(),i=oe();let o;return()=>{const s=t.default&&pe(t.default(),!0);if(!s||!s.length)return;const a=(0,r.IU)(e),{mode:u}=a;const c=s[0];if(i.isLeaving)return he(c);const l=de(c);if(!l)return he(c);const h=le(l,a,i,n);fe(l,h);const d=n.subTree,f=d&&de(d);let p=!1;const{getTransitionKey:m}=l.type;if(m){const e=m();void 0===o?o=e:e!==o&&(o=e,p=!0)}if(f&&f.type!==Ft&&(!Wt(l,f)||p)){const e=le(f,a,i,n);if(fe(f,e),"out-in"===u)return i.isLeaving=!0,e.afterLeave=()=>{i.isLeaving=!1,n.update()},he(c);"in-out"===u&&l.type!==Ft&&(e.delayLeave=(e,t,n)=>{const r=ce(i,f);r[String(f.key)]=f,e._leaveCb=()=>{t(),e._leaveCb=void 0,delete h.delayedLeave},h.delayedLeave=n})}return c}}},ue=ae;function ce(e,t){const{leavingVNodes:n}=e;let r=n.get(t.type);return r||(r=Object.create(null),n.set(t.type,r)),r}function le(e,t,n,r){const{appear:i,mode:o,persisted:a=!1,onBeforeEnter:u,onEnter:c,onAfterEnter:l,onEnterCancelled:h,onBeforeLeave:d,onLeave:f,onAfterLeave:p,onLeaveCancelled:m,onBeforeAppear:v,onAppear:g,onAfterAppear:_,onAppearCancelled:y}=t,b=String(e.key),w=ce(n,e),I=(e,t)=>{e&&s(e,r,9,t)},E={mode:o,persisted:a,beforeEnter(t){let r=u;if(!n.isMounted){if(!i)return;r=v||u}t._leaveCb&&t._leaveCb(!0);const o=w[b];o&&Wt(e,o)&&o.el._leaveCb&&o.el._leaveCb(),I(r,[t])},enter(e){let t=c,r=l,o=h;if(!n.isMounted){if(!i)return;t=g||c,r=_||l,o=y||h}let s=!1;const a=e._enterCb=t=>{s||(s=!0,I(t?o:r,[e]),E.delayedLeave&&E.delayedLeave(),e._enterCb=void 0)};t?(t(e,a),t.length<=1&&a()):a()},leave(t,r){const i=String(e.key);if(t._enterCb&&t._enterCb(!0),n.isUnmounting)return r();I(d,[t]);let o=!1;const s=t._leaveCb=n=>{o||(o=!0,r(),I(n?m:p,[t]),t._leaveCb=void 0,w[i]===e&&delete w[i])};w[i]=e,f?(f(t,s),f.length<=1&&s()):s()},clone(e){return le(e,t,n,r)}};return E}function he(e){if(ge(e))return e=en(e),e.children=null,e}function de(e){return ge(e)?e.children?e.children[0]:void 0:e}function fe(e,t){6&e.shapeFlag&&e.component?fe(e.component.subTree,t):128&e.shapeFlag?(e.ssContent.transition=t.clone(e.ssContent),e.ssFallback.transition=t.clone(e.ssFallback)):e.transition=t}function pe(e,t=!1){let n=[],r=0;for(let i=0;i<e.length;i++){const o=e[i];o.type===Pt?(128&o.patchFlag&&r++,n=n.concat(pe(o.children,t))):(t||o.type!==Ft)&&n.push(o)}if(r>1)for(let i=0;i<n.length;i++)n[i].patchFlag=-2;return n}function me(e){return(0,i.mf)(e)?{setup:e,name:e.name}:e}const ve=e=>!!e.type.__asyncLoader;const ge=e=>e.type.__isKeepAlive;RegExp,RegExp;function _e(e,t){return(0,i.kJ)(e)?e.some((e=>_e(e,t))):(0,i.HD)(e)?e.split(",").includes(t):!!e.test&&e.test(t)}function ye(e,t){we(e,"a",t)}function be(e,t){we(e,"da",t)}function we(e,t,n=gn){const r=e.__wdc||(e.__wdc=()=>{let t=n;while(t){if(t.isDeactivated)return;t=t.parent}return e()});if(Te(t,r,n),n){let e=n.parent;while(e&&e.parent)ge(e.parent.vnode)&&Ie(r,t,n,e),e=e.parent}}function Ie(e,t,n,r){const o=Te(t,e,r,!0);Pe((()=>{(0,i.Od)(r[t],o)}),n)}function Ee(e){let t=e.shapeFlag;256&t&&(t-=256),512&t&&(t-=512),e.shapeFlag=t}function ke(e){return 128&e.shapeFlag?e.ssContent:e}function Te(e,t,n=gn,i=!1){if(n){const o=n[e]||(n[e]=[]),a=t.__weh||(t.__weh=(...i)=>{if(n.isUnmounted)return;(0,r.Jd)(),yn(n);const o=s(t,n,e,i);return bn(),(0,r.lk)(),o});return i?o.unshift(a):o.push(a),a}}const Oe=e=>(t,n=gn)=>(!kn||"sp"===e)&&Te(e,t,n),Se=Oe("bm"),Re=Oe("m"),Ae=Oe("bu"),Ce=Oe("u"),xe=Oe("bum"),Pe=Oe("um"),Ne=Oe("sp"),Fe=Oe("rtg"),De=Oe("rtc");function je(e,t=gn){Te("ec",e,t)}let Le=!0;function Ue(e){const t=ze(e),n=e.proxy,o=e.ctx;Le=!1,t.beforeCreate&&$e(t.beforeCreate,e,"bc");const{data:s,computed:a,methods:u,watch:c,provide:l,inject:h,created:d,beforeMount:f,mounted:p,beforeUpdate:m,updated:v,activated:g,deactivated:_,beforeDestroy:y,beforeUnmount:b,destroyed:w,unmounted:I,render:E,renderTracked:k,renderTriggered:T,errorCaptured:O,serverPrefetch:S,expose:R,inheritAttrs:A,components:C,directives:x,filters:P}=t,N=null;if(h&&Me(h,o,N,e.appContext.config.unwrapInjectedRef),u)for(const r in u){const e=u[r];(0,i.mf)(e)&&(o[r]=e.bind(n))}if(s){0;const t=s.call(n,n);0,(0,i.Kn)(t)&&(e.data=(0,r.qj)(t))}if(Le=!0,a)for(const r in a){const e=a[r],t=(0,i.mf)(e)?e.bind(n,n):(0,i.mf)(e.get)?e.get.bind(n,n):i.dG;0;const s=!(0,i.mf)(e)&&(0,i.mf)(e.set)?e.set.bind(n):i.dG,u=Fn({get:t,set:s});Object.defineProperty(o,r,{enumerable:!0,configurable:!0,get:()=>u.value,set:e=>u.value=e})}if(c)for(const r in c)Ve(c[r],o,n,r);if(l){const e=(0,i.mf)(l)?l.call(n):l;Reflect.ownKeys(e).forEach((t=>{Z(t,e[t])}))}function F(e,t){(0,i.kJ)(t)?t.forEach((t=>e(t.bind(n)))):t&&e(t.bind(n))}if(d&&$e(d,e,"c"),F(Se,f),F(Re,p),F(Ae,m),F(Ce,v),F(ye,g),F(be,_),F(je,O),F(De,k),F(Fe,T),F(xe,b),F(Pe,I),F(Ne,S),(0,i.kJ)(R))if(R.length){const t=e.exposed||(e.exposed={});R.forEach((e=>{Object.defineProperty(t,e,{get:()=>n[e],set:t=>n[e]=t})}))}else e.exposed||(e.exposed={});E&&e.render===i.dG&&(e.render=E),null!=A&&(e.inheritAttrs=A),C&&(e.components=C),x&&(e.directives=x)}function Me(e,t,n=i.dG,o=!1){(0,i.kJ)(e)&&(e=Ge(e));for(const s in e){const n=e[s];let a;a=(0,i.Kn)(n)?"default"in n?X(n.from||s,n.default,!0):X(n.from||s):X(n),(0,r.dq)(a)&&o?Object.defineProperty(t,s,{enumerable:!0,configurable:!0,get:()=>a.value,set:e=>a.value=e}):t[s]=a}}function $e(e,t,n){s((0,i.kJ)(e)?e.map((e=>e.bind(t.proxy))):e.bind(t.proxy),t,n)}function Ve(e,t,n,r){const o=r.includes(".")?re(n,r):()=>n[r];if((0,i.HD)(e)){const n=t[e];(0,i.mf)(n)&&ee(o,n)}else if((0,i.mf)(e))ee(o,e.bind(n));else if((0,i.Kn)(e))if((0,i.kJ)(e))e.forEach((e=>Ve(e,t,n,r)));else{const r=(0,i.mf)(e.handler)?e.handler.bind(n):t[e.handler];(0,i.mf)(r)&&ee(o,r,e)}else 0}function ze(e){const t=e.type,{mixins:n,extends:r}=t,{mixins:i,optionsCache:o,config:{optionMergeStrategies:s}}=e.appContext,a=o.get(t);let u;return a?u=a:i.length||n||r?(u={},i.length&&i.forEach((e=>Be(u,e,s,!0))),Be(u,t,s)):u=t,o.set(t,u),u}function Be(e,t,n,r=!1){const{mixins:i,extends:o}=t;o&&Be(e,o,n,!0),i&&i.forEach((t=>Be(e,t,n,!0)));for(const s in t)if(r&&"expose"===s);else{const r=He[s]||n&&n[s];e[s]=r?r(e[s],t[s]):t[s]}return e}const He={data:qe,props:Ke,emits:Ke,methods:Ke,computed:Ke,beforeCreate:Je,created:Je,beforeMount:Je,mounted:Je,beforeUpdate:Je,updated:Je,beforeDestroy:Je,beforeUnmount:Je,destroyed:Je,unmounted:Je,activated:Je,deactivated:Je,errorCaptured:Je,serverPrefetch:Je,components:Ke,directives:Ke,watch:Ye,provide:qe,inject:We};function qe(e,t){return t?e?function(){return(0,i.l7)((0,i.mf)(e)?e.call(this,this):e,(0,i.mf)(t)?t.call(this,this):t)}:t:e}function We(e,t){return Ke(Ge(e),Ge(t))}function Ge(e){if((0,i.kJ)(e)){const t={};for(let n=0;n<e.length;n++)t[e[n]]=e[n];return t}return e}function Je(e,t){return e?[...new Set([].concat(e,t))]:t}function Ke(e,t){return e?(0,i.l7)((0,i.l7)(Object.create(null),e),t):t}function Ye(e,t){if(!e)return t;if(!t)return e;const n=(0,i.l7)(Object.create(null),e);for(const r in t)n[r]=Je(e[r],t[r]);return n}function Ze(e,t,n,o=!1){const s={},a={};(0,i.Nj)(a,Gt,1),e.propsDefaults=Object.create(null),Qe(e,t,s,a);for(const r in e.propsOptions[0])r in s||(s[r]=void 0);n?e.props=o?s:(0,r.Um)(s):e.type.props?e.props=s:e.props=a,e.attrs=a}function Xe(e,t,n,o){const{props:s,attrs:a,vnode:{patchFlag:u}}=e,c=(0,r.IU)(s),[l]=e.propsOptions;let h=!1;if(!(o||u>0)||16&u){let r;Qe(e,t,s,a)&&(h=!0);for(const o in c)t&&((0,i.RI)(t,o)||(r=(0,i.rs)(o))!==o&&(0,i.RI)(t,r))||(l?!n||void 0===n[o]&&void 0===n[r]||(s[o]=et(l,c,o,void 0,e,!0)):delete s[o]);if(a!==c)for(const e in a)t&&(0,i.RI)(t,e)||(delete a[e],h=!0)}else if(8&u){const n=e.vnode.dynamicProps;for(let r=0;r<n.length;r++){let o=n[r];const u=t[o];if(l)if((0,i.RI)(a,o))u!==a[o]&&(a[o]=u,h=!0);else{const t=(0,i._A)(o);s[t]=et(l,c,t,u,e,!1)}else u!==a[o]&&(a[o]=u,h=!0)}}h&&(0,r.X$)(e,"set","$attrs")}function Qe(e,t,n,o){const[s,a]=e.propsOptions;let u,c=!1;if(t)for(let r in t){if((0,i.Gg)(r))continue;const l=t[r];let h;s&&(0,i.RI)(s,h=(0,i._A)(r))?a&&a.includes(h)?(u||(u={}))[h]=l:n[h]=l:j(e.emitsOptions,r)||r in o&&l===o[r]||(o[r]=l,c=!0)}if(a){const t=(0,r.IU)(n),o=u||i.kT;for(let r=0;r<a.length;r++){const u=a[r];n[u]=et(s,t,u,o[u],e,!(0,i.RI)(o,u))}}return c}function et(e,t,n,r,o,s){const a=e[n];if(null!=a){const e=(0,i.RI)(a,"default");if(e&&void 0===r){const e=a.default;if(a.type!==Function&&(0,i.mf)(e)){const{propsDefaults:i}=o;n in i?r=i[n]:(yn(o),r=i[n]=e.call(null,t),bn())}else r=e}a[0]&&(s&&!e?r=!1:!a[1]||""!==r&&r!==(0,i.rs)(n)||(r=!0))}return r}function tt(e,t,n=!1){const r=t.propsCache,o=r.get(e);if(o)return o;const s=e.props,a={},u=[];let c=!1;if(!(0,i.mf)(e)){const r=e=>{c=!0;const[n,r]=tt(e,t,!0);(0,i.l7)(a,n),r&&u.push(...r)};!n&&t.mixins.length&&t.mixins.forEach(r),e.extends&&r(e.extends),e.mixins&&e.mixins.forEach(r)}if(!s&&!c)return r.set(e,i.Z6),i.Z6;if((0,i.kJ)(s))for(let h=0;h<s.length;h++){0;const e=(0,i._A)(s[h]);nt(e)&&(a[e]=i.kT)}else if(s){0;for(const e in s){const t=(0,i._A)(e);if(nt(t)){const n=s[e],r=a[t]=(0,i.kJ)(n)||(0,i.mf)(n)?{type:n}:n;if(r){const e=ot(Boolean,r.type),n=ot(String,r.type);r[0]=e>-1,r[1]=n<0||e<n,(e>-1||(0,i.RI)(r,"default"))&&u.push(t)}}}}const l=[a,u];return r.set(e,l),l}function nt(e){return"$"!==e[0]}function rt(e){const t=e&&e.toString().match(/^\s*function (\w+)/);return t?t[1]:null===e?"null":""}function it(e,t){return rt(e)===rt(t)}function ot(e,t){return(0,i.kJ)(t)?t.findIndex((t=>it(t,e))):(0,i.mf)(t)&&it(t,e)?0:-1}const st=e=>"_"===e[0]||"$stable"===e,at=e=>(0,i.kJ)(e)?e.map(rn):[rn(e)],ut=(e,t,n)=>{const r=z(((...e)=>at(t(...e))),n);return r._c=!1,r},ct=(e,t,n)=>{const r=e._ctx;for(const o in e){if(st(o))continue;const n=e[o];if((0,i.mf)(n))t[o]=ut(o,n,r);else if(null!=n){0;const e=at(n);t[o]=()=>e}}},lt=(e,t)=>{const n=at(t);e.slots.default=()=>n},ht=(e,t)=>{if(32&e.vnode.shapeFlag){const n=t._;n?(e.slots=(0,r.IU)(t),(0,i.Nj)(t,"_",n)):ct(t,e.slots={})}else e.slots={},t&&lt(e,t);(0,i.Nj)(e.slots,Gt,1)},dt=(e,t,n)=>{const{vnode:r,slots:o}=e;let s=!0,a=i.kT;if(32&r.shapeFlag){const e=t._;e?n&&1===e?s=!1:((0,i.l7)(o,t),n||1!==e||delete o._):(s=!t.$stable,ct(t,o)),a=t}else t&&(lt(e,t),a={default:1});if(s)for(const i in o)st(i)||i in a||delete o[i]};function ft(e,t){const n=L;if(null===n)return e;const r=n.proxy,o=e.dirs||(e.dirs=[]);for(let s=0;s<t.length;s++){let[e,n,a,u=i.kT]=t[s];(0,i.mf)(e)&&(e={mounted:e,updated:e}),e.deep&&ie(n),o.push({dir:e,instance:r,value:n,oldValue:void 0,arg:a,modifiers:u})}return e}function pt(e,t,n,i){const o=e.dirs,a=t&&t.dirs;for(let u=0;u<o.length;u++){const c=o[u];a&&(c.oldValue=a[u].value);let l=c.dir[i];l&&((0,r.Jd)(),s(l,n,8,[e.el,c,e,t]),(0,r.lk)())}}function mt(){return{app:null,config:{isNativeTag:i.NO,performance:!1,globalProperties:{},optionMergeStrategies:{},errorHandler:void 0,warnHandler:void 0,compilerOptions:{}},mixins:[],components:{},directives:{},provides:Object.create(null),optionsCache:new WeakMap,propsCache:new WeakMap,emitsCache:new WeakMap}}let vt=0;function gt(e,t){return function(n,r=null){null==r||(0,i.Kn)(r)||(r=null);const o=mt(),s=new Set;let a=!1;const u=o.app={_uid:vt++,_component:n,_props:r,_container:null,_context:o,_instance:null,version:jn,get config(){return o.config},set config(e){0},use(e,...t){return s.has(e)||(e&&(0,i.mf)(e.install)?(s.add(e),e.install(u,...t)):(0,i.mf)(e)&&(s.add(e),e(u,...t))),u},mixin(e){return o.mixins.includes(e)||o.mixins.push(e),u},component(e,t){return t?(o.components[e]=t,u):o.components[e]},directive(e,t){return t?(o.directives[e]=t,u):o.directives[e]},mount(i,s,c){if(!a){const l=Zt(n,r);return l.appContext=o,s&&t?t(l,i):e(l,i,c),a=!0,u._container=i,i.__vue_app__=u,xn(l.component)||l.component.proxy}},unmount(){a&&(e(null,u._container),delete u._container.__vue_app__)},provide(e,t){return o.provides[e]=t,u}};return u}}function _t(e,t,n,s,a=!1){if((0,i.kJ)(e))return void e.forEach(((e,r)=>_t(e,t&&((0,i.kJ)(t)?t[r]:t),n,s,a)));if(ve(s)&&!a)return;const u=4&s.shapeFlag?xn(s.component)||s.component.proxy:s.el,c=a?null:u,{i:l,r:h}=e;const d=t&&t.r,f=l.refs===i.kT?l.refs={}:l.refs,p=l.setupState;if(null!=d&&d!==h&&((0,i.HD)(d)?(f[d]=null,(0,i.RI)(p,d)&&(p[d]=null)):(0,r.dq)(d)&&(d.value=null)),(0,i.mf)(h))o(h,l,12,[c,f]);else{const t=(0,i.HD)(h),o=(0,r.dq)(h);if(t||o){const o=()=>{if(e.f){const n=t?f[h]:h.value;a?(0,i.kJ)(n)&&(0,i.Od)(n,u):(0,i.kJ)(n)?n.includes(u)||n.push(u):t?f[h]=[u]:(h.value=[u],e.k&&(f[e.k]=h.value))}else t?(f[h]=c,(0,i.RI)(p,h)&&(p[h]=c)):(0,r.dq)(h)&&(h.value=c,e.k&&(f[e.k]=c))};c?(o.id=-1,bt(o,n)):o()}else 0}}function yt(){}const bt=Y;function wt(e){return It(e)}function It(e,t){yt();const n=(0,i.E9)();n.__VUE__=!0;const{insert:o,remove:s,patchProp:a,createElement:u,createText:c,createComment:l,setText:h,setElementText:d,parentNode:f,nextSibling:p,setScopeId:m=i.dG,cloneNode:v,insertStaticContent:g}=e,_=(e,t,n,r=null,i=null,o=null,s=!1,a=null,u=!!t.dynamicChildren)=>{if(e===t)return;e&&!Wt(e,t)&&(r=Q(e),G(e,i,o,!0),e=null),-2===t.patchFlag&&(u=!1,t.dynamicChildren=null);const{type:c,ref:l,shapeFlag:h}=t;switch(c){case Nt:y(e,t,n,r);break;case Ft:b(e,t,n,r);break;case Dt:null==e&&w(t,n,r,s);break;case Pt:D(e,t,n,r,i,o,s,a,u);break;default:1&h?T(e,t,n,r,i,o,s,a,u):6&h?j(e,t,n,r,i,o,s,a,u):(64&h||128&h)&&c.process(e,t,n,r,i,o,s,a,u,te)}null!=l&&i&&_t(l,e&&e.ref,o,t||e,!t)},y=(e,t,n,r)=>{if(null==e)o(t.el=c(t.children),n,r);else{const n=t.el=e.el;t.children!==e.children&&h(n,t.children)}},b=(e,t,n,r)=>{null==e?o(t.el=l(t.children||""),n,r):t.el=e.el},w=(e,t,n,r)=>{[e.el,e.anchor]=g(e.children,t,n,r,e.el,e.anchor)},I=({el:e,anchor:t},n,r)=>{let i;while(e&&e!==t)i=p(e),o(e,n,r),e=i;o(t,n,r)},E=({el:e,anchor:t})=>{let n;while(e&&e!==t)n=p(e),s(e),e=n;s(t)},T=(e,t,n,r,i,o,s,a,u)=>{s=s||"svg"===t.type,null==e?S(t,n,r,i,o,s,a,u):P(e,t,i,o,s,a,u)},S=(e,t,n,r,s,c,l,h)=>{let f,p;const{type:m,props:g,shapeFlag:_,transition:y,patchFlag:b,dirs:w}=e;if(e.el&&void 0!==v&&-1===b)f=e.el=v(e.el);else{if(f=e.el=u(e.type,c,g&&g.is,g),8&_?d(f,e.children):16&_&&A(e.children,f,null,r,s,c&&"foreignObject"!==m,l,h),w&&pt(e,null,r,"created"),g){for(const t in g)"value"===t||(0,i.Gg)(t)||a(f,t,null,g[t],c,e.children,r,s,X);"value"in g&&a(f,"value",null,g.value),(p=g.onVnodeBeforeMount)&&un(p,r,e)}R(f,e,e.scopeId,l,r)}w&&pt(e,null,r,"beforeMount");const I=(!s||s&&!s.pendingBranch)&&y&&!y.persisted;I&&y.beforeEnter(f),o(f,t,n),((p=g&&g.onVnodeMounted)||I||w)&&bt((()=>{p&&un(p,r,e),I&&y.enter(f),w&&pt(e,null,r,"mounted")}),s)},R=(e,t,n,r,i)=>{if(n&&m(e,n),r)for(let o=0;o<r.length;o++)m(e,r[o]);if(i){let n=i.subTree;if(t===n){const t=i.vnode;R(e,t,t.scopeId,t.slotScopeIds,i.parent)}}},A=(e,t,n,r,i,o,s,a,u=0)=>{for(let c=u;c<e.length;c++){const u=e[c]=a?on(e[c]):rn(e[c]);_(null,u,t,n,r,i,o,s,a)}},P=(e,t,n,r,o,s,u)=>{const c=t.el=e.el;let{patchFlag:l,dynamicChildren:h,dirs:f}=t;l|=16&e.patchFlag;const p=e.props||i.kT,m=t.props||i.kT;let v;n&&Et(n,!1),(v=m.onVnodeBeforeUpdate)&&un(v,n,t,e),f&&pt(t,e,n,"beforeUpdate"),n&&Et(n,!0);const g=o&&"foreignObject"!==t.type;if(h?N(e.dynamicChildren,h,c,n,r,g,s):u||V(e,t,c,null,n,r,g,s,!1),l>0){if(16&l)F(c,t,p,m,n,r,o);else if(2&l&&p.class!==m.class&&a(c,"class",null,m.class,o),4&l&&a(c,"style",p.style,m.style,o),8&l){const i=t.dynamicProps;for(let t=0;t<i.length;t++){const s=i[t],u=p[s],l=m[s];l===u&&"value"!==s||a(c,s,u,l,o,e.children,n,r,X)}}1&l&&e.children!==t.children&&d(c,t.children)}else u||null!=h||F(c,t,p,m,n,r,o);((v=m.onVnodeUpdated)||f)&&bt((()=>{v&&un(v,n,t,e),f&&pt(t,e,n,"updated")}),r)},N=(e,t,n,r,i,o,s)=>{for(let a=0;a<t.length;a++){const u=e[a],c=t[a],l=u.el&&(u.type===Pt||!Wt(u,c)||70&u.shapeFlag)?f(u.el):n;_(u,c,l,null,r,i,o,s,!0)}},F=(e,t,n,r,o,s,u)=>{if(n!==r){for(const c in r){if((0,i.Gg)(c))continue;const l=r[c],h=n[c];l!==h&&"value"!==c&&a(e,c,h,l,u,t.children,o,s,X)}if(n!==i.kT)for(const c in n)(0,i.Gg)(c)||c in r||a(e,c,n[c],null,u,t.children,o,s,X);"value"in r&&a(e,"value",n.value,r.value)}},D=(e,t,n,r,i,s,a,u,l)=>{const h=t.el=e?e.el:c(""),d=t.anchor=e?e.anchor:c("");let{patchFlag:f,dynamicChildren:p,slotScopeIds:m}=t;m&&(u=u?u.concat(m):m),null==e?(o(h,n,r),o(d,n,r),A(t.children,n,d,i,s,a,u,l)):f>0&&64&f&&p&&e.dynamicChildren?(N(e.dynamicChildren,p,n,i,s,a,u),(null!=t.key||i&&t===i.subTree)&&kt(e,t,!0)):V(e,t,n,d,i,s,a,u,l)},j=(e,t,n,r,i,o,s,a,u)=>{t.slotScopeIds=a,null==e?512&t.shapeFlag?i.ctx.activate(t,n,r,s,u):L(t,n,r,i,o,s,u):U(e,t,u)},L=(e,t,n,r,i,o,s)=>{const a=e.component=vn(e,r,i);if(ge(e)&&(a.ctx.renderer=te),Tn(a),a.asyncDep){if(i&&i.registerDep(a,M),!e.el){const e=a.subTree=Zt(Ft);b(null,e,t,n)}}else M(a,e,t,n,i,o,s)},U=(e,t,n)=>{const r=t.component=e.component;if(W(e,t,n)){if(r.asyncDep&&!r.asyncResolved)return void $(r,t,n);r.next=t,O(r.update),r.update()}else t.component=e.component,t.el=e.el,r.vnode=t},M=(e,t,n,o,s,a,u)=>{const c=()=>{if(e.isMounted){let t,{next:n,bu:r,u:o,parent:c,vnode:l}=e,h=n;0,Et(e,!1),n?(n.el=l.el,$(e,n,u)):n=l,r&&(0,i.ir)(r),(t=n.props&&n.props.onVnodeBeforeUpdate)&&un(t,c,n,l),Et(e,!0);const d=B(e);0;const p=e.subTree;e.subTree=d,_(p,d,f(p.el),Q(p),e,s,a),n.el=d.el,null===h&&J(e,d.el),o&&bt(o,s),(t=n.props&&n.props.onVnodeUpdated)&&bt((()=>un(t,c,n,l)),s)}else{let r;const{el:u,props:c}=t,{bm:l,m:h,parent:d}=e,f=ve(t);if(Et(e,!1),l&&(0,i.ir)(l),!f&&(r=c&&c.onVnodeBeforeMount)&&un(r,d,t),Et(e,!0),u&&re){const n=()=>{e.subTree=B(e),re(u,e.subTree,e,s,null)};f?t.type.__asyncLoader().then((()=>!e.isUnmounted&&n())):n()}else{0;const r=e.subTree=B(e);0,_(null,r,n,o,e,s,a),t.el=r.el}if(h&&bt(h,s),!f&&(r=c&&c.onVnodeMounted)){const e=t;bt((()=>un(r,d,e)),s)}256&t.shapeFlag&&e.a&&bt(e.a,s),e.isMounted=!0,t=n=o=null}},l=e.effect=new r.qq(c,(()=>k(e.update)),e.scope),h=e.update=l.run.bind(l);h.id=e.uid,Et(e,!0),h()},$=(e,t,n)=>{t.component=e;const i=e.vnode.props;e.vnode=t,e.next=null,Xe(e,t.props,i,n),dt(e,t.children,n),(0,r.Jd)(),C(void 0,e.update),(0,r.lk)()},V=(e,t,n,r,i,o,s,a,u=!1)=>{const c=e&&e.children,l=e?e.shapeFlag:0,h=t.children,{patchFlag:f,shapeFlag:p}=t;if(f>0){if(128&f)return void H(c,h,n,r,i,o,s,a,u);if(256&f)return void z(c,h,n,r,i,o,s,a,u)}8&p?(16&l&&X(c,i,o),h!==c&&d(n,h)):16&l?16&p?H(c,h,n,r,i,o,s,a,u):X(c,i,o,!0):(8&l&&d(n,""),16&p&&A(h,n,r,i,o,s,a,u))},z=(e,t,n,r,o,s,a,u,c)=>{e=e||i.Z6,t=t||i.Z6;const l=e.length,h=t.length,d=Math.min(l,h);let f;for(f=0;f<d;f++){const r=t[f]=c?on(t[f]):rn(t[f]);_(e[f],r,n,null,o,s,a,u,c)}l>h?X(e,o,s,!0,!1,d):A(t,n,r,o,s,a,u,c,d)},H=(e,t,n,r,o,s,a,u,c)=>{let l=0;const h=t.length;let d=e.length-1,f=h-1;while(l<=d&&l<=f){const r=e[l],i=t[l]=c?on(t[l]):rn(t[l]);if(!Wt(r,i))break;_(r,i,n,null,o,s,a,u,c),l++}while(l<=d&&l<=f){const r=e[d],i=t[f]=c?on(t[f]):rn(t[f]);if(!Wt(r,i))break;_(r,i,n,null,o,s,a,u,c),d--,f--}if(l>d){if(l<=f){const e=f+1,i=e<h?t[e].el:r;while(l<=f)_(null,t[l]=c?on(t[l]):rn(t[l]),n,i,o,s,a,u,c),l++}}else if(l>f)while(l<=d)G(e[l],o,s,!0),l++;else{const p=l,m=l,v=new Map;for(l=m;l<=f;l++){const e=t[l]=c?on(t[l]):rn(t[l]);null!=e.key&&v.set(e.key,l)}let g,y=0;const b=f-m+1;let w=!1,I=0;const E=new Array(b);for(l=0;l<b;l++)E[l]=0;for(l=p;l<=d;l++){const r=e[l];if(y>=b){G(r,o,s,!0);continue}let i;if(null!=r.key)i=v.get(r.key);else for(g=m;g<=f;g++)if(0===E[g-m]&&Wt(r,t[g])){i=g;break}void 0===i?G(r,o,s,!0):(E[i-m]=l+1,i>=I?I=i:w=!0,_(r,t[i],n,null,o,s,a,u,c),y++)}const k=w?Tt(E):i.Z6;for(g=k.length-1,l=b-1;l>=0;l--){const e=m+l,i=t[e],d=e+1<h?t[e+1].el:r;0===E[l]?_(null,i,n,d,o,s,a,u,c):w&&(g<0||l!==k[g]?q(i,n,d,2):g--)}}},q=(e,t,n,r,i=null)=>{const{el:s,type:a,transition:u,children:c,shapeFlag:l}=e;if(6&l)return void q(e.component.subTree,t,n,r);if(128&l)return void e.suspense.move(t,n,r);if(64&l)return void a.move(e,t,n,te);if(a===Pt){o(s,t,n);for(let e=0;e<c.length;e++)q(c[e],t,n,r);return void o(e.anchor,t,n)}if(a===Dt)return void I(e,t,n);const h=2!==r&&1&l&&u;if(h)if(0===r)u.beforeEnter(s),o(s,t,n),bt((()=>u.enter(s)),i);else{const{leave:e,delayLeave:r,afterLeave:i}=u,a=()=>o(s,t,n),c=()=>{e(s,(()=>{a(),i&&i()}))};r?r(s,a,c):c()}else o(s,t,n)},G=(e,t,n,r=!1,i=!1)=>{const{type:o,props:s,ref:a,children:u,dynamicChildren:c,shapeFlag:l,patchFlag:h,dirs:d}=e;if(null!=a&&_t(a,null,n,e,!0),256&l)return void t.ctx.deactivate(e);const f=1&l&&d,p=!ve(e);let m;if(p&&(m=s&&s.onVnodeBeforeUnmount)&&un(m,t,e),6&l)Z(e.component,n,r);else{if(128&l)return void e.suspense.unmount(n,r);f&&pt(e,null,t,"beforeUnmount"),64&l?e.type.remove(e,t,n,i,te,r):c&&(o!==Pt||h>0&&64&h)?X(c,t,n,!1,!0):(o===Pt&&384&h||!i&&16&l)&&X(u,t,n),r&&K(e)}(p&&(m=s&&s.onVnodeUnmounted)||f)&&bt((()=>{m&&un(m,t,e),f&&pt(e,null,t,"unmounted")}),n)},K=e=>{const{type:t,el:n,anchor:r,transition:i}=e;if(t===Pt)return void Y(n,r);if(t===Dt)return void E(e);const o=()=>{s(n),i&&!i.persisted&&i.afterLeave&&i.afterLeave()};if(1&e.shapeFlag&&i&&!i.persisted){const{leave:t,delayLeave:r}=i,s=()=>t(n,o);r?r(e.el,o,s):s()}else o()},Y=(e,t)=>{let n;while(e!==t)n=p(e),s(e),e=n;s(t)},Z=(e,t,n)=>{const{bum:r,scope:o,update:s,subTree:a,um:u}=e;r&&(0,i.ir)(r),o.stop(),s&&(s.active=!1,G(a,e,t,n)),u&&bt(u,t),bt((()=>{e.isUnmounted=!0}),t),t&&t.pendingBranch&&!t.isUnmounted&&e.asyncDep&&!e.asyncResolved&&e.suspenseId===t.pendingId&&(t.deps--,0===t.deps&&t.resolve())},X=(e,t,n,r=!1,i=!1,o=0)=>{for(let s=o;s<e.length;s++)G(e[s],t,n,r,i)},Q=e=>6&e.shapeFlag?Q(e.component.subTree):128&e.shapeFlag?e.suspense.next():p(e.anchor||e.el),ee=(e,t,n)=>{null==e?t._vnode&&G(t._vnode,null,null,!0):_(t._vnode||null,e,t,null,null,null,n),x(),t._vnode=e},te={p:_,um:G,m:q,r:K,mt:L,mc:A,pc:V,pbc:N,n:Q,o:e};let ne,re;return t&&([ne,re]=t(te)),{render:ee,hydrate:ne,createApp:gt(ee,ne)}}function Et({effect:e,update:t},n){e.allowRecurse=t.allowRecurse=n}function kt(e,t,n=!1){const r=e.children,o=t.children;if((0,i.kJ)(r)&&(0,i.kJ)(o))for(let i=0;i<r.length;i++){const e=r[i];let t=o[i];1&t.shapeFlag&&!t.dynamicChildren&&((t.patchFlag<=0||32===t.patchFlag)&&(t=o[i]=on(o[i]),t.el=e.el),n||kt(e,t))}}function Tt(e){const t=e.slice(),n=[0];let r,i,o,s,a;const u=e.length;for(r=0;r<u;r++){const u=e[r];if(0!==u){if(i=n[n.length-1],e[i]<u){t[r]=i,n.push(r);continue}o=0,s=n.length-1;while(o<s)a=o+s>>1,e[n[a]]<u?o=a+1:s=a;u<e[n[o]]&&(o>0&&(t[r]=n[o-1]),n[o]=r)}}o=n.length,s=n[o-1];while(o-- >0)n[o]=s,s=t[s];return n}const Ot=e=>e.__isTeleport;const St="components";function Rt(e,t){return Ct(St,e,!0,t)||e}const At=Symbol();function Ct(e,t,n=!0,r=!1){const o=L||gn;if(o){const n=o.type;if(e===St){const e=Pn(n);if(e&&(e===t||e===(0,i._A)(t)||e===(0,i.kC)((0,i._A)(t))))return n}const s=xt(o[e]||n[e],t)||xt(o.appContext[e],t);return!s&&r?n:s}}function xt(e,t){return e&&(e[t]||e[(0,i._A)(t)]||e[(0,i.kC)((0,i._A)(t))])}const Pt=Symbol(void 0),Nt=Symbol(void 0),Ft=Symbol(void 0),Dt=Symbol(void 0),jt=[];let Lt=null;function Ut(e=!1){jt.push(Lt=e?null:[])}function Mt(){jt.pop(),Lt=jt[jt.length-1]||null}let $t=1;function Vt(e){$t+=e}function zt(e){return e.dynamicChildren=$t>0?Lt||i.Z6:null,Mt(),$t>0&&Lt&&Lt.push(e),e}function Bt(e,t,n,r,i,o){return zt(Yt(e,t,n,r,i,o,!0))}function Ht(e,t,n,r,i){return zt(Zt(e,t,n,r,i,!0))}function qt(e){return!!e&&!0===e.__v_isVNode}function Wt(e,t){return e.type===t.type&&e.key===t.key}const Gt="__vInternal",Jt=({key:e})=>null!=e?e:null,Kt=({ref:e,ref_key:t,ref_for:n})=>null!=e?(0,i.HD)(e)||(0,r.dq)(e)||(0,i.mf)(e)?{i:L,r:e,k:t,f:!!n}:e:null;function Yt(e,t=null,n=null,r=0,o=null,s=(e===Pt?0:1),a=!1,u=!1){const c={__v_isVNode:!0,__v_skip:!0,type:e,props:t,key:t&&Jt(t),ref:t&&Kt(t),scopeId:U,slotScopeIds:null,children:n,component:null,suspense:null,ssContent:null,ssFallback:null,dirs:null,transition:null,el:null,anchor:null,target:null,targetAnchor:null,staticCount:0,shapeFlag:s,patchFlag:r,dynamicProps:o,dynamicChildren:null,appContext:null};return u?(sn(c,n),128&s&&e.normalize(c)):n&&(c.shapeFlag|=(0,i.HD)(n)?8:16),$t>0&&!a&&Lt&&(c.patchFlag>0||6&s)&&32!==c.patchFlag&&Lt.push(c),c}const Zt=Xt;function Xt(e,t=null,n=null,o=0,s=null,a=!1){if(e&&e!==At||(e=Ft),qt(e)){const r=en(e,t,!0);return n&&sn(r,n),r}if(Nn(e)&&(e=e.__vccOpts),t){t=Qt(t);let{class:e,style:n}=t;e&&!(0,i.HD)(e)&&(t.class=(0,i.C_)(e)),(0,i.Kn)(n)&&((0,r.X3)(n)&&!(0,i.kJ)(n)&&(n=(0,i.l7)({},n)),t.style=(0,i.j5)(n))}const u=(0,i.HD)(e)?1:K(e)?128:Ot(e)?64:(0,i.Kn)(e)?4:(0,i.mf)(e)?2:0;return Yt(e,t,n,o,s,u,a,!0)}function Qt(e){return e?(0,r.X3)(e)||Gt in e?(0,i.l7)({},e):e:null}function en(e,t,n=!1){const{props:r,ref:o,patchFlag:s,children:a}=e,u=t?an(r||{},t):r,c={__v_isVNode:!0,__v_skip:!0,type:e.type,props:u,key:u&&Jt(u),ref:t&&t.ref?n&&o?(0,i.kJ)(o)?o.concat(Kt(t)):[o,Kt(t)]:Kt(t):o,scopeId:e.scopeId,slotScopeIds:e.slotScopeIds,children:a,target:e.target,targetAnchor:e.targetAnchor,staticCount:e.staticCount,shapeFlag:e.shapeFlag,patchFlag:t&&e.type!==Pt?-1===s?16:16|s:s,dynamicProps:e.dynamicProps,dynamicChildren:e.dynamicChildren,appContext:e.appContext,dirs:e.dirs,transition:e.transition,component:e.component,suspense:e.suspense,ssContent:e.ssContent&&en(e.ssContent),ssFallback:e.ssFallback&&en(e.ssFallback),el:e.el,anchor:e.anchor};return c}function tn(e=" ",t=0){return Zt(Nt,null,e,t)}function nn(e="",t=!1){return t?(Ut(),Ht(Ft,null,e)):Zt(Ft,null,e)}function rn(e){return null==e||"boolean"===typeof e?Zt(Ft):(0,i.kJ)(e)?Zt(Pt,null,e.slice()):"object"===typeof e?on(e):Zt(Nt,null,String(e))}function on(e){return null===e.el||e.memo?e:en(e)}function sn(e,t){let n=0;const{shapeFlag:r}=e;if(null==t)t=null;else if((0,i.kJ)(t))n=16;else if("object"===typeof t){if(65&r){const n=t.default;return void(n&&(n._c&&(n._d=!1),sn(e,n()),n._c&&(n._d=!0)))}{n=32;const r=t._;r||Gt in t?3===r&&L&&(1===L.slots._?t._=1:(t._=2,e.patchFlag|=1024)):t._ctx=L}}else(0,i.mf)(t)?(t={default:t,_ctx:L},n=32):(t=String(t),64&r?(n=16,t=[tn(t)]):n=8);e.children=t,e.shapeFlag|=n}function an(...e){const t={};for(let n=0;n<e.length;n++){const r=e[n];for(const e in r)if("class"===e)t.class!==r.class&&(t.class=(0,i.C_)([t.class,r.class]));else if("style"===e)t.style=(0,i.j5)([t.style,r.style]);else if((0,i.F7)(e)){const n=t[e],o=r[e];!o||n===o||(0,i.kJ)(n)&&n.includes(o)||(t[e]=n?[].concat(n,o):o)}else""!==e&&(t[e]=r[e])}return t}function un(e,t,n,r=null){s(e,t,7,[n,r])}function cn(e,t,n={},r,i){if(L.isCE)return Zt("slot","default"===t?null:{name:t},r&&r());let o=e[t];o&&o._c&&(o._d=!1),Ut();const s=o&&ln(o(n)),a=Ht(Pt,{key:n.key||`_${t}`},s||(r?r():[]),s&&1===e._?64:-2);return!i&&a.scopeId&&(a.slotScopeIds=[a.scopeId+"-s"]),o&&o._c&&(o._d=!0),a}function ln(e){return e.some((e=>!qt(e)||e.type!==Ft&&!(e.type===Pt&&!ln(e.children))))?e:null}const hn=e=>e?wn(e)?xn(e)||e.proxy:hn(e.parent):null,dn=(0,i.l7)(Object.create(null),{$:e=>e,$el:e=>e.vnode.el,$data:e=>e.data,$props:e=>e.props,$attrs:e=>e.attrs,$slots:e=>e.slots,$refs:e=>e.refs,$parent:e=>hn(e.parent),$root:e=>hn(e.root),$emit:e=>e.emit,$options:e=>ze(e),$forceUpdate:e=>()=>k(e.update),$nextTick:e=>I.bind(e.proxy),$watch:e=>ne.bind(e)}),fn={get({_:e},t){const{ctx:n,setupState:o,data:s,props:a,accessCache:u,type:c,appContext:l}=e;let h;if("$"!==t[0]){const r=u[t];if(void 0!==r)switch(r){case 1:return o[t];case 2:return s[t];case 4:return n[t];case 3:return a[t]}else{if(o!==i.kT&&(0,i.RI)(o,t))return u[t]=1,o[t];if(s!==i.kT&&(0,i.RI)(s,t))return u[t]=2,s[t];if((h=e.propsOptions[0])&&(0,i.RI)(h,t))return u[t]=3,a[t];if(n!==i.kT&&(0,i.RI)(n,t))return u[t]=4,n[t];Le&&(u[t]=0)}}const d=dn[t];let f,p;return d?("$attrs"===t&&(0,r.j)(e,"get",t),d(e)):(f=c.__cssModules)&&(f=f[t])?f:n!==i.kT&&(0,i.RI)(n,t)?(u[t]=4,n[t]):(p=l.config.globalProperties,(0,i.RI)(p,t)?p[t]:void 0)},set({_:e},t,n){const{data:r,setupState:o,ctx:s}=e;return o!==i.kT&&(0,i.RI)(o,t)?(o[t]=n,!0):r!==i.kT&&(0,i.RI)(r,t)?(r[t]=n,!0):!(0,i.RI)(e.props,t)&&(("$"!==t[0]||!(t.slice(1)in e))&&(s[t]=n,!0))},has({_:{data:e,setupState:t,accessCache:n,ctx:r,appContext:o,propsOptions:s}},a){let u;return!!n[a]||e!==i.kT&&(0,i.RI)(e,a)||t!==i.kT&&(0,i.RI)(t,a)||(u=s[0])&&(0,i.RI)(u,a)||(0,i.RI)(r,a)||(0,i.RI)(dn,a)||(0,i.RI)(o.config.globalProperties,a)},defineProperty(e,t,n){return null!=n.get?this.set(e,t,n.get(),null):null!=n.value&&this.set(e,t,n.value,null),Reflect.defineProperty(e,t,n)}};const pn=mt();let mn=0;function vn(e,t,n){const o=e.type,s=(t?t.appContext:e.appContext)||pn,a={uid:mn++,vnode:e,type:o,parent:t,appContext:s,root:null,next:null,subTree:null,effect:null,update:null,scope:new r.Bj(!0),render:null,proxy:null,exposed:null,exposeProxy:null,withProxy:null,provides:t?t.provides:Object.create(s.provides),accessCache:null,renderCache:[],components:null,directives:null,propsOptions:tt(o,s),emitsOptions:D(o,s),emit:null,emitted:null,propsDefaults:i.kT,inheritAttrs:o.inheritAttrs,ctx:i.kT,data:i.kT,props:i.kT,attrs:i.kT,slots:i.kT,refs:i.kT,setupState:i.kT,setupContext:null,suspense:n,suspenseId:n?n.pendingId:0,asyncDep:null,asyncResolved:!1,isMounted:!1,isUnmounted:!1,isDeactivated:!1,bc:null,c:null,bm:null,m:null,bu:null,u:null,um:null,bum:null,da:null,a:null,rtg:null,rtc:null,ec:null,sp:null};return a.ctx={_:a},a.root=t?t.root:a,a.emit=F.bind(null,a),e.ce&&e.ce(a),a}let gn=null;const _n=()=>gn||L,yn=e=>{gn=e,e.scope.on()},bn=()=>{gn&&gn.scope.off(),gn=null};function wn(e){return 4&e.vnode.shapeFlag}let In,En,kn=!1;function Tn(e,t=!1){kn=t;const{props:n,children:r}=e.vnode,i=wn(e);Ze(e,n,i,t),ht(e,r);const o=i?On(e,t):void 0;return kn=!1,o}function On(e,t){const n=e.type;e.accessCache=Object.create(null),e.proxy=(0,r.Xl)(new Proxy(e.ctx,fn));const{setup:s}=n;if(s){const n=e.setupContext=s.length>1?Cn(e):null;yn(e),(0,r.Jd)();const u=o(s,e,0,[e.props,n]);if((0,r.lk)(),bn(),(0,i.tI)(u)){if(u.then(bn,bn),t)return u.then((n=>{Sn(e,n,t)})).catch((t=>{a(t,e,0)}));e.asyncDep=u}else Sn(e,u,t)}else Rn(e,t)}function Sn(e,t,n){(0,i.mf)(t)?e.type.__ssrInlineRender?e.ssrRender=t:e.render=t:(0,i.Kn)(t)&&(e.setupState=(0,r.WL)(t)),Rn(e,n)}function Rn(e,t,n){const o=e.type;if(!e.render){if(!t&&In&&!o.render){const t=o.template;if(t){0;const{isCustomElement:n,compilerOptions:r}=e.appContext.config,{delimiters:s,compilerOptions:a}=o,u=(0,i.l7)((0,i.l7)({isCustomElement:n,delimiters:s},r),a);o.render=In(t,u)}}e.render=o.render||i.dG,En&&En(e)}yn(e),(0,r.Jd)(),Ue(e),(0,r.lk)(),bn()}function An(e){return new Proxy(e.attrs,{get(t,n){return(0,r.j)(e,"get","$attrs"),t[n]}})}function Cn(e){const t=t=>{e.exposed=t||{}};let n;return{get attrs(){return n||(n=An(e))},slots:e.slots,emit:e.emit,expose:t}}function xn(e){if(e.exposed)return e.exposeProxy||(e.exposeProxy=new Proxy((0,r.WL)((0,r.Xl)(e.exposed)),{get(t,n){return n in t?t[n]:n in dn?dn[n](e):void 0}}))}function Pn(e){return(0,i.mf)(e)&&e.displayName||e.name}function Nn(e){return(0,i.mf)(e)&&"__vccOpts"in e}const Fn=(e,t)=>(0,r.Fl)(e,t,kn);function Dn(e,t,n){const r=arguments.length;return 2===r?(0,i.Kn)(t)&&!(0,i.kJ)(t)?qt(t)?Zt(e,null,[t]):Zt(e,t):Zt(e,null,t):(r>3?n=Array.prototype.slice.call(arguments,2):3===r&&qt(n)&&(n=[n]),Zt(e,t,n))}Symbol("");const jn="3.2.31"},9242:function(e,t,n){"use strict";n.d(t,{iM:function(){return se},nr:function(){return re},ri:function(){return le}});var r=n(7139),i=n(3396);n(4870);const o="http://www.w3.org/2000/svg",s="undefined"!==typeof document?document:null,a=s&&s.createElement("template"),u={insert:(e,t,n)=>{t.insertBefore(e,n||null)},remove:e=>{const t=e.parentNode;t&&t.removeChild(e)},createElement:(e,t,n,r)=>{const i=t?s.createElementNS(o,e):s.createElement(e,n?{is:n}:void 0);return"select"===e&&r&&null!=r.multiple&&i.setAttribute("multiple",r.multiple),i},createText:e=>s.createTextNode(e),createComment:e=>s.createComment(e),setText:(e,t)=>{e.nodeValue=t},setElementText:(e,t)=>{e.textContent=t},parentNode:e=>e.parentNode,nextSibling:e=>e.nextSibling,querySelector:e=>s.querySelector(e),setScopeId(e,t){e.setAttribute(t,"")},cloneNode(e){const t=e.cloneNode(!0);return"_value"in e&&(t._value=e._value),t},insertStaticContent(e,t,n,r,i,o){const s=n?n.previousSibling:t.lastChild;if(i&&(i===o||i.nextSibling)){while(1)if(t.insertBefore(i.cloneNode(!0),n),i===o||!(i=i.nextSibling))break}else{a.innerHTML=r?`<svg>${e}</svg>`:e;const i=a.content;if(r){const e=i.firstChild;while(e.firstChild)i.appendChild(e.firstChild);i.removeChild(e)}t.insertBefore(i,n)}return[s?s.nextSibling:t.firstChild,n?n.previousSibling:t.lastChild]}};function c(e,t,n){const r=e._vtc;r&&(t=(t?[t,...r]:[...r]).join(" ")),null==t?e.removeAttribute("class"):n?e.setAttribute("class",t):e.className=t}function l(e,t,n){const i=e.style,o=(0,r.HD)(n);if(n&&!o){for(const e in n)d(i,e,n[e]);if(t&&!(0,r.HD)(t))for(const e in t)null==n[e]&&d(i,e,"")}else{const r=i.display;o?t!==n&&(i.cssText=n):t&&e.removeAttribute("style"),"_vod"in e&&(i.display=r)}}const h=/\s*!important$/;function d(e,t,n){if((0,r.kJ)(n))n.forEach((n=>d(e,t,n)));else if(t.startsWith("--"))e.setProperty(t,n);else{const i=m(e,t);h.test(n)?e.setProperty((0,r.rs)(i),n.replace(h,""),"important"):e[i]=n}}const f=["Webkit","Moz","ms"],p={};function m(e,t){const n=p[t];if(n)return n;let i=(0,r._A)(t);if("filter"!==i&&i in e)return p[t]=i;i=(0,r.kC)(i);for(let r=0;r<f.length;r++){const n=f[r]+i;if(n in e)return p[t]=n}return t}const v="http://www.w3.org/1999/xlink";function g(e,t,n,i,o){if(i&&t.startsWith("xlink:"))null==n?e.removeAttributeNS(v,t.slice(6,t.length)):e.setAttributeNS(v,t,n);else{const i=(0,r.Pq)(t);null==n||i&&!(0,r.yA)(n)?e.removeAttribute(t):e.setAttribute(t,i?"":n)}}function _(e,t,n,i,o,s,a){if("innerHTML"===t||"textContent"===t)return i&&a(i,o,s),void(e[t]=null==n?"":n);if("value"===t&&"PROGRESS"!==e.tagName&&!e.tagName.includes("-")){e._value=n;const r=null==n?"":n;return e.value===r&&"OPTION"!==e.tagName||(e.value=r),void(null==n&&e.removeAttribute(t))}if(""===n||null==n){const i=typeof e[t];if("boolean"===i)return void(e[t]=(0,r.yA)(n));if(null==n&&"string"===i)return e[t]="",void e.removeAttribute(t);if("number"===i){try{e[t]=0}catch(u){}return void e.removeAttribute(t)}}try{e[t]=n}catch(c){0}}let y=Date.now,b=!1;if("undefined"!==typeof window){y()>document.createEvent("Event").timeStamp&&(y=()=>performance.now());const e=navigator.userAgent.match(/firefox\/(\d+)/i);b=!!(e&&Number(e[1])<=53)}let w=0;const I=Promise.resolve(),E=()=>{w=0},k=()=>w||(I.then(E),w=y());function T(e,t,n,r){e.addEventListener(t,n,r)}function O(e,t,n,r){e.removeEventListener(t,n,r)}function S(e,t,n,r,i=null){const o=e._vei||(e._vei={}),s=o[t];if(r&&s)s.value=r;else{const[n,a]=A(t);if(r){const s=o[t]=C(r,i);T(e,n,s,a)}else s&&(O(e,n,s,a),o[t]=void 0)}}const R=/(?:Once|Passive|Capture)$/;function A(e){let t;if(R.test(e)){let n;t={};while(n=e.match(R))e=e.slice(0,e.length-n[0].length),t[n[0].toLowerCase()]=!0}return[(0,r.rs)(e.slice(2)),t]}function C(e,t){const n=e=>{const r=e.timeStamp||y();(b||r>=n.attached-1)&&(0,i.$d)(x(e,n.value),t,5,[e])};return n.value=e,n.attached=k(),n}function x(e,t){if((0,r.kJ)(t)){const n=e.stopImmediatePropagation;return e.stopImmediatePropagation=()=>{n.call(e),e._stopped=!0},t.map((e=>t=>!t._stopped&&e&&e(t)))}return t}const P=/^on[a-z]/,N=(e,t,n,i,o=!1,s,a,u,h)=>{"class"===t?c(e,i,o):"style"===t?l(e,n,i):(0,r.F7)(t)?(0,r.tR)(t)||S(e,t,n,i,a):("."===t[0]?(t=t.slice(1),1):"^"===t[0]?(t=t.slice(1),0):F(e,t,i,o))?_(e,t,i,s,a,u,h):("true-value"===t?e._trueValue=i:"false-value"===t&&(e._falseValue=i),g(e,t,i,o))};function F(e,t,n,i){return i?"innerHTML"===t||"textContent"===t||!!(t in e&&P.test(t)&&(0,r.mf)(n)):"spellcheck"!==t&&"draggable"!==t&&("form"!==t&&(("list"!==t||"INPUT"!==e.tagName)&&(("type"!==t||"TEXTAREA"!==e.tagName)&&((!P.test(t)||!(0,r.HD)(n))&&t in e))))}"undefined"!==typeof HTMLElement&&HTMLElement;const D="transition",j="animation",L=(e,{slots:t})=>(0,i.h)(i.P$,V(e),t);L.displayName="Transition";const U={name:String,type:String,css:{type:Boolean,default:!0},duration:[String,Number,Object],enterFromClass:String,enterActiveClass:String,enterToClass:String,appearFromClass:String,appearActiveClass:String,appearToClass:String,leaveFromClass:String,leaveActiveClass:String,leaveToClass:String},M=(L.props=(0,r.l7)({},i.P$.props,U),(e,t=[])=>{(0,r.kJ)(e)?e.forEach((e=>e(...t))):e&&e(...t)}),$=e=>!!e&&((0,r.kJ)(e)?e.some((e=>e.length>1)):e.length>1);function V(e){const t={};for(const r in e)r in U||(t[r]=e[r]);if(!1===e.css)return t;const{name:n="v",type:i,duration:o,enterFromClass:s=`${n}-enter-from`,enterActiveClass:a=`${n}-enter-active`,enterToClass:u=`${n}-enter-to`,appearFromClass:c=s,appearActiveClass:l=a,appearToClass:h=u,leaveFromClass:d=`${n}-leave-from`,leaveActiveClass:f=`${n}-leave-active`,leaveToClass:p=`${n}-leave-to`}=e,m=z(o),v=m&&m[0],g=m&&m[1],{onBeforeEnter:_,onEnter:y,onEnterCancelled:b,onLeave:w,onLeaveCancelled:I,onBeforeAppear:E=_,onAppear:k=y,onAppearCancelled:T=b}=t,O=(e,t,n)=>{q(e,t?h:u),q(e,t?l:a),n&&n()},S=(e,t)=>{q(e,p),q(e,f),t&&t()},R=e=>(t,n)=>{const r=e?k:y,o=()=>O(t,e,n);M(r,[t,o]),W((()=>{q(t,e?c:s),H(t,e?h:u),$(r)||J(t,i,v,o)}))};return(0,r.l7)(t,{onBeforeEnter(e){M(_,[e]),H(e,s),H(e,a)},onBeforeAppear(e){M(E,[e]),H(e,c),H(e,l)},onEnter:R(!1),onAppear:R(!0),onLeave(e,t){const n=()=>S(e,t);H(e,d),X(),H(e,f),W((()=>{q(e,d),H(e,p),$(w)||J(e,i,g,n)})),M(w,[e,n])},onEnterCancelled(e){O(e,!1),M(b,[e])},onAppearCancelled(e){O(e,!0),M(T,[e])},onLeaveCancelled(e){S(e),M(I,[e])}})}function z(e){if(null==e)return null;if((0,r.Kn)(e))return[B(e.enter),B(e.leave)];{const t=B(e);return[t,t]}}function B(e){const t=(0,r.He)(e);return t}function H(e,t){t.split(/\s+/).forEach((t=>t&&e.classList.add(t))),(e._vtc||(e._vtc=new Set)).add(t)}function q(e,t){t.split(/\s+/).forEach((t=>t&&e.classList.remove(t)));const{_vtc:n}=e;n&&(n.delete(t),n.size||(e._vtc=void 0))}function W(e){requestAnimationFrame((()=>{requestAnimationFrame(e)}))}let G=0;function J(e,t,n,r){const i=e._endId=++G,o=()=>{i===e._endId&&r()};if(n)return setTimeout(o,n);const{type:s,timeout:a,propCount:u}=K(e,t);if(!s)return r();const c=s+"end";let l=0;const h=()=>{e.removeEventListener(c,d),o()},d=t=>{t.target===e&&++l>=u&&h()};setTimeout((()=>{l<u&&h()}),a+1),e.addEventListener(c,d)}function K(e,t){const n=window.getComputedStyle(e),r=e=>(n[e]||"").split(", "),i=r(D+"Delay"),o=r(D+"Duration"),s=Y(i,o),a=r(j+"Delay"),u=r(j+"Duration"),c=Y(a,u);let l=null,h=0,d=0;t===D?s>0&&(l=D,h=s,d=o.length):t===j?c>0&&(l=j,h=c,d=u.length):(h=Math.max(s,c),l=h>0?s>c?D:j:null,d=l?l===D?o.length:u.length:0);const f=l===D&&/\b(transform|all)(,|$)/.test(n[D+"Property"]);return{type:l,timeout:h,propCount:d,hasTransform:f}}function Y(e,t){while(e.length<t.length)e=e.concat(e);return Math.max(...t.map(((t,n)=>Z(t)+Z(e[n]))))}function Z(e){return 1e3*Number(e.slice(0,-1).replace(",","."))}function X(){return document.body.offsetHeight}new WeakMap,new WeakMap;const Q=e=>{const t=e.props["onUpdate:modelValue"];return(0,r.kJ)(t)?e=>(0,r.ir)(t,e):t};function ee(e){e.target.composing=!0}function te(e){const t=e.target;t.composing&&(t.composing=!1,ne(t,"input"))}function ne(e,t){const n=document.createEvent("HTMLEvents");n.initEvent(t,!0,!0),e.dispatchEvent(n)}const re={created(e,{modifiers:{lazy:t,trim:n,number:i}},o){e._assign=Q(o);const s=i||o.props&&"number"===o.props.type;T(e,t?"change":"input",(t=>{if(t.target.composing)return;let i=e.value;n?i=i.trim():s&&(i=(0,r.He)(i)),e._assign(i)})),n&&T(e,"change",(()=>{e.value=e.value.trim()})),t||(T(e,"compositionstart",ee),T(e,"compositionend",te),T(e,"change",te))},mounted(e,{value:t}){e.value=null==t?"":t},beforeUpdate(e,{value:t,modifiers:{lazy:n,trim:i,number:o}},s){if(e._assign=Q(s),e.composing)return;if(document.activeElement===e){if(n)return;if(i&&e.value.trim()===t)return;if((o||"number"===e.type)&&(0,r.He)(e.value)===t)return}const a=null==t?"":t;e.value!==a&&(e.value=a)}};const ie=["ctrl","shift","alt","meta"],oe={stop:e=>e.stopPropagation(),prevent:e=>e.preventDefault(),self:e=>e.target!==e.currentTarget,ctrl:e=>!e.ctrlKey,shift:e=>!e.shiftKey,alt:e=>!e.altKey,meta:e=>!e.metaKey,left:e=>"button"in e&&0!==e.button,middle:e=>"button"in e&&1!==e.button,right:e=>"button"in e&&2!==e.button,exact:(e,t)=>ie.some((n=>e[`${n}Key`]&&!t.includes(n)))},se=(e,t)=>(n,...r)=>{for(let e=0;e<t.length;e++){const r=oe[t[e]];if(r&&r(n,t))return}return e(n,...r)};const ae=(0,r.l7)({patchProp:N},u);let ue;function ce(){return ue||(ue=(0,i.Us)(ae))}const le=(...e)=>{const t=ce().createApp(...e);const{mount:n}=t;return t.mount=e=>{const i=he(e);if(!i)return;const o=t._component;(0,r.mf)(o)||o.render||o.template||(o.template=i.innerHTML),i.innerHTML="";const s=n(i,!1,i instanceof SVGElement);return i instanceof Element&&(i.removeAttribute("v-cloak"),i.setAttribute("data-v-app","")),s},t};function he(e){if((0,r.HD)(e)){const t=document.querySelector(e);return t}return e}},7139:function(e,t,n){"use strict";function r(e,t){const n=Object.create(null),r=e.split(",");for(let i=0;i<r.length;i++)n[r[i]]=!0;return t?e=>!!n[e.toLowerCase()]:e=>!!n[e]}n.d(t,{C_:function(){return f},DM:function(){return P},E9:function(){return re},F7:function(){return k},Gg:function(){return H},HD:function(){return D},He:function(){return te},Kn:function(){return L},NO:function(){return I},Nj:function(){return ee},Od:function(){return S},PO:function(){return z},Pq:function(){return a},RI:function(){return A},S0:function(){return B},W7:function(){return V},WV:function(){return m},Z6:function(){return b},_A:function(){return G},_N:function(){return x},aU:function(){return X},dG:function(){return w},e1:function(){return o},fY:function(){return r},hR:function(){return Z},hq:function(){return v},ir:function(){return Q},j5:function(){return c},kC:function(){return Y},kJ:function(){return C},kT:function(){return y},l7:function(){return O},mf:function(){return F},rs:function(){return K},tI:function(){return U},tR:function(){return T},yA:function(){return u},yk:function(){return j},zw:function(){return g}});const i="Infinity,undefined,NaN,isFinite,isNaN,parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,BigInt",o=r(i);const s="itemscope,allowfullscreen,formnovalidate,ismap,nomodule,novalidate,readonly",a=r(s);function u(e){return!!e||""===e}function c(e){if(C(e)){const t={};for(let n=0;n<e.length;n++){const r=e[n],i=D(r)?d(r):c(r);if(i)for(const e in i)t[e]=i[e]}return t}return D(e)||L(e)?e:void 0}const l=/;(?![^(]*\))/g,h=/:(.+)/;function d(e){const t={};return e.split(l).forEach((e=>{if(e){const n=e.split(h);n.length>1&&(t[n[0].trim()]=n[1].trim())}})),t}function f(e){let t="";if(D(e))t=e;else if(C(e))for(let n=0;n<e.length;n++){const r=f(e[n]);r&&(t+=r+" ")}else if(L(e))for(const n in e)e[n]&&(t+=n+" ");return t.trim()}function p(e,t){if(e.length!==t.length)return!1;let n=!0;for(let r=0;n&&r<e.length;r++)n=m(e[r],t[r]);return n}function m(e,t){if(e===t)return!0;let n=N(e),r=N(t);if(n||r)return!(!n||!r)&&e.getTime()===t.getTime();if(n=C(e),r=C(t),n||r)return!(!n||!r)&&p(e,t);if(n=L(e),r=L(t),n||r){if(!n||!r)return!1;const i=Object.keys(e).length,o=Object.keys(t).length;if(i!==o)return!1;for(const n in e){const r=e.hasOwnProperty(n),i=t.hasOwnProperty(n);if(r&&!i||!r&&i||!m(e[n],t[n]))return!1}}return String(e)===String(t)}function v(e,t){return e.findIndex((e=>m(e,t)))}const g=e=>D(e)?e:null==e?"":C(e)||L(e)&&(e.toString===M||!F(e.toString))?JSON.stringify(e,_,2):String(e),_=(e,t)=>t&&t.__v_isRef?_(e,t.value):x(t)?{[`Map(${t.size})`]:[...t.entries()].reduce(((e,[t,n])=>(e[`${t} =>`]=n,e)),{})}:P(t)?{[`Set(${t.size})`]:[...t.values()]}:!L(t)||C(t)||z(t)?t:String(t),y={},b=[],w=()=>{},I=()=>!1,E=/^on[^a-z]/,k=e=>E.test(e),T=e=>e.startsWith("onUpdate:"),O=Object.assign,S=(e,t)=>{const n=e.indexOf(t);n>-1&&e.splice(n,1)},R=Object.prototype.hasOwnProperty,A=(e,t)=>R.call(e,t),C=Array.isArray,x=e=>"[object Map]"===$(e),P=e=>"[object Set]"===$(e),N=e=>e instanceof Date,F=e=>"function"===typeof e,D=e=>"string"===typeof e,j=e=>"symbol"===typeof e,L=e=>null!==e&&"object"===typeof e,U=e=>L(e)&&F(e.then)&&F(e.catch),M=Object.prototype.toString,$=e=>M.call(e),V=e=>$(e).slice(8,-1),z=e=>"[object Object]"===$(e),B=e=>D(e)&&"NaN"!==e&&"-"!==e[0]&&""+parseInt(e,10)===e,H=r(",key,ref,ref_for,ref_key,onVnodeBeforeMount,onVnodeMounted,onVnodeBeforeUpdate,onVnodeUpdated,onVnodeBeforeUnmount,onVnodeUnmounted"),q=e=>{const t=Object.create(null);return n=>{const r=t[n];return r||(t[n]=e(n))}},W=/-(\w)/g,G=q((e=>e.replace(W,((e,t)=>t?t.toUpperCase():"")))),J=/\B([A-Z])/g,K=q((e=>e.replace(J,"-$1").toLowerCase())),Y=q((e=>e.charAt(0).toUpperCase()+e.slice(1))),Z=q((e=>e?`on${Y(e)}`:"")),X=(e,t)=>!Object.is(e,t),Q=(e,t)=>{for(let n=0;n<e.length;n++)e[n](t)},ee=(e,t,n)=>{Object.defineProperty(e,t,{configurable:!0,enumerable:!1,value:n})},te=e=>{const t=parseFloat(e);return isNaN(t)?e:t};let ne;const re=()=>ne||(ne="undefined"!==typeof globalThis?globalThis:"undefined"!==typeof self?self:"undefined"!==typeof window?window:"undefined"!==typeof n.g?n.g:{})},4275:function(e,t,n){"use strict";n.d(t,{ZF:function(){return r.initializeApp}});var r=n(9684),i="firebase",o="9.6.10";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(0,r.registerVersion)(i,o,"app")},5866:function(e,t,n){"use strict";n.d(t,{v0:function(){return r.v0}});var r=n(2575)},6780:function(e,t,n){"use strict";n.d(t,{Z:function(){return r.Z}});var r=n(2661),i="firebase",o="9.6.10";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
r.Z.registerVersion(i,o,"app-compat")},6471:function(e,t,n){"use strict";var r=n(2661),i=(n(8675),n(3462),n(1703),n(8895)),o=n(223),s=(n(9684),n(5168),n(7142));
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function a(){return window}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const u=2e3;async function c(e,t,n){var r;const{BuildInfo:o}=a();(0,i.ap)(t.sessionId,"AuthEvent did not contain a session ID");const s=await p(t.sessionId),u={};return(0,i.aq)()?u["ibi"]=o.packageName:(0,i.ar)()?u["apn"]=o.packageName:(0,i.as)(e,"operation-not-supported-in-this-environment"),o.displayName&&(u["appDisplayName"]=o.displayName),u["sessionId"]=s,(0,i.at)(e,n,t.type,void 0,null!==(r=t.eventId)&&void 0!==r?r:void 0,u)}async function l(e){const{BuildInfo:t}=a(),n={};(0,i.aq)()?n.iosBundleId=t.packageName:(0,i.ar)()?n.androidPackageName=t.packageName:(0,i.as)(e,"operation-not-supported-in-this-environment"),await(0,i.au)(e,n)}function h(e){const{cordova:t}=a();return new Promise((n=>{t.plugins.browsertab.isAvailable((r=>{let o=null;r?t.plugins.browsertab.openUrl(e):o=t.InAppBrowser.open(e,(0,i.ao)()?"_blank":"_system","location=yes"),n(o)}))}))}async function d(e,t,n){const{cordova:r}=a();let o=()=>{};try{await new Promise(((s,a)=>{let c=null;function l(){var e;s();const t=null===(e=r.plugins.browsertab)||void 0===e?void 0:e.close;"function"===typeof t&&t(),"function"===typeof(null===n||void 0===n?void 0:n.close)&&n.close()}function h(){c||(c=window.setTimeout((()=>{a((0,i.av)(e,"redirect-cancelled-by-user"))}),u))}function d(){"visible"===(null===document||void 0===document?void 0:document.visibilityState)&&h()}t.addPassiveListener(l),document.addEventListener("resume",h,!1),(0,i.ar)()&&document.addEventListener("visibilitychange",d,!1),o=()=>{t.removePassiveListener(l),document.removeEventListener("resume",h,!1),document.removeEventListener("visibilitychange",d,!1),c&&window.clearTimeout(c)}}))}finally{o()}}function f(e){var t,n,r,o,s,u,c,l,h,d;const f=a();(0,i.aw)("function"===typeof(null===(t=null===f||void 0===f?void 0:f.universalLinks)||void 0===t?void 0:t.subscribe),e,"invalid-cordova-configuration",{missingPlugin:"cordova-universal-links-plugin-fix"}),(0,i.aw)("undefined"!==typeof(null===(n=null===f||void 0===f?void 0:f.BuildInfo)||void 0===n?void 0:n.packageName),e,"invalid-cordova-configuration",{missingPlugin:"cordova-plugin-buildInfo"}),(0,i.aw)("function"===typeof(null===(s=null===(o=null===(r=null===f||void 0===f?void 0:f.cordova)||void 0===r?void 0:r.plugins)||void 0===o?void 0:o.browsertab)||void 0===s?void 0:s.openUrl),e,"invalid-cordova-configuration",{missingPlugin:"cordova-plugin-browsertab"}),(0,i.aw)("function"===typeof(null===(l=null===(c=null===(u=null===f||void 0===f?void 0:f.cordova)||void 0===u?void 0:u.plugins)||void 0===c?void 0:c.browsertab)||void 0===l?void 0:l.isAvailable),e,"invalid-cordova-configuration",{missingPlugin:"cordova-plugin-browsertab"}),(0,i.aw)("function"===typeof(null===(d=null===(h=null===f||void 0===f?void 0:f.cordova)||void 0===h?void 0:h.InAppBrowser)||void 0===d?void 0:d.open),e,"invalid-cordova-configuration",{missingPlugin:"cordova-plugin-inappbrowser"})}async function p(e){const t=m(e),n=await crypto.subtle.digest("SHA-256",t),r=Array.from(new Uint8Array(n));return r.map((e=>e.toString(16).padStart(2,"0"))).join("")}function m(e){if((0,i.ap)(/[0-9a-zA-Z]+/.test(e),"Can only convert alpha-numeric strings"),"undefined"!==typeof TextEncoder)return(new TextEncoder).encode(e);const t=new ArrayBuffer(e.length),n=new Uint8Array(t);for(let r=0;r<e.length;r++)n[r]=e.charCodeAt(r);return n}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const v=20;class g extends i.az{constructor(){super(...arguments),this.passiveListeners=new Set,this.initPromise=new Promise((e=>{this.resolveInialized=e}))}addPassiveListener(e){this.passiveListeners.add(e)}removePassiveListener(e){this.passiveListeners.delete(e)}resetRedirect(){this.queuedRedirectEvent=null,this.hasHandledPotentialRedirect=!1}onEvent(e){return this.resolveInialized(),this.passiveListeners.forEach((t=>t(e))),super.onEvent(e)}async initialized(){await this.initPromise}}function _(e,t,n=null){return{type:t,eventId:n,urlResponse:null,sessionId:I(),postBody:null,tenantId:e.tenantId,error:(0,i.av)(e,"no-auth-event")}}function y(e,t){return E()._set(k(e),t)}async function b(e){const t=await E()._get(k(e));return t&&await E()._remove(k(e)),t}function w(e,t){var n,r;const o=O(t);if(o.includes("/__/auth/callback")){const t=S(o),s=t["firebaseError"]?T(decodeURIComponent(t["firebaseError"])):null,a=null===(r=null===(n=null===s||void 0===s?void 0:s["code"])||void 0===n?void 0:n.split("auth/"))||void 0===r?void 0:r[1],u=a?(0,i.av)(a):null;return u?{type:e.type,eventId:e.eventId,tenantId:e.tenantId,error:u,urlResponse:null,sessionId:null,postBody:null}:{type:e.type,eventId:e.eventId,tenantId:e.tenantId,sessionId:e.sessionId,urlResponse:o,postBody:null}}return null}function I(){const e=[],t="1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";for(let n=0;n<v;n++){const n=Math.floor(Math.random()*t.length);e.push(t.charAt(n))}return e.join("")}function E(){return(0,i.ax)(i.b)}function k(e){return(0,i.ay)("authEvent",e.config.apiKey,e.name)}function T(e){try{return JSON.parse(e)}catch(t){return null}}function O(e){const t=S(e),n=t["link"]?decodeURIComponent(t["link"]):void 0,r=S(n)["link"],i=t["deep_link_id"]?decodeURIComponent(t["deep_link_id"]):void 0,o=S(i)["link"];return o||i||r||n||e}function S(e){if(!(null===e||void 0===e?void 0:e.includes("?")))return{};const[t,...n]=e.split("?");return(0,o.zd)(n.join("?"))}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const R=500;class A{constructor(){this._redirectPersistence=i.a,this._shouldInitProactively=!0,this.eventManagers=new Map,this.originValidationPromises={},this._completeRedirectFn=i.aA}async _initialize(e){const t=e._key();let n=this.eventManagers.get(t);return n||(n=new g(e),this.eventManagers.set(t,n),this.attachCallbackListeners(e,n)),n}_openPopup(e){(0,i.as)(e,"operation-not-supported-in-this-environment")}async _openRedirect(e,t,n,r){f(e);const o=await this._initialize(e);await o.initialized(),o.resetRedirect(),(0,i.aB)(),await this._originValidation(e);const s=_(e,n,r);await y(e,s);const a=await c(e,s,t),u=await h(a);return d(e,o,u)}_isIframeWebStorageSupported(e,t){throw new Error("Method not implemented.")}_originValidation(e){const t=e._key();return this.originValidationPromises[t]||(this.originValidationPromises[t]=l(e)),this.originValidationPromises[t]}attachCallbackListeners(e,t){const{universalLinks:n,handleOpenURL:r,BuildInfo:i}=a(),o=setTimeout((async()=>{await b(e),t.onEvent(x())}),R),s=async n=>{clearTimeout(o);const r=await b(e);let i=null;r&&(null===n||void 0===n?void 0:n["url"])&&(i=w(r,n["url"])),t.onEvent(i||x())};"undefined"!==typeof n&&"function"===typeof n.subscribe&&n.subscribe(null,s);const u=r,c=`${i.packageName.toLowerCase()}://`;a().handleOpenURL=async e=>{if(e.toLowerCase().startsWith(c)&&s({url:e}),"function"===typeof u)try{u(e)}catch(t){console.error(t)}}}}const C=A;function x(){return{type:"unknown",eventId:null,sessionId:null,urlResponse:null,postBody:null,tenantId:null,error:(0,i.av)("no-auth-event")}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function P(e,t){(0,i.aC)(e)._logFramework(t)}var N="@firebase/auth-compat",F="0.2.11";
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const D=1e3;function j(){var e;return(null===(e=null===self||void 0===self?void 0:self.location)||void 0===e?void 0:e.protocol)||null}function L(){return"http:"===j()||"https:"===j()}function U(e=(0,o.z$)()){return!("file:"!==j()&&"ionic:"!==j()||!e.toLowerCase().match(/iphone|ipad|ipod|android/))}function M(){return(0,o.b$)()||(0,o.UG)()}function $(){return(0,o.w1)()&&11===(null===document||void 0===document?void 0:document.documentMode)}function V(e=(0,o.z$)()){return/Edge\/\d+/.test(e)}function z(e=(0,o.z$)()){return $()||V(e)}function B(){try{const e=self.localStorage,t=i.aG();if(e)return e["setItem"](t,"1"),e["removeItem"](t),!z()||(0,o.hl)()}catch(e){return H()&&(0,o.hl)()}return!1}function H(){return"undefined"!==typeof n.g&&"WorkerGlobalScope"in n.g&&"importScripts"in n.g}function q(){return(L()||(0,o.ru)()||U())&&!M()&&B()&&!H()}function W(){return U()&&"undefined"!==typeof document}async function G(){return!!W()&&new Promise((e=>{const t=setTimeout((()=>{e(!1)}),D);document.addEventListener("deviceready",(()=>{clearTimeout(t),e(!0)}))}))}function J(){return"undefined"!==typeof window?window:null}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const K={LOCAL:"local",NONE:"none",SESSION:"session"},Y=i.aw,Z="persistence";function X(e,t){Y(Object.values(K).includes(t),e,"invalid-persistence-type"),(0,o.b$)()?Y(t!==K.SESSION,e,"unsupported-persistence-type"):(0,o.UG)()?Y(t===K.NONE,e,"unsupported-persistence-type"):H()?Y(t===K.NONE||t===K.LOCAL&&(0,o.hl)(),e,"unsupported-persistence-type"):Y(t===K.NONE||B(),e,"unsupported-persistence-type")}async function Q(e){await e._initializationPromise;const t=te(),n=i.ay(Z,e.config.apiKey,e.name);t&&t.setItem(n,e._getPersistence())}function ee(e,t){const n=te();if(!n)return[];const r=i.ay(Z,e,t),o=n.getItem(r);switch(o){case K.NONE:return[i.K];case K.LOCAL:return[i.i,i.a];case K.SESSION:return[i.a];default:return[]}}function te(){var e;try{return(null===(e=J())||void 0===e?void 0:e.sessionStorage)||null}catch(t){return null}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ne=i.aw;class re{constructor(){this.browserResolver=i.ax(i.k),this.cordovaResolver=i.ax(C),this.underlyingResolver=null,this._redirectPersistence=i.a,this._completeRedirectFn=i.aA}async _initialize(e){return await this.selectUnderlyingResolver(),this.assertedUnderlyingResolver._initialize(e)}async _openPopup(e,t,n,r){return await this.selectUnderlyingResolver(),this.assertedUnderlyingResolver._openPopup(e,t,n,r)}async _openRedirect(e,t,n,r){return await this.selectUnderlyingResolver(),this.assertedUnderlyingResolver._openRedirect(e,t,n,r)}_isIframeWebStorageSupported(e,t){this.assertedUnderlyingResolver._isIframeWebStorageSupported(e,t)}_originValidation(e){return this.assertedUnderlyingResolver._originValidation(e)}get _shouldInitProactively(){return W()||this.browserResolver._shouldInitProactively}get assertedUnderlyingResolver(){return ne(this.underlyingResolver,"internal-error"),this.underlyingResolver}async selectUnderlyingResolver(){if(this.underlyingResolver)return;const e=await G();this.underlyingResolver=e?this.cordovaResolver:this.browserResolver}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function ie(e){return e.unwrap()}function oe(e){return e.wrapped()}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function se(e){return ue(e)}function ae(e,t){var n;const r=null===(n=t.customData)||void 0===n?void 0:n._tokenResponse;if("auth/multi-factor-auth-required"===t.code){const n=t;n.resolver=new he(e,i.am(e,t))}else if(r){const e=ue(t),n=t;e&&(n.credential=e,n.tenantId=r.tenantId||void 0,n.email=r.email||void 0,n.phoneNumber=r.phoneNumber||void 0)}}function ue(e){const{_tokenResponse:t}=e instanceof o.ZR?e.customData:e;if(!t)return null;if(!(e instanceof o.ZR)&&"temporaryProof"in t&&"phoneNumber"in t)return i.P.credentialFromResult(e);const n=t.providerId;if(!n||n===i.o.PASSWORD)return null;let r;switch(n){case i.o.GOOGLE:r=i.N;break;case i.o.FACEBOOK:r=i.M;break;case i.o.GITHUB:r=i.Q;break;case i.o.TWITTER:r=i.V;break;default:const{oauthIdToken:e,oauthAccessToken:o,oauthTokenSecret:s,pendingToken:a,nonce:u}=t;return o||s||e||a?a?n.startsWith("saml.")?i.aJ._create(n,a):i.I._fromParams({providerId:n,signInMethod:n,pendingToken:a,idToken:e,accessToken:o}):new i.T(n).credential({idToken:e,accessToken:o,rawNonce:u}):null}return e instanceof o.ZR?r.credentialFromError(e):r.credentialFromResult(e)}function ce(e,t){return t.catch((t=>{throw t instanceof o.ZR&&ae(e,t),t})).then((e=>{const t=e.operationType,n=e.user;return{operationType:t,credential:se(e),additionalUserInfo:i.ak(e),user:de.getOrCreate(n)}}))}async function le(e,t){const n=await t;return{verificationId:n.verificationId,confirm:t=>ce(e,n.confirm(t))}}class he{constructor(e,t){this.resolver=t,this.auth=oe(e)}get session(){return this.resolver.session}get hints(){return this.resolver.hints}resolveSignIn(e){return ce(ie(this.auth),this.resolver.resolveSignIn(e))}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class de{constructor(e){this._delegate=e,this.multiFactor=i.an(e)}static getOrCreate(e){return de.USER_MAP.has(e)||de.USER_MAP.set(e,new de(e)),de.USER_MAP.get(e)}delete(){return this._delegate.delete()}reload(){return this._delegate.reload()}toJSON(){return this._delegate.toJSON()}getIdTokenResult(e){return this._delegate.getIdTokenResult(e)}getIdToken(e){return this._delegate.getIdToken(e)}linkAndRetrieveDataWithCredential(e){return this.linkWithCredential(e)}async linkWithCredential(e){return ce(this.auth,i.Y(this._delegate,e))}async linkWithPhoneNumber(e,t){return le(this.auth,i.l(this._delegate,e,t))}async linkWithPopup(e){return ce(this.auth,i.d(this._delegate,e,re))}async linkWithRedirect(e){return await Q(i.aC(this.auth)),i.g(this._delegate,e,re)}reauthenticateAndRetrieveDataWithCredential(e){return this.reauthenticateWithCredential(e)}async reauthenticateWithCredential(e){return ce(this.auth,i.Z(this._delegate,e))}reauthenticateWithPhoneNumber(e,t){return le(this.auth,i.r(this._delegate,e,t))}reauthenticateWithPopup(e){return ce(this.auth,i.e(this._delegate,e,re))}async reauthenticateWithRedirect(e){return await Q(i.aC(this.auth)),i.h(this._delegate,e,re)}sendEmailVerification(e){return i.aa(this._delegate,e)}async unlink(e){return await i.aj(this._delegate,e),this}updateEmail(e){return i.af(this._delegate,e)}updatePassword(e){return i.ag(this._delegate,e)}updatePhoneNumber(e){return i.u(this._delegate,e)}updateProfile(e){return i.ae(this._delegate,e)}verifyBeforeUpdateEmail(e,t){return i.ab(this._delegate,e,t)}get emailVerified(){return this._delegate.emailVerified}get isAnonymous(){return this._delegate.isAnonymous}get metadata(){return this._delegate.metadata}get phoneNumber(){return this._delegate.phoneNumber}get providerData(){return this._delegate.providerData}get refreshToken(){return this._delegate.refreshToken}get tenantId(){return this._delegate.tenantId}get displayName(){return this._delegate.displayName}get email(){return this._delegate.email}get photoURL(){return this._delegate.photoURL}get providerId(){return this._delegate.providerId}get uid(){return this._delegate.uid}get auth(){return this._delegate.auth}}de.USER_MAP=new WeakMap;
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const fe=i.aw;class pe{constructor(e,t){if(this.app=e,t.isInitialized())return this._delegate=t.getImmediate(),void this.linkUnderlyingAuth();const{apiKey:n}=e.options;fe(n,"invalid-api-key",{appName:e.name}),fe(n,"invalid-api-key",{appName:e.name});const r="undefined"!==typeof window?re:void 0;this._delegate=t.initialize({options:{persistence:ve(n,e.name),popupRedirectResolver:r}}),this._delegate._updateErrorMap(i.z),this.linkUnderlyingAuth()}get emulatorConfig(){return this._delegate.emulatorConfig}get currentUser(){return this._delegate.currentUser?de.getOrCreate(this._delegate.currentUser):null}get languageCode(){return this._delegate.languageCode}set languageCode(e){this._delegate.languageCode=e}get settings(){return this._delegate.settings}get tenantId(){return this._delegate.tenantId}set tenantId(e){this._delegate.tenantId=e}useDeviceLanguage(){this._delegate.useDeviceLanguage()}signOut(){return this._delegate.signOut()}useEmulator(e,t){i.E(this._delegate,e,t)}applyActionCode(e){return i.a1(this._delegate,e)}checkActionCode(e){return i.a2(this._delegate,e)}confirmPasswordReset(e,t){return i.a0(this._delegate,e,t)}async createUserWithEmailAndPassword(e,t){return ce(this._delegate,i.a4(this._delegate,e,t))}fetchProvidersForEmail(e){return this.fetchSignInMethodsForEmail(e)}fetchSignInMethodsForEmail(e){return i.a9(this._delegate,e)}isSignInWithEmailLink(e){return i.a7(this._delegate,e)}async getRedirectResult(){fe(q(),this._delegate,"operation-not-supported-in-this-environment");const e=await i.j(this._delegate,re);return e?ce(this._delegate,Promise.resolve(e)):{credential:null,user:null}}addFrameworkForLogging(e){P(this._delegate,e)}onAuthStateChanged(e,t,n){const{next:r,error:i,complete:o}=me(e,t,n);return this._delegate.onAuthStateChanged(r,i,o)}onIdTokenChanged(e,t,n){const{next:r,error:i,complete:o}=me(e,t,n);return this._delegate.onIdTokenChanged(r,i,o)}sendSignInLinkToEmail(e,t){return i.a6(this._delegate,e,t)}sendPasswordResetEmail(e,t){return i.$(this._delegate,e,t||void 0)}async setPersistence(e){let t;switch(X(this._delegate,e),e){case K.SESSION:t=i.a;break;case K.LOCAL:const e=await i.ax(i.i)._isAvailable();t=e?i.i:i.b;break;case K.NONE:t=i.K;break;default:return i.as("argument-error",{appName:this._delegate.name})}return this._delegate.setPersistence(t)}signInAndRetrieveDataWithCredential(e){return this.signInWithCredential(e)}signInAnonymously(){return ce(this._delegate,i.W(this._delegate))}signInWithCredential(e){return ce(this._delegate,i.X(this._delegate,e))}signInWithCustomToken(e){return ce(this._delegate,i._(this._delegate,e))}signInWithEmailAndPassword(e,t){return ce(this._delegate,i.a5(this._delegate,e,t))}signInWithEmailLink(e,t){return ce(this._delegate,i.a8(this._delegate,e,t))}signInWithPhoneNumber(e,t){return le(this._delegate,i.s(this._delegate,e,t))}async signInWithPopup(e){return fe(q(),this._delegate,"operation-not-supported-in-this-environment"),ce(this._delegate,i.c(this._delegate,e,re))}async signInWithRedirect(e){return fe(q(),this._delegate,"operation-not-supported-in-this-environment"),await Q(this._delegate),i.f(this._delegate,e,re)}updateCurrentUser(e){return this._delegate.updateCurrentUser(e)}verifyPasswordResetCode(e){return i.a3(this._delegate,e)}unwrap(){return this._delegate}_delete(){return this._delegate._delete()}linkUnderlyingAuth(){this._delegate.wrapped=()=>this}}function me(e,t,n){let r=e;"function"!==typeof e&&({next:r,error:t,complete:n}=e);const i=r,o=e=>i(e&&de.getOrCreate(e));return{next:o,error:t,complete:n}}function ve(e,t){const n=ee(e,t);if("undefined"===typeof self||n.includes(i.i)||n.push(i.i),"undefined"!==typeof window)for(const r of[i.b,i.a])n.includes(r)||n.push(r);return n.includes(i.K)||n.push(i.K),n}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */pe.Persistence=K;class ge{constructor(){this.providerId="phone",this._delegate=new i.P(ie(r.Z.auth()))}static credential(e,t){return i.P.credential(e,t)}verifyPhoneNumber(e,t){return this._delegate.verifyPhoneNumber(e,t)}unwrap(){return this._delegate}}ge.PHONE_SIGN_IN_METHOD=i.P.PHONE_SIGN_IN_METHOD,ge.PROVIDER_ID=i.P.PROVIDER_ID;
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const _e=i.aw;class ye{constructor(e,t,n=r.Z.app()){var o;_e(null===(o=n.options)||void 0===o?void 0:o.apiKey,"invalid-api-key",{appName:n.name}),this._delegate=new i.R(e,t,n.auth()),this.type=this._delegate.type}clear(){this._delegate.clear()}render(){return this._delegate.render()}verify(){return this._delegate.verify()}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const be="auth-compat";function we(e){e.INTERNAL.registerComponent(new s.wA(be,(e=>{const t=e.getProvider("app-compat").getImmediate(),n=e.getProvider("auth");return new pe(t,n)}),"PUBLIC").setServiceProps({ActionCodeInfo:{Operation:{EMAIL_SIGNIN:i.A.EMAIL_SIGNIN,PASSWORD_RESET:i.A.PASSWORD_RESET,RECOVER_EMAIL:i.A.RECOVER_EMAIL,REVERT_SECOND_FACTOR_ADDITION:i.A.REVERT_SECOND_FACTOR_ADDITION,VERIFY_AND_CHANGE_EMAIL:i.A.VERIFY_AND_CHANGE_EMAIL,VERIFY_EMAIL:i.A.VERIFY_EMAIL}},EmailAuthProvider:i.L,FacebookAuthProvider:i.M,GithubAuthProvider:i.Q,GoogleAuthProvider:i.N,OAuthProvider:i.T,SAMLAuthProvider:i.U,PhoneAuthProvider:ge,PhoneMultiFactorGenerator:i.m,RecaptchaVerifier:ye,TwitterAuthProvider:i.V,Auth:pe,AuthCredential:i.G,Error:o.ZR}).setInstantiationMode("LAZY").setMultipleInstances(!1)),e.registerVersion(N,F)}we(r.Z)},2154:function(e,t,n){"use strict";var r=n(2661),i=n(2087),o=n(7142);
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class s{constructor(e,t,n){this._delegate=e,this.task=t,this.ref=n}get bytesTransferred(){return this._delegate.bytesTransferred}get metadata(){return this._delegate.metadata}get state(){return this._delegate.state}get totalBytes(){return this._delegate.totalBytes}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class a{constructor(e,t){this._delegate=e,this._ref=t,this.cancel=this._delegate.cancel.bind(this._delegate),this.catch=this._delegate.catch.bind(this._delegate),this.pause=this._delegate.pause.bind(this._delegate),this.resume=this._delegate.resume.bind(this._delegate)}get snapshot(){return new s(this._delegate.snapshot,this,this._ref)}then(e,t){return this._delegate.then((t=>{if(e)return e(new s(t,this,this._ref))}),t)}on(e,t,n,r){let i;return t&&(i="function"===typeof t?e=>t(new s(e,this,this._ref)):{next:t.next?e=>t.next(new s(e,this,this._ref)):void 0,complete:t.complete||void 0,error:t.error||void 0}),this._delegate.on(e,i,n||void 0,r||void 0)}}class u{constructor(e,t){this._delegate=e,this._service=t}get prefixes(){return this._delegate.prefixes.map((e=>new c(e,this._service)))}get items(){return this._delegate.items.map((e=>new c(e,this._service)))}get nextPageToken(){return this._delegate.nextPageToken||null}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class c{constructor(e,t){this._delegate=e,this.storage=t}get name(){return this._delegate.name}get bucket(){return this._delegate.bucket}get fullPath(){return this._delegate.fullPath}toString(){return this._delegate.toString()}child(e){const t=(0,i.g6)(this._delegate,e);return new c(t,this.storage)}get root(){return new c(this._delegate.root,this.storage)}get parent(){const e=this._delegate.parent;return null==e?null:new c(e,this.storage)}put(e,t){return this._throwIfRoot("put"),new a((0,i.B0)(this._delegate,e,t),this)}putString(e,t=i.bm.RAW,n){this._throwIfRoot("putString");const r=(0,i.qm)(t,e),o=Object.assign({},n);return null==o["contentType"]&&null!=r.contentType&&(o["contentType"]=r.contentType),new a(new i.IX(this._delegate,new i.UJ(r.data,!0),o),this)}listAll(){return(0,i.aF)(this._delegate).then((e=>new u(e,this.storage)))}list(e){return(0,i.pb)(this._delegate,e||void 0).then((e=>new u(e,this.storage)))}getMetadata(){return(0,i.sd)(this._delegate)}updateMetadata(e){return(0,i.Ym)(this._delegate,e)}getDownloadURL(){return(0,i.Jt)(this._delegate)}delete(){return this._throwIfRoot("delete"),(0,i.oq)(this._delegate)}_throwIfRoot(e){if(""===this._delegate._location.path)throw(0,i.y4)(e)}}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class l{constructor(e,t){this.app=e,this._delegate=t}get maxOperationRetryTime(){return this._delegate.maxOperationRetryTime}get maxUploadRetryTime(){return this._delegate.maxUploadRetryTime}ref(e){if(h(e))throw(0,i.gH)("ref() expected a child path but got a URL, use refFromURL instead.");return new c((0,i.iH)(this._delegate,e),this)}refFromURL(e){if(!h(e))throw(0,i.gH)("refFromURL() expected a full URL but got a child path, use ref() instead.");try{i.gE.makeFromUrl(e,this._delegate.host)}catch(t){throw(0,i.gH)("refFromUrl() expected a valid full URL but got an invalid one.")}return new c((0,i.iH)(this._delegate,e),this)}setMaxUploadRetryTime(e){this._delegate.maxUploadRetryTime=e}setMaxOperationRetryTime(e){this._delegate.maxOperationRetryTime=e}useEmulator(e,t,n={}){(0,i.a1)(this._delegate,e,t,n)}}function h(e){return/^[A-Za-z]+:\/\//.test(e)}const d="@firebase/storage-compat",f="0.1.12",p="storage-compat";function m(e,{instanceIdentifier:t}){const n=e.getProvider("app-compat").getImmediate(),r=e.getProvider("storage").getImmediate({identifier:t}),i=new l(n,r);return i}function v(e){const t={TaskState:i.$Y,TaskEvent:i.FN,StringFormat:i.bm,Storage:l,Reference:c};e.INTERNAL.registerComponent(new o.wA(p,m,"PUBLIC").setServiceProps(t).setMultipleInstances(!0)),e.registerVersion(d,f)}v(r.Z)},3283:function(e,t,n){"use strict";n.d(t,{KV:function(){return r.KV}});var r=n(2087)},4353:function(e){"use strict";function t(e){this._maxSize=e,this.clear()}t.prototype.clear=function(){this._size=0,this._values=Object.create(null)},t.prototype.get=function(e){return this._values[e]},t.prototype.set=function(e,t){return this._size>=this._maxSize&&this.clear(),e in this._values||this._size++,this._values[e]=t};var n=/[^.^\]^[]+|(?=\[\]|\.\.)/g,r=/^\d+$/,i=/^\d/,o=/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g,s=/^\s*(['"]?)(.*?)(\1)\s*$/,a=512,u=new t(a),c=new t(a),l=new t(a);function h(e){return u.get(e)||u.set(e,d(e).map((function(e){return e.replace(s,"$2")})))}function d(e){return e.match(n)||[""]}function f(e,t,n){var r,i,o,s,a=e.length;for(i=0;i<a;i++)r=e[i],r&&(g(r)&&(r='"'+r+'"'),s=p(r),o=!s&&/^\d+$/.test(r),t.call(n,r,s,o,i,e))}function p(e){return"string"===typeof e&&e&&-1!==["'",'"'].indexOf(e.charAt(0))}function m(e){return e.match(i)&&!e.match(r)}function v(e){return o.test(e)}function g(e){return!p(e)&&(m(e)||v(e))}e.exports={Cache:t,split:d,normalizePath:h,setter:function(e){var t=h(e);return c.get(e)||c.set(e,(function(e,n){var r=0,i=t.length,o=e;while(r<i-1){var s=t[r];if("__proto__"===s||"constructor"===s||"prototype"===s)return e;o=o[t[r++]]}o[t[r]]=n}))},getter:function(e,t){var n=h(e);return l.get(e)||l.set(e,(function(e){var r=0,i=n.length;while(r<i){if(null==e&&t)return;e=e[n[r++]]}return e}))},join:function(e){return e.reduce((function(e,t){return e+(p(t)||r.test(t)?"["+t+"]":(e?".":"")+t)}),"")},forEach:function(e,t,n){f(Array.isArray(e)?e:d(e),t,n)}}},1414:function(e,t,n){function r(e,t){var n=e.length,r=new Array(n),i={},a=n,u=o(t),c=s(e);t.forEach((function(e){if(!c.has(e[0])||!c.has(e[1]))throw new Error("Unknown node. There is an unknown node in the supplied edges.")}));while(a--)i[a]||l(e[a],a,new Set);return r;function l(e,t,o){if(o.has(e)){var s;try{s=", node was:"+JSON.stringify(e)}catch(d){s=""}throw new Error("Cyclic dependency"+s)}if(!c.has(e))throw new Error("Found unknown node. Make sure to provided all involved nodes. Unknown node: "+JSON.stringify(e));if(!i[t]){i[t]=!0;var a=u.get(e)||new Set;if(a=Array.from(a),t=a.length){o.add(e);do{var h=a[--t];l(h,c.get(h),o)}while(t);o.delete(e)}r[--n]=e}}}function i(e){for(var t=new Set,n=0,r=e.length;n<r;n++){var i=e[n];t.add(i[0]),t.add(i[1])}return Array.from(t)}function o(e){for(var t=new Map,n=0,r=e.length;n<r;n++){var i=e[n];t.has(i[0])||t.set(i[0],new Set),t.has(i[1])||t.set(i[1],new Set),t.get(i[0]).add(i[1])}return t}function s(e){for(var t=new Map,n=0,r=e.length;n<r;n++)t.set(e[n],n);return t}n(1703),e.exports=function(e){return r(i(e),e)},e.exports.array=r},536:function(e,t,n){"use strict";n.d(t,{Z:function(){return p}});n(8675),n(3462),n(1703);var r,i=new Uint8Array(16);function o(){if(!r&&(r="undefined"!==typeof crypto&&crypto.getRandomValues&&crypto.getRandomValues.bind(crypto)||"undefined"!==typeof msCrypto&&"function"===typeof msCrypto.getRandomValues&&msCrypto.getRandomValues.bind(msCrypto),!r))throw new Error("crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported");return r(i)}var s=/^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i;function a(e){return"string"===typeof e&&s.test(e)}for(var u=a,c=[],l=0;l<256;++l)c.push((l+256).toString(16).substr(1));function h(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,n=(c[e[t+0]]+c[e[t+1]]+c[e[t+2]]+c[e[t+3]]+"-"+c[e[t+4]]+c[e[t+5]]+"-"+c[e[t+6]]+c[e[t+7]]+"-"+c[e[t+8]]+c[e[t+9]]+"-"+c[e[t+10]]+c[e[t+11]]+c[e[t+12]]+c[e[t+13]]+c[e[t+14]]+c[e[t+15]]).toLowerCase();if(!u(n))throw TypeError("Stringified UUID is invalid");return n}var d=h;function f(e,t,n){e=e||{};var r=e.random||(e.rng||o)();if(r[6]=15&r[6]|64,r[8]=63&r[8]|128,t){n=n||0;for(var i=0;i<16;++i)t[n+i]=r[i];return t}return d(r)}var p=f},89:function(e,t){"use strict";t.Z=(e,t)=>{const n=e.__vccOpts||e;for(const[r,i]of t)n[r]=i;return n}},678:function(e,t,n){"use strict";n.d(t,{PO:function(){return q},p7:function(){return tt}});n(1703);var r=n(3396),i=n(4870);
/*!
  * vue-router v4.0.14
  * (c) 2022 Eduardo San Martin Morote
  * @license MIT
  */
const o="function"===typeof Symbol&&"symbol"===typeof Symbol.toStringTag,s=e=>o?Symbol(e):"_vr_"+e,a=s("rvlm"),u=s("rvd"),c=s("r"),l=s("rl"),h=s("rvl"),d="undefined"!==typeof window;function f(e){return e.__esModule||o&&"Module"===e[Symbol.toStringTag]}const p=Object.assign;function m(e,t){const n={};for(const r in t){const i=t[r];n[r]=Array.isArray(i)?i.map(e):e(i)}return n}const v=()=>{};const g=/\/$/,_=e=>e.replace(g,"");function y(e,t,n="/"){let r,i={},o="",s="";const a=t.indexOf("?"),u=t.indexOf("#",a>-1?a:0);return a>-1&&(r=t.slice(0,a),o=t.slice(a+1,u>-1?u:t.length),i=e(o)),u>-1&&(r=r||t.slice(0,u),s=t.slice(u,t.length)),r=S(null!=r?r:t,n),{fullPath:r+(o&&"?")+o+s,path:r,query:i,hash:s}}function b(e,t){const n=t.query?e(t.query):"";return t.path+(n&&"?")+n+(t.hash||"")}function w(e,t){return t&&e.toLowerCase().startsWith(t.toLowerCase())?e.slice(t.length)||"/":e}function I(e,t,n){const r=t.matched.length-1,i=n.matched.length-1;return r>-1&&r===i&&E(t.matched[r],n.matched[i])&&k(t.params,n.params)&&e(t.query)===e(n.query)&&t.hash===n.hash}function E(e,t){return(e.aliasOf||e)===(t.aliasOf||t)}function k(e,t){if(Object.keys(e).length!==Object.keys(t).length)return!1;for(const n in e)if(!T(e[n],t[n]))return!1;return!0}function T(e,t){return Array.isArray(e)?O(e,t):Array.isArray(t)?O(t,e):e===t}function O(e,t){return Array.isArray(t)?e.length===t.length&&e.every(((e,n)=>e===t[n])):1===e.length&&e[0]===t}function S(e,t){if(e.startsWith("/"))return e;if(!e)return t;const n=t.split("/"),r=e.split("/");let i,o,s=n.length-1;for(i=0;i<r.length;i++)if(o=r[i],1!==s&&"."!==o){if(".."!==o)break;s--}return n.slice(0,s).join("/")+"/"+r.slice(i-(i===r.length?1:0)).join("/")}var R,A;(function(e){e["pop"]="pop",e["push"]="push"})(R||(R={})),function(e){e["back"]="back",e["forward"]="forward",e["unknown"]=""}(A||(A={}));function C(e){if(!e)if(d){const t=document.querySelector("base");e=t&&t.getAttribute("href")||"/",e=e.replace(/^\w+:\/\/[^\/]+/,"")}else e="/";return"/"!==e[0]&&"#"!==e[0]&&(e="/"+e),_(e)}const x=/^[^#]+#/;function P(e,t){return e.replace(x,"#")+t}function N(e,t){const n=document.documentElement.getBoundingClientRect(),r=e.getBoundingClientRect();return{behavior:t.behavior,left:r.left-n.left-(t.left||0),top:r.top-n.top-(t.top||0)}}const F=()=>({left:window.pageXOffset,top:window.pageYOffset});function D(e){let t;if("el"in e){const n=e.el,r="string"===typeof n&&n.startsWith("#");0;const i="string"===typeof n?r?document.getElementById(n.slice(1)):document.querySelector(n):n;if(!i)return;t=N(i,e)}else t=e;"scrollBehavior"in document.documentElement.style?window.scrollTo(t):window.scrollTo(null!=t.left?t.left:window.pageXOffset,null!=t.top?t.top:window.pageYOffset)}function j(e,t){const n=history.state?history.state.position-t:-1;return n+e}const L=new Map;function U(e,t){L.set(e,t)}function M(e){const t=L.get(e);return L.delete(e),t}let $=()=>location.protocol+"//"+location.host;function V(e,t){const{pathname:n,search:r,hash:i}=t,o=e.indexOf("#");if(o>-1){let t=i.includes(e.slice(o))?e.slice(o).length:1,n=i.slice(t);return"/"!==n[0]&&(n="/"+n),w(n,"")}const s=w(n,e);return s+r+i}function z(e,t,n,r){let i=[],o=[],s=null;const a=({state:o})=>{const a=V(e,location),u=n.value,c=t.value;let l=0;if(o){if(n.value=a,t.value=o,s&&s===u)return void(s=null);l=c?o.position-c.position:0}else r(a);i.forEach((e=>{e(n.value,u,{delta:l,type:R.pop,direction:l?l>0?A.forward:A.back:A.unknown})}))};function u(){s=n.value}function c(e){i.push(e);const t=()=>{const t=i.indexOf(e);t>-1&&i.splice(t,1)};return o.push(t),t}function l(){const{history:e}=window;e.state&&e.replaceState(p({},e.state,{scroll:F()}),"")}function h(){for(const e of o)e();o=[],window.removeEventListener("popstate",a),window.removeEventListener("beforeunload",l)}return window.addEventListener("popstate",a),window.addEventListener("beforeunload",l),{pauseListeners:u,listen:c,destroy:h}}function B(e,t,n,r=!1,i=!1){return{back:e,current:t,forward:n,replaced:r,position:window.history.length,scroll:i?F():null}}function H(e){const{history:t,location:n}=window,r={value:V(e,n)},i={value:t.state};function o(r,o,s){const a=e.indexOf("#"),u=a>-1?(n.host&&document.querySelector("base")?e:e.slice(a))+r:$()+e+r;try{t[s?"replaceState":"pushState"](o,"",u),i.value=o}catch(c){console.error(c),n[s?"replace":"assign"](u)}}function s(e,n){const s=p({},t.state,B(i.value.back,e,i.value.forward,!0),n,{position:i.value.position});o(e,s,!0),r.value=e}function a(e,n){const s=p({},i.value,t.state,{forward:e,scroll:F()});o(s.current,s,!0);const a=p({},B(r.value,e,null),{position:s.position+1},n);o(e,a,!1),r.value=e}return i.value||o(r.value,{back:null,current:r.value,forward:null,position:t.length-1,replaced:!0,scroll:null},!0),{location:r,state:i,push:a,replace:s}}function q(e){e=C(e);const t=H(e),n=z(e,t.state,t.location,t.replace);function r(e,t=!0){t||n.pauseListeners(),history.go(e)}const i=p({location:"",base:e,go:r,createHref:P.bind(null,e)},t,n);return Object.defineProperty(i,"location",{enumerable:!0,get:()=>t.location.value}),Object.defineProperty(i,"state",{enumerable:!0,get:()=>t.state.value}),i}function W(e){return"string"===typeof e||e&&"object"===typeof e}function G(e){return"string"===typeof e||"symbol"===typeof e}const J={path:"/",name:void 0,params:{},query:{},hash:"",fullPath:"/",matched:[],meta:{},redirectedFrom:void 0},K=s("nf");var Y;(function(e){e[e["aborted"]=4]="aborted",e[e["cancelled"]=8]="cancelled",e[e["duplicated"]=16]="duplicated"})(Y||(Y={}));function Z(e,t){return p(new Error,{type:e,[K]:!0},t)}function X(e,t){return e instanceof Error&&K in e&&(null==t||!!(e.type&t))}const Q="[^/]+?",ee={sensitive:!1,strict:!1,start:!0,end:!0},te=/[.+*?^${}()[\]/\\]/g;function ne(e,t){const n=p({},ee,t),r=[];let i=n.start?"^":"";const o=[];for(const l of e){const e=l.length?[]:[90];n.strict&&!l.length&&(i+="/");for(let t=0;t<l.length;t++){const r=l[t];let s=40+(n.sensitive?.25:0);if(0===r.type)t||(i+="/"),i+=r.value.replace(te,"\\$&"),s+=40;else if(1===r.type){const{value:e,repeatable:n,optional:a,regexp:u}=r;o.push({name:e,repeatable:n,optional:a});const h=u||Q;if(h!==Q){s+=10;try{new RegExp(`(${h})`)}catch(c){throw new Error(`Invalid custom RegExp for param "${e}" (${h}): `+c.message)}}let d=n?`((?:${h})(?:/(?:${h}))*)`:`(${h})`;t||(d=a&&l.length<2?`(?:/${d})`:"/"+d),a&&(d+="?"),i+=d,s+=20,a&&(s+=-8),n&&(s+=-20),".*"===h&&(s+=-50)}e.push(s)}r.push(e)}if(n.strict&&n.end){const e=r.length-1;r[e][r[e].length-1]+=.7000000000000001}n.strict||(i+="/?"),n.end?i+="$":n.strict&&(i+="(?:/|$)");const s=new RegExp(i,n.sensitive?"":"i");function a(e){const t=e.match(s),n={};if(!t)return null;for(let r=1;r<t.length;r++){const e=t[r]||"",i=o[r-1];n[i.name]=e&&i.repeatable?e.split("/"):e}return n}function u(t){let n="",r=!1;for(const i of e){r&&n.endsWith("/")||(n+="/"),r=!1;for(const e of i)if(0===e.type)n+=e.value;else if(1===e.type){const{value:o,repeatable:s,optional:a}=e,u=o in t?t[o]:"";if(Array.isArray(u)&&!s)throw new Error(`Provided param "${o}" is an array but it is not repeatable (* or + modifiers)`);const c=Array.isArray(u)?u.join("/"):u;if(!c){if(!a)throw new Error(`Missing required param "${o}"`);i.length<2&&(n.endsWith("/")?n=n.slice(0,-1):r=!0)}n+=c}}return n}return{re:s,score:r,keys:o,parse:a,stringify:u}}function re(e,t){let n=0;while(n<e.length&&n<t.length){const r=t[n]-e[n];if(r)return r;n++}return e.length<t.length?1===e.length&&80===e[0]?-1:1:e.length>t.length?1===t.length&&80===t[0]?1:-1:0}function ie(e,t){let n=0;const r=e.score,i=t.score;while(n<r.length&&n<i.length){const e=re(r[n],i[n]);if(e)return e;n++}return i.length-r.length}const oe={type:0,value:""},se=/[a-zA-Z0-9_]/;function ae(e){if(!e)return[[]];if("/"===e)return[[oe]];if(!e.startsWith("/"))throw new Error(`Invalid path "${e}"`);function t(e){throw new Error(`ERR (${n})/"${c}": ${e}`)}let n=0,r=n;const i=[];let o;function s(){o&&i.push(o),o=[]}let a,u=0,c="",l="";function h(){c&&(0===n?o.push({type:0,value:c}):1===n||2===n||3===n?(o.length>1&&("*"===a||"+"===a)&&t(`A repeatable param (${c}) must be alone in its segment. eg: '/:ids+.`),o.push({type:1,value:c,regexp:l,repeatable:"*"===a||"+"===a,optional:"*"===a||"?"===a})):t("Invalid state to consume buffer"),c="")}function d(){c+=a}while(u<e.length)if(a=e[u++],"\\"!==a||2===n)switch(n){case 0:"/"===a?(c&&h(),s()):":"===a?(h(),n=1):d();break;case 4:d(),n=r;break;case 1:"("===a?n=2:se.test(a)?d():(h(),n=0,"*"!==a&&"?"!==a&&"+"!==a&&u--);break;case 2:")"===a?"\\"==l[l.length-1]?l=l.slice(0,-1)+a:n=3:l+=a;break;case 3:h(),n=0,"*"!==a&&"?"!==a&&"+"!==a&&u--,l="";break;default:t("Unknown state");break}else r=n,n=4;return 2===n&&t(`Unfinished custom RegExp for param "${c}"`),h(),s(),i}function ue(e,t,n){const r=ne(ae(e.path),n);const i=p(r,{record:e,parent:t,children:[],alias:[]});return t&&!i.record.aliasOf===!t.record.aliasOf&&t.children.push(i),i}function ce(e,t){const n=[],r=new Map;function i(e){return r.get(e)}function o(e,n,r){const i=!r,a=he(e);a.aliasOf=r&&r.record;const c=me(t,e),l=[a];if("alias"in e){const t="string"===typeof e.alias?[e.alias]:e.alias;for(const e of t)l.push(p({},a,{components:r?r.record.components:a.components,path:e,aliasOf:r?r.record:a}))}let h,d;for(const t of l){const{path:l}=t;if(n&&"/"!==l[0]){const e=n.record.path,r="/"===e[e.length-1]?"":"/";t.path=n.record.path+(l&&r+l)}if(h=ue(t,n,c),r?r.alias.push(h):(d=d||h,d!==h&&d.alias.push(h),i&&e.name&&!fe(h)&&s(e.name)),"children"in a){const e=a.children;for(let t=0;t<e.length;t++)o(e[t],h,r&&r.children[t])}r=r||h,u(h)}return d?()=>{s(d)}:v}function s(e){if(G(e)){const t=r.get(e);t&&(r.delete(e),n.splice(n.indexOf(t),1),t.children.forEach(s),t.alias.forEach(s))}else{const t=n.indexOf(e);t>-1&&(n.splice(t,1),e.record.name&&r.delete(e.record.name),e.children.forEach(s),e.alias.forEach(s))}}function a(){return n}function u(e){let t=0;while(t<n.length&&ie(e,n[t])>=0&&(e.record.path!==n[t].record.path||!ve(e,n[t])))t++;n.splice(t,0,e),e.record.name&&!fe(e)&&r.set(e.record.name,e)}function c(e,t){let i,o,s,a={};if("name"in e&&e.name){if(i=r.get(e.name),!i)throw Z(1,{location:e});s=i.record.name,a=p(le(t.params,i.keys.filter((e=>!e.optional)).map((e=>e.name))),e.params),o=i.stringify(a)}else if("path"in e)o=e.path,i=n.find((e=>e.re.test(o))),i&&(a=i.parse(o),s=i.record.name);else{if(i=t.name?r.get(t.name):n.find((e=>e.re.test(t.path))),!i)throw Z(1,{location:e,currentLocation:t});s=i.record.name,a=p({},t.params,e.params),o=i.stringify(a)}const u=[];let c=i;while(c)u.unshift(c.record),c=c.parent;return{name:s,path:o,params:a,matched:u,meta:pe(u)}}return t=me({strict:!1,end:!0,sensitive:!1},t),e.forEach((e=>o(e))),{addRoute:o,resolve:c,removeRoute:s,getRoutes:a,getRecordMatcher:i}}function le(e,t){const n={};for(const r of t)r in e&&(n[r]=e[r]);return n}function he(e){return{path:e.path,redirect:e.redirect,name:e.name,meta:e.meta||{},aliasOf:void 0,beforeEnter:e.beforeEnter,props:de(e),children:e.children||[],instances:{},leaveGuards:new Set,updateGuards:new Set,enterCallbacks:{},components:"components"in e?e.components||{}:{default:e.component}}}function de(e){const t={},n=e.props||!1;if("component"in e)t.default=n;else for(const r in e.components)t[r]="boolean"===typeof n?n:n[r];return t}function fe(e){while(e){if(e.record.aliasOf)return!0;e=e.parent}return!1}function pe(e){return e.reduce(((e,t)=>p(e,t.meta)),{})}function me(e,t){const n={};for(const r in e)n[r]=r in t?t[r]:e[r];return n}function ve(e,t){return t.children.some((t=>t===e||ve(e,t)))}const ge=/#/g,_e=/&/g,ye=/\//g,be=/=/g,we=/\?/g,Ie=/\+/g,Ee=/%5B/g,ke=/%5D/g,Te=/%5E/g,Oe=/%60/g,Se=/%7B/g,Re=/%7C/g,Ae=/%7D/g,Ce=/%20/g;function xe(e){return encodeURI(""+e).replace(Re,"|").replace(Ee,"[").replace(ke,"]")}function Pe(e){return xe(e).replace(Se,"{").replace(Ae,"}").replace(Te,"^")}function Ne(e){return xe(e).replace(Ie,"%2B").replace(Ce,"+").replace(ge,"%23").replace(_e,"%26").replace(Oe,"`").replace(Se,"{").replace(Ae,"}").replace(Te,"^")}function Fe(e){return Ne(e).replace(be,"%3D")}function De(e){return xe(e).replace(ge,"%23").replace(we,"%3F")}function je(e){return null==e?"":De(e).replace(ye,"%2F")}function Le(e){try{return decodeURIComponent(""+e)}catch(t){}return""+e}function Ue(e){const t={};if(""===e||"?"===e)return t;const n="?"===e[0],r=(n?e.slice(1):e).split("&");for(let i=0;i<r.length;++i){const e=r[i].replace(Ie," "),n=e.indexOf("="),o=Le(n<0?e:e.slice(0,n)),s=n<0?null:Le(e.slice(n+1));if(o in t){let e=t[o];Array.isArray(e)||(e=t[o]=[e]),e.push(s)}else t[o]=s}return t}function Me(e){let t="";for(let n in e){const r=e[n];if(n=Fe(n),null==r){void 0!==r&&(t+=(t.length?"&":"")+n);continue}const i=Array.isArray(r)?r.map((e=>e&&Ne(e))):[r&&Ne(r)];i.forEach((e=>{void 0!==e&&(t+=(t.length?"&":"")+n,null!=e&&(t+="="+e))}))}return t}function $e(e){const t={};for(const n in e){const r=e[n];void 0!==r&&(t[n]=Array.isArray(r)?r.map((e=>null==e?null:""+e)):null==r?r:""+r)}return t}function Ve(){let e=[];function t(t){return e.push(t),()=>{const n=e.indexOf(t);n>-1&&e.splice(n,1)}}function n(){e=[]}return{add:t,list:()=>e,reset:n}}function ze(e,t,n,r,i){const o=r&&(r.enterCallbacks[i]=r.enterCallbacks[i]||[]);return()=>new Promise(((s,a)=>{const u=e=>{!1===e?a(Z(4,{from:n,to:t})):e instanceof Error?a(e):W(e)?a(Z(2,{from:t,to:e})):(o&&r.enterCallbacks[i]===o&&"function"===typeof e&&o.push(e),s())},c=e.call(r&&r.instances[i],t,n,u);let l=Promise.resolve(c);e.length<3&&(l=l.then(u)),l.catch((e=>a(e)))}))}function Be(e,t,n,r){const i=[];for(const o of e)for(const e in o.components){let s=o.components[e];if("beforeRouteEnter"===t||o.instances[e])if(He(s)){const a=s.__vccOpts||s,u=a[t];u&&i.push(ze(u,n,r,o,e))}else{let a=s();0,i.push((()=>a.then((i=>{if(!i)return Promise.reject(new Error(`Couldn't resolve component "${e}" at "${o.path}"`));const s=f(i)?i.default:i;o.components[e]=s;const a=s.__vccOpts||s,u=a[t];return u&&ze(u,n,r,o,e)()}))))}}return i}function He(e){return"object"===typeof e||"displayName"in e||"props"in e||"__vccOpts"in e}function qe(e){const t=(0,r.f3)(c),n=(0,r.f3)(l),o=(0,r.Fl)((()=>t.resolve((0,i.SU)(e.to)))),s=(0,r.Fl)((()=>{const{matched:e}=o.value,{length:t}=e,r=e[t-1],i=n.matched;if(!r||!i.length)return-1;const s=i.findIndex(E.bind(null,r));if(s>-1)return s;const a=Ye(e[t-2]);return t>1&&Ye(r)===a&&i[i.length-1].path!==a?i.findIndex(E.bind(null,e[t-2])):s})),a=(0,r.Fl)((()=>s.value>-1&&Ke(n.params,o.value.params))),u=(0,r.Fl)((()=>s.value>-1&&s.value===n.matched.length-1&&k(n.params,o.value.params)));function h(n={}){return Je(n)?t[(0,i.SU)(e.replace)?"replace":"push"]((0,i.SU)(e.to)).catch(v):Promise.resolve()}return{route:o,href:(0,r.Fl)((()=>o.value.href)),isActive:a,isExactActive:u,navigate:h}}const We=(0,r.aZ)({name:"RouterLink",props:{to:{type:[String,Object],required:!0},replace:Boolean,activeClass:String,exactActiveClass:String,custom:Boolean,ariaCurrentValue:{type:String,default:"page"}},useLink:qe,setup(e,{slots:t}){const n=(0,i.qj)(qe(e)),{options:o}=(0,r.f3)(c),s=(0,r.Fl)((()=>({[Ze(e.activeClass,o.linkActiveClass,"router-link-active")]:n.isActive,[Ze(e.exactActiveClass,o.linkExactActiveClass,"router-link-exact-active")]:n.isExactActive})));return()=>{const i=t.default&&t.default(n);return e.custom?i:(0,r.h)("a",{"aria-current":n.isExactActive?e.ariaCurrentValue:null,href:n.href,onClick:n.navigate,class:s.value},i)}}}),Ge=We;function Je(e){if(!(e.metaKey||e.altKey||e.ctrlKey||e.shiftKey)&&!e.defaultPrevented&&(void 0===e.button||0===e.button)){if(e.currentTarget&&e.currentTarget.getAttribute){const t=e.currentTarget.getAttribute("target");if(/\b_blank\b/i.test(t))return}return e.preventDefault&&e.preventDefault(),!0}}function Ke(e,t){for(const n in t){const r=t[n],i=e[n];if("string"===typeof r){if(r!==i)return!1}else if(!Array.isArray(i)||i.length!==r.length||r.some(((e,t)=>e!==i[t])))return!1}return!0}function Ye(e){return e?e.aliasOf?e.aliasOf.path:e.path:""}const Ze=(e,t,n)=>null!=e?e:null!=t?t:n,Xe=(0,r.aZ)({name:"RouterView",inheritAttrs:!1,props:{name:{type:String,default:"default"},route:Object},setup(e,{attrs:t,slots:n}){const o=(0,r.f3)(h),s=(0,r.Fl)((()=>e.route||o.value)),c=(0,r.f3)(u,0),l=(0,r.Fl)((()=>s.value.matched[c]));(0,r.JJ)(u,c+1),(0,r.JJ)(a,l),(0,r.JJ)(h,s);const d=(0,i.iH)();return(0,r.YP)((()=>[d.value,l.value,e.name]),(([e,t,n],[r,i,o])=>{t&&(t.instances[n]=e,i&&i!==t&&e&&e===r&&(t.leaveGuards.size||(t.leaveGuards=i.leaveGuards),t.updateGuards.size||(t.updateGuards=i.updateGuards))),!e||!t||i&&E(t,i)&&r||(t.enterCallbacks[n]||[]).forEach((t=>t(e)))}),{flush:"post"}),()=>{const i=s.value,o=l.value,a=o&&o.components[e.name],u=e.name;if(!a)return Qe(n.default,{Component:a,route:i});const c=o.props[e.name],h=c?!0===c?i.params:"function"===typeof c?c(i):c:null,f=e=>{e.component.isUnmounted&&(o.instances[u]=null)},m=(0,r.h)(a,p({},h,t,{onVnodeUnmounted:f,ref:d}));return Qe(n.default,{Component:m,route:i})||m}}});function Qe(e,t){if(!e)return null;const n=e(t);return 1===n.length?n[0]:n}const et=Xe;function tt(e){const t=ce(e.routes,e),n=e.parseQuery||Ue,o=e.stringifyQuery||Me,s=e.history;const a=Ve(),u=Ve(),f=Ve(),g=(0,i.XI)(J);let _=J;d&&e.scrollBehavior&&"scrollRestoration"in history&&(history.scrollRestoration="manual");const w=m.bind(null,(e=>""+e)),E=m.bind(null,je),k=m.bind(null,Le);function T(e,n){let r,i;return G(e)?(r=t.getRecordMatcher(e),i=n):i=e,t.addRoute(i,r)}function O(e){const n=t.getRecordMatcher(e);n&&t.removeRoute(n)}function S(){return t.getRoutes().map((e=>e.record))}function A(e){return!!t.getRecordMatcher(e)}function C(e,r){if(r=p({},r||g.value),"string"===typeof e){const i=y(n,e,r.path),o=t.resolve({path:i.path},r),a=s.createHref(i.fullPath);return p(i,o,{params:k(o.params),hash:Le(i.hash),redirectedFrom:void 0,href:a})}let i;if("path"in e)i=p({},e,{path:y(n,e.path,r.path).path});else{const t=p({},e.params);for(const e in t)null==t[e]&&delete t[e];i=p({},e,{params:E(e.params)}),r.params=E(r.params)}const a=t.resolve(i,r),u=e.hash||"";a.params=w(k(a.params));const c=b(o,p({},e,{hash:Pe(u),path:a.path})),l=s.createHref(c);return p({fullPath:c,hash:u,query:o===Me?$e(e.query):e.query||{}},a,{redirectedFrom:void 0,href:l})}function x(e){return"string"===typeof e?y(n,e,g.value.path):p({},e)}function P(e,t){if(_!==e)return Z(8,{from:t,to:e})}function N(e){return V(e)}function L(e){return N(p(x(e),{replace:!0}))}function $(e){const t=e.matched[e.matched.length-1];if(t&&t.redirect){const{redirect:n}=t;let r="function"===typeof n?n(e):n;return"string"===typeof r&&(r=r.includes("?")||r.includes("#")?r=x(r):{path:r},r.params={}),p({query:e.query,hash:e.hash,params:e.params},r)}}function V(e,t){const n=_=C(e),r=g.value,i=e.state,s=e.force,a=!0===e.replace,u=$(n);if(u)return V(p(x(u),{state:i,force:s,replace:a}),t||n);const c=n;let l;return c.redirectedFrom=t,!s&&I(o,r,n)&&(l=Z(16,{to:c,from:r}),ie(r,r,!0,!1)),(l?Promise.resolve(l):B(c,r)).catch((e=>X(e)?X(e,2)?e:re(e):te(e,c,r))).then((e=>{if(e){if(X(e,2))return V(p(x(e.to),{state:i,force:s,replace:a}),t||c)}else e=q(c,r,!0,a,i);return H(c,r,e),e}))}function z(e,t){const n=P(e,t);return n?Promise.reject(n):Promise.resolve()}function B(e,t){let n;const[r,i,o]=rt(e,t);n=Be(r.reverse(),"beforeRouteLeave",e,t);for(const a of r)a.leaveGuards.forEach((r=>{n.push(ze(r,e,t))}));const s=z.bind(null,e,t);return n.push(s),nt(n).then((()=>{n=[];for(const r of a.list())n.push(ze(r,e,t));return n.push(s),nt(n)})).then((()=>{n=Be(i,"beforeRouteUpdate",e,t);for(const r of i)r.updateGuards.forEach((r=>{n.push(ze(r,e,t))}));return n.push(s),nt(n)})).then((()=>{n=[];for(const r of e.matched)if(r.beforeEnter&&!t.matched.includes(r))if(Array.isArray(r.beforeEnter))for(const i of r.beforeEnter)n.push(ze(i,e,t));else n.push(ze(r.beforeEnter,e,t));return n.push(s),nt(n)})).then((()=>(e.matched.forEach((e=>e.enterCallbacks={})),n=Be(o,"beforeRouteEnter",e,t),n.push(s),nt(n)))).then((()=>{n=[];for(const r of u.list())n.push(ze(r,e,t));return n.push(s),nt(n)})).catch((e=>X(e,8)?e:Promise.reject(e)))}function H(e,t,n){for(const r of f.list())r(e,t,n)}function q(e,t,n,r,i){const o=P(e,t);if(o)return o;const a=t===J,u=d?history.state:{};n&&(r||a?s.replace(e.fullPath,p({scroll:a&&u&&u.scroll},i)):s.push(e.fullPath,i)),g.value=e,ie(e,t,n,a),re()}let W;function K(){W=s.listen(((e,t,n)=>{const r=C(e),i=$(r);if(i)return void V(p(i,{replace:!0}),r).catch(v);_=r;const o=g.value;d&&U(j(o.fullPath,n.delta),F()),B(r,o).catch((e=>X(e,12)?e:X(e,2)?(V(e.to,r).then((e=>{X(e,20)&&!n.delta&&n.type===R.pop&&s.go(-1,!1)})).catch(v),Promise.reject()):(n.delta&&s.go(-n.delta,!1),te(e,r,o)))).then((e=>{e=e||q(r,o,!1),e&&(n.delta?s.go(-n.delta,!1):n.type===R.pop&&X(e,20)&&s.go(-1,!1)),H(r,o,e)})).catch(v)}))}let Y,Q=Ve(),ee=Ve();function te(e,t,n){re(e);const r=ee.list();return r.length?r.forEach((r=>r(e,t,n))):console.error(e),Promise.reject(e)}function ne(){return Y&&g.value!==J?Promise.resolve():new Promise(((e,t)=>{Q.add([e,t])}))}function re(e){return Y||(Y=!e,K(),Q.list().forEach((([t,n])=>e?n(e):t())),Q.reset()),e}function ie(t,n,i,o){const{scrollBehavior:s}=e;if(!d||!s)return Promise.resolve();const a=!i&&M(j(t.fullPath,0))||(o||!i)&&history.state&&history.state.scroll||null;return(0,r.Y3)().then((()=>s(t,n,a))).then((e=>e&&D(e))).catch((e=>te(e,t,n)))}const oe=e=>s.go(e);let se;const ae=new Set,ue={currentRoute:g,addRoute:T,removeRoute:O,hasRoute:A,getRoutes:S,resolve:C,options:e,push:N,replace:L,go:oe,back:()=>oe(-1),forward:()=>oe(1),beforeEach:a.add,beforeResolve:u.add,afterEach:f.add,onError:ee.add,isReady:ne,install(e){const t=this;e.component("RouterLink",Ge),e.component("RouterView",et),e.config.globalProperties.$router=t,Object.defineProperty(e.config.globalProperties,"$route",{enumerable:!0,get:()=>(0,i.SU)(g)}),d&&!se&&g.value===J&&(se=!0,N(s.location).catch((e=>{0})));const n={};for(const i in J)n[i]=(0,r.Fl)((()=>g.value[i]));e.provide(c,t),e.provide(l,(0,i.qj)(n)),e.provide(h,g);const o=e.unmount;ae.add(e),e.unmount=function(){ae.delete(e),ae.size<1&&(_=J,W&&W(),g.value=J,se=!1,Y=!1),o()}}};return ue}function nt(e){return e.reduce(((e,t)=>e.then((()=>t()))),Promise.resolve())}function rt(e,t){const n=[],r=[],i=[],o=Math.max(t.matched.length,e.matched.length);for(let s=0;s<o;s++){const o=t.matched[s];o&&(e.matched.find((e=>E(e,o)))?r.push(o):n.push(o));const a=e.matched[s];a&&(t.matched.find((e=>E(e,a)))||i.push(a))}return[n,r,i]}},65:function(e,t,n){"use strict";n.d(t,{MT:function(){return te},oR:function(){return g}});n(1703);var r=n(3396),i=n(4870);function o(){return s().__VUE_DEVTOOLS_GLOBAL_HOOK__}function s(){return"undefined"!==typeof navigator&&"undefined"!==typeof window?window:"undefined"!==typeof n.g?n.g:{}}const a="function"===typeof Proxy,u="devtools-plugin:setup",c="plugin:settings:set";let l,h;function d(){var e;return void 0!==l||("undefined"!==typeof window&&window.performance?(l=!0,h=window.performance):"undefined"!==typeof n.g&&(null===(e=n.g.perf_hooks)||void 0===e?void 0:e.performance)?(l=!0,h=n.g.perf_hooks.performance):l=!1),l}function f(){return d()?h.now():Date.now()}class p{constructor(e,t){this.target=null,this.targetQueue=[],this.onQueue=[],this.plugin=e,this.hook=t;const n={};if(e.settings)for(const s in e.settings){const t=e.settings[s];n[s]=t.defaultValue}const r=`__vue-devtools-plugin-settings__${e.id}`;let i=Object.assign({},n);try{const e=localStorage.getItem(r),t=JSON.parse(e);Object.assign(i,t)}catch(o){}this.fallbacks={getSettings(){return i},setSettings(e){try{localStorage.setItem(r,JSON.stringify(e))}catch(o){}i=e},now(){return f()}},t&&t.on(c,((e,t)=>{e===this.plugin.id&&this.fallbacks.setSettings(t)})),this.proxiedOn=new Proxy({},{get:(e,t)=>this.target?this.target.on[t]:(...e)=>{this.onQueue.push({method:t,args:e})}}),this.proxiedTarget=new Proxy({},{get:(e,t)=>this.target?this.target[t]:"on"===t?this.proxiedOn:Object.keys(this.fallbacks).includes(t)?(...e)=>(this.targetQueue.push({method:t,args:e,resolve:()=>{}}),this.fallbacks[t](...e)):(...e)=>new Promise((n=>{this.targetQueue.push({method:t,args:e,resolve:n})}))})}async setRealTarget(e){this.target=e;for(const t of this.onQueue)this.target.on[t.method](...t.args);for(const t of this.targetQueue)t.resolve(await this.target[t.method](...t.args))}}function m(e,t){const n=e,r=s(),i=o(),c=a&&n.enableEarlyProxy;if(!i||!r.__VUE_DEVTOOLS_PLUGIN_API_AVAILABLE__&&c){const e=c?new p(n,i):null,o=r.__VUE_DEVTOOLS_PLUGINS__=r.__VUE_DEVTOOLS_PLUGINS__||[];o.push({pluginDescriptor:n,setupFn:t,proxy:e}),e&&t(e.proxiedTarget)}else i.emit(u,e,t)}
/*!
 * vuex v4.0.2
 * (c) 2021 Evan You
 * @license MIT
 */
var v="store";function g(e){return void 0===e&&(e=null),(0,r.f3)(null!==e?e:v)}function _(e,t){Object.keys(e).forEach((function(n){return t(e[n],n)}))}function y(e){return null!==e&&"object"===typeof e}function b(e){return e&&"function"===typeof e.then}function w(e,t){return function(){return e(t)}}function I(e,t,n){return t.indexOf(e)<0&&(n&&n.prepend?t.unshift(e):t.push(e)),function(){var n=t.indexOf(e);n>-1&&t.splice(n,1)}}function E(e,t){e._actions=Object.create(null),e._mutations=Object.create(null),e._wrappedGetters=Object.create(null),e._modulesNamespaceMap=Object.create(null);var n=e.state;T(e,n,[],e._modules.root,!0),k(e,n,t)}function k(e,t,n){var r=e._state;e.getters={},e._makeLocalGettersCache=Object.create(null);var o=e._wrappedGetters,s={};_(o,(function(t,n){s[n]=w(t,e),Object.defineProperty(e.getters,n,{get:function(){return s[n]()},enumerable:!0})})),e._state=(0,i.qj)({data:t}),e.strict&&x(e),r&&n&&e._withCommit((function(){r.data=null}))}function T(e,t,n,r,i){var o=!n.length,s=e._modules.getNamespace(n);if(r.namespaced&&(e._modulesNamespaceMap[s],e._modulesNamespaceMap[s]=r),!o&&!i){var a=P(t,n.slice(0,-1)),u=n[n.length-1];e._withCommit((function(){a[u]=r.state}))}var c=r.context=O(e,s,n);r.forEachMutation((function(t,n){var r=s+n;R(e,r,t,c)})),r.forEachAction((function(t,n){var r=t.root?n:s+n,i=t.handler||t;A(e,r,i,c)})),r.forEachGetter((function(t,n){var r=s+n;C(e,r,t,c)})),r.forEachChild((function(r,o){T(e,t,n.concat(o),r,i)}))}function O(e,t,n){var r=""===t,i={dispatch:r?e.dispatch:function(n,r,i){var o=N(n,r,i),s=o.payload,a=o.options,u=o.type;return a&&a.root||(u=t+u),e.dispatch(u,s)},commit:r?e.commit:function(n,r,i){var o=N(n,r,i),s=o.payload,a=o.options,u=o.type;a&&a.root||(u=t+u),e.commit(u,s,a)}};return Object.defineProperties(i,{getters:{get:r?function(){return e.getters}:function(){return S(e,t)}},state:{get:function(){return P(e.state,n)}}}),i}function S(e,t){if(!e._makeLocalGettersCache[t]){var n={},r=t.length;Object.keys(e.getters).forEach((function(i){if(i.slice(0,r)===t){var o=i.slice(r);Object.defineProperty(n,o,{get:function(){return e.getters[i]},enumerable:!0})}})),e._makeLocalGettersCache[t]=n}return e._makeLocalGettersCache[t]}function R(e,t,n,r){var i=e._mutations[t]||(e._mutations[t]=[]);i.push((function(t){n.call(e,r.state,t)}))}function A(e,t,n,r){var i=e._actions[t]||(e._actions[t]=[]);i.push((function(t){var i=n.call(e,{dispatch:r.dispatch,commit:r.commit,getters:r.getters,state:r.state,rootGetters:e.getters,rootState:e.state},t);return b(i)||(i=Promise.resolve(i)),e._devtoolHook?i.catch((function(t){throw e._devtoolHook.emit("vuex:error",t),t})):i}))}function C(e,t,n,r){e._wrappedGetters[t]||(e._wrappedGetters[t]=function(e){return n(r.state,r.getters,e.state,e.getters)})}function x(e){(0,r.YP)((function(){return e._state.data}),(function(){0}),{deep:!0,flush:"sync"})}function P(e,t){return t.reduce((function(e,t){return e[t]}),e)}function N(e,t,n){return y(e)&&e.type&&(n=t,t=e,e=e.type),{type:e,payload:t,options:n}}var F="vuex bindings",D="vuex:mutations",j="vuex:actions",L="vuex",U=0;function M(e,t){m({id:"org.vuejs.vuex",app:e,label:"Vuex",homepage:"https://next.vuex.vuejs.org/",logo:"https://vuejs.org/images/icons/favicon-96x96.png",packageName:"vuex",componentStateTypes:[F]},(function(n){n.addTimelineLayer({id:D,label:"Vuex Mutations",color:$}),n.addTimelineLayer({id:j,label:"Vuex Actions",color:$}),n.addInspector({id:L,label:"Vuex",icon:"storage",treeFilterPlaceholder:"Filter stores..."}),n.on.getInspectorTree((function(n){if(n.app===e&&n.inspectorId===L)if(n.filter){var r=[];W(r,t._modules.root,n.filter,""),n.rootNodes=r}else n.rootNodes=[q(t._modules.root,"")]})),n.on.getInspectorState((function(n){if(n.app===e&&n.inspectorId===L){var r=n.nodeId;S(t,r),n.state=G(K(t._modules,r),"root"===r?t.getters:t._makeLocalGettersCache,r)}})),n.on.editInspectorState((function(n){if(n.app===e&&n.inspectorId===L){var r=n.nodeId,i=n.path;"root"!==r&&(i=r.split("/").filter(Boolean).concat(i)),t._withCommit((function(){n.set(t._state.data,i,n.state.value)}))}})),t.subscribe((function(e,t){var r={};e.payload&&(r.payload=e.payload),r.state=t,n.notifyComponentUpdate(),n.sendInspectorTree(L),n.sendInspectorState(L),n.addTimelineEvent({layerId:D,event:{time:Date.now(),title:e.type,data:r}})})),t.subscribeAction({before:function(e,t){var r={};e.payload&&(r.payload=e.payload),e._id=U++,e._time=Date.now(),r.state=t,n.addTimelineEvent({layerId:j,event:{time:e._time,title:e.type,groupId:e._id,subtitle:"start",data:r}})},after:function(e,t){var r={},i=Date.now()-e._time;r.duration={_custom:{type:"duration",display:i+"ms",tooltip:"Action duration",value:i}},e.payload&&(r.payload=e.payload),r.state=t,n.addTimelineEvent({layerId:j,event:{time:Date.now(),title:e.type,groupId:e._id,subtitle:"end",data:r}})}})}))}var $=8702998,V=6710886,z=16777215,B={label:"namespaced",textColor:z,backgroundColor:V};function H(e){return e&&"root"!==e?e.split("/").slice(-2,-1)[0]:"Root"}function q(e,t){return{id:t||"root",label:H(t),tags:e.namespaced?[B]:[],children:Object.keys(e._children).map((function(n){return q(e._children[n],t+n+"/")}))}}function W(e,t,n,r){r.includes(n)&&e.push({id:r||"root",label:r.endsWith("/")?r.slice(0,r.length-1):r||"Root",tags:t.namespaced?[B]:[]}),Object.keys(t._children).forEach((function(i){W(e,t._children[i],n,r+i+"/")}))}function G(e,t,n){t="root"===n?t:t[n];var r=Object.keys(t),i={state:Object.keys(e.state).map((function(t){return{key:t,editable:!0,value:e.state[t]}}))};if(r.length){var o=J(t);i.getters=Object.keys(o).map((function(e){return{key:e.endsWith("/")?H(e):e,editable:!1,value:Y((function(){return o[e]}))}}))}return i}function J(e){var t={};return Object.keys(e).forEach((function(n){var r=n.split("/");if(r.length>1){var i=t,o=r.pop();r.forEach((function(e){i[e]||(i[e]={_custom:{value:{},display:e,tooltip:"Module",abstract:!0}}),i=i[e]._custom.value})),i[o]=Y((function(){return e[n]}))}else t[n]=Y((function(){return e[n]}))})),t}function K(e,t){var n=t.split("/").filter((function(e){return e}));return n.reduce((function(e,r,i){var o=e[r];if(!o)throw new Error('Missing module "'+r+'" for path "'+t+'".');return i===n.length-1?o:o._children}),"root"===t?e:e.root._children)}function Y(e){try{return e()}catch(t){return t}}var Z=function(e,t){this.runtime=t,this._children=Object.create(null),this._rawModule=e;var n=e.state;this.state=("function"===typeof n?n():n)||{}},X={namespaced:{configurable:!0}};X.namespaced.get=function(){return!!this._rawModule.namespaced},Z.prototype.addChild=function(e,t){this._children[e]=t},Z.prototype.removeChild=function(e){delete this._children[e]},Z.prototype.getChild=function(e){return this._children[e]},Z.prototype.hasChild=function(e){return e in this._children},Z.prototype.update=function(e){this._rawModule.namespaced=e.namespaced,e.actions&&(this._rawModule.actions=e.actions),e.mutations&&(this._rawModule.mutations=e.mutations),e.getters&&(this._rawModule.getters=e.getters)},Z.prototype.forEachChild=function(e){_(this._children,e)},Z.prototype.forEachGetter=function(e){this._rawModule.getters&&_(this._rawModule.getters,e)},Z.prototype.forEachAction=function(e){this._rawModule.actions&&_(this._rawModule.actions,e)},Z.prototype.forEachMutation=function(e){this._rawModule.mutations&&_(this._rawModule.mutations,e)},Object.defineProperties(Z.prototype,X);var Q=function(e){this.register([],e,!1)};function ee(e,t,n){if(t.update(n),n.modules)for(var r in n.modules){if(!t.getChild(r))return void 0;ee(e.concat(r),t.getChild(r),n.modules[r])}}Q.prototype.get=function(e){return e.reduce((function(e,t){return e.getChild(t)}),this.root)},Q.prototype.getNamespace=function(e){var t=this.root;return e.reduce((function(e,n){return t=t.getChild(n),e+(t.namespaced?n+"/":"")}),"")},Q.prototype.update=function(e){ee([],this.root,e)},Q.prototype.register=function(e,t,n){var r=this;void 0===n&&(n=!0);var i=new Z(t,n);if(0===e.length)this.root=i;else{var o=this.get(e.slice(0,-1));o.addChild(e[e.length-1],i)}t.modules&&_(t.modules,(function(t,i){r.register(e.concat(i),t,n)}))},Q.prototype.unregister=function(e){var t=this.get(e.slice(0,-1)),n=e[e.length-1],r=t.getChild(n);r&&r.runtime&&t.removeChild(n)},Q.prototype.isRegistered=function(e){var t=this.get(e.slice(0,-1)),n=e[e.length-1];return!!t&&t.hasChild(n)};function te(e){return new ne(e)}var ne=function(e){var t=this;void 0===e&&(e={});var n=e.plugins;void 0===n&&(n=[]);var r=e.strict;void 0===r&&(r=!1);var i=e.devtools;this._committing=!1,this._actions=Object.create(null),this._actionSubscribers=[],this._mutations=Object.create(null),this._wrappedGetters=Object.create(null),this._modules=new Q(e),this._modulesNamespaceMap=Object.create(null),this._subscribers=[],this._makeLocalGettersCache=Object.create(null),this._devtools=i;var o=this,s=this,a=s.dispatch,u=s.commit;this.dispatch=function(e,t){return a.call(o,e,t)},this.commit=function(e,t,n){return u.call(o,e,t,n)},this.strict=r;var c=this._modules.root.state;T(this,c,[],this._modules.root),k(this,c),n.forEach((function(e){return e(t)}))},re={state:{configurable:!0}};ne.prototype.install=function(e,t){e.provide(t||v,this),e.config.globalProperties.$store=this;var n=void 0!==this._devtools&&this._devtools;n&&M(e,this)},re.state.get=function(){return this._state.data},re.state.set=function(e){0},ne.prototype.commit=function(e,t,n){var r=this,i=N(e,t,n),o=i.type,s=i.payload,a=(i.options,{type:o,payload:s}),u=this._mutations[o];u&&(this._withCommit((function(){u.forEach((function(e){e(s)}))})),this._subscribers.slice().forEach((function(e){return e(a,r.state)})))},ne.prototype.dispatch=function(e,t){var n=this,r=N(e,t),i=r.type,o=r.payload,s={type:i,payload:o},a=this._actions[i];if(a){try{this._actionSubscribers.slice().filter((function(e){return e.before})).forEach((function(e){return e.before(s,n.state)}))}catch(c){0}var u=a.length>1?Promise.all(a.map((function(e){return e(o)}))):a[0](o);return new Promise((function(e,t){u.then((function(t){try{n._actionSubscribers.filter((function(e){return e.after})).forEach((function(e){return e.after(s,n.state)}))}catch(c){0}e(t)}),(function(e){try{n._actionSubscribers.filter((function(e){return e.error})).forEach((function(t){return t.error(s,n.state,e)}))}catch(c){0}t(e)}))}))}},ne.prototype.subscribe=function(e,t){return I(e,this._subscribers,t)},ne.prototype.subscribeAction=function(e,t){var n="function"===typeof e?{before:e}:e;return I(n,this._actionSubscribers,t)},ne.prototype.watch=function(e,t,n){var i=this;return(0,r.YP)((function(){return e(i.state,i.getters)}),t,Object.assign({},n))},ne.prototype.replaceState=function(e){var t=this;this._withCommit((function(){t._state.data=e}))},ne.prototype.registerModule=function(e,t,n){void 0===n&&(n={}),"string"===typeof e&&(e=[e]),this._modules.register(e,t),T(this,this.state,e,this._modules.get(e),n.preserveState),k(this,this.state)},ne.prototype.unregisterModule=function(e){var t=this;"string"===typeof e&&(e=[e]),this._modules.unregister(e),this._withCommit((function(){var n=P(t.state,e.slice(0,-1));delete n[e[e.length-1]]})),E(this)},ne.prototype.hasModule=function(e){return"string"===typeof e&&(e=[e]),this._modules.isRegistered(e)},ne.prototype.hotUpdate=function(e){this._modules.update(e),E(this,!0)},ne.prototype._withCommit=function(e){var t=this._committing;this._committing=!0,e(),this._committing=t},Object.defineProperties(ne.prototype,re);se((function(e,t){var n={};return ie(t).forEach((function(t){var r=t.key,i=t.val;n[r]=function(){var t=this.$store.state,n=this.$store.getters;if(e){var r=ae(this.$store,"mapState",e);if(!r)return;t=r.context.state,n=r.context.getters}return"function"===typeof i?i.call(this,t,n):t[i]},n[r].vuex=!0})),n})),se((function(e,t){var n={};return ie(t).forEach((function(t){var r=t.key,i=t.val;n[r]=function(){var t=[],n=arguments.length;while(n--)t[n]=arguments[n];var r=this.$store.commit;if(e){var o=ae(this.$store,"mapMutations",e);if(!o)return;r=o.context.commit}return"function"===typeof i?i.apply(this,[r].concat(t)):r.apply(this.$store,[i].concat(t))}})),n})),se((function(e,t){var n={};return ie(t).forEach((function(t){var r=t.key,i=t.val;i=e+i,n[r]=function(){if(!e||ae(this.$store,"mapGetters",e))return this.$store.getters[i]},n[r].vuex=!0})),n})),se((function(e,t){var n={};return ie(t).forEach((function(t){var r=t.key,i=t.val;n[r]=function(){var t=[],n=arguments.length;while(n--)t[n]=arguments[n];var r=this.$store.dispatch;if(e){var o=ae(this.$store,"mapActions",e);if(!o)return;r=o.context.dispatch}return"function"===typeof i?i.apply(this,[r].concat(t)):r.apply(this.$store,[i].concat(t))}})),n}));function ie(e){return oe(e)?Array.isArray(e)?e.map((function(e){return{key:e,val:e}})):Object.keys(e).map((function(t){return{key:t,val:e[t]}})):[]}function oe(e){return Array.isArray(e)||y(e)}function se(e){return function(t,n){return"string"!==typeof t?(n=t,t=""):"/"!==t.charAt(t.length-1)&&(t+="/"),e(t,n)}}function ae(e,t,n){var r=e._modulesNamespaceMap[n];return r}},1593:function(e,t,n){"use strict";n.d(t,{Ry:function(){return Sd},iH:function(){return Cd},Z_:function(){return xl}});n(1703);function r(){return r=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var r in n)Object.prototype.hasOwnProperty.call(n,r)&&(e[r]=n[r])}return e},r.apply(this,arguments)}function i(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}function o(e,t,n){return t&&i(e.prototype,t),n&&i(e,n),Object.defineProperty(e,"prototype",{writable:!1}),e}var s=Object.prototype,a=s.hasOwnProperty;function u(e,t){return null!=e&&a.call(e,t)}var c=u,l=Array.isArray,h=l,d="object"==typeof global&&global&&global.Object===Object&&global,f=d,p="object"==typeof self&&self&&self.Object===Object&&self,m=f||p||Function("return this")(),v=m,g=v.Symbol,_=g,y=Object.prototype,b=y.hasOwnProperty,w=y.toString,I=_?_.toStringTag:void 0;function E(e){var t=b.call(e,I),n=e[I];try{e[I]=void 0;var r=!0}catch(o){}var i=w.call(e);return r&&(t?e[I]=n:delete e[I]),i}var k=E,T=Object.prototype,O=T.toString;function S(e){return O.call(e)}var R=S,A="[object Null]",C="[object Undefined]",x=_?_.toStringTag:void 0;function P(e){return null==e?void 0===e?C:A:x&&x in Object(e)?k(e):R(e)}var N=P;function F(e){return null!=e&&"object"==typeof e}var D=F,j="[object Symbol]";function L(e){return"symbol"==typeof e||D(e)&&N(e)==j}var U=L,M=/\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,$=/^\w*$/;function V(e,t){if(h(e))return!1;var n=typeof e;return!("number"!=n&&"symbol"!=n&&"boolean"!=n&&null!=e&&!U(e))||($.test(e)||!M.test(e)||null!=t&&e in Object(t))}var z=V;function B(e){var t=typeof e;return null!=e&&("object"==t||"function"==t)}var H=B,q="[object AsyncFunction]",W="[object Function]",G="[object GeneratorFunction]",J="[object Proxy]";function K(e){if(!H(e))return!1;var t=N(e);return t==W||t==G||t==q||t==J}var Y=K,Z=v["__core-js_shared__"],X=Z,Q=function(){var e=/[^.]+$/.exec(X&&X.keys&&X.keys.IE_PROTO||"");return e?"Symbol(src)_1."+e:""}();function ee(e){return!!Q&&Q in e}var te=ee,ne=Function.prototype,re=ne.toString;function ie(e){if(null!=e){try{return re.call(e)}catch(t){}try{return e+""}catch(t){}}return""}var oe=ie,se=/[\\^$.*+?()[\]{}|]/g,ae=/^\[object .+?Constructor\]$/,ue=Function.prototype,ce=Object.prototype,le=ue.toString,he=ce.hasOwnProperty,de=RegExp("^"+le.call(he).replace(se,"\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g,"$1.*?")+"$");function fe(e){if(!H(e)||te(e))return!1;var t=Y(e)?de:ae;return t.test(oe(e))}var pe=fe;function me(e,t){return null==e?void 0:e[t]}var ve=me;function ge(e,t){var n=ve(e,t);return pe(n)?n:void 0}var _e=ge,ye=_e(Object,"create"),be=ye;function we(){this.__data__=be?be(null):{},this.size=0}var Ie=we;function Ee(e){var t=this.has(e)&&delete this.__data__[e];return this.size-=t?1:0,t}var ke=Ee,Te="__lodash_hash_undefined__",Oe=Object.prototype,Se=Oe.hasOwnProperty;function Re(e){var t=this.__data__;if(be){var n=t[e];return n===Te?void 0:n}return Se.call(t,e)?t[e]:void 0}var Ae=Re,Ce=Object.prototype,xe=Ce.hasOwnProperty;function Pe(e){var t=this.__data__;return be?void 0!==t[e]:xe.call(t,e)}var Ne=Pe,Fe="__lodash_hash_undefined__";function De(e,t){var n=this.__data__;return this.size+=this.has(e)?0:1,n[e]=be&&void 0===t?Fe:t,this}var je=De;function Le(e){var t=-1,n=null==e?0:e.length;this.clear();while(++t<n){var r=e[t];this.set(r[0],r[1])}}Le.prototype.clear=Ie,Le.prototype["delete"]=ke,Le.prototype.get=Ae,Le.prototype.has=Ne,Le.prototype.set=je;var Ue=Le;function Me(){this.__data__=[],this.size=0}var $e=Me;function Ve(e,t){return e===t||e!==e&&t!==t}var ze=Ve;function Be(e,t){var n=e.length;while(n--)if(ze(e[n][0],t))return n;return-1}var He=Be,qe=Array.prototype,We=qe.splice;function Ge(e){var t=this.__data__,n=He(t,e);if(n<0)return!1;var r=t.length-1;return n==r?t.pop():We.call(t,n,1),--this.size,!0}var Je=Ge;function Ke(e){var t=this.__data__,n=He(t,e);return n<0?void 0:t[n][1]}var Ye=Ke;function Ze(e){return He(this.__data__,e)>-1}var Xe=Ze;function Qe(e,t){var n=this.__data__,r=He(n,e);return r<0?(++this.size,n.push([e,t])):n[r][1]=t,this}var et=Qe;function tt(e){var t=-1,n=null==e?0:e.length;this.clear();while(++t<n){var r=e[t];this.set(r[0],r[1])}}tt.prototype.clear=$e,tt.prototype["delete"]=Je,tt.prototype.get=Ye,tt.prototype.has=Xe,tt.prototype.set=et;var nt=tt,rt=_e(v,"Map"),it=rt;function ot(){this.size=0,this.__data__={hash:new Ue,map:new(it||nt),string:new Ue}}var st=ot;function at(e){var t=typeof e;return"string"==t||"number"==t||"symbol"==t||"boolean"==t?"__proto__"!==e:null===e}var ut=at;function ct(e,t){var n=e.__data__;return ut(t)?n["string"==typeof t?"string":"hash"]:n.map}var lt=ct;function ht(e){var t=lt(this,e)["delete"](e);return this.size-=t?1:0,t}var dt=ht;function ft(e){return lt(this,e).get(e)}var pt=ft;function mt(e){return lt(this,e).has(e)}var vt=mt;function gt(e,t){var n=lt(this,e),r=n.size;return n.set(e,t),this.size+=n.size==r?0:1,this}var _t=gt;function yt(e){var t=-1,n=null==e?0:e.length;this.clear();while(++t<n){var r=e[t];this.set(r[0],r[1])}}yt.prototype.clear=st,yt.prototype["delete"]=dt,yt.prototype.get=pt,yt.prototype.has=vt,yt.prototype.set=_t;var bt=yt,wt="Expected a function";function It(e,t){if("function"!=typeof e||null!=t&&"function"!=typeof t)throw new TypeError(wt);var n=function(){var r=arguments,i=t?t.apply(this,r):r[0],o=n.cache;if(o.has(i))return o.get(i);var s=e.apply(this,r);return n.cache=o.set(i,s)||o,s};return n.cache=new(It.Cache||bt),n}It.Cache=bt;var Et=It,kt=500;function Tt(e){var t=Et(e,(function(e){return n.size===kt&&n.clear(),e})),n=t.cache;return t}var Ot=Tt,St=/[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,Rt=/\\(\\)?/g,At=Ot((function(e){var t=[];return 46===e.charCodeAt(0)&&t.push(""),e.replace(St,(function(e,n,r,i){t.push(r?i.replace(Rt,"$1"):n||e)})),t})),Ct=At;function xt(e,t){var n=-1,r=null==e?0:e.length,i=Array(r);while(++n<r)i[n]=t(e[n],n,e);return i}var Pt=xt,Nt=1/0,Ft=_?_.prototype:void 0,Dt=Ft?Ft.toString:void 0;function jt(e){if("string"==typeof e)return e;if(h(e))return Pt(e,jt)+"";if(U(e))return Dt?Dt.call(e):"";var t=e+"";return"0"==t&&1/e==-Nt?"-0":t}var Lt=jt;function Ut(e){return null==e?"":Lt(e)}var Mt=Ut;function $t(e,t){return h(e)?e:z(e,t)?[e]:Ct(Mt(e))}var Vt=$t,zt="[object Arguments]";function Bt(e){return D(e)&&N(e)==zt}var Ht=Bt,qt=Object.prototype,Wt=qt.hasOwnProperty,Gt=qt.propertyIsEnumerable,Jt=Ht(function(){return arguments}())?Ht:function(e){return D(e)&&Wt.call(e,"callee")&&!Gt.call(e,"callee")},Kt=Jt,Yt=9007199254740991,Zt=/^(?:0|[1-9]\d*)$/;function Xt(e,t){var n=typeof e;return t=null==t?Yt:t,!!t&&("number"==n||"symbol"!=n&&Zt.test(e))&&e>-1&&e%1==0&&e<t}var Qt=Xt,en=9007199254740991;function tn(e){return"number"==typeof e&&e>-1&&e%1==0&&e<=en}var nn=tn,rn=1/0;function on(e){if("string"==typeof e||U(e))return e;var t=e+"";return"0"==t&&1/e==-rn?"-0":t}var sn=on;function an(e,t,n){t=Vt(t,e);var r=-1,i=t.length,o=!1;while(++r<i){var s=sn(t[r]);if(!(o=null!=e&&n(e,s)))break;e=e[s]}return o||++r!=i?o:(i=null==e?0:e.length,!!i&&nn(i)&&Qt(s,i)&&(h(e)||Kt(e)))}var un=an;function cn(e,t){return null!=e&&un(e,t,c)}var ln=cn;function hn(){this.__data__=new nt,this.size=0}var dn=hn;function fn(e){var t=this.__data__,n=t["delete"](e);return this.size=t.size,n}var pn=fn;function mn(e){return this.__data__.get(e)}var vn=mn;function gn(e){return this.__data__.has(e)}var _n=gn,yn=200;function bn(e,t){var n=this.__data__;if(n instanceof nt){var r=n.__data__;if(!it||r.length<yn-1)return r.push([e,t]),this.size=++n.size,this;n=this.__data__=new bt(r)}return n.set(e,t),this.size=n.size,this}var wn=bn;function In(e){var t=this.__data__=new nt(e);this.size=t.size}In.prototype.clear=dn,In.prototype["delete"]=pn,In.prototype.get=vn,In.prototype.has=_n,In.prototype.set=wn;var En=In;function kn(e,t){var n=-1,r=null==e?0:e.length;while(++n<r)if(!1===t(e[n],n,e))break;return e}var Tn=kn,On=function(){try{var e=_e(Object,"defineProperty");return e({},"",{}),e}catch(t){}}(),Sn=On;function Rn(e,t,n){"__proto__"==t&&Sn?Sn(e,t,{configurable:!0,enumerable:!0,value:n,writable:!0}):e[t]=n}var An=Rn,Cn=Object.prototype,xn=Cn.hasOwnProperty;function Pn(e,t,n){var r=e[t];xn.call(e,t)&&ze(r,n)&&(void 0!==n||t in e)||An(e,t,n)}var Nn=Pn;function Fn(e,t,n,r){var i=!n;n||(n={});var o=-1,s=t.length;while(++o<s){var a=t[o],u=r?r(n[a],e[a],a,n,e):void 0;void 0===u&&(u=e[a]),i?An(n,a,u):Nn(n,a,u)}return n}var Dn=Fn;function jn(e,t){var n=-1,r=Array(e);while(++n<e)r[n]=t(n);return r}var Ln=jn;function Un(){return!1}var Mn=Un,$n="object"==typeof exports&&exports&&!exports.nodeType&&exports,Vn=$n&&"object"==typeof module&&module&&!module.nodeType&&module,zn=Vn&&Vn.exports===$n,Bn=zn?v.Buffer:void 0,Hn=Bn?Bn.isBuffer:void 0,qn=Hn||Mn,Wn=qn,Gn="[object Arguments]",Jn="[object Array]",Kn="[object Boolean]",Yn="[object Date]",Zn="[object Error]",Xn="[object Function]",Qn="[object Map]",er="[object Number]",tr="[object Object]",nr="[object RegExp]",rr="[object Set]",ir="[object String]",or="[object WeakMap]",sr="[object ArrayBuffer]",ar="[object DataView]",ur="[object Float32Array]",cr="[object Float64Array]",lr="[object Int8Array]",hr="[object Int16Array]",dr="[object Int32Array]",fr="[object Uint8Array]",pr="[object Uint8ClampedArray]",mr="[object Uint16Array]",vr="[object Uint32Array]",gr={};function _r(e){return D(e)&&nn(e.length)&&!!gr[N(e)]}gr[ur]=gr[cr]=gr[lr]=gr[hr]=gr[dr]=gr[fr]=gr[pr]=gr[mr]=gr[vr]=!0,gr[Gn]=gr[Jn]=gr[sr]=gr[Kn]=gr[ar]=gr[Yn]=gr[Zn]=gr[Xn]=gr[Qn]=gr[er]=gr[tr]=gr[nr]=gr[rr]=gr[ir]=gr[or]=!1;var yr=_r;function br(e){return function(t){return e(t)}}var wr=br,Ir="object"==typeof exports&&exports&&!exports.nodeType&&exports,Er=Ir&&"object"==typeof module&&module&&!module.nodeType&&module,kr=Er&&Er.exports===Ir,Tr=kr&&f.process,Or=function(){try{var e=Er&&Er.require&&Er.require("util").types;return e||Tr&&Tr.binding&&Tr.binding("util")}catch(t){}}(),Sr=Or,Rr=Sr&&Sr.isTypedArray,Ar=Rr?wr(Rr):yr,Cr=Ar,xr=Object.prototype,Pr=xr.hasOwnProperty;function Nr(e,t){var n=h(e),r=!n&&Kt(e),i=!n&&!r&&Wn(e),o=!n&&!r&&!i&&Cr(e),s=n||r||i||o,a=s?Ln(e.length,String):[],u=a.length;for(var c in e)!t&&!Pr.call(e,c)||s&&("length"==c||i&&("offset"==c||"parent"==c)||o&&("buffer"==c||"byteLength"==c||"byteOffset"==c)||Qt(c,u))||a.push(c);return a}var Fr=Nr,Dr=Object.prototype;function jr(e){var t=e&&e.constructor,n="function"==typeof t&&t.prototype||Dr;return e===n}var Lr=jr;function Ur(e,t){return function(n){return e(t(n))}}var Mr=Ur,$r=Mr(Object.keys,Object),Vr=$r,zr=Object.prototype,Br=zr.hasOwnProperty;function Hr(e){if(!Lr(e))return Vr(e);var t=[];for(var n in Object(e))Br.call(e,n)&&"constructor"!=n&&t.push(n);return t}var qr=Hr;function Wr(e){return null!=e&&nn(e.length)&&!Y(e)}var Gr=Wr;function Jr(e){return Gr(e)?Fr(e):qr(e)}var Kr=Jr;function Yr(e,t){return e&&Dn(t,Kr(t),e)}var Zr=Yr;function Xr(e){var t=[];if(null!=e)for(var n in Object(e))t.push(n);return t}var Qr=Xr,ei=Object.prototype,ti=ei.hasOwnProperty;function ni(e){if(!H(e))return Qr(e);var t=Lr(e),n=[];for(var r in e)("constructor"!=r||!t&&ti.call(e,r))&&n.push(r);return n}var ri=ni;function ii(e){return Gr(e)?Fr(e,!0):ri(e)}var oi=ii;function si(e,t){return e&&Dn(t,oi(t),e)}var ai=si,ui="object"==typeof exports&&exports&&!exports.nodeType&&exports,ci=ui&&"object"==typeof module&&module&&!module.nodeType&&module,li=ci&&ci.exports===ui,hi=li?v.Buffer:void 0,di=hi?hi.allocUnsafe:void 0;function fi(e,t){if(t)return e.slice();var n=e.length,r=di?di(n):new e.constructor(n);return e.copy(r),r}var pi=fi;function mi(e,t){var n=-1,r=e.length;t||(t=Array(r));while(++n<r)t[n]=e[n];return t}var vi=mi;function gi(e,t){var n=-1,r=null==e?0:e.length,i=0,o=[];while(++n<r){var s=e[n];t(s,n,e)&&(o[i++]=s)}return o}var _i=gi;function yi(){return[]}var bi=yi,wi=Object.prototype,Ii=wi.propertyIsEnumerable,Ei=Object.getOwnPropertySymbols,ki=Ei?function(e){return null==e?[]:(e=Object(e),_i(Ei(e),(function(t){return Ii.call(e,t)})))}:bi,Ti=ki;function Oi(e,t){return Dn(e,Ti(e),t)}var Si=Oi;function Ri(e,t){var n=-1,r=t.length,i=e.length;while(++n<r)e[i+n]=t[n];return e}var Ai=Ri,Ci=Mr(Object.getPrototypeOf,Object),xi=Ci,Pi=Object.getOwnPropertySymbols,Ni=Pi?function(e){var t=[];while(e)Ai(t,Ti(e)),e=xi(e);return t}:bi,Fi=Ni;function Di(e,t){return Dn(e,Fi(e),t)}var ji=Di;function Li(e,t,n){var r=t(e);return h(e)?r:Ai(r,n(e))}var Ui=Li;function Mi(e){return Ui(e,Kr,Ti)}var $i=Mi;function Vi(e){return Ui(e,oi,Fi)}var zi=Vi,Bi=_e(v,"DataView"),Hi=Bi,qi=_e(v,"Promise"),Wi=qi,Gi=_e(v,"Set"),Ji=Gi,Ki=_e(v,"WeakMap"),Yi=Ki,Zi="[object Map]",Xi="[object Object]",Qi="[object Promise]",eo="[object Set]",to="[object WeakMap]",no="[object DataView]",ro=oe(Hi),io=oe(it),oo=oe(Wi),so=oe(Ji),ao=oe(Yi),uo=N;(Hi&&uo(new Hi(new ArrayBuffer(1)))!=no||it&&uo(new it)!=Zi||Wi&&uo(Wi.resolve())!=Qi||Ji&&uo(new Ji)!=eo||Yi&&uo(new Yi)!=to)&&(uo=function(e){var t=N(e),n=t==Xi?e.constructor:void 0,r=n?oe(n):"";if(r)switch(r){case ro:return no;case io:return Zi;case oo:return Qi;case so:return eo;case ao:return to}return t});var co=uo,lo=Object.prototype,ho=lo.hasOwnProperty;function fo(e){var t=e.length,n=new e.constructor(t);return t&&"string"==typeof e[0]&&ho.call(e,"index")&&(n.index=e.index,n.input=e.input),n}var po=fo,mo=v.Uint8Array,vo=mo;function go(e){var t=new e.constructor(e.byteLength);return new vo(t).set(new vo(e)),t}var _o=go;function yo(e,t){var n=t?_o(e.buffer):e.buffer;return new e.constructor(n,e.byteOffset,e.byteLength)}var bo=yo,wo=/\w*$/;function Io(e){var t=new e.constructor(e.source,wo.exec(e));return t.lastIndex=e.lastIndex,t}var Eo=Io,ko=_?_.prototype:void 0,To=ko?ko.valueOf:void 0;function Oo(e){return To?Object(To.call(e)):{}}var So=Oo;function Ro(e,t){var n=t?_o(e.buffer):e.buffer;return new e.constructor(n,e.byteOffset,e.length)}var Ao=Ro,Co="[object Boolean]",xo="[object Date]",Po="[object Map]",No="[object Number]",Fo="[object RegExp]",Do="[object Set]",jo="[object String]",Lo="[object Symbol]",Uo="[object ArrayBuffer]",Mo="[object DataView]",$o="[object Float32Array]",Vo="[object Float64Array]",zo="[object Int8Array]",Bo="[object Int16Array]",Ho="[object Int32Array]",qo="[object Uint8Array]",Wo="[object Uint8ClampedArray]",Go="[object Uint16Array]",Jo="[object Uint32Array]";function Ko(e,t,n){var r=e.constructor;switch(t){case Uo:return _o(e);case Co:case xo:return new r(+e);case Mo:return bo(e,n);case $o:case Vo:case zo:case Bo:case Ho:case qo:case Wo:case Go:case Jo:return Ao(e,n);case Po:return new r;case No:case jo:return new r(e);case Fo:return Eo(e);case Do:return new r;case Lo:return So(e)}}var Yo=Ko,Zo=Object.create,Xo=function(){function e(){}return function(t){if(!H(t))return{};if(Zo)return Zo(t);e.prototype=t;var n=new e;return e.prototype=void 0,n}}(),Qo=Xo;function es(e){return"function"!=typeof e.constructor||Lr(e)?{}:Qo(xi(e))}var ts=es,ns="[object Map]";function rs(e){return D(e)&&co(e)==ns}var is=rs,os=Sr&&Sr.isMap,ss=os?wr(os):is,as=ss,us="[object Set]";function cs(e){return D(e)&&co(e)==us}var ls=cs,hs=Sr&&Sr.isSet,ds=hs?wr(hs):ls,fs=ds,ps=1,ms=2,vs=4,gs="[object Arguments]",_s="[object Array]",ys="[object Boolean]",bs="[object Date]",ws="[object Error]",Is="[object Function]",Es="[object GeneratorFunction]",ks="[object Map]",Ts="[object Number]",Os="[object Object]",Ss="[object RegExp]",Rs="[object Set]",As="[object String]",Cs="[object Symbol]",xs="[object WeakMap]",Ps="[object ArrayBuffer]",Ns="[object DataView]",Fs="[object Float32Array]",Ds="[object Float64Array]",js="[object Int8Array]",Ls="[object Int16Array]",Us="[object Int32Array]",Ms="[object Uint8Array]",$s="[object Uint8ClampedArray]",Vs="[object Uint16Array]",zs="[object Uint32Array]",Bs={};function Hs(e,t,n,r,i,o){var s,a=t&ps,u=t&ms,c=t&vs;if(n&&(s=i?n(e,r,i,o):n(e)),void 0!==s)return s;if(!H(e))return e;var l=h(e);if(l){if(s=po(e),!a)return vi(e,s)}else{var d=co(e),f=d==Is||d==Es;if(Wn(e))return pi(e,a);if(d==Os||d==gs||f&&!i){if(s=u||f?{}:ts(e),!a)return u?ji(e,ai(s,e)):Si(e,Zr(s,e))}else{if(!Bs[d])return i?e:{};s=Yo(e,d,a)}}o||(o=new En);var p=o.get(e);if(p)return p;o.set(e,s),fs(e)?e.forEach((function(r){s.add(Hs(r,t,n,r,e,o))})):as(e)&&e.forEach((function(r,i){s.set(i,Hs(r,t,n,i,e,o))}));var m=c?u?zi:$i:u?oi:Kr,v=l?void 0:m(e);return Tn(v||e,(function(r,i){v&&(i=r,r=e[i]),Nn(s,i,Hs(r,t,n,i,e,o))})),s}Bs[gs]=Bs[_s]=Bs[Ps]=Bs[Ns]=Bs[ys]=Bs[bs]=Bs[Fs]=Bs[Ds]=Bs[js]=Bs[Ls]=Bs[Us]=Bs[ks]=Bs[Ts]=Bs[Os]=Bs[Ss]=Bs[Rs]=Bs[As]=Bs[Cs]=Bs[Ms]=Bs[$s]=Bs[Vs]=Bs[zs]=!0,Bs[ws]=Bs[Is]=Bs[xs]=!1;var qs=Hs,Ws=1,Gs=4;function Js(e,t){return t="function"==typeof t?t:void 0,qs(e,Ws|Gs,t)}var Ks=Js,Ys="[object String]";function Zs(e){return"string"==typeof e||!h(e)&&D(e)&&N(e)==Ys}var Xs=Zs;function Qs(e){var t,n=[];while(!(t=e.next()).done)n.push(t.value);return n}var ea=Qs;function ta(e){var t=-1,n=Array(e.size);return e.forEach((function(e,r){n[++t]=[r,e]})),n}var na=ta;function ra(e){var t=-1,n=Array(e.size);return e.forEach((function(e){n[++t]=e})),n}var ia=ra;function oa(e){return e.split("")}var sa=oa,aa="\\ud800-\\udfff",ua="\\u0300-\\u036f",ca="\\ufe20-\\ufe2f",la="\\u20d0-\\u20ff",ha=ua+ca+la,da="\\ufe0e\\ufe0f",fa="\\u200d",pa=RegExp("["+fa+aa+ha+da+"]");function ma(e){return pa.test(e)}var va=ma,ga="\\ud800-\\udfff",_a="\\u0300-\\u036f",ya="\\ufe20-\\ufe2f",ba="\\u20d0-\\u20ff",wa=_a+ya+ba,Ia="\\ufe0e\\ufe0f",Ea="["+ga+"]",ka="["+wa+"]",Ta="\\ud83c[\\udffb-\\udfff]",Oa="(?:"+ka+"|"+Ta+")",Sa="[^"+ga+"]",Ra="(?:\\ud83c[\\udde6-\\uddff]){2}",Aa="[\\ud800-\\udbff][\\udc00-\\udfff]",Ca="\\u200d",xa=Oa+"?",Pa="["+Ia+"]?",Na="(?:"+Ca+"(?:"+[Sa,Ra,Aa].join("|")+")"+Pa+xa+")*",Fa=Pa+xa+Na,Da="(?:"+[Sa+ka+"?",ka,Ra,Aa,Ea].join("|")+")",ja=RegExp(Ta+"(?="+Ta+")|"+Da+Fa,"g");function La(e){return e.match(ja)||[]}var Ua=La;function Ma(e){return va(e)?Ua(e):sa(e)}var $a=Ma;function Va(e,t){return Pt(t,(function(t){return e[t]}))}var za=Va;function Ba(e){return null==e?[]:za(e,Kr(e))}var Ha=Ba,qa="[object Map]",Wa="[object Set]",Ga=_?_.iterator:void 0;function Ja(e){if(!e)return[];if(Gr(e))return Xs(e)?$a(e):vi(e);if(Ga&&e[Ga])return ea(e[Ga]());var t=co(e),n=t==qa?na:t==Wa?ia:Ha;return n(e)}var Ka=Ja,Ya=Object.prototype.toString,Za=Error.prototype.toString,Xa=RegExp.prototype.toString,Qa="undefined"!==typeof Symbol?Symbol.prototype.toString:function(){return""},eu=/^Symbol\((.*)\)(.*)$/;function tu(e){if(e!=+e)return"NaN";var t=0===e&&1/e<0;return t?"-0":""+e}function nu(e,t){if(void 0===t&&(t=!1),null==e||!0===e||!1===e)return""+e;var n=typeof e;if("number"===n)return tu(e);if("string"===n)return t?'"'+e+'"':e;if("function"===n)return"[Function "+(e.name||"anonymous")+"]";if("symbol"===n)return Qa.call(e).replace(eu,"Symbol($1)");var r=Ya.call(e).slice(8,-1);return"Date"===r?isNaN(e.getTime())?""+e:e.toISOString(e):"Error"===r||e instanceof Error?"["+Za.call(e)+"]":"RegExp"===r?Xa.call(e):null}function ru(e,t){var n=nu(e,t);return null!==n?n:JSON.stringify(e,(function(e,n){var r=nu(this[e],t);return null!==r?r:n}),2)}var iu={default:"${path} is invalid",required:"${path} is a required field",oneOf:"${path} must be one of the following values: ${values}",notOneOf:"${path} must not be one of the following values: ${values}",notType:function(e){var t=e.path,n=e.type,r=e.value,i=e.originalValue,o=null!=i&&i!==r,s=t+" must be a `"+n+"` type, but the final value was: `"+ru(r,!0)+"`"+(o?" (cast from the value `"+ru(i,!0)+"`).":".");return null===r&&(s+='\n If "null" is intended as an empty value be sure to mark the schema as `.nullable()`'),s},defined:"${path} must be defined"},ou={length:"${path} must be exactly ${length} characters",min:"${path} must be at least ${min} characters",max:"${path} must be at most ${max} characters",matches:'${path} must match the following: "${regex}"',email:"${path} must be a valid email",url:"${path} must be a valid URL",uuid:"${path} must be a valid UUID",trim:"${path} must be a trimmed string",lowercase:"${path} must be a lowercase string",uppercase:"${path} must be a upper case string"},su={min:"${path} must be greater than or equal to ${min}",max:"${path} must be less than or equal to ${max}",lessThan:"${path} must be less than ${less}",moreThan:"${path} must be greater than ${more}",notEqual:"${path} must be not equal to ${notEqual}",positive:"${path} must be a positive number",negative:"${path} must be a negative number",integer:"${path} must be an integer"},au={min:"${path} field must be later than ${min}",max:"${path} field must be at earlier than ${max}"},uu={isValue:"${path} field must be ${value}"},cu={noUnknown:"${path} field has unspecified keys: ${unknown}"},lu={min:"${path} field must have at least ${min} items",max:"${path} field must have less than or equal to ${max} items"},hu=(r(Object.create(null),{mixed:iu,string:ou,number:su,date:au,object:cu,array:lu,boolean:uu}),function(e){return e&&e.__isYupSchema__}),du=function(){function e(e,t){if(this.refs=e,"function"!==typeof t){if(!ln(t,"is"))throw new TypeError("`is:` is required for `when()` conditions");if(!t.then&&!t.otherwise)throw new TypeError("either `then:` or `otherwise:` is required for `when()` conditions");var n=t.is,r=t.then,i=t.otherwise,o="function"===typeof n?n:function(){for(var e=arguments.length,t=new Array(e),r=0;r<e;r++)t[r]=arguments[r];return t.every((function(e){return e===n}))};this.fn=function(){for(var e=arguments.length,t=new Array(e),n=0;n<e;n++)t[n]=arguments[n];var s=t.pop(),a=t.pop(),u=o.apply(void 0,t)?r:i;if(u)return"function"===typeof u?u(a):a.concat(u.resolve(s))}}else this.fn=t}var t=e.prototype;return t.resolve=function(e,t){var n=this.refs.map((function(e){return e.getValue(null==t?void 0:t.value,null==t?void 0:t.parent,null==t?void 0:t.context)})),r=this.fn.apply(e,n.concat(e,t));if(void 0===r||r===e)return e;if(!hu(r))throw new TypeError("conditions must return a schema object");return r.resolve(t)},e}(),fu=du,pu=/\$\{\s*(\w+)\s*\}/g;function mu(e,t,n,r){var i=this;this.name="ValidationError",this.value=t,this.path=n,this.type=r,this.errors=[],this.inner=[],e&&[].concat(e).forEach((function(e){i.errors=i.errors.concat(e.errors||e),e.inner&&(i.inner=i.inner.concat(e.inner.length?e.inner:e))})),this.message=this.errors.length>1?this.errors.length+" errors occurred":this.errors[0],Error.captureStackTrace&&Error.captureStackTrace(this,mu)}mu.prototype=Object.create(Error.prototype),mu.prototype.constructor=mu,mu.isError=function(e){return e&&"ValidationError"===e.name},mu.formatError=function(e,t){var n=t.label||t.path||"this";return n!==t.path&&(t=r({},t,{path:n})),"string"===typeof e?e.replace(pu,(function(e,n){return ru(t[n])})):"function"===typeof e?e(t):e};var vu=function(e){var t=!1;return function(){t||(t=!0,e.apply(void 0,arguments))}};function gu(e,t){var n=e.endEarly,r=e.tests,i=e.args,o=e.value,s=e.errors,a=e.sort,u=e.path,c=vu(t),l=r.length,h=[];if(s=s||[],!l)return s.length?c(new mu(s,o,u)):c(null,o);for(var d=0;d<r.length;d++){var f=r[d];f(i,(function(e){if(e){if(!mu.isError(e))return c(e);if(n)return e.value=o,c(e);h.push(e)}if(--l<=0){if(h.length&&(a&&h.sort(a),s.length&&h.push.apply(h,s),s=h),s.length)return void c(new mu(s,o,u));c(null,o)}}))}}var _u=function(e){return"[object Object]"===Object.prototype.toString.call(e)};function yu(e,t){for(var n in t)if(ln(t,n)){var r=t[n],i=e[n];if(void 0===i)e[n]=r;else{if(i===r)continue;hu(i)?hu(r)&&(e[n]=r.concat(i)):_u(i)?_u(r)&&(e[n]=yu(i,r)):Array.isArray(i)&&Array.isArray(r)&&(e[n]=r.concat(i))}}return e}function bu(e,t){if(null==e)return{};var n,r,i={},o=Object.keys(e);for(r=0;r<o.length;r++)n=o[r],t.indexOf(n)>=0||(i[n]=e[n]);return i}function wu(e){return function(t,n,r){var i=-1,o=Object(t),s=r(t),a=s.length;while(a--){var u=s[e?a:++i];if(!1===n(o[u],u,o))break}return t}}var Iu=wu,Eu=Iu(),ku=Eu;function Tu(e,t){return e&&ku(e,t,Kr)}var Ou=Tu,Su="__lodash_hash_undefined__";function Ru(e){return this.__data__.set(e,Su),this}var Au=Ru;function Cu(e){return this.__data__.has(e)}var xu=Cu;function Pu(e){var t=-1,n=null==e?0:e.length;this.__data__=new bt;while(++t<n)this.add(e[t])}Pu.prototype.add=Pu.prototype.push=Au,Pu.prototype.has=xu;var Nu=Pu;function Fu(e,t){var n=-1,r=null==e?0:e.length;while(++n<r)if(t(e[n],n,e))return!0;return!1}var Du=Fu;function ju(e,t){return e.has(t)}var Lu=ju,Uu=1,Mu=2;function $u(e,t,n,r,i,o){var s=n&Uu,a=e.length,u=t.length;if(a!=u&&!(s&&u>a))return!1;var c=o.get(e),l=o.get(t);if(c&&l)return c==t&&l==e;var h=-1,d=!0,f=n&Mu?new Nu:void 0;o.set(e,t),o.set(t,e);while(++h<a){var p=e[h],m=t[h];if(r)var v=s?r(m,p,h,t,e,o):r(p,m,h,e,t,o);if(void 0!==v){if(v)continue;d=!1;break}if(f){if(!Du(t,(function(e,t){if(!Lu(f,t)&&(p===e||i(p,e,n,r,o)))return f.push(t)}))){d=!1;break}}else if(p!==m&&!i(p,m,n,r,o)){d=!1;break}}return o["delete"](e),o["delete"](t),d}var Vu=$u,zu=1,Bu=2,Hu="[object Boolean]",qu="[object Date]",Wu="[object Error]",Gu="[object Map]",Ju="[object Number]",Ku="[object RegExp]",Yu="[object Set]",Zu="[object String]",Xu="[object Symbol]",Qu="[object ArrayBuffer]",ec="[object DataView]",tc=_?_.prototype:void 0,nc=tc?tc.valueOf:void 0;function rc(e,t,n,r,i,o,s){switch(n){case ec:if(e.byteLength!=t.byteLength||e.byteOffset!=t.byteOffset)return!1;e=e.buffer,t=t.buffer;case Qu:return!(e.byteLength!=t.byteLength||!o(new vo(e),new vo(t)));case Hu:case qu:case Ju:return ze(+e,+t);case Wu:return e.name==t.name&&e.message==t.message;case Ku:case Zu:return e==t+"";case Gu:var a=na;case Yu:var u=r&zu;if(a||(a=ia),e.size!=t.size&&!u)return!1;var c=s.get(e);if(c)return c==t;r|=Bu,s.set(e,t);var l=Vu(a(e),a(t),r,i,o,s);return s["delete"](e),l;case Xu:if(nc)return nc.call(e)==nc.call(t)}return!1}var ic=rc,oc=1,sc=Object.prototype,ac=sc.hasOwnProperty;function uc(e,t,n,r,i,o){var s=n&oc,a=$i(e),u=a.length,c=$i(t),l=c.length;if(u!=l&&!s)return!1;var h=u;while(h--){var d=a[h];if(!(s?d in t:ac.call(t,d)))return!1}var f=o.get(e),p=o.get(t);if(f&&p)return f==t&&p==e;var m=!0;o.set(e,t),o.set(t,e);var v=s;while(++h<u){d=a[h];var g=e[d],_=t[d];if(r)var y=s?r(_,g,d,t,e,o):r(g,_,d,e,t,o);if(!(void 0===y?g===_||i(g,_,n,r,o):y)){m=!1;break}v||(v="constructor"==d)}if(m&&!v){var b=e.constructor,w=t.constructor;b==w||!("constructor"in e)||!("constructor"in t)||"function"==typeof b&&b instanceof b&&"function"==typeof w&&w instanceof w||(m=!1)}return o["delete"](e),o["delete"](t),m}var cc=uc,lc=1,hc="[object Arguments]",dc="[object Array]",fc="[object Object]",pc=Object.prototype,mc=pc.hasOwnProperty;function vc(e,t,n,r,i,o){var s=h(e),a=h(t),u=s?dc:co(e),c=a?dc:co(t);u=u==hc?fc:u,c=c==hc?fc:c;var l=u==fc,d=c==fc,f=u==c;if(f&&Wn(e)){if(!Wn(t))return!1;s=!0,l=!1}if(f&&!l)return o||(o=new En),s||Cr(e)?Vu(e,t,n,r,i,o):ic(e,t,u,n,r,i,o);if(!(n&lc)){var p=l&&mc.call(e,"__wrapped__"),m=d&&mc.call(t,"__wrapped__");if(p||m){var v=p?e.value():e,g=m?t.value():t;return o||(o=new En),i(v,g,n,r,o)}}return!!f&&(o||(o=new En),cc(e,t,n,r,i,o))}var gc=vc;function _c(e,t,n,r,i){return e===t||(null==e||null==t||!D(e)&&!D(t)?e!==e&&t!==t:gc(e,t,n,r,_c,i))}var yc=_c,bc=1,wc=2;function Ic(e,t,n,r){var i=n.length,o=i,s=!r;if(null==e)return!o;e=Object(e);while(i--){var a=n[i];if(s&&a[2]?a[1]!==e[a[0]]:!(a[0]in e))return!1}while(++i<o){a=n[i];var u=a[0],c=e[u],l=a[1];if(s&&a[2]){if(void 0===c&&!(u in e))return!1}else{var h=new En;if(r)var d=r(c,l,u,e,t,h);if(!(void 0===d?yc(l,c,bc|wc,r,h):d))return!1}}return!0}var Ec=Ic;function kc(e){return e===e&&!H(e)}var Tc=kc;function Oc(e){var t=Kr(e),n=t.length;while(n--){var r=t[n],i=e[r];t[n]=[r,i,Tc(i)]}return t}var Sc=Oc;function Rc(e,t){return function(n){return null!=n&&(n[e]===t&&(void 0!==t||e in Object(n)))}}var Ac=Rc;function Cc(e){var t=Sc(e);return 1==t.length&&t[0][2]?Ac(t[0][0],t[0][1]):function(n){return n===e||Ec(n,e,t)}}var xc=Cc;function Pc(e,t){t=Vt(t,e);var n=0,r=t.length;while(null!=e&&n<r)e=e[sn(t[n++])];return n&&n==r?e:void 0}var Nc=Pc;function Fc(e,t,n){var r=null==e?void 0:Nc(e,t);return void 0===r?n:r}var Dc=Fc;function jc(e,t){return null!=e&&t in Object(e)}var Lc=jc;function Uc(e,t){return null!=e&&un(e,t,Lc)}var Mc=Uc,$c=1,Vc=2;function zc(e,t){return z(e)&&Tc(t)?Ac(sn(e),t):function(n){var r=Dc(n,e);return void 0===r&&r===t?Mc(n,e):yc(t,r,$c|Vc)}}var Bc=zc;function Hc(e){return e}var qc=Hc;function Wc(e){return function(t){return null==t?void 0:t[e]}}var Gc=Wc;function Jc(e){return function(t){return Nc(t,e)}}var Kc=Jc;function Yc(e){return z(e)?Gc(sn(e)):Kc(e)}var Zc=Yc;function Xc(e){return"function"==typeof e?e:null==e?qc:"object"==typeof e?h(e)?Bc(e[0],e[1]):xc(e):Zc(e)}var Qc=Xc;function el(e,t){var n={};return t=Qc(t,3),Ou(e,(function(e,r,i){An(n,r,t(e,r,i))})),n}var tl=el,nl=n(4353),rl={context:"$",value:"."},il=function(){function e(e,t){if(void 0===t&&(t={}),"string"!==typeof e)throw new TypeError("ref must be a string, got: "+e);if(this.key=e.trim(),""===e)throw new TypeError("ref must be a non-empty string");this.isContext=this.key[0]===rl.context,this.isValue=this.key[0]===rl.value,this.isSibling=!this.isContext&&!this.isValue;var n=this.isContext?rl.context:this.isValue?rl.value:"";this.path=this.key.slice(n.length),this.getter=this.path&&(0,nl.getter)(this.path,!0),this.map=t.map}var t=e.prototype;return t.getValue=function(e,t,n){var r=this.isContext?n:this.isValue?e:t;return this.getter&&(r=this.getter(r||{})),this.map&&(r=this.map(r)),r},t.cast=function(e,t){return this.getValue(e,null==t?void 0:t.parent,null==t?void 0:t.context)},t.resolve=function(){return this},t.describe=function(){return{type:"ref",key:this.key}},t.toString=function(){return"Ref("+this.key+")"},e.isRef=function(e){return e&&e.__isYupRef},e}();function ol(e){function t(t,n){var i=t.value,o=t.path,s=t.label,a=t.options,u=t.originalValue,c=t.sync,l=bu(t,["value","path","label","options","originalValue","sync"]),h=e.name,d=e.test,f=e.params,p=e.message,m=a.parent,v=a.context;function g(e){return il.isRef(e)?e.getValue(i,m,v):e}function _(e){void 0===e&&(e={});var t=tl(r({value:i,originalValue:u,label:s,path:e.path||o},f,e.params),g),n=new mu(mu.formatError(e.message||p,t),i,t.path,e.type||h);return n.params=t,n}var y=r({path:o,parent:m,type:h,createError:_,resolve:g,options:a,originalValue:u},l);if(c){var b;try{var w;if(b=d.call(y,i,y),"function"===typeof(null==(w=b)?void 0:w.then))throw new Error('Validation test of type: "'+y.type+'" returned a Promise during a synchronous validate. This test will finish after the validate call has returned')}catch(I){return void n(I)}mu.isError(b)?n(b):b?n(null,b):n(_())}else try{Promise.resolve(d.call(y,i,y)).then((function(e){mu.isError(e)?n(e):e?n(null,e):n(_())}))}catch(I){n(I)}}return t.OPTIONS=e,t}il.prototype.__isYupRef=!0;var sl=function(e){return e.substr(0,e.length-1).substr(1)};function al(e,t,n,r){var i,o,s;return void 0===r&&(r=n),t?((0,nl.forEach)(t,(function(a,u,c){var l=u?sl(a):a;if(e=e.resolve({context:r,parent:i,value:n}),e.innerType){var h=c?parseInt(l,10):0;if(n&&h>=n.length)throw new Error("Yup.reach cannot resolve an array item at index: "+a+", in the path: "+t+". because there is no value at that index. ");i=n,n=n&&n[h],e=e.innerType}if(!c){if(!e.fields||!e.fields[l])throw new Error("The schema does not contain the path: "+t+". (failed at: "+s+' which is a type: "'+e._type+'")');i=n,n=n&&n[l],e=e.fields[l]}o=l,s=u?"["+a+"]":"."+a})),{schema:e,parent:i,parentPath:o}):{parent:i,parentPath:t,schema:e}}function ul(e,t){var n;if("undefined"===typeof Symbol||null==e[Symbol.iterator]){if(Array.isArray(e)||(n=cl(e))||t&&e&&"number"===typeof e.length){n&&(e=n);var r=0;return function(){return r>=e.length?{done:!0}:{done:!1,value:e[r++]}}}throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}return n=e[Symbol.iterator](),n.next.bind(n)}function cl(e,t){if(e){if("string"===typeof e)return ll(e,t);var n=Object.prototype.toString.call(e).slice(8,-1);return"Object"===n&&e.constructor&&(n=e.constructor.name),"Map"===n||"Set"===n?Array.from(e):"Arguments"===n||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)?ll(e,t):void 0}}function ll(e,t){(null==t||t>e.length)&&(t=e.length);for(var n=0,r=new Array(t);n<t;n++)r[n]=e[n];return r}var hl=function(){function e(){this.list=new Set,this.refs=new Map}var t=e.prototype;return t.describe=function(){for(var e,t=[],n=ul(this.list);!(e=n()).done;){var r=e.value;t.push(r)}for(var i,o=ul(this.refs);!(i=o()).done;){var s=i.value,a=s[1];t.push(a.describe())}return t},t.toArray=function(){return Ka(this.list).concat(Ka(this.refs.values()))},t.add=function(e){il.isRef(e)?this.refs.set(e.key,e):this.list.add(e)},t.delete=function(e){il.isRef(e)?this.refs.delete(e.key):this.list.delete(e)},t.has=function(e,t){if(this.list.has(e))return!0;var n,r=this.refs.values();while(n=r.next(),!n.done)if(t(n.value)===e)return!0;return!1},t.clone=function(){var t=new e;return t.list=new Set(this.list),t.refs=new Map(this.refs),t},t.merge=function(e,t){var n=this.clone();return e.list.forEach((function(e){return n.add(e)})),e.refs.forEach((function(e){return n.add(e)})),t.list.forEach((function(e){return n.delete(e)})),t.refs.forEach((function(e){return n.delete(e)})),n},o(e,[{key:"size",get:function(){return this.list.size+this.refs.size}}]),e}();function dl(e){var t=this;if(void 0===e&&(e={}),!(this instanceof dl))return new dl;this._deps=[],this._conditions=[],this._options={abortEarly:!0,recursive:!0},this._exclusive=Object.create(null),this._whitelist=new hl,this._blacklist=new hl,this.tests=[],this.transforms=[],this.withMutation((function(){t.typeError(iu.notType)})),ln(e,"default")&&(this._defaultDefault=e.default),this.type=e.type||"mixed",this._type=e.type||"mixed"}for(var fl=dl.prototype={__isYupSchema__:!0,constructor:dl,clone:function(){var e=this;return this._mutate?this:Ks(this,(function(t,n){return hu(t)&&t!==e?t:"_whitelist"===n||"_blacklist"===n?t.clone():void 0}))},label:function(e){var t=this.clone();return t._label=e,t},meta:function(e){if(0===arguments.length)return this._meta;var t=this.clone();return t._meta=r(t._meta||{},e),t},withMutation:function(e){var t=this._mutate;this._mutate=!0;var n=e(this);return this._mutate=t,n},concat:function(e){if(!e||e===this)return this;if(e._type!==this._type&&"mixed"!==this._type)throw new TypeError("You cannot `concat()` schema's of different types: "+this._type+" and "+e._type);var t=yu(e.clone(),this);return ln(e,"_default")&&(t._default=e._default),t.tests=this.tests,t._exclusive=this._exclusive,t._whitelist=this._whitelist.merge(e._whitelist,e._blacklist),t._blacklist=this._blacklist.merge(e._blacklist,e._whitelist),t.withMutation((function(t){e.tests.forEach((function(e){t.test(e.OPTIONS)}))})),t},isType:function(e){return!(!this._nullable||null!==e)||(!this._typeCheck||this._typeCheck(e))},resolve:function(e){var t=this;if(t._conditions.length){var n=t._conditions;t=t.clone(),t._conditions=[],t=n.reduce((function(t,n){return n.resolve(t,e)}),t),t=t.resolve(e)}return t},cast:function(e,t){void 0===t&&(t={});var n=this.resolve(r({value:e},t)),i=n._cast(e,t);if(void 0!==e&&!1!==t.assert&&!0!==n.isType(i)){var o=ru(e),s=ru(i);throw new TypeError("The value of "+(t.path||"field")+' could not be cast to a value that satisfies the schema type: "'+n._type+'". \n\nattempted value: '+o+" \n"+(s!==o?"result of cast: "+s:""))}return i},_cast:function(e){var t=this,n=void 0===e?e:this.transforms.reduce((function(n,r){return r.call(t,n,e)}),e);return void 0===n&&ln(this,"_default")&&(n=this.getDefault()),n},_validate:function(e,t,n){var i=this;void 0===t&&(t={});var o=t,s=o.sync,a=o.path,u=o.from,c=void 0===u?[]:u,l=o.originalValue,h=void 0===l?e:l,d=o.strict,f=void 0===d?this._options.strict:d,p=o.abortEarly,m=void 0===p?this._options.abortEarly:p,v=e;f||(this._validating=!0,v=this._cast(v,r({assert:!1},t)),this._validating=!1);var g={value:v,path:a,options:t,originalValue:h,schema:this,label:this._label,sync:s,from:c},_=[];return this._typeError&&_.push(this._typeError),this._whitelistError&&_.push(this._whitelistError),this._blacklistError&&_.push(this._blacklistError),gu({args:g,value:v,path:a,sync:s,tests:_,endEarly:m},(function(e){e?n(e):gu({tests:i.tests,args:g,path:a,sync:s,value:v,endEarly:m},n)}))},validate:function(e,t,n){void 0===t&&(t={});var i=this.resolve(r({},t,{value:e}));return"function"===typeof n?i._validate(e,t,n):new Promise((function(n,r){return i._validate(e,t,(function(e,t){e?r(e):n(t)}))}))},validateSync:function(e,t){void 0===t&&(t={});var n,i=this.resolve(r({},t,{value:e}));return i._validate(e,r({},t,{sync:!0}),(function(e,t){if(e)throw e;n=t})),n},isValid:function(e,t){return this.validate(e,t).then((function(){return!0})).catch((function(e){if("ValidationError"===e.name)return!1;throw e}))},isValidSync:function(e,t){try{return this.validateSync(e,t),!0}catch(n){if("ValidationError"===n.name)return!1;throw n}},_getDefault:function(){var e=ln(this,"_default")?this._default:this._defaultDefault;return"function"===typeof e?e.call(this):Ks(e)},getDefault:function(e){void 0===e&&(e={});var t=this.resolve(e);return t._getDefault()},default:function(e){if(0===arguments.length)return console.warn("Calling `schema.default()` as a getter to retrieve a default is deprecated and will be removed in the next version. \nUse `schema.getDefault()` instead."),this._getDefault();var t=this.clone();return t._default=e,t},strict:function(e){void 0===e&&(e=!0);var t=this.clone();return t._options.strict=e,t},_isPresent:function(e){return null!=e},required:function(e){return void 0===e&&(e=iu.required),this.test({message:e,name:"required",exclusive:!0,test:function(e){return this.schema._isPresent(e)}})},notRequired:function(){var e=this.clone();return e.tests=e.tests.filter((function(e){return"required"!==e.OPTIONS.name})),e},nullable:function(e){void 0===e&&(e=!0);var t=this.clone();return t._nullable=e,t},transform:function(e){var t=this.clone();return t.transforms.push(e),t},test:function(){var e;if(e=1===arguments.length?"function"===typeof(arguments.length<=0?void 0:arguments[0])?{test:arguments.length<=0?void 0:arguments[0]}:arguments.length<=0?void 0:arguments[0]:2===arguments.length?{name:arguments.length<=0?void 0:arguments[0],test:arguments.length<=1?void 0:arguments[1]}:{name:arguments.length<=0?void 0:arguments[0],message:arguments.length<=1?void 0:arguments[1],test:arguments.length<=2?void 0:arguments[2]},void 0===e.message&&(e.message=iu["default"]),"function"!==typeof e.test)throw new TypeError("`test` is a required parameters");var t=this.clone(),n=ol(e),r=e.exclusive||e.name&&!0===t._exclusive[e.name];if(e.exclusive&&!e.name)throw new TypeError("Exclusive tests must provide a unique `name` identifying the test");return t._exclusive[e.name]=!!e.exclusive,t.tests=t.tests.filter((function(t){if(t.OPTIONS.name===e.name){if(r)return!1;if(t.OPTIONS.test===n.OPTIONS.test)return!1}return!0})),t.tests.push(n),t},when:function(e,t){1===arguments.length&&(t=e,e=".");var n=this.clone(),r=[].concat(e).map((function(e){return new il(e)}));return r.forEach((function(e){e.isSibling&&n._deps.push(e.key)})),n._conditions.push(new fu(r,t)),n},typeError:function(e){var t=this.clone();return t._typeError=ol({message:e,name:"typeError",test:function(e){return!(void 0!==e&&!this.schema.isType(e))||this.createError({params:{type:this.schema._type}})}}),t},oneOf:function(e,t){void 0===t&&(t=iu.oneOf);var n=this.clone();return e.forEach((function(e){n._whitelist.add(e),n._blacklist.delete(e)})),n._whitelistError=ol({message:t,name:"oneOf",test:function(e){if(void 0===e)return!0;var t=this.schema._whitelist;return!!t.has(e,this.resolve)||this.createError({params:{values:t.toArray().join(", ")}})}}),n},notOneOf:function(e,t){void 0===t&&(t=iu.notOneOf);var n=this.clone();return e.forEach((function(e){n._blacklist.add(e),n._whitelist.delete(e)})),n._blacklistError=ol({message:t,name:"notOneOf",test:function(e){var t=this.schema._blacklist;return!t.has(e,this.resolve)||this.createError({params:{values:t.toArray().join(", ")}})}}),n},strip:function(e){void 0===e&&(e=!0);var t=this.clone();return t._strip=e,t},_option:function(e,t){return ln(t,e)?t[e]:this._options[e]},describe:function(){var e=this.clone(),t={type:e._type,meta:e._meta,label:e._label,tests:e.tests.map((function(e){return{name:e.OPTIONS.name,params:e.OPTIONS.params}})).filter((function(e,t,n){return n.findIndex((function(t){return t.name===e.name}))===t}))};return e._whitelist.size&&(t.oneOf=e._whitelist.describe()),e._blacklist.size&&(t.notOneOf=e._blacklist.describe()),t},defined:function(e){return void 0===e&&(e=iu.defined),this.test({message:e,name:"defined",exclusive:!0,test:function(e){return void 0!==e}})}},pl=function(){var e=vl[ml];fl[e+"At"]=function(t,n,i){void 0===i&&(i={});var o=al(this,t,n,i.context),s=o.parent,a=o.parentPath,u=o.schema;return u[e](s&&s[a],r({},i,{parent:s,path:t}))}},ml=0,vl=["validate","validateSync"];ml<vl.length;ml++)pl();for(var gl=0,_l=["equals","is"];gl<_l.length;gl++){var yl=_l[gl];fl[yl]=fl.oneOf}for(var bl=0,wl=["not","nope"];bl<wl.length;bl++){var Il=wl[bl];fl[Il]=fl.notOneOf}function El(e,t,n){e.prototype=Object.create(t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),r(e.prototype,n)}fl.optional=fl.notRequired;var kl=function(e){return null==e};function Tl(){var e=this;if(!(this instanceof Tl))return new Tl;dl.call(this,{type:"boolean"}),this.withMutation((function(){e.transform((function(e){if(!this.isType(e)){if(/^(true|1)$/i.test(e))return!0;if(/^(false|0)$/i.test(e))return!1}return e}))}))}El(Tl,dl,{_typeCheck:function(e){return e instanceof Boolean&&(e=e.valueOf()),"boolean"===typeof e},isTrue:function(e){return void 0===e&&(e=uu.isValue),this.test({message:e,name:"is-value",exclusive:!0,params:{value:"true"},test:function(e){return kl(e)||!0===e}})},isFalse:function(e){return void 0===e&&(e=uu.isValue),this.test({message:e,name:"is-value",exclusive:!0,params:{value:"false"},test:function(e){return kl(e)||!1===e}})}});var Ol=/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i,Sl=/^((https?|ftp):)?\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,Rl=/^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i,Al=function(e){return kl(e)||e===e.trim()},Cl={}.toString();function xl(){var e=this;if(!(this instanceof xl))return new xl;dl.call(this,{type:"string"}),this.withMutation((function(){e.transform((function(e){if(this.isType(e))return e;if(Array.isArray(e))return e;var t=null!=e&&e.toString?e.toString():e;return t===Cl?e:t}))}))}El(xl,dl,{_typeCheck:function(e){return e instanceof String&&(e=e.valueOf()),"string"===typeof e},_isPresent:function(e){return dl.prototype._isPresent.call(this,e)&&!!e.length},length:function(e,t){return void 0===t&&(t=ou.length),this.test({message:t,name:"length",exclusive:!0,params:{length:e},test:function(t){return kl(t)||t.length===this.resolve(e)}})},min:function(e,t){return void 0===t&&(t=ou.min),this.test({message:t,name:"min",exclusive:!0,params:{min:e},test:function(t){return kl(t)||t.length>=this.resolve(e)}})},max:function(e,t){return void 0===t&&(t=ou.max),this.test({name:"max",exclusive:!0,message:t,params:{max:e},test:function(t){return kl(t)||t.length<=this.resolve(e)}})},matches:function(e,t){var n,r,i=!1;return t&&("object"===typeof t?(i=t.excludeEmptyString,n=t.message,r=t.name):n=t),this.test({name:r||"matches",message:n||ou.matches,params:{regex:e},test:function(t){return kl(t)||""===t&&i||-1!==t.search(e)}})},email:function(e){return void 0===e&&(e=ou.email),this.matches(Ol,{name:"email",message:e,excludeEmptyString:!0})},url:function(e){return void 0===e&&(e=ou.url),this.matches(Sl,{name:"url",message:e,excludeEmptyString:!0})},uuid:function(e){return void 0===e&&(e=ou.uuid),this.matches(Rl,{name:"uuid",message:e,excludeEmptyString:!1})},ensure:function(){return this.default("").transform((function(e){return null===e?"":e}))},trim:function(e){return void 0===e&&(e=ou.trim),this.transform((function(e){return null!=e?e.trim():e})).test({message:e,name:"trim",test:Al})},lowercase:function(e){return void 0===e&&(e=ou.lowercase),this.transform((function(e){return kl(e)?e:e.toLowerCase()})).test({message:e,name:"string_case",exclusive:!0,test:function(e){return kl(e)||e===e.toLowerCase()}})},uppercase:function(e){return void 0===e&&(e=ou.uppercase),this.transform((function(e){return kl(e)?e:e.toUpperCase()})).test({message:e,name:"string_case",exclusive:!0,test:function(e){return kl(e)||e===e.toUpperCase()}})}});var Pl=function(e){return e!=+e};function Nl(){var e=this;if(!(this instanceof Nl))return new Nl;dl.call(this,{type:"number"}),this.withMutation((function(){e.transform((function(e){var t=e;if("string"===typeof t){if(t=t.replace(/\s/g,""),""===t)return NaN;t=+t}return this.isType(t)?t:parseFloat(t)}))}))}El(Nl,dl,{_typeCheck:function(e){return e instanceof Number&&(e=e.valueOf()),"number"===typeof e&&!Pl(e)},min:function(e,t){return void 0===t&&(t=su.min),this.test({message:t,name:"min",exclusive:!0,params:{min:e},test:function(t){return kl(t)||t>=this.resolve(e)}})},max:function(e,t){return void 0===t&&(t=su.max),this.test({message:t,name:"max",exclusive:!0,params:{max:e},test:function(t){return kl(t)||t<=this.resolve(e)}})},lessThan:function(e,t){return void 0===t&&(t=su.lessThan),this.test({message:t,name:"max",exclusive:!0,params:{less:e},test:function(t){return kl(t)||t<this.resolve(e)}})},moreThan:function(e,t){return void 0===t&&(t=su.moreThan),this.test({message:t,name:"min",exclusive:!0,params:{more:e},test:function(t){return kl(t)||t>this.resolve(e)}})},positive:function(e){return void 0===e&&(e=su.positive),this.moreThan(0,e)},negative:function(e){return void 0===e&&(e=su.negative),this.lessThan(0,e)},integer:function(e){return void 0===e&&(e=su.integer),this.test({name:"integer",message:e,test:function(e){return kl(e)||Number.isInteger(e)}})},truncate:function(){return this.transform((function(e){return kl(e)?e:0|e}))},round:function(e){var t=["ceil","floor","round","trunc"];if(e=e&&e.toLowerCase()||"round","trunc"===e)return this.truncate();if(-1===t.indexOf(e.toLowerCase()))throw new TypeError("Only valid options for round() are: "+t.join(", "));return this.transform((function(t){return kl(t)?t:Math[e](t)}))}});var Fl=/^(\d{4}|[+\-]\d{6})(?:-?(\d{2})(?:-?(\d{2}))?)?(?:[ T]?(\d{2}):?(\d{2})(?::?(\d{2})(?:[,\.](\d{1,}))?)?(?:(Z)|([+\-])(\d{2})(?::?(\d{2}))?)?)?$/;function Dl(e){var t,n,r=[1,4,5,6,7,10,11],i=0;if(n=Fl.exec(e)){for(var o,s=0;o=r[s];++s)n[o]=+n[o]||0;n[2]=(+n[2]||1)-1,n[3]=+n[3]||1,n[7]=n[7]?String(n[7]).substr(0,3):0,void 0!==n[8]&&""!==n[8]||void 0!==n[9]&&""!==n[9]?("Z"!==n[8]&&void 0!==n[9]&&(i=60*n[10]+n[11],"+"===n[9]&&(i=0-i)),t=Date.UTC(n[1],n[2],n[3],n[4],n[5]+i,n[6],n[7])):t=+new Date(n[1],n[2],n[3],n[4],n[5],n[6],n[7])}else t=Date.parse?Date.parse(e):NaN;return t}var jl=new Date(""),Ll=function(e){return"[object Date]"===Object.prototype.toString.call(e)};function Ul(){var e=this;if(!(this instanceof Ul))return new Ul;dl.call(this,{type:"date"}),this.withMutation((function(){e.transform((function(e){return this.isType(e)?e:(e=Dl(e),isNaN(e)?jl:new Date(e))}))}))}function Ml(e,t,n,r){var i=-1,o=null==e?0:e.length;r&&o&&(n=e[++i]);while(++i<o)n=t(n,e[i],i,e);return n}El(Ul,dl,{_typeCheck:function(e){return Ll(e)&&!isNaN(e.getTime())},min:function(e,t){void 0===t&&(t=au.min);var n=e;if(!il.isRef(n)&&(n=this.cast(e),!this._typeCheck(n)))throw new TypeError("`min` must be a Date or a value that can be `cast()` to a Date");return this.test({message:t,name:"min",exclusive:!0,params:{min:e},test:function(e){return kl(e)||e>=this.resolve(n)}})},max:function(e,t){void 0===t&&(t=au.max);var n=e;if(!il.isRef(n)&&(n=this.cast(e),!this._typeCheck(n)))throw new TypeError("`max` must be a Date or a value that can be `cast()` to a Date");return this.test({message:t,name:"max",exclusive:!0,params:{max:e},test:function(e){return kl(e)||e<=this.resolve(n)}})}});var $l=Ml;function Vl(e){return function(t){return null==e?void 0:e[t]}}var zl=Vl,Bl={"À":"A","Á":"A","Â":"A","Ã":"A","Ä":"A","Å":"A","à":"a","á":"a","â":"a","ã":"a","ä":"a","å":"a","Ç":"C","ç":"c","Ð":"D","ð":"d","È":"E","É":"E","Ê":"E","Ë":"E","è":"e","é":"e","ê":"e","ë":"e","Ì":"I","Í":"I","Î":"I","Ï":"I","ì":"i","í":"i","î":"i","ï":"i","Ñ":"N","ñ":"n","Ò":"O","Ó":"O","Ô":"O","Õ":"O","Ö":"O","Ø":"O","ò":"o","ó":"o","ô":"o","õ":"o","ö":"o","ø":"o","Ù":"U","Ú":"U","Û":"U","Ü":"U","ù":"u","ú":"u","û":"u","ü":"u","Ý":"Y","ý":"y","ÿ":"y","Æ":"Ae","æ":"ae","Þ":"Th","þ":"th","ß":"ss","Ā":"A","Ă":"A","Ą":"A","ā":"a","ă":"a","ą":"a","Ć":"C","Ĉ":"C","Ċ":"C","Č":"C","ć":"c","ĉ":"c","ċ":"c","č":"c","Ď":"D","Đ":"D","ď":"d","đ":"d","Ē":"E","Ĕ":"E","Ė":"E","Ę":"E","Ě":"E","ē":"e","ĕ":"e","ė":"e","ę":"e","ě":"e","Ĝ":"G","Ğ":"G","Ġ":"G","Ģ":"G","ĝ":"g","ğ":"g","ġ":"g","ģ":"g","Ĥ":"H","Ħ":"H","ĥ":"h","ħ":"h","Ĩ":"I","Ī":"I","Ĭ":"I","Į":"I","İ":"I","ĩ":"i","ī":"i","ĭ":"i","į":"i","ı":"i","Ĵ":"J","ĵ":"j","Ķ":"K","ķ":"k","ĸ":"k","Ĺ":"L","Ļ":"L","Ľ":"L","Ŀ":"L","Ł":"L","ĺ":"l","ļ":"l","ľ":"l","ŀ":"l","ł":"l","Ń":"N","Ņ":"N","Ň":"N","Ŋ":"N","ń":"n","ņ":"n","ň":"n","ŋ":"n","Ō":"O","Ŏ":"O","Ő":"O","ō":"o","ŏ":"o","ő":"o","Ŕ":"R","Ŗ":"R","Ř":"R","ŕ":"r","ŗ":"r","ř":"r","Ś":"S","Ŝ":"S","Ş":"S","Š":"S","ś":"s","ŝ":"s","ş":"s","š":"s","Ţ":"T","Ť":"T","Ŧ":"T","ţ":"t","ť":"t","ŧ":"t","Ũ":"U","Ū":"U","Ŭ":"U","Ů":"U","Ű":"U","Ų":"U","ũ":"u","ū":"u","ŭ":"u","ů":"u","ű":"u","ų":"u","Ŵ":"W","ŵ":"w","Ŷ":"Y","ŷ":"y","Ÿ":"Y","Ź":"Z","Ż":"Z","Ž":"Z","ź":"z","ż":"z","ž":"z","Ĳ":"IJ","ĳ":"ij","Œ":"Oe","œ":"oe","ŉ":"'n","ſ":"s"},Hl=zl(Bl),ql=Hl,Wl=/[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g,Gl="\\u0300-\\u036f",Jl="\\ufe20-\\ufe2f",Kl="\\u20d0-\\u20ff",Yl=Gl+Jl+Kl,Zl="["+Yl+"]",Xl=RegExp(Zl,"g");function Ql(e){return e=Mt(e),e&&e.replace(Wl,ql).replace(Xl,"")}var eh=Ql,th=/[^\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+/g;function nh(e){return e.match(th)||[]}var rh=nh,ih=/[a-z][A-Z]|[A-Z]{2}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/;function oh(e){return ih.test(e)}var sh=oh,ah="\\ud800-\\udfff",uh="\\u0300-\\u036f",ch="\\ufe20-\\ufe2f",lh="\\u20d0-\\u20ff",hh=uh+ch+lh,dh="\\u2700-\\u27bf",fh="a-z\\xdf-\\xf6\\xf8-\\xff",ph="\\xac\\xb1\\xd7\\xf7",mh="\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf",vh="\\u2000-\\u206f",gh=" \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000",_h="A-Z\\xc0-\\xd6\\xd8-\\xde",yh="\\ufe0e\\ufe0f",bh=ph+mh+vh+gh,wh="['’]",Ih="["+bh+"]",Eh="["+hh+"]",kh="\\d+",Th="["+dh+"]",Oh="["+fh+"]",Sh="[^"+ah+bh+kh+dh+fh+_h+"]",Rh="\\ud83c[\\udffb-\\udfff]",Ah="(?:"+Eh+"|"+Rh+")",Ch="[^"+ah+"]",xh="(?:\\ud83c[\\udde6-\\uddff]){2}",Ph="[\\ud800-\\udbff][\\udc00-\\udfff]",Nh="["+_h+"]",Fh="\\u200d",Dh="(?:"+Oh+"|"+Sh+")",jh="(?:"+Nh+"|"+Sh+")",Lh="(?:"+wh+"(?:d|ll|m|re|s|t|ve))?",Uh="(?:"+wh+"(?:D|LL|M|RE|S|T|VE))?",Mh=Ah+"?",$h="["+yh+"]?",Vh="(?:"+Fh+"(?:"+[Ch,xh,Ph].join("|")+")"+$h+Mh+")*",zh="\\d*(?:1st|2nd|3rd|(?![123])\\dth)(?=\\b|[A-Z_])",Bh="\\d*(?:1ST|2ND|3RD|(?![123])\\dTH)(?=\\b|[a-z_])",Hh=$h+Mh+Vh,qh="(?:"+[Th,xh,Ph].join("|")+")"+Hh,Wh=RegExp([Nh+"?"+Oh+"+"+Lh+"(?="+[Ih,Nh,"$"].join("|")+")",jh+"+"+Uh+"(?="+[Ih,Nh+Dh,"$"].join("|")+")",Nh+"?"+Dh+"+"+Lh,Nh+"+"+Uh,Bh,zh,kh,qh].join("|"),"g");function Gh(e){return e.match(Wh)||[]}var Jh=Gh;function Kh(e,t,n){return e=Mt(e),t=n?void 0:t,void 0===t?sh(e)?Jh(e):rh(e):e.match(t)||[]}var Yh=Kh,Zh="['’]",Xh=RegExp(Zh,"g");function Qh(e){return function(t){return $l(Yh(eh(t).replace(Xh,"")),e,"")}}var ed=Qh,td=ed((function(e,t,n){return e+(n?"_":"")+t.toLowerCase()})),nd=td;function rd(e,t,n){var r=-1,i=e.length;t<0&&(t=-t>i?0:i+t),n=n>i?i:n,n<0&&(n+=i),i=t>n?0:n-t>>>0,t>>>=0;var o=Array(i);while(++r<i)o[r]=e[r+t];return o}var id=rd;function od(e,t,n){var r=e.length;return n=void 0===n?r:n,!t&&n>=r?e:id(e,t,n)}var sd=od;function ad(e){return function(t){t=Mt(t);var n=va(t)?$a(t):void 0,r=n?n[0]:t.charAt(0),i=n?sd(n,1).join(""):t.slice(1);return r[e]()+i}}var ud=ad,cd=ud("toUpperCase"),ld=cd;function hd(e){return ld(Mt(e).toLowerCase())}var dd=hd,fd=ed((function(e,t,n){return t=t.toLowerCase(),e+(n?dd(t):t)})),pd=fd;function md(e,t){var n={};return t=Qc(t,3),Ou(e,(function(e,r,i){An(n,t(e,r,i),e)})),n}var vd=md,gd=n(1414),_d=n.n(gd);function yd(e,t){void 0===t&&(t=[]);var n=[],r=[];function i(e,i){var o=(0,nl.split)(e)[0];~r.indexOf(o)||r.push(o),~t.indexOf(i+"-"+o)||n.push([i,o])}var o=function(t){if(ln(e,t)){var n=e[t];~r.indexOf(t)||r.push(t),il.isRef(n)&&n.isSibling?i(n.path,t):hu(n)&&n._deps&&n._deps.forEach((function(e){return i(e,t)}))}};for(var s in e)o(s);return _d().array(r,n).reverse()}function bd(e,t){var n=1/0;return e.some((function(e,r){if(-1!==t.path.indexOf(e))return n=r,!0})),n}function wd(e){return function(t,n){return bd(e,t)-bd(e,n)}}function Id(e,t){var n;if("undefined"===typeof Symbol||null==e[Symbol.iterator]){if(Array.isArray(e)||(n=Ed(e))||t&&e&&"number"===typeof e.length){n&&(e=n);var r=0;return function(){return r>=e.length?{done:!0}:{done:!1,value:e[r++]}}}throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}return n=e[Symbol.iterator](),n.next.bind(n)}function Ed(e,t){if(e){if("string"===typeof e)return kd(e,t);var n=Object.prototype.toString.call(e).slice(8,-1);return"Object"===n&&e.constructor&&(n=e.constructor.name),"Map"===n||"Set"===n?Array.from(e):"Arguments"===n||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)?kd(e,t):void 0}}function kd(e,t){(null==t||t>e.length)&&(t=e.length);for(var n=0,r=new Array(t);n<t;n++)r[n]=e[n];return r}var Td=function(e){return"[object Object]"===Object.prototype.toString.call(e)};function Od(e,t){var n=Object.keys(e.fields);return Object.keys(t).filter((function(e){return-1===n.indexOf(e)}))}function Sd(e){var t=this;if(!(this instanceof Sd))return new Sd(e);dl.call(this,{type:"object",default:function(){var e=this;if(this._nodes.length){var t={};return this._nodes.forEach((function(n){t[n]=e.fields[n].default?e.fields[n].getDefault():void 0})),t}}}),this.fields=Object.create(null),this._sortErrors=wd([]),this._nodes=[],this._excludedEdges=[],this.withMutation((function(){t.transform((function(e){if("string"===typeof e)try{e=JSON.parse(e)}catch(t){e=null}return this.isType(e)?e:null})),e&&t.shape(e)}))}El(Sd,dl,{_typeCheck:function(e){return Td(e)||"function"===typeof e},_cast:function(e,t){var n=this;void 0===t&&(t={});var i=dl.prototype._cast.call(this,e);if(void 0===i)return this.getDefault();if(!this._typeCheck(i))return i;for(var o,s=this.fields,a=!0===this._option("stripUnknown",t),u=this._nodes.concat(Object.keys(i).filter((function(e){return-1===n._nodes.indexOf(e)}))),c={},l=r({},t,{parent:c,__validating:t.__validating||!1}),h=!1,d=Id(u);!(o=d()).done;){var f=o.value,p=s[f],m=ln(i,f);if(p){var v=void 0,g=p._options&&p._options.strict;if(l.path=(t.path?t.path+".":"")+f,l.value=i[f],p=p.resolve(l),!0===p._strip){h=h||f in i;continue}v=t.__validating&&g?i[f]:p.cast(i[f],l),void 0!==v&&(c[f]=v)}else m&&!a&&(c[f]=i[f]);c[f]!==i[f]&&(h=!0)}return h?c:i},_validate:function(e,t,n){var i=this;void 0===t&&(t={});var o=[],s=t,a=s.sync,u=s.from,c=void 0===u?[]:u,l=s.originalValue,h=void 0===l?e:l,d=s.abortEarly,f=void 0===d?this._options.abortEarly:d,p=s.recursive,m=void 0===p?this._options.recursive:p;c=[{schema:this,value:h}].concat(c),t.__validating=!0,t.originalValue=h,t.from=c,dl.prototype._validate.call(this,e,t,(function(e,s){if(e){if(f)return void n(e);o.push(e),s=e.value}if(m&&Td(s)){h=h||s;var u=i._nodes.map((function(e){return function(n,o){var a=-1===e.indexOf(".")?(t.path?t.path+".":"")+e:(t.path||"")+'["'+e+'"]',u=i.fields[e];u&&u.validate?u.validate(s[e],r({},t,{path:a,from:c,strict:!0,parent:s,originalValue:h[e]}),o):o(null)}}));gu({sync:a,tests:u,value:s,errors:o,endEarly:f,sort:i._sortErrors,path:t.path},n)}else n(o[0]||null,s)}))},concat:function(e){var t=dl.prototype.concat.call(this,e);return t._nodes=yd(t.fields,t._excludedEdges),t},shape:function(e,t){void 0===t&&(t=[]);var n=this.clone(),i=r(n.fields,e);if(n.fields=i,n._sortErrors=wd(Object.keys(i)),t.length){Array.isArray(t[0])||(t=[t]);var o=t.map((function(e){var t=e[0],n=e[1];return t+"-"+n}));n._excludedEdges=n._excludedEdges.concat(o)}return n._nodes=yd(i,n._excludedEdges),n},pick:function(e){for(var t,n={},r=Id(e);!(t=r()).done;){var i=t.value;this.fields[i]&&(n[i]=this.fields[i])}return this.clone().withMutation((function(e){return e.fields={},e.shape(n)}))},omit:function(e){var t=this.clone(),n=t.fields;t.fields={};for(var r,i=Id(e);!(r=i()).done;){var o=r.value;delete n[o]}return t.withMutation((function(e){return e.shape(n)}))},from:function(e,t,n){var i=(0,nl.getter)(e,!0);return this.transform((function(o){if(null==o)return o;var s=o;return ln(o,e)&&(s=r({},o),n||delete s[e],s[t]=i(o)),s}))},noUnknown:function(e,t){void 0===e&&(e=!0),void 0===t&&(t=cu.noUnknown),"string"===typeof e&&(t=e,e=!0);var n=this.test({name:"noUnknown",exclusive:!0,message:t,test:function(t){if(null==t)return!0;var n=Od(this.schema,t);return!e||0===n.length||this.createError({params:{unknown:n.join(", ")}})}});return n._options.stripUnknown=e,n},unknown:function(e,t){return void 0===e&&(e=!0),void 0===t&&(t=cu.noUnknown),this.noUnknown(!e,t)},transformKeys:function(e){return this.transform((function(t){return t&&vd(t,(function(t,n){return e(n)}))}))},camelCase:function(){return this.transformKeys(pd)},snakeCase:function(){return this.transformKeys(nd)},constantCase:function(){return this.transformKeys((function(e){return nd(e).toUpperCase()}))},describe:function(){var e=dl.prototype.describe.call(this);return e.fields=tl(this.fields,(function(e){return e.describe()})),e}});function Rd(e){var t=this;if(!(this instanceof Rd))return new Rd(e);dl.call(this,{type:"array"}),this._subType=void 0,this.innerType=void 0,this.withMutation((function(){t.transform((function(e){if("string"===typeof e)try{e=JSON.parse(e)}catch(t){e=null}return this.isType(e)?e:null})),e&&t.of(e)}))}El(Rd,dl,{_typeCheck:function(e){return Array.isArray(e)},_cast:function(e,t){var n=this,i=dl.prototype._cast.call(this,e,t);if(!this._typeCheck(i)||!this.innerType)return i;var o=!1,s=i.map((function(e,i){var s=n.innerType.cast(e,r({},t,{path:(t.path||"")+"["+i+"]"}));return s!==e&&(o=!0),s}));return o?s:i},_validate:function(e,t,n){var i=this;void 0===t&&(t={});var o=[],s=t.sync,a=t.path,u=this.innerType,c=this._option("abortEarly",t),l=this._option("recursive",t),h=null!=t.originalValue?t.originalValue:e;dl.prototype._validate.call(this,e,t,(function(e,d){if(e){if(c)return void n(e);o.push(e),d=e.value}if(l&&u&&i._typeCheck(d)){h=h||d;for(var f=new Array(d.length),p=function(e){var n=d[e],i=(t.path||"")+"["+e+"]",o=r({},t,{path:i,strict:!0,parent:d,index:e,originalValue:h[e]});f[e]=function(e,t){return u.validate?u.validate(n,o,t):t(null)}},m=0;m<d.length;m++)p(m);gu({sync:s,path:a,value:d,errors:o,endEarly:c,tests:f},n)}else n(o[0]||null,d)}))},of:function(e){var t=this.clone();if(!1!==e&&!hu(e))throw new TypeError("`array.of()` sub-schema must be a valid yup schema, or `false` to negate a current sub-schema. not: "+ru(e));return t._subType=e,t.innerType=e,t},min:function(e,t){return t=t||lu.min,this.test({message:t,name:"min",exclusive:!0,params:{min:e},test:function(t){return kl(t)||t.length>=this.resolve(e)}})},max:function(e,t){return t=t||lu.max,this.test({message:t,name:"max",exclusive:!0,params:{max:e},test:function(t){return kl(t)||t.length<=this.resolve(e)}})},length:function(e,t){return t=t||lu.length,this.test({message:t,name:"length",exclusive:!0,params:{length:e},test:function(t){return kl(t)||t.length===this.resolve(e)}})},ensure:function(){var e=this;return this.default((function(){return[]})).transform((function(t,n){return e._typeCheck(t)?t:null==n?[]:[].concat(n)}))},compact:function(e){var t=e?function(t,n,r){return!e(t,n,r)}:function(e){return!!e};return this.transform((function(e){return null!=e?e.filter(t):e}))},describe:function(){var e=dl.prototype.describe.call(this);return this.innerType&&(e.innerType=this.innerType.describe()),e}});var Ad=function(){function e(e){this._resolve=function(t,n){var r=e(t,n);if(!hu(r))throw new TypeError("lazy() functions must return a valid schema");return r.resolve(n)}}var t=e.prototype;return t.resolve=function(e){return this._resolve(e.value,e)},t.cast=function(e,t){return this._resolve(e,t).cast(e,t)},t.validate=function(e,t,n){return this._resolve(e,t).validate(e,t,n)},t.validateSync=function(e,t){return this._resolve(e,t).validateSync(e,t)},t.validateAt=function(e,t,n){return this._resolve(t,n).validateAt(e,t,n)},t.validateSyncAt=function(e,t,n){return this._resolve(t,n).validateSyncAt(e,t,n)},e}();Ad.prototype.__isYupSchema__=!0;var Cd=function(e,t){return new il(e,t)}},2661:function(e,t,n){"use strict";n.d(t,{Z:function(){return g}});var r=n(223),i=n(7142),o=n(9684),s=n(5168);
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class a{constructor(e,t){this._delegate=e,this.firebase=t,(0,o._addComponent)(e,new i.wA("app-compat",(()=>this),"PUBLIC")),this.container=e.container}get automaticDataCollectionEnabled(){return this._delegate.automaticDataCollectionEnabled}set automaticDataCollectionEnabled(e){this._delegate.automaticDataCollectionEnabled=e}get name(){return this._delegate.name}get options(){return this._delegate.options}delete(){return new Promise((e=>{this._delegate.checkDestroyed(),e()})).then((()=>(this.firebase.INTERNAL.removeApp(this.name),(0,o.deleteApp)(this._delegate))))}_getService(e,t=o._DEFAULT_ENTRY_NAME){var n;this._delegate.checkDestroyed();const r=this._delegate.container.getProvider(e);return r.isInitialized()||"EXPLICIT"!==(null===(n=r.getComponent())||void 0===n?void 0:n.instantiationMode)||r.initialize(),r.getImmediate({identifier:t})}_removeServiceInstance(e,t=o._DEFAULT_ENTRY_NAME){this._delegate.container.getProvider(e).clearInstance(t)}_addComponent(e){(0,o._addComponent)(this._delegate,e)}_addOrOverwriteComponent(e){(0,o._addOrOverwriteComponent)(this._delegate,e)}toJSON(){return{name:this.name,automaticDataCollectionEnabled:this.automaticDataCollectionEnabled,options:this.options}}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const u={["no-app"]:"No Firebase App '{$appName}' has been created - call Firebase App.initializeApp()",["invalid-app-argument"]:"firebase.{$appName}() takes either no argument or a Firebase App instance."},c=new r.LL("app-compat","Firebase",u);
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function l(e){const t={},n={__esModule:!0,initializeApp:a,app:s,registerVersion:o.registerVersion,setLogLevel:o.setLogLevel,onLog:o.onLog,apps:null,SDK_VERSION:o.SDK_VERSION,INTERNAL:{registerComponent:l,removeApp:i,useAsService:h,modularAPIs:o}};function i(e){delete t[e]}function s(e){if(e=e||o._DEFAULT_ENTRY_NAME,!(0,r.r3)(t,e))throw c.create("no-app",{appName:e});return t[e]}function a(i,s={}){const a=o.initializeApp(i,s);if((0,r.r3)(t,a.name))return t[a.name];const u=new e(a,n);return t[a.name]=u,u}function u(){return Object.keys(t).map((e=>t[e]))}function l(t){const i=t.name,a=i.replace("-compat","");if(o._registerComponent(t)&&"PUBLIC"===t.type){const o=(e=s())=>{if("function"!==typeof e[a])throw c.create("invalid-app-argument",{appName:i});return e[a]()};void 0!==t.serviceProps&&(0,r.ZB)(o,t.serviceProps),n[a]=o,e.prototype[a]=function(...e){const n=this._getService.bind(this,i);return n.apply(this,t.multipleInstances?e:[])}}return"PUBLIC"===t.type?n[a]:null}function h(e,t){if("serverAuth"===t)return null;const n=t;return n}return n["default"]=n,Object.defineProperty(n,"apps",{get:u}),s["App"]=e,n}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function h(){const e=l(a);function t(t){(0,r.ZB)(e,t)}return e.INTERNAL=Object.assign(Object.assign({},e.INTERNAL),{createFirebaseNamespace:h,extendNamespace:t,createSubscribe:r.ne,ErrorFactory:r.LL,deepExtend:r.ZB}),e}const d=h(),f=new s.Yd("@firebase/app-compat"),p="@firebase/app-compat",m="0.1.21";
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function v(e){(0,o.registerVersion)(p,m,e)}
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */if((0,r.jU)()&&void 0!==self.firebase){f.warn("\n    Warning: Firebase is already defined in the global scope. Please make sure\n    Firebase library is only loaded once.\n  ");const e=self.firebase.SDK_VERSION;e&&e.indexOf("LITE")>=0&&f.warn("\n    Warning: You are trying to load Firebase while using Firebase Performance standalone script.\n    You should load Firebase Performance with this instance of Firebase to avoid loading duplicate code.\n    ")}const g=d;v()},9684:function(e,t,n){"use strict";n.r(t),n.d(t,{FirebaseError:function(){return o.ZR},SDK_VERSION:function(){return K},_DEFAULT_ENTRY_NAME:function(){return j},_addComponent:function(){return $},_addOrOverwriteComponent:function(){return V},_apps:function(){return U},_clearComponents:function(){return q},_components:function(){return M},_getProvider:function(){return B},_registerComponent:function(){return z},_removeServiceInstance:function(){return H},deleteApp:function(){return Q},getApp:function(){return Z},getApps:function(){return X},initializeApp:function(){return Y},onLog:function(){return te},registerVersion:function(){return ee},setLogLevel:function(){return ne}});var r=n(7142),i=n(5168),o=n(223);
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class s{constructor(e){this.container=e}getPlatformInfoString(){const e=this.container.getProviders();return e.map((e=>{if(a(e)){const t=e.getImmediate();return`${t.library}/${t.version}`}return null})).filter((e=>e)).join(" ")}}function a(e){const t=e.getComponent();return"VERSION"===(null===t||void 0===t?void 0:t.type)}const u="@firebase/app",c="0.7.20",l=new i.Yd("@firebase/app"),h="@firebase/app-compat",d="@firebase/analytics-compat",f="@firebase/analytics",p="@firebase/app-check-compat",m="@firebase/app-check",v="@firebase/auth",g="@firebase/auth-compat",_="@firebase/database",y="@firebase/database-compat",b="@firebase/functions",w="@firebase/functions-compat",I="@firebase/installations",E="@firebase/installations-compat",k="@firebase/messaging",T="@firebase/messaging-compat",O="@firebase/performance",S="@firebase/performance-compat",R="@firebase/remote-config",A="@firebase/remote-config-compat",C="@firebase/storage",x="@firebase/storage-compat",P="@firebase/firestore",N="@firebase/firestore-compat",F="firebase",D="9.6.10",j="[DEFAULT]",L={[u]:"fire-core",[h]:"fire-core-compat",[f]:"fire-analytics",[d]:"fire-analytics-compat",[m]:"fire-app-check",[p]:"fire-app-check-compat",[v]:"fire-auth",[g]:"fire-auth-compat",[_]:"fire-rtdb",[y]:"fire-rtdb-compat",[b]:"fire-fn",[w]:"fire-fn-compat",[I]:"fire-iid",[E]:"fire-iid-compat",[k]:"fire-fcm",[T]:"fire-fcm-compat",[O]:"fire-perf",[S]:"fire-perf-compat",[R]:"fire-rc",[A]:"fire-rc-compat",[C]:"fire-gcs",[x]:"fire-gcs-compat",[P]:"fire-fst",[N]:"fire-fst-compat","fire-js":"fire-js",[F]:"fire-js-all"},U=new Map,M=new Map;function $(e,t){try{e.container.addComponent(t)}catch(n){l.debug(`Component ${t.name} failed to register with FirebaseApp ${e.name}`,n)}}function V(e,t){e.container.addOrOverwriteComponent(t)}function z(e){const t=e.name;if(M.has(t))return l.debug(`There were multiple attempts to register component ${t}.`),!1;M.set(t,e);for(const n of U.values())$(n,e);return!0}function B(e,t){const n=e.container.getProvider("heartbeat").getImmediate({optional:!0});return n&&n.triggerHeartbeat(),e.container.getProvider(t)}function H(e,t,n=j){B(e,t).clearInstance(n)}function q(){M.clear()}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const W={["no-app"]:"No Firebase App '{$appName}' has been created - call Firebase App.initializeApp()",["bad-app-name"]:"Illegal App name: '{$appName}",["duplicate-app"]:"Firebase App named '{$appName}' already exists with different options or config",["app-deleted"]:"Firebase App named '{$appName}' already deleted",["invalid-app-argument"]:"firebase.{$appName}() takes either no argument or a Firebase App instance.",["invalid-log-argument"]:"First argument to `onLog` must be null or a function.",["storage-open"]:"Error thrown when opening storage. Original error: {$originalErrorMessage}.",["storage-get"]:"Error thrown when reading from storage. Original error: {$originalErrorMessage}.",["storage-set"]:"Error thrown when writing to storage. Original error: {$originalErrorMessage}.",["storage-delete"]:"Error thrown when deleting from storage. Original error: {$originalErrorMessage}."},G=new o.LL("app","Firebase",W);
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class J{constructor(e,t,n){this._isDeleted=!1,this._options=Object.assign({},e),this._config=Object.assign({},t),this._name=t.name,this._automaticDataCollectionEnabled=t.automaticDataCollectionEnabled,this._container=n,this.container.addComponent(new r.wA("app",(()=>this),"PUBLIC"))}get automaticDataCollectionEnabled(){return this.checkDestroyed(),this._automaticDataCollectionEnabled}set automaticDataCollectionEnabled(e){this.checkDestroyed(),this._automaticDataCollectionEnabled=e}get name(){return this.checkDestroyed(),this._name}get options(){return this.checkDestroyed(),this._options}get config(){return this.checkDestroyed(),this._config}get container(){return this._container}get isDeleted(){return this._isDeleted}set isDeleted(e){this._isDeleted=e}checkDestroyed(){if(this.isDeleted)throw G.create("app-deleted",{appName:this._name})}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const K=D;function Y(e,t={}){if("object"!==typeof t){const e=t;t={name:e}}const n=Object.assign({name:j,automaticDataCollectionEnabled:!1},t),i=n.name;if("string"!==typeof i||!i)throw G.create("bad-app-name",{appName:String(i)});const s=U.get(i);if(s){if((0,o.vZ)(e,s.options)&&(0,o.vZ)(n,s.config))return s;throw G.create("duplicate-app",{appName:i})}const a=new r.H0(i);for(const r of M.values())a.addComponent(r);const u=new J(e,n,a);return U.set(i,u),u}function Z(e=j){const t=U.get(e);if(!t)throw G.create("no-app",{appName:e});return t}function X(){return Array.from(U.values())}async function Q(e){const t=e.name;U.has(t)&&(U.delete(t),await Promise.all(e.container.getProviders().map((e=>e.delete()))),e.isDeleted=!0)}function ee(e,t,n){var i;let o=null!==(i=L[e])&&void 0!==i?i:e;n&&(o+=`-${n}`);const s=o.match(/\s|\//),a=t.match(/\s|\//);if(s||a){const e=[`Unable to register library "${o}" with version "${t}":`];return s&&e.push(`library name "${o}" contains illegal characters (whitespace or "/")`),s&&a&&e.push("and"),a&&e.push(`version name "${t}" contains illegal characters (whitespace or "/")`),void l.warn(e.join(" "))}z(new r.wA(`${o}-version`,(()=>({library:o,version:t})),"VERSION"))}function te(e,t){if(null!==e&&"function"!==typeof e)throw G.create("invalid-log-argument");(0,i.Am)(e,t)}function ne(e){(0,i.Ub)(e)}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const re="firebase-heartbeat-database",ie=1,oe="firebase-heartbeat-store";let se=null;function ae(){return se||(se=(0,o.X3)(re,ie,((e,t)=>{switch(t){case 0:e.createObjectStore(oe)}})).catch((e=>{throw G.create("storage-open",{originalErrorMessage:e.message})}))),se}async function ue(e){try{const t=await ae();return t.transaction(oe).objectStore(oe).get(le(e))}catch(t){throw G.create("storage-get",{originalErrorMessage:t.message})}}async function ce(e,t){try{const n=await ae(),r=n.transaction(oe,"readwrite"),i=r.objectStore(oe);return await i.put(t,le(e)),r.complete}catch(n){throw G.create("storage-set",{originalErrorMessage:n.message})}}function le(e){return`${e.name}!${e.options.appId}`}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const he=1024,de=2592e6;class fe{constructor(e){this.container=e,this._heartbeatsCache=null;const t=this.container.getProvider("app").getImmediate();this._storage=new ve(t),this._heartbeatsCachePromise=this._storage.read().then((e=>(this._heartbeatsCache=e,e)))}async triggerHeartbeat(){const e=this.container.getProvider("platform-logger").getImmediate(),t=e.getPlatformInfoString(),n=pe();if(null===this._heartbeatsCache&&(this._heartbeatsCache=await this._heartbeatsCachePromise),this._heartbeatsCache.lastSentHeartbeatDate!==n&&!this._heartbeatsCache.heartbeats.some((e=>e.date===n)))return this._heartbeatsCache.heartbeats.push({date:n,agent:t}),this._heartbeatsCache.heartbeats=this._heartbeatsCache.heartbeats.filter((e=>{const t=new Date(e.date).valueOf(),n=Date.now();return n-t<=de})),this._storage.overwrite(this._heartbeatsCache)}async getHeartbeatsHeader(){if(null===this._heartbeatsCache&&await this._heartbeatsCachePromise,null===this._heartbeatsCache||0===this._heartbeatsCache.heartbeats.length)return"";const e=pe(),{heartbeatsToSend:t,unsentEntries:n}=me(this._heartbeatsCache.heartbeats),r=(0,o.L)(JSON.stringify({version:2,heartbeats:t}));return this._heartbeatsCache.lastSentHeartbeatDate=e,n.length>0?(this._heartbeatsCache.heartbeats=n,await this._storage.overwrite(this._heartbeatsCache)):(this._heartbeatsCache.heartbeats=[],this._storage.overwrite(this._heartbeatsCache)),r}}function pe(){const e=new Date;return e.toISOString().substring(0,10)}function me(e,t=he){const n=[];let r=e.slice();for(const i of e){const e=n.find((e=>e.agent===i.agent));if(e){if(e.dates.push(i.date),ge(n)>t){e.dates.pop();break}}else if(n.push({agent:i.agent,dates:[i.date]}),ge(n)>t){n.pop();break}r=r.slice(1)}return{heartbeatsToSend:n,unsentEntries:r}}class ve{constructor(e){this.app=e,this._canUseIndexedDBPromise=this.runIndexedDBEnvironmentCheck()}async runIndexedDBEnvironmentCheck(){return!!(0,o.hl)()&&(0,o.eu)().then((()=>!0)).catch((()=>!1))}async read(){const e=await this._canUseIndexedDBPromise;if(e){const e=await ue(this.app);return e||{heartbeats:[]}}return{heartbeats:[]}}async overwrite(e){var t;const n=await this._canUseIndexedDBPromise;if(n){const n=await this.read();return ce(this.app,{lastSentHeartbeatDate:null!==(t=e.lastSentHeartbeatDate)&&void 0!==t?t:n.lastSentHeartbeatDate,heartbeats:e.heartbeats})}}async add(e){var t;const n=await this._canUseIndexedDBPromise;if(n){const n=await this.read();return ce(this.app,{lastSentHeartbeatDate:null!==(t=e.lastSentHeartbeatDate)&&void 0!==t?t:n.lastSentHeartbeatDate,heartbeats:[...n.heartbeats,...e.heartbeats]})}}}function ge(e){return(0,o.L)(JSON.stringify({version:2,heartbeats:e})).length}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function _e(e){z(new r.wA("platform-logger",(e=>new s(e)),"PRIVATE")),z(new r.wA("heartbeat",(e=>new fe(e)),"PRIVATE")),ee(u,c,e),ee(u,c,"esm2017"),ee("fire-js","")}_e("")},7142:function(e,t,n){"use strict";n.d(t,{H0:function(){return c},wA:function(){return i}});n(1703);var r=n(223);class i{constructor(e,t,n){this.name=e,this.instanceFactory=t,this.type=n,this.multipleInstances=!1,this.serviceProps={},this.instantiationMode="LAZY",this.onInstanceCreated=null}setInstantiationMode(e){return this.instantiationMode=e,this}setMultipleInstances(e){return this.multipleInstances=e,this}setServiceProps(e){return this.serviceProps=e,this}setInstanceCreatedCallback(e){return this.onInstanceCreated=e,this}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const o="[DEFAULT]";
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class s{constructor(e,t){this.name=e,this.container=t,this.component=null,this.instances=new Map,this.instancesDeferred=new Map,this.instancesOptions=new Map,this.onInitCallbacks=new Map}get(e){const t=this.normalizeInstanceIdentifier(e);if(!this.instancesDeferred.has(t)){const e=new r.BH;if(this.instancesDeferred.set(t,e),this.isInitialized(t)||this.shouldAutoInitialize())try{const n=this.getOrInitializeService({instanceIdentifier:t});n&&e.resolve(n)}catch(n){}}return this.instancesDeferred.get(t).promise}getImmediate(e){var t;const n=this.normalizeInstanceIdentifier(null===e||void 0===e?void 0:e.identifier),r=null!==(t=null===e||void 0===e?void 0:e.optional)&&void 0!==t&&t;if(!this.isInitialized(n)&&!this.shouldAutoInitialize()){if(r)return null;throw Error(`Service ${this.name} is not available`)}try{return this.getOrInitializeService({instanceIdentifier:n})}catch(i){if(r)return null;throw i}}getComponent(){return this.component}setComponent(e){if(e.name!==this.name)throw Error(`Mismatching Component ${e.name} for Provider ${this.name}.`);if(this.component)throw Error(`Component for ${this.name} has already been provided`);if(this.component=e,this.shouldAutoInitialize()){if(u(e))try{this.getOrInitializeService({instanceIdentifier:o})}catch(t){}for(const[e,n]of this.instancesDeferred.entries()){const r=this.normalizeInstanceIdentifier(e);try{const e=this.getOrInitializeService({instanceIdentifier:r});n.resolve(e)}catch(t){}}}}clearInstance(e=o){this.instancesDeferred.delete(e),this.instancesOptions.delete(e),this.instances.delete(e)}async delete(){const e=Array.from(this.instances.values());await Promise.all([...e.filter((e=>"INTERNAL"in e)).map((e=>e.INTERNAL.delete())),...e.filter((e=>"_delete"in e)).map((e=>e._delete()))])}isComponentSet(){return null!=this.component}isInitialized(e=o){return this.instances.has(e)}getOptions(e=o){return this.instancesOptions.get(e)||{}}initialize(e={}){const{options:t={}}=e,n=this.normalizeInstanceIdentifier(e.instanceIdentifier);if(this.isInitialized(n))throw Error(`${this.name}(${n}) has already been initialized`);if(!this.isComponentSet())throw Error(`Component ${this.name} has not been registered yet`);const r=this.getOrInitializeService({instanceIdentifier:n,options:t});for(const[i,o]of this.instancesDeferred.entries()){const e=this.normalizeInstanceIdentifier(i);n===e&&o.resolve(r)}return r}onInit(e,t){var n;const r=this.normalizeInstanceIdentifier(t),i=null!==(n=this.onInitCallbacks.get(r))&&void 0!==n?n:new Set;i.add(e),this.onInitCallbacks.set(r,i);const o=this.instances.get(r);return o&&e(o,r),()=>{i.delete(e)}}invokeOnInitCallbacks(e,t){const n=this.onInitCallbacks.get(t);if(n)for(const i of n)try{i(e,t)}catch(r){}}getOrInitializeService({instanceIdentifier:e,options:t={}}){let n=this.instances.get(e);if(!n&&this.component&&(n=this.component.instanceFactory(this.container,{instanceIdentifier:a(e),options:t}),this.instances.set(e,n),this.instancesOptions.set(e,t),this.invokeOnInitCallbacks(n,e),this.component.onInstanceCreated))try{this.component.onInstanceCreated(this.container,e,n)}catch(r){}return n||null}normalizeInstanceIdentifier(e=o){return this.component?this.component.multipleInstances?e:o:e}shouldAutoInitialize(){return!!this.component&&"EXPLICIT"!==this.component.instantiationMode}}function a(e){return e===o?void 0:e}function u(e){return"EAGER"===e.instantiationMode}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class c{constructor(e){this.name=e,this.providers=new Map}addComponent(e){const t=this.getProvider(e.name);if(t.isComponentSet())throw new Error(`Component ${e.name} has already been registered with ${this.name}`);t.setComponent(e)}addOrOverwriteComponent(e){const t=this.getProvider(e.name);t.isComponentSet()&&this.providers.delete(e.name),this.addComponent(e)}getProvider(e){if(this.providers.has(e))return this.providers.get(e);const t=new s(e,this);return this.providers.set(e,t),t}getProviders(){return Array.from(this.providers.values())}}},5168:function(e,t,n){"use strict";n.d(t,{Am:function(){return h},Ub:function(){return l},Yd:function(){return c},in:function(){return i}});n(1703);
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const r=[];var i;(function(e){e[e["DEBUG"]=0]="DEBUG",e[e["VERBOSE"]=1]="VERBOSE",e[e["INFO"]=2]="INFO",e[e["WARN"]=3]="WARN",e[e["ERROR"]=4]="ERROR",e[e["SILENT"]=5]="SILENT"})(i||(i={}));const o={debug:i.DEBUG,verbose:i.VERBOSE,info:i.INFO,warn:i.WARN,error:i.ERROR,silent:i.SILENT},s=i.INFO,a={[i.DEBUG]:"log",[i.VERBOSE]:"log",[i.INFO]:"info",[i.WARN]:"warn",[i.ERROR]:"error"},u=(e,t,...n)=>{if(t<e.logLevel)return;const r=(new Date).toISOString(),i=a[t];if(!i)throw new Error(`Attempted to log a message with an invalid logType (value: ${t})`);console[i](`[${r}]  ${e.name}:`,...n)};class c{constructor(e){this.name=e,this._logLevel=s,this._logHandler=u,this._userLogHandler=null,r.push(this)}get logLevel(){return this._logLevel}set logLevel(e){if(!(e in i))throw new TypeError(`Invalid value "${e}" assigned to \`logLevel\``);this._logLevel=e}setLogLevel(e){this._logLevel="string"===typeof e?o[e]:e}get logHandler(){return this._logHandler}set logHandler(e){if("function"!==typeof e)throw new TypeError("Value assigned to `logHandler` must be a function");this._logHandler=e}get userLogHandler(){return this._userLogHandler}set userLogHandler(e){this._userLogHandler=e}debug(...e){this._userLogHandler&&this._userLogHandler(this,i.DEBUG,...e),this._logHandler(this,i.DEBUG,...e)}log(...e){this._userLogHandler&&this._userLogHandler(this,i.VERBOSE,...e),this._logHandler(this,i.VERBOSE,...e)}info(...e){this._userLogHandler&&this._userLogHandler(this,i.INFO,...e),this._logHandler(this,i.INFO,...e)}warn(...e){this._userLogHandler&&this._userLogHandler(this,i.WARN,...e),this._logHandler(this,i.WARN,...e)}error(...e){this._userLogHandler&&this._userLogHandler(this,i.ERROR,...e),this._logHandler(this,i.ERROR,...e)}}function l(e){r.forEach((t=>{t.setLogLevel(e)}))}function h(e,t){for(const n of r){let r=null;t&&t.level&&(r=o[t.level]),n.userLogHandler=null===e?null:(t,n,...o)=>{const s=o.map((e=>{if(null==e)return null;if("string"===typeof e)return e;if("number"===typeof e||"boolean"===typeof e)return e.toString();if(e instanceof Error)return e.message;try{return JSON.stringify(e)}catch(t){return null}})).filter((e=>e)).join(" ");n>=(null!==r&&void 0!==r?r:t.logLevel)&&e({level:i[n].toLowerCase(),message:s,args:o,type:t.name})}}}}}]);
//# sourceMappingURL=chunk-vendors.a8d13db2.js.map