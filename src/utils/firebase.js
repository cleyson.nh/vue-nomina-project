import { initializeApp } from "firebase/app";
import firebase from 'firebase/compat/app'
import "firebase/auth"
import 'firebase/compat/auth';
import { getAuth } from "firebase/auth";
import 'firebase/compat/storage'
import { ref } from "vue";
import { uploadBytes } from "firebase/storage";


const firebaseConfig = {
   apiKey: "AIzaSyBPVygLl2NL7H3ICHfqTXK6iUKVj0H5kwk",
   authDomain: "nomina-project.firebaseapp.com",
   projectId: "nomina-project",
   storageBucket: "nomina-project.appspot.com",
   messagingSenderId: "795879887800",
   appId: "1:795879887800:web:b045ddc9c5ea02ee1a354d"
};

export function storeFile(nameFile, file) {
   const fileRef = ref(storage, Auth.currentUser.uid + "/" + nameFile + ".pdf");

   return uploadBytes(fileRef, file);
}

// Initialize Firebase
const app = initializeApp(firebaseConfig);
firebase.initializeApp(firebaseConfig);

//Utils

const auth = getAuth(app);
const Auth = firebase.auth();
const storage = firebase.storage();
export { auth, Auth, storage }
