import firebase from 'firebase/compat/app'
import { Auth } from './firebase'
import 'firebase/compat/storage'


export function reauthenticate(password) {
   const user = Auth.currentUser;
   const credentials = firebase.auth.EmailAuthProvider.credential(
      user.email,
      password
   )

   return user.reauthenticateWithCredential(credentials);
}








